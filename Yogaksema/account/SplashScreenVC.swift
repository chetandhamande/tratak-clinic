//
//  SplashScreenVC.swift
//  Salk
//
//  Created by Chetan  on 13/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class SplashScreenVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnRegistration: UIButton!
    
    @IBOutlet var imgBackgroud: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnLogin.backgroundColor = PrimaryColor.colorPrimaryDark()
        btnRegistration.backgroundColor = PrimaryColor.colorPrimaryDark()
        
//        self.scrollView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height)
        let scrollViewWidth:CGFloat = UIScreen.main.bounds.width
        let scrollViewHeight:CGFloat = UIScreen.main.bounds.height - 80
        
        scrollView.backgroundColor = PrimaryColor.colorPrimaryBackgroud()
        navigationItem.titleView?.backgroundColor = PrimaryColor.colorPrimaryBackgroud()
        
        
        let firstIconSize:CGFloat = 250
        let imgOne = UIImageView(frame: CGRect(x:(scrollViewWidth - firstIconSize) / 2, y:(scrollViewHeight - firstIconSize) / 2, width:firstIconSize, height:firstIconSize))
        imgOne.image = UIImage(named: "app_icon_black")
        
        let imgTwo = UIImageView(frame: CGRect(x:scrollViewWidth, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgTwo.image = UIImage(named: "step1")
        let imgThree = UIImageView(frame: CGRect(x:scrollViewWidth*2, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgThree.image = UIImage(named: "step2")
        let imgFour = UIImageView(frame: CGRect(x:scrollViewWidth*3, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgFour.image = UIImage(named: "step3")
        let imgFive = UIImageView(frame: CGRect(x:scrollViewWidth*4, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgFive.image = UIImage(named: "step4")
        
        self.scrollView.addSubview(imgOne)
        self.scrollView.addSubview(imgTwo)
        self.scrollView.addSubview(imgThree)
        self.scrollView.addSubview(imgFour)
        self.scrollView.addSubview(imgFive)
        
        self.scrollView.contentSize = CGSize(width:scrollViewWidth * 5, height:scrollViewHeight - 150)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UIScrollView Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        
        self.pageControl.currentPage = Int(currentPage);
        
        // Change the text accordingly
        if Int(currentPage) == 0{
            
        } else if Int(currentPage) == 1 {
            
        } else if Int(currentPage) == 2 {
            
        } else if Int(currentPage) == 3 {
            
        } else{
            
        }
    }
    
    
    @IBAction func loginClickEvent(sender: UIButton) {
        let mainControl = MainViewController()
        mainControl.startLogin()        
    }
    
    @IBAction func registerClickEvent(sender: UIButton) {
        let regView = RegistrationVC(nibName: "RegistrationVC", bundle: nil)
        self.navigationController?.pushViewController(regView, animated: true)
    }
    
    
}
