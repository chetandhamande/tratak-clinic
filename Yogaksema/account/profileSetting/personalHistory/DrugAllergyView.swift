//
//  DrugAllergyView.swift
//  Salk
//
//  Created by Chetan on 07/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class DrugAllergyView: UIView {

    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var lblCmpt1: UILabel!
    @IBOutlet weak var txtCmpt1: MKTextField!
    @IBOutlet weak var lblSince1: UILabel!
    @IBOutlet weak var txtDur1: MKTextField!
    @IBOutlet weak var btn1: UIButton!
    
    
    @IBOutlet weak var lblCmpt2: UILabel!
    @IBOutlet weak var txtCmpt2: MKTextField!
    @IBOutlet weak var lblSince2: UILabel!
    @IBOutlet weak var txtDur2: MKTextField!
    @IBOutlet weak var btn2: UIButton!
    
    @IBOutlet weak var lblCmpt3: UILabel!
    @IBOutlet weak var txtCmpt3: MKTextField!
    @IBOutlet weak var lblSince3: UILabel!
    @IBOutlet weak var txtDur3: MKTextField!
    @IBOutlet weak var btn3: UIButton!
    
    
    @IBOutlet weak var lblCmpt4: UILabel!
    @IBOutlet weak var txtCmpt4: MKTextField!
    @IBOutlet weak var lblSince4: UILabel!
    @IBOutlet weak var txtDur4: MKTextField!
    @IBOutlet weak var btn4: UIButton!
    
    @IBOutlet weak var lblCmpt5: UILabel!
    @IBOutlet weak var txtCmpt5: MKTextField!
    @IBOutlet weak var lblSince5: UILabel!
    @IBOutlet weak var txtDur5: MKTextField!
    @IBOutlet weak var btn5: UIButton!
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
                self.viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "DrugAllergyView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        //        headerView.backgroundColor = PrimaryColor.blackTransparent1()
        //
        //        lblHeading.text = "Major Medical Complaints"
        
        configureTextField(txtCmpt1, placeholder: "Enter Drug name 1")
        configureTextField(txtCmpt2, placeholder: "Enter Drug name 2")
        configureTextField(txtCmpt3, placeholder: "Enter Drug name 3")
        configureTextField(txtCmpt4, placeholder: "Enter Drug name 4")
        configureTextField(txtCmpt5, placeholder: "Enter Drug name 5")
        
        configureTextField(txtDur1, placeholder: "Duration")
        configureTextField(txtDur2, placeholder: "Duration")
        configureTextField(txtDur3, placeholder: "Duration")
        configureTextField(txtDur4, placeholder: "Duration")
        configureTextField(txtDur5, placeholder: "Duration")
        
        
        configureLabel(lblCmpt1, setString: "Drug name 1")
        configureLabel(lblCmpt2, setString: "Drug name 2")
        configureLabel(lblCmpt3, setString: "Drug name 3")
        configureLabel(lblCmpt4, setString: "Drug name 4")
        configureLabel(lblCmpt5, setString: "Drug name 5")
        
        configureLabel(lblSince1, setString: "Since When (Before)")
        configureLabel(lblSince2, setString: "Since When (Before)")
        configureLabel(lblSince3, setString: "Since When (Before)")
        configureLabel(lblSince4, setString: "Since When (Before)")
        configureLabel(lblSince5, setString: "Since When (Before)")
        
        configureButton(btn1, setString: "Day")
        configureButton(btn2, setString: "Day")
        configureButton(btn3, setString: "Day")
        configureButton(btn4, setString: "Day")
        configureButton(btn5, setString: "Day")
    }
    
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
        
        if textField == txtCmpt1 || textField == txtCmpt2 || textField == txtCmpt3 || textField == txtCmpt4 || textField == txtCmpt5 {
            textField.returnKeyType = .next
        } else {
            textField.keyboardType = .decimalPad
        }
    }
    
    func configureLabel(_ label: UILabel, setString: String) {
        label.text = setString
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = CustomColor.darker_gray()
    }
    
    func configureButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textGrayColor(), for: UIControlState.normal)
    }
    
    func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textBlackColor(), for: UIControlState.normal)
        
        refreshView()
    }
    
    func refreshView() {
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }

}
