//
//  PainHistoryVC.swift
//  Salk
//
//  Created by Chetan on 22/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class PainHistoryVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var objPainHis: PainHistoryView!
    
    fileprivate var loadingObj = CustomLoading()
    fileprivate let arr = ["None", "Mild", "Moderate", "Severve"]
    fileprivate let arrDay = ["Day", "Week", "Month", "Year"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mainScrollView.backgroundColor = CustomColor.background()
        picker.isHidden = true
        picker.backgroundColor = CustomColor.grayLight()
        picker.showsSelectionIndicator = true
        self.picker.delegate = self
        self.picker.dataSource = self
        UIApplication.shared.keyWindow!.bringSubview(toFront: picker)
        
        self.prepareNavigationItem()
        
        init1();
        
        //getting Latest record
        getRecordServer();
    }
    
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviPainHist".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
    }
    
    private func init1() {
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(PainHistoryVC.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        objPainHis.btnAche.addTarget(self, action: #selector(perAche), for: .touchUpInside)
        objPainHis.btnHead.addTarget(self, action: #selector(perHead), for: .touchUpInside)
        objPainHis.btnUpperLimb.addTarget(self, action: #selector(perUpperLimb), for: .touchUpInside)
        objPainHis.btnLowerLimb.addTarget(self, action: #selector(perLowerLimb), for: .touchUpInside)
        objPainHis.btnChest.addTarget(self, action: #selector(perChest), for: .touchUpInside)
        objPainHis.btnBack.addTarget(self, action: #selector(perBack), for: .touchUpInside)
        objPainHis.btnAbdomen.addTarget(self, action: #selector(perAbdomen), for: .touchUpInside)
     
        objPainHis.btnAcheDay.addTarget(self, action: #selector(perAcheDay), for: .touchUpInside)
        objPainHis.btnHeadDay.addTarget(self, action: #selector(perHeadDay), for: .touchUpInside)
        objPainHis.btnUpperLimbDay.addTarget(self, action: #selector(perUpperLimbDay), for: .touchUpInside)
        objPainHis.btnLowerLimbDay.addTarget(self, action: #selector(perLowerLimbDay), for: .touchUpInside)
        objPainHis.btnChestDay.addTarget(self, action: #selector(perChestDay), for: .touchUpInside)
        objPainHis.btnBackDay.addTarget(self, action: #selector(perBackDay), for: .touchUpInside)
        objPainHis.btnAbdomenDay.addTarget(self, action: #selector(perAbdomenDay), for: .touchUpInside)
        
        refreshParameters()
    }
    
    public func refreshParameters() {
    
    
    let datasource = DBOperation()
    datasource.openDatabase(false)
        let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
            + " WHERE " + datasource.dbGroupType + " = '" + "assGrpPainHistory".localized + "' ")
        
        if cursor != nil {
            while cursor!.next() {
                if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("aches_severity") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objPainHis.configureSelectButton(objPainHis.btnAche, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("aches_since") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ");
                        objPainHis.editAche.text = duraArr[0]
                        objPainHis.configureSelectButton(objPainHis.btnAcheDay, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("head_neck_severity") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objPainHis.configureSelectButton(objPainHis.btnHead, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("head_neck_since") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ");
                        objPainHis.editHead.text = duraArr[0]
                        objPainHis.configureSelectButton(objPainHis.btnHeadDay, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("upper_limb_severity") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objPainHis.configureSelectButton(objPainHis.btnUpperLimb, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("upper_limb_since") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ");
                        objPainHis.editUpperLimb.text = duraArr[0]
                        objPainHis.configureSelectButton(objPainHis.btnUpperLimbDay, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("lower_limb_severity") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objPainHis.configureSelectButton(objPainHis.btnLowerLimb, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("lower_limb_since") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ");
                        objPainHis.editLowerLimb.text = duraArr[0]
                        objPainHis.configureSelectButton(objPainHis.btnLowerLimbDay, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("chest_severity") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objPainHis.configureSelectButton(objPainHis.btnChest, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("chest_since") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ");
                        objPainHis.editChest.text = duraArr[0]
                        objPainHis.configureSelectButton(objPainHis.btnChestDay, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("back_severity") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objPainHis.configureSelectButton(objPainHis.btnBack, setString: cursor!.string(forColumnIndex: 1))
                    }
                    
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("back_since") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ");
                        objPainHis.editBack.text = duraArr[0]
                        objPainHis.configureSelectButton(objPainHis.btnBackDay, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("abdomen_severity") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objPainHis.configureSelectButton(objPainHis.btnAbdomen, setString: cursor!.string(forColumnIndex: 1))
                    }
                    
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("abdomen_since") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ");
                        objPainHis.editAbdomen.text = duraArr[0]
                        objPainHis.configureSelectButton(objPainHis.btnAbdomenDay, setString: duraArr[1])
                    }
                }
            }
            cursor?.close()
            datasource.closeDatabase()
        }
    }
    
    @objc func performNextOpr() {
        dismissKeyboard()
        
        let datasource = DBOperation()
        datasource.openDatabase(true)
        
        if !objPainHis.btnAche.titleLabel!.text!.isEmpty && objPainHis.btnAche.titleLabel != nil  {
            datasource.UpdateAssessmentItemTable(paramName: "aches_severity", paramVal: objPainHis.btnAche.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            if !objPainHis.btnAcheDay.titleLabel!.text!.isEmpty {
                datasource.UpdateAssessmentItemTable(paramName: "aches_since", paramVal: objPainHis.editAche.text! + " " + objPainHis.btnAcheDay.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            }
        }
        
        if !objPainHis.btnHead.titleLabel!.text!.isEmpty && objPainHis.btnHead.titleLabel != nil {
            datasource.UpdateAssessmentItemTable(paramName: "head_neck_severity", paramVal: objPainHis.btnHead.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            if !objPainHis.btnHeadDay.titleLabel!.text!.isEmpty {
                datasource.UpdateAssessmentItemTable(paramName: "head_neck_since", paramVal: objPainHis.editHead.text! + " " + objPainHis.btnHeadDay.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            }
        }
        
        if !objPainHis.btnUpperLimb.titleLabel!.text!.isEmpty && objPainHis.btnUpperLimb.titleLabel != nil {
            datasource.UpdateAssessmentItemTable(paramName: "upper_limb_severity", paramVal: objPainHis.btnUpperLimb.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            if !objPainHis.btnUpperLimbDay.titleLabel!.text!.isEmpty {
                datasource.UpdateAssessmentItemTable(paramName: "upper_limb_since", paramVal: objPainHis.editUpperLimb.text! + " " + objPainHis.btnUpperLimbDay.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            }
        }
        
        if !objPainHis.btnLowerLimb.titleLabel!.text!.isEmpty && objPainHis.btnLowerLimb.titleLabel != nil {
            datasource.UpdateAssessmentItemTable(paramName: "lower_limb_severity", paramVal: objPainHis.btnLowerLimb.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            if !objPainHis.btnLowerLimbDay.titleLabel!.text!.isEmpty {
                datasource.UpdateAssessmentItemTable(paramName: "lower_limb_since", paramVal: objPainHis.editLowerLimb.text! + " " + objPainHis.btnLowerLimbDay.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            }
        }
        
        if !objPainHis.btnChest.titleLabel!.text!.isEmpty && objPainHis.btnChest.titleLabel != nil {
            datasource.UpdateAssessmentItemTable(paramName: "chest_severity", paramVal: objPainHis.btnChest.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            if !objPainHis.btnChestDay.titleLabel!.text!.isEmpty {
                datasource.UpdateAssessmentItemTable(paramName: "chest_since", paramVal: objPainHis.editChest.text! + " " + objPainHis.btnChestDay.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            }
        }
       
        if !objPainHis.btnBack.titleLabel!.text!.isEmpty && objPainHis.btnBack.titleLabel != nil {
            datasource.UpdateAssessmentItemTable(paramName: "back_severity", paramVal: objPainHis.btnBack.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            if !objPainHis.btnBackDay.titleLabel!.text!.isEmpty {
                datasource.UpdateAssessmentItemTable(paramName: "back_since", paramVal: objPainHis.editBack.text! + " " + objPainHis.btnBackDay.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            }
        }
        
        if !objPainHis.btnAbdomen.titleLabel!.text!.isEmpty && objPainHis.btnAbdomen.titleLabel != nil {
            datasource.UpdateAssessmentItemTable(paramName: "abdomen_severity", paramVal: objPainHis.btnAbdomen.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            if !objPainHis.btnAbdomenDay.titleLabel!.text!.isEmpty {
                datasource.UpdateAssessmentItemTable(paramName: "abdomen_since", paramVal: objPainHis.editAbdomen.text! + " " + objPainHis.btnAbdomenDay.titleLabel!.text!, groupType: "assGrpPainHistory".localized)
            }
        }
        
        datasource.closeDatabase()
        
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpPainHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    private func getRecordServer() {
    // call for getting Register user
        
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpPainHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        picker.isHidden = true
    }
    
    @objc func perAche() {
        picker.isHidden = false
        picker.tag = 1
        picker.reloadAllComponents()
    }
    @objc func perHead() {
        picker.isHidden = false
        picker.tag = 2
        picker.reloadAllComponents()
    }
    @objc func perUpperLimb() {
        picker.isHidden = false
        picker.tag = 3
        picker.reloadAllComponents()
    }
    @objc func perLowerLimb() {
        picker.isHidden = false
        picker.tag = 4
        picker.reloadAllComponents()
    }
    @objc func perChest() {
        picker.isHidden = false
        picker.tag = 5
        picker.reloadAllComponents()
    }
    @objc func perBack() {
        picker.isHidden = false
        picker.tag = 6
        picker.reloadAllComponents()
    }
    @objc func perAbdomen() {
        picker.isHidden = false
        picker.tag = 7
        picker.reloadAllComponents()
    }
    
    @objc func perAcheDay() {
        picker.isHidden = false
        picker.tag = 8
        picker.reloadAllComponents()
    }
    @objc func perHeadDay() {
        picker.isHidden = false
        picker.tag = 9
        picker.reloadAllComponents()
    }
    @objc func perUpperLimbDay() {
        picker.isHidden = false
        picker.tag = 10
        picker.reloadAllComponents()
    }
    @objc func perLowerLimbDay() {
        picker.isHidden = false
        picker.tag = 11
        picker.reloadAllComponents()
    }
    @objc func perChestDay() {
        picker.isHidden = false
        picker.tag = 12
        picker.reloadAllComponents()
    }
    @objc func perBackDay() {
        picker.isHidden = false
        picker.tag = 13
        picker.reloadAllComponents()
    }
    @objc func perAbdomenDay() {
        picker.isHidden = false
        picker.tag = 14
        picker.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arr.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 || pickerView.tag == 2 || pickerView.tag == 3 || pickerView.tag == 4 || pickerView.tag == 5 || pickerView.tag == 6 || pickerView.tag == 7 {
            return arr[row]
        } else {
            return arrDay[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnAche, setString: arr[row])
        } else if pickerView.tag == 2 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnHead, setString: arr[row])
        } else if pickerView.tag == 3 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnUpperLimb, setString: arr[row])
        } else if pickerView.tag == 4 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnLowerLimb, setString: arr[row])
        } else if pickerView.tag == 5 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnChest, setString: arr[row])
        } else if pickerView.tag == 6 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnBackDay, setString: arr[row])
        } else if pickerView.tag == 7 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnAbdomen, setString: arr[row])
        } else if pickerView.tag == 8 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnAcheDay, setString: arrDay[row])
        } else if pickerView.tag == 9 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnHeadDay, setString: arrDay[row])
        }  else if pickerView.tag == 10 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnUpperLimbDay, setString: arrDay[row])
        } else if pickerView.tag == 11 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnLowerLimbDay, setString: arrDay[row])
        } else if pickerView.tag == 12 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnChestDay, setString: arrDay[row])
        } else if pickerView.tag == 13 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnBackDay, setString: arrDay[row])
        } else if pickerView.tag == 14 {
            self.objPainHis.configureSelectButton(self.objPainHis.btnAbdomenDay, setString: arrDay[row])
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
