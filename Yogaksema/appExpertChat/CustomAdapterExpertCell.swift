//
//  CustomAdapterRecentCell.swift
//  Salk
//
//  Created by Chetan  on 01/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import SnapKit
import Material

class CustomAdapterExpertCell: UICollectionViewCell {

    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var backgroundGadgetView: SalkCardView!
    @IBOutlet weak var backgroundCellView: UIView!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle0: UILabel!
    @IBOutlet weak var lblSubTitle1: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundGadgetView.backgroundColor = CustomColor.grayBackground()
        backgroundGadgetView.layer.cornerRadius = 5
//        backgroundGadgetView.shadowOffsetHeight = 1
        
        backgroundCellView.backgroundColor = CustomColor.white()
        
        SalkRippleEffect.SalkRippleEffect(backgroundCellView)
        
        imgUser.layer.borderWidth = 1
        imgUser.layer.borderColor = CustomColor.light_gray().cgColor
        imgUser.layer.masksToBounds = true
    }

}

extension CustomAdapterExpertCell {
    
    func onBindViewHolder(_ featureList: [ArrExpertChat], position: Int, classRef: AnyObject)
    {
        classRefs = classRef
        
        let user: ArrExpertChat = featureList[position]
        
        self.lblTitle.text = user.expertName
        self.lblSubTitle0.text = user.qualification
        self.lblSubTitle1.text = user.subMsg
    }
}
