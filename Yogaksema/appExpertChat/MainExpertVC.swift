//
//  MainExpertVC.swift
//  Salk
//
//  Created by Chetan  on 13/06/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import Kingfisher

class MainExpertVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    fileprivate var mRefreshControl: UIRefreshControl!
    
    fileprivate var arrMainList = [ArrExpertChat]()
    let CustomAdapterExpertIdentifire = "CustomAdapterExpertCell"
    var offscreenCells = Dictionary<String, UICollectionViewCell>()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // Stop listening notification
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "app_name".applocalized), object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let appPref = MySharedPreferences()
        appPref.setLastDashboardTypeOpen(text: "Chat")
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        let nib = UINib(nibName: CustomAdapterExpertIdentifire, bundle: nil)
        mainCollectionView.register(nib, forCellWithReuseIdentifier: CustomAdapterExpertIdentifire)
        
        self.prepareNavigationItem()
        viewConfiguration()
        onCreateView()
        
        if arrMainList.count <= 0 {
            performServerRequest(isForce: false)
        }
        
        //pull to refresh
        self.mainCollectionView.alwaysBounceVertical = true
        mRefreshControl = UIRefreshControl()
        mRefreshControl.addTarget(self, action: #selector(self.handleRefresh), for: UIControlEvents.valueChanged)
        mainCollectionView.addSubview(mRefreshControl)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onReceive(_:)), name: NSNotification.Name(rawValue: "app_name".applocalized), object: nil)
    }
    
    func handleRefresh() {
        performServerRequest(isForce: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Expert Chat"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        self.navigationController?.navigationBar.hideBottomHairline()
    }

    func viewConfiguration() {
        self.mainCollectionView.backgroundColor = CustomColor.background()
        self.view.backgroundColor = CustomColor.background()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CustomAdapterExpertCell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomAdapterExpertIdentifire, for: indexPath) as! CustomAdapterExpertCell
        
        cell.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        // Make sure layout subviews
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        if(!arrMainList[indexPath.row].expertID.isEmpty) {
            let URL = Foundation.URL(string: "\(SalkProjectConstant().ServeraddExpertImg)\(arrMainList[indexPath.row].expertID.sha1() + Constant.FolderNames.imageExtensionjpg)")!
            cell.imgUser.kf.setImage(with: URL, placeholder: nil, options: [.transition(.fade(1))], progressBlock: { receivedSize, totalSize in
                Print.printLog("\(receivedSize)/\(totalSize)")
            }, completionHandler: { image, error, cacheType, imageURL in
                if error != nil {
                    cell.imgUser.image = UIImage().imageWithImage(UIImage(named: "ic_person_white_gray_24dp.png")!, scaledToSize: CGSize(width: 45, height: 45))
                }
            })
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //open vehical details list
        
        self.moveToDetailController(indexPath.row)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let orientation = UIApplication.shared.statusBarOrientation
        var cellWidth = mainCollectionView.frame.width
        
        // Use fake cell to calculate height
        let reuseIdentifier = CustomAdapterExpertIdentifire
        var cell: CustomAdapterExpertCell? = self.offscreenCells[reuseIdentifier] as? CustomAdapterExpertCell
        if cell == nil {
            cell = Bundle.main.loadNibNamed(CustomAdapterExpertIdentifire, owner: self, options: nil)![0] as? CustomAdapterExpertCell
            self.offscreenCells[reuseIdentifier] = cell
        }
        
        cell!.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        if traitCollection.horizontalSizeClass == .regular || traitCollection.verticalSizeClass == .regular
        {
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                if(orientation == .portrait) {
                    cellWidth = (cellWidth - 1) / 2
                } else {
                    cellWidth = (cellWidth - 5) / 3
                }
            } else {
                if(orientation == .portrait) {
                } else {
                    cellWidth = (cellWidth - 1) / 2
                }
            }
        } else {
            if(orientation == .portrait) {
            } else {
                cellWidth = (cellWidth - 1) / 2
            }
        }
        
        cell!.bounds = CGRect(x: 0, y: 0, width: cellWidth, height: cell!.bounds.height)
        cell!.contentView.bounds = cell!.bounds
        
        // Layout subviews, this will let labels on this cell to set preferredMaxLayoutWidth
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        
        var size = cell!.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        // Still need to force the width, since width can be smalled due to break mode of labels
        size.width = cellWidth
        return size
    }
    
    func moveToDetailController(_ index: Int) {
        let a = ChatViewController()
        a.expertID = arrMainList[index].expertID
        a.expertName = arrMainList[index].expertName
        a.expertCatID = arrMainList[index].expertCatID
        self.navigationController?.pushViewController(a, animated: true)
    }
}

extension MainExpertVC {
    fileprivate func performServerRequest(isForce: Bool) {
    
        let isInternet = CheckInternetConnection().isCheckInternetConnection()
        if (isInternet) {
            let appPref = MySharedPreferences()
            var time:Double = ConvertionClass().currentTime() - appPref.getLastExpertListSycnTime()
            time = (time / (60)) //in minute
            
            if(time > 5 || isForce)
            {
                FitnessRequestManager().getRequest(Constant.ExpertChat.MethodSyncExpertUser, classRef: self)
            }
        } else {
            mRefreshControl.endRefreshing()
        }
    }
    
    func refreshExpertList() {
        stopProgessList()
        
        onCreateView()
    }
    
    func stopProgessList() {
        mRefreshControl.endRefreshing()
    }
    
    // Define the callback for what to do when data is received
    func onReceive(_ notification: Notification) {
        guard let intent = notification.userInfo else {
            return
        }
        let resultCode = intent["resultCode"] as? Int
        let nType = intent["udpateType"] as? String
        //Print.printLog("Broadcaster : \(String(describing: nType))")
        
        if resultCode == 1 {
            if nType == "gcmNTypeChat".localized {
                do {
                    try self.onLoad()
                } catch _ {}
            }
        }
    }
}

extension MainExpertVC {
    
    fileprivate func onCreateView() {
        do {
            try self.onLoad()
        } catch _ {}
        
        let appPref = MySharedPreferences()
        if(appPref.getIsPremiumPlanActive()) {
            //rootView.findViewById(R.id.relPremium).setVisibility(View.GONE);
        } else {
            //rootView.findViewById(R.id.relPremium).setVisibility(View.VISIBLE);
        }
    }
    
    func onLoad() throws {
        
        arrMainList = [ArrExpertChat]()
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        var sql = "Select " + datasource.dbExpertID + ", " + datasource.dbExpertName
            + ", CM." + datasource.dbDateTime
            + ", sum(CASE WHEN "+datasource.dbIsRead + " = '0' THEN 1 ELSE 0 END) "
            + ", CM." + datasource.dbIsInbox + ", CM." + datasource.dbMessage
            + " from " + datasource.ExpertCommMsgList_tlb + " as CM "
            + " GROUP BY CM." + datasource.dbExpertID
            + " ORDER BY CAST(CM." + datasource.dbDateTime + " as INTEGER) DESC "
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        var cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let temp = ArrExpertChat()
                temp.expertName = cursor!.string(forColumnIndex: 1)
                temp.expertID = cursor!.string(forColumnIndex: 0)
                temp.subMsg = cursor!.string(forColumnIndex: 5)
                
                temp.dateTime = ConvertionClass().conLongToDate(cursor!.double(forColumnIndex: 2), dateFormat: dateFormatter)
                temp.totalUnread = cursor!.string(forColumnIndex: 3)
                arrMainList.append(temp)
            }
            cursor!.close()
        }
        
        sql = "SELECT * FROM "+datasource.ExpertUserList_tlb
        
        cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let temp = ArrExpertChat()
                temp.expertName = cursor!.string(forColumnIndex: 1)
                temp.expertID = cursor!.string(forColumnIndex: 0)
                temp.expertCatID = cursor!.string(forColumnIndex: 6)
                temp.subMsg = cursor!.string(forColumnIndex: 2)
                temp.qualification = cursor!.string(forColumnIndex: 3)
                
                var isAvail:Bool = false
                for i in 0 ..< arrMainList.count {
                    if(arrMainList[i].expertID == temp.expertID) {
                        arrMainList[i].expertName = cursor!.string(forColumnIndex: 1)
                        arrMainList[i].qualification = cursor!.string(forColumnIndex: 3)
                        arrMainList[i].expertCatID = cursor!.string(forColumnIndex: 6)
                        isAvail = true;
                        break;
                    }
                }
                
                if(!isAvail) {
                    arrMainList.append(temp)
                }
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        mainCollectionView.reloadData()
        mainCollectionView.isHidden = false
    }
    
}
