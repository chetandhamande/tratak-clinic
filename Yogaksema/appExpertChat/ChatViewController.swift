//
//  ChatViewController.swift
//  SwiftExample
//
//  Created by Dan Leonard on 5/11/16.
//  Copyright © 2016 MacMeDan. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import RAMAnimatedTabBarController
import Material
import Kingfisher

class ChatViewController: JSQMessagesViewController {
    
    var messages = [JSQMessage]()
    let defaults = UserDefaults.standard
    var conversation: Conversation?
    var incomingBubble: JSQMessagesBubbleImage!
    var outgoingBubble: JSQMessagesBubbleImage!
    
    var expertID = "", expertName = "", expertCatID = "", sendFileType = "", oriFileName = ""
    
    fileprivate var moreButton: UIBarButtonItem? = nil
    
    var senderAvtar: JSQMessagesAvatarImage!
    var receiverAvtar: JSQMessagesAvatarImage!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
        
        // Stop listening notification
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "app_name".applocalized), object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appPref = MySharedPreferences()
        self.senderId = "\(appPref.getUserID())"
        self.senderDisplayName = appPref.getFirstName() + " " + appPref.getLastName()
        
        // Setup navigation
        prepareNavigationItem()
        
        // Bubbles with tails
        incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
        outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor.lightGray)
        
        
        collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        
        
        collectionView?.collectionViewLayout.springinessEnabled = false
        automaticallyScrollsToMostRecentMessage = true
       
        senderAvtar = JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: "Me", backgroundColor: UIColor.jsq_messageBubbleGreen(), textColor: UIColor.white, font: UIFont.systemFont(ofSize: 12), diameter: 30)
        receiverAvtar = JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: UIImage(named: "ic_person_white_gray_24dp.png"), diameter: 30)
        
        
        self.collectionView?.reloadData()
        self.collectionView?.layoutIfNeeded()
        
        self.onCreateView() //load previous data
        
        NotificationCenter.default.addObserver(self, selector: #selector(onReceive(_:)), name: NSNotification.Name(rawValue: "app_name".applocalized), object: nil)
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = expertName
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let moreImage = Icon.cm.moreVertical
        moreButton = UIBarButtonItem(image: moreImage, style: .plain, target: self, action: #selector(self.addOptionsMenu))
        
        navigationItem.rightBarButtonItems = [moreButton!]
    }
    
    func addOptionsMenu() {
        let sheet = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        
        let clearAllAction = UIAlertAction(title: "action_clear_all".localized, style: .default) { (action) in
            self.performClearAll()
        }
        let infoAction = UIAlertAction(title: "action_info".localized, style: .default) { (action) in
            //self.settingButtonClick()
        }
        let refreshAction = UIAlertAction(title: "action_refresh".localized, style: .default) { (action) in
            self.gettingRecordServerChat()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        sheet.addAction(clearAllAction)
        sheet.addAction(infoAction)
        sheet.addAction(refreshAction)
        sheet.addAction(cancelAction)
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    
    
    // MARK: JSQMessagesViewController method overrides
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: Date)
    {
        self.finishSendingMessage(animated: true)
        
        // read message
        let datasource = DBOperation()
        datasource.openDatabase(true)
        datasource.InsertExpertCommunicationList(msgID: "0", msg: text, expID: expertID, expName: expertName, dateTime: "\(ConvertionClass().currentTime())", isInbox: "0", isRead: "1", fileName: "", msgType: "text", IsServerSync: "0")
        datasource.closeDatabase()
        
        self.onCreateView()
        
        let appPref = MySharedPreferences()
        appPref.setIsPendingChatRec(text: true)
        
        let isInternet = CheckInternetConnection().isCheckInternetConnection()
        if(isInternet) {
            FitnessRequestManager().getRequest(Constant.ExpertChat.MethodSyncExpertChat, classRef: self)
        }
    }
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        
        let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Send photo", style: .default) { (action) in
            /**
             *  Create fake photo
             */
            let photoItem = JSQPhotoMediaItem(image: UIImage(named: "goldengate"))
            //self.addMedia(photoItem!)
        }
        
        let locationAction = UIAlertAction(title: "Send location", style: .default) { (action) in
            /**
             *  Add fake location
             */
            let locationItem = self.buildLocationItem()
            
            //self.addMedia(locationItem)
        }
        
        let videoAction = UIAlertAction(title: "Send video", style: .default) { (action) in
            /**
             *  Add fake video
             */
            let videoItem = self.buildVideoItem()
            
            //self.addMedia(videoItem)
        }
        
        let audioAction = UIAlertAction(title: "Send audio", style: .default) { (action) in
            /**
             *  Add fake audio
             */
            let audioItem = self.buildAudioItem()
            
            //self.addMedia(audioItem)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        sheet.addAction(photoAction)
        sheet.addAction(videoAction)
        sheet.addAction(audioAction)
        sheet.addAction(cancelAction)
        
        self.present(sheet, animated: true, completion: nil)
    }
    
    func buildVideoItem() -> JSQVideoMediaItem {
        let videoURL = URL(fileURLWithPath: "file://")
        
        let videoItem = JSQVideoMediaItem(fileURL: videoURL, isReadyToPlay: true)
        
        return videoItem!
    }
    
    func buildAudioItem() -> JSQAudioMediaItem {
        let sample = Bundle.main.path(forResource: "jsq_messages_sample", ofType: "m4a")
        let audioData = try? Data(contentsOf: URL(fileURLWithPath: sample!))
        
        let audioItem = JSQAudioMediaItem(data: audioData)
        
        return audioItem
    }
    
    func buildLocationItem() -> JSQLocationMediaItem {
        let ferryBuildingInSF = CLLocation(latitude: 37.795313, longitude: -122.393757)
        
        let locationItem = JSQLocationMediaItem()
        locationItem.setLocation(ferryBuildingInSF) {
            self.collectionView!.reloadData()
        }
        
        return locationItem
    }
    
    func addMedia(_ media:JSQMediaItem) {
        let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: media)
        self.messages.append(message!)
        
        //Optional: play sent sound
        
        self.finishSendingMessage(animated: true)
    }
    
    fileprivate func onCreateView() {
        
        messages.removeAll() //clear previous
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
    
        let sql = "Select " + datasource.dbDateTime + ", CM." + datasource.dbIsInbox + ", CM." + datasource.dbMessage + ", CM." + datasource.dbMsgID + ", CM." + datasource.dbIsRead + ", CM." + datasource.dbMsgType + ", CM." + datasource.dbFileName + " from " + datasource.ExpertCommMsgList_tlb + " as CM " + " WHERE CM." + datasource.dbExpertID + " = '" + expertID + "' " + " ORDER BY CAST(CM." + datasource.dbDateTime + " as INTEGER) "
        
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                if cursor!.string(forColumnIndex: 1) == "1"
                {
                    //received
                    let message = JSQMessage(senderId: expertID, displayName: expertName, text: cursor!.string(forColumnIndex: 2))
                    self.messages.append(message!)
                    
                } else {
                    //send
                    let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, text: cursor!.string(forColumnIndex: 2))
                    self.messages.append(message!)
                }
                
                //temp.msgID = cursor.getString(3);
                //temp.msgType = cursor.getString(5);
                //temp.fileName = cursor.getString(6);
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        self.collectionView?.reloadData()
    }
    
    fileprivate func gettingRecordServerChat() {
        let checkConn = CheckInternetConnection().isCheckInternetConnection()
        if (checkConn) {
            let appPref = MySharedPreferences()
            appPref.setExpertChatID(text: expertID)
    
            FitnessRequestManager().getRequest(Constant.ExpertChat.MethodSyncChatMessage, classRef: self)
        } else {
            SweetAlert().showOnlyAlert("Oops...", subTitle: "noInternet".localized, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
        }
    }
    
    func refreshChatMessage() {
        self.onCreateView()
    }
    
    fileprivate func performClearAll() {
        // truncate database of current user chat
        let dialog = SweetAlert()
        
        //remove all record in trash folder
        dialog.showAlert("action_chat_clear".localized, subTitle: "chatClearMsg".localized, style: AlertStyle.warning, buttonTitle:"btnCancel".localized, buttonColor:CustomColor.light_gray() , otherButtonTitle:  "btnOK".localized, otherButtonColor: CustomColor.google1())
            { (isOtherButton) -> Void in
                if isOtherButton == false {
                    
                    //maintain delete id
                    let appPref = MySharedPreferences()
                    var deleteID = appPref.getIsPendingDeleteChatRec()
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true) // read-write
                    
                    let sql = "Select " + datasource.dbMsgID + " from " + datasource.ExpertCommMsgList_tlb + " as CM " + " WHERE CM." + datasource.dbExpertID + " = '" + self.expertID + "' "
                    let cursor = datasource.selectRecords(sql)
                    if cursor != nil {
                        while cursor!.next() {
                            deleteID += deleteID.isEmpty ? cursor!.string(forColumnIndex: 0) : "," + cursor!.string(forColumnIndex: 0)
                        }
                    }
                    cursor!.close()
                    appPref.setIsPendingDeleteChatRec(text: deleteID)
                    
                    datasource.TruncateExpertCommunicationList(expertID: self.expertID)
                    datasource.closeDatabase()
                    
                    self.syncChatStatus()
                    self.onCreateView()
                }
            }
    }
    
    func syncChatStatus() {
        //check for pending ids
        let appPref = MySharedPreferences()
        if(!appPref.getIsPendingDeleteChatRec().isEmpty
            || !appPref.getIsPendingDeliveredChatRec().isEmpty
            || !appPref.getIsPendingReadChatRec().isEmpty)
        {
            let checkConn = CheckInternetConnection().isCheckInternetConnection()
            if (checkConn) {
                
                FitnessRequestManager().getRequest(Constant.ExpertChat.MethodSyncChatStatus, classRef: self)
            }
        }
    }
    
    // Define the callback for what to do when data is received
    func onReceive(_ notification: Notification) {
        guard let intent = notification.userInfo else {
            return
        }
        let resultCode = intent["resultCode"] as? Int
        let nType = intent["udpateType"] as? String
        //Print.printLog("Broadcaster : \(String(describing: nType))")
        
        if resultCode == 1 {
            if nType == "gcmNTypeChat".localized {
                refreshChatMessage()
            }
        }
    }
}

extension ChatViewController {
    //MARK: JSQMessages CollectionView DataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageDataForItemAt indexPath: IndexPath) -> JSQMessageData {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageBubbleImageDataForItemAt indexPath: IndexPath) -> JSQMessageBubbleImageDataSource {
        
        return messages[indexPath.item].senderId == self.senderId ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
        
        let message = messages[indexPath.item]
        if message.senderId == self.senderId {
            return senderAvtar
        } else {
            return receiverAvtar
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        
        if (indexPath.item % 3 == 0) {
            let message = self.messages[indexPath.item]
            
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.item % 3 == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        
        return 0.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
        
        return 0.0
    }
}
