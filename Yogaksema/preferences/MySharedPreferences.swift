//
//  MySharedPreferences.swift
//  Salk
//
//  Created by Chetan  on 14/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class MySharedPreferences: NSObject {
    
    var objCommon: UserDefaults
    
    public override init() {
        objCommon = UserDefaults.standard
    }
    
    // IS Discalamer accept
    func getISAppDisclamerAccepted() -> Bool {
        return objCommon.bool(forKey: "ISAppDisclamerAccepted")
    }
    func setISAppDisclamerAccepted(text: Bool) {
        objCommon.set(text, forKey: "ISAppDisclamerAccepted")
        objCommon.synchronize()
    }
    
    // User case
    func getCompanyID() -> String {
        if objCommon.string(forKey: "CompanyID") != nil {
            return objCommon.string(forKey: "CompanyID")!
        } else {
            return ""
        }
    }
    func setCompanyID(text: String) {
        objCommon.set(text, forKey: "CompanyID")
        objCommon.synchronize()
    }    
    //Server Version of Application
    func getServerCurrentVersionCode() -> Int {
        return objCommon.integer(forKey: "ServerVersionCode")
    }
    func setServerCurrentVersionCode(text: Int) {
        objCommon.set(text, forKey: "ServerVersionCode")
        objCommon.synchronize()
    }
    //Current photo camera action
    func getSelectPhotoOption() -> String {
        if objCommon.string(forKey: "SelectPhotoOption") != nil {
            return objCommon.string(forKey: "SelectPhotoOption")!
        } else {
            return ""
        }
    }
    func setSelectPhotoOption(ID: String) {
        objCommon.set(ID, forKey: "SelectPhotoOption")
        objCommon.synchronize()
    }
    // Application Default mode
    func getAppDefaultMode() -> String {
        if objCommon.string(forKey: "AppDefaultMode") != nil {
            return objCommon.string(forKey: "AppDefaultMode")!
        } else {
            return ""
        }
    }
    func setAppDefaultMode(text: String) {
        objCommon.set(text, forKey: "AppDefaultMode")
        objCommon.synchronize()
    }
    //Last Dashboard open Type
    func getLastDashboardTypeOpen() -> String {
        if objCommon.string(forKey: "LastDashboardTypeOpen") != nil {
            return objCommon.string(forKey: "LastDashboardTypeOpen")!
        } else {
            return ""
        }
    }
    func setLastDashboardTypeOpen(text: String) {
        objCommon.set(text, forKey: "LastDashboardTypeOpen")
        objCommon.synchronize()
    }
    
    //Last date of update version change
    func getLastDateVersionCodeChange() -> String {
        if objCommon.string(forKey: "LastDateVersionCodeChange") != nil {
            return objCommon.string(forKey: "LastDateVersionCodeChange")!
        } else {
            return ""
        }
    }
    func setLastDateVersionCodeChange(text: String) {
        objCommon.set(text, forKey: "LastDateVersionCodeChange")
        objCommon.synchronize()
    }
    
    //need to update database
    func getIsNeedBuildApplication() -> Bool {
        return objCommon.bool(forKey: "IsNeedBuildApplication")
    }
    func setIsNeedBuildApplication(text: Bool) {
        objCommon.set(text, forKey: "IsNeedBuildApplication")
        objCommon.synchronize()
    }
    // Get Version of Application
    func getAppCurrentVersionCode() -> String! {
        if objCommon.string(forKey: "VersionCode") == nil {
            return ""
        } else {
            return objCommon.string(forKey: "VersionCode")
        }
    }
    func setAppCurrentVersionCode(_ text: String!) {
        objCommon.set(text, forKey: "VersionCode")
        objCommon.synchronize()
    }
    
    // Application APN Token
    func getAPNToken() -> String! {
        if objCommon.string(forKey: "APNToken") == nil {
            return ""
        } else {
            return objCommon.string(forKey: "APNToken")
        }
    }
    func setAPNToken(_ text: String!) {
        objCommon.set(text, forKey: "APNToken")
        objCommon.synchronize()
    }
    //need to update APN On Server
    func getIsAPNUpdateOnServer() -> Bool {
        return objCommon.bool(forKey: "IsAPNUpdateOnServer")
    }
    func setIsAPNUpdateOnServer(_ text: Bool) {
        objCommon.set(text, forKey: "IsAPNUpdateOnServer")
        objCommon.synchronize()
    }
    
    
    // Get Long Version of Application
    func getAppCurrentVersionCodeLong() -> String! {
        if objCommon.string(forKey: "VersionCodeLong") == nil {
            return ""
        } else {
            return objCommon.string(forKey: "VersionCodeLong")
        }
    }
    func setAppCurrentVersionCodeLong(_ text: String!) {
        objCommon.set(text, forKey: "VersionCodeLong")
        objCommon.synchronize()
    }
    
    func setTimeZone(_ text: String) {
        objCommon.setValue(text, forKey: "TimeZone")
        objCommon.synchronize()
    }
    func getTimeZone() -> String {
        if objCommon.string(forKey: "TimeZone") == nil {
            return ""
        } else {
            return objCommon.string(forKey: "TimeZone")!
        }
    }
    func setTimeZoneID(_ text: String) {
        objCommon.setValue(text, forKey: "TimeZoneID")
        objCommon.synchronize()
    }
    func getTimeZoneID() -> String {
        if objCommon.string(forKey: "TimeZoneID") == nil {
            return ""
        } else {
            return objCommon.string(forKey: "TimeZoneID")!
        }
    }
    
    // Get Application ID
    func getApplicationID() -> String! {
        if objCommon.string(forKey: "ApplicationID") == nil {
            return ""
        } else {
            return objCommon.string(forKey: "ApplicationID")
        }
    }
    func setApplicationID(_ text: String!) {
        objCommon.set(text, forKey: "ApplicationID")
        objCommon.synchronize()
    }
    
    // Get Version of Application
    func getMobileCurrentVersionCode() -> String! {
        if objCommon.string(forKey: "MobileCurrentVersionCode") == nil {
            return ""
        } else {
            return objCommon.string(forKey: "MobileCurrentVersionCode")
        }
    }
    func setMobileCurrentVersionCode(_ text: String!) {
        objCommon.set(text, forKey: "MobileCurrentVersionCode")
        objCommon.synchronize()
    }
    
    // Get MobileModel
    func getMobileModel() -> String! {
        if objCommon.string(forKey: "MobileModel") == nil {
            return ""
        } else {
            return objCommon.string(forKey: "MobileModel")
        }
    }
    func setMobileModel(_ text: String!) {
        objCommon.set(text, forKey: "MobileModel")
        objCommon.synchronize()
    }
    
    
    ////////////// Database parameter
    //need to update database
    func getISDBOperationCreated() -> Bool {
        return objCommon.bool(forKey: "ISDBOperationCreated")
    }
    func setISDBOperationCreated(_ text: Bool) {
        objCommon.set(text, forKey: "ISDBOperationCreated")
        objCommon.synchronize()
    }    
    //previous version of database
    func getPreviousDBVersion() -> Int {
        return objCommon.integer(forKey: "PreviousDBVersion")
    }
    func setPreviousDBVersion(text: Int) {
        objCommon.set(text, forKey: "PreviousDBVersion")
        objCommon.synchronize()
    }
    //Is getting weather activity open
    func getISDatabaseWriteMode() -> Bool {
        return objCommon.bool(forKey: "ISDatabaseWriteMode")
    }
    func setISDatabaseWriteMode(text: Bool) {
        objCommon.set(text, forKey: "ISDatabaseWriteMode")
        objCommon.synchronize()
    }
    //////////////End Database parameter
    
    //Device ID of user
    func getDeviceID() -> String {
        if objCommon.string(forKey: "DeviceID") != nil {
            return objCommon.string(forKey: "DeviceID")!
        } else {
            return ""
        }
    }
    func setDeviceID(text: String) {
        objCommon.set(text, forKey: "DeviceID")
        objCommon.synchronize()
    }
    //temp device ID
    func getServerPhoneDeviceID() -> String {
        if objCommon.string(forKey: "ServerPhoneDeviceID") != nil {
            return objCommon.string(forKey: "ServerPhoneDeviceID")!
        } else {
            return ""
        }
    }
    func setServerPhoneDeviceID(text: String) {
        objCommon.set(text, forKey: "ServerPhoneDeviceID")
        objCommon.synchronize()
    }
    //is app running
    func getIsAppRunning() -> Bool {
        return objCommon.bool(forKey: "IsAppRunning")
    }
    func setIsAppRunning(text: Bool) {
        objCommon.set(text, forKey: "IsAppRunning")
        objCommon.synchronize()
    }
    
    
    ////////////user information
    //MobileID
    func getUserID() -> Int {
        return objCommon.integer(forKey: "UserID")
    }
    func setUserID(text: Int) {
        objCommon.set(text, forKey: "UserID")
        objCommon.synchronize()
    }
    //Temp UserID
    func getTempUserID() -> Int {
        return objCommon.integer(forKey: "TempUserID")
    }
    func setTempUserID(text: Int) {
        objCommon.set(text, forKey: "TempUserID")
        objCommon.synchronize()
    }
    //phonebookID
    func getPhonebookID() -> Int {
        return objCommon.integer(forKey: "PhonebookID")
    }
    func setPhonebookID(text: Int) {
        objCommon.set(text, forKey: "PhonebookID")
        objCommon.synchronize()
    }
    //IS Photo change
    func getIsPhotoChange() -> Bool {
        return objCommon.bool(forKey: "IsPhotoChange")
    }
    func setIsPhotoChange(text: Bool) {
        objCommon.set(text, forKey: "IsPhotoChange")
        objCommon.synchronize()
    }
    //Is Interested Author
    func getIsInterestedAuthor() -> Bool {
        return objCommon.bool(forKey: "IsInterestedAuthor")
    }
    func setIsInterestedAuthor(text: Bool) {
        objCommon.set(text, forKey: "IsInterestedAuthor")
        objCommon.synchronize()
    }
    
    //IS App Active
    func getIsAppActive() -> Bool {
        return objCommon.bool(forKey: "IsAppActive")
    }
    func setIsAppActive(text: Bool) {
        objCommon.set(text, forKey: "IsAppActive")
        objCommon.synchronize()
    }
    //Country Code
    func getMobileCountryCode() -> String {
        if objCommon.string(forKey: "MobileCountryCode") != nil {
            return objCommon.string(forKey: "MobileCountryCode")!
        } else {
            return ""
        }
    }
    func setMobileCountryCode(text: String) {
  		objCommon.set(text, forKey: "MobileCountryCode")
        objCommon.synchronize()
    }
    //Country Code Integer
    func getMobileCountryCodeNumber() -> String {
        if objCommon.string(forKey: "MobileCountryCodeNumber") != nil {
            return objCommon.string(forKey: "MobileCountryCodeNumber")!
        } else {
            return ""
        }
    }
    func setMobileCountryCodeNumber(text: String) {
  		objCommon.set(text, forKey: "MobileCountryCodeNumber")
        objCommon.synchronize()
    }
    ////////////////end user info.
    
    ////////////user info
    // FirstName
    func getFirstName() -> String {
        if objCommon.string(forKey: "FirstName") != nil {
            return objCommon.string(forKey: "FirstName")!
        } else {
            return ""
        }
    }
    func setFirstName(text: String) {
        objCommon.set(text, forKey: "FirstName")
        objCommon.synchronize()
    }
    //LastName
    func getLastName() -> String {
        if objCommon.string(forKey: "LastName") != nil {
            return objCommon.string(forKey: "LastName")!
        } else {
            return ""
        }
    }
    func setLastName(text: String) {
        objCommon.set(text, forKey: "LastName")
        objCommon.synchronize()
    }
    
    func getMobile() -> String {
        if objCommon.string(forKey: "Mobile") != nil {
            return objCommon.string(forKey: "Mobile")!
        } else {
            return ""
        }
    }
    func setMobile(text: String) {
        objCommon.set(text, forKey: "Mobile")
        objCommon.synchronize()
    }
    
    
    //Gender
    func getGender() -> String {
        if objCommon.string(forKey: "Gender") != nil {
            return objCommon.string(forKey: "Gender")!
        } else {
            return ""
        }
    }
    func setGender(text: String) {
        objCommon.set(text, forKey: "Gender")
        objCommon.synchronize()
    }
    //DOB
    func getDOB() -> String {
        if objCommon.string(forKey: "DOB") != nil {
            return objCommon.string(forKey: "DOB")!
        } else {
            return ""
        }
    }
    func setDOB(text: String) {
        objCommon.set(text, forKey: "DOB")
        objCommon.synchronize()
    }
    //Is all setting receive
    func getISAllProfSettingReceive() -> Bool {
        return objCommon.bool(forKey: "ISAllProfSettingReceive")
    }
    func setISAllProfSettingReceive(text: Bool) {
        objCommon.set(text, forKey: "ISAllProfSettingReceive")
        objCommon.synchronize()
    }
    //Is Mobile Verification Complete
    func getIsMobileVerifiCmpt() -> Bool {
        return objCommon.bool(forKey: "IsMobileVerifiCmpt")
    }
    func setIsMobileVerifiCmpt(text: Bool) {
        objCommon.set(text, forKey: "IsMobileVerifiCmpt")
        objCommon.synchronize()
    }
    
    //Email ID
    func getEmailID() -> String {
        if objCommon.string(forKey: "EmailID") != nil {
            return objCommon.string(forKey: "EmailID")!
        } else {
            return ""
        }
    }
    func setEmailID(text: String) {
        objCommon.set(text, forKey: "EmailID")
        objCommon.synchronize()
    }
    //Refence Code
    func getDrRefCode() -> String {
        if objCommon.string(forKey: "DrRefCode") != nil {
            return objCommon.string(forKey: "DrRefCode")!
        } else {
            return ""
        }
    }
    func setDrRefCode(text: String) {
        objCommon.set(text, forKey: "DrRefCode")
        objCommon.synchronize()
    }
    //Temp Refence Code
    func getTempDrRefCode() -> String {
        if objCommon.string(forKey: "TempDrRefCode") != nil {
            return objCommon.string(forKey: "TempDrRefCode")!
        } else {
            return ""
        }
    }
    func setTempDrRefCode(text: String) {
        objCommon.set(text, forKey: "TempDrRefCode")
        objCommon.synchronize()
    }
    // user Password
    func getUserPassword() -> String {
        if objCommon.string(forKey: "UserPassword") != nil {
            return objCommon.string(forKey: "UserPassword")!
        } else {
            return ""
        }
    }
    func setUserPassword(text: String) {
        objCommon.set(text, forKey: "UserPassword")
        objCommon.synchronize()
    }
    // is forgot password request
    func getVerifyOTP() -> String {
        if objCommon.string(forKey: "VerifyOTP") != nil {
            return objCommon.string(forKey: "VerifyOTP")!
        } else {
            return ""
        }
    }
    func setVerifyOTP(text: String) {
        objCommon.set(text, forKey: "VerifyOTP")
        objCommon.synchronize()
    }
    // is forgot password request
    func getVerificationSMSComeCode() -> String {
        if objCommon.string(forKey: "VerificationSMSComeCode") != nil {
            return objCommon.string(forKey: "VerificationSMSComeCode")!
        } else {
            return ""
        }
    }
    func setVerificationSMSComeCode(text: String) {
        objCommon.set(text, forKey: "VerificationSMSComeCode")
        objCommon.synchronize()
    }
    //Country Code
    func getCountryCode() -> String {
        if objCommon.string(forKey: "CountryCode") != nil {
            return objCommon.string(forKey: "CountryCode")!
        } else {
            return ""
        }
    }
    func setCountryCode(text: String) {
        objCommon.set(text, forKey: "CountryCode")
        objCommon.synchronize()
    }
    
    //LoginType
    func getLoginType() -> String {
        if objCommon.string(forKey: "LoginType") != nil {
            return objCommon.string(forKey: "LoginType")!
        } else {
            return "phone"
        }
    }
    func setLoginType(text: String) {
        objCommon.set(text, forKey: "LoginType")
        objCommon.synchronize()
    }
    //register User Type
    func getRegUserType() -> String {
        if objCommon.string(forKey: "RegUserType") != nil {
            return objCommon.string(forKey: "RegUserType")!
        } else {
            return "ALL"
        }
    }
    func setRegUserType(text: String) {
        objCommon.set(text, forKey: "RegUserType")
        objCommon.synchronize()
    }
    //UpdateReqType
    func getUpdateReqType() -> String {
        if objCommon.string(forKey: "UpdateReqType") != nil {
            return objCommon.string(forKey: "UpdateReqType")!
        } else {
            return ""
        }
    }
    func setUpdateReqType(text: String) {
        objCommon.set(text, forKey: "UpdateReqType")
        objCommon.synchronize()
    }
    
    // phone verification code
    func getVerificationCode() -> String {
        if objCommon.string(forKey: "VerificationCode") != nil {
            return objCommon.string(forKey: "VerificationCode")!
        } else {
            return ""
        }
    }
    func setVerificationCode(text: String) {
        objCommon.set(text, forKey: "VerificationCode")
        objCommon.synchronize()
    }
    /////////////////end user info
    
    
    /////////// temp user profile
    // Temp user Password
    func getTempUserPassword() -> String {
        if objCommon.string(forKey: "TempUserPassword") != nil {
            return objCommon.string(forKey: "TempUserPassword")!
        } else {
            return ""
        }
    }
    func setTempUserPassword(text: String) {
        objCommon.set(text, forKey: "TempUserPassword")
        objCommon.synchronize()
    }
    //////////////////// end temp user profile
    
    
    ////////////////Http response
    // Error Message
    func getErrorMessage() -> String {
        if objCommon.string(forKey: "RespMessage") != nil {
            return objCommon.string(forKey: "RespMessage")!
        } else {
            return ""
        }
    }
    func setErrorMessage(text: String) {
        objCommon.set(text, forKey: "RespMessage")
        objCommon.synchronize()
    }
    // Error Code
    func getErrorCode() -> Int {
        return objCommon.integer(forKey: "RespCode")
    }
    func setErrorCode(text: Int) {
        objCommon.set(text, forKey: "RespCode")
        objCommon.synchronize()
    }
    //Server Mobile No
    func getServerMobileNo() -> String {
        if objCommon.string(forKey: "ServerMobileNo") != nil {
            return objCommon.string(forKey: "ServerMobileNo")!
        } else {
            return ""
        }
    }
    func setServerMobileNo(text: String) {
        objCommon.set(text, forKey: "ServerMobileNo")
        objCommon.synchronize()
    }
    //////////////end http response
    
    
    /////////////Dashboard information
    // Screen density
    func getScreenDensity() -> String {
        if objCommon.string(forKey: "ScreenDensity") != nil {
            return objCommon.string(forKey: "ScreenDensity")!
        } else {
            return ""
        }
    }
    func setScreenDensity(text: String) {
        objCommon.set(text, forKey: "ScreenDensity")
        objCommon.synchronize()
    }
    
    // phone is verification
    func getISVerificationCode() -> Bool {
        return objCommon.bool(forKey: "ISVerificationCode")
    }
    func setISVerificationCode(text: Bool) {
        objCommon.set(text, forKey: "ISVerificationCode")
        objCommon.synchronize()
    }
    
    // store feedback
    func getUserFeedbackMsg() -> String {
        if objCommon.string(forKey: "UserFeedback") != nil {
            return objCommon.string(forKey: "UserFeedback")!
        } else {
            return ""
        }
    }
    func setUserFeedbackMsg(text: String) {
        objCommon.set(text, forKey: "UserFeedback")
        objCommon.synchronize()
    }
    
    ///////GCM Information
    //GCM Reg ID
    func getGCMRegID() -> String {
        if objCommon.string(forKey: "GCMRegID") != nil {
            return objCommon.string(forKey: "GCMRegID")!
        } else {
            return ""
        }
    }
    func setGCMRegID(text: String) {
        objCommon.set(text, forKey: "GCMRegID")
        objCommon.synchronize()
    }
    //GCM Reg ID On Server
    func getIsGCMRegOnServer() -> Bool {
        return objCommon.bool(forKey: "IsGCMRegOnServer")
    }
    func setIsGCMRegOnServer(text: Bool) {
        objCommon.set(text, forKey: "IsGCMRegOnServer")
        objCommon.synchronize()
    }
    /////////End GCM Information
    
    
    ///////////Health Profile Info
    //Is Health profile Complete
    func getIsProfileHealthCmpt() -> Bool {
        return objCommon.bool(forKey: "IsProfileHealthCmpt")
    }
    func setIsProfileHealthCmpt(text: Bool) {
        objCommon.set(text, forKey: "IsProfileHealthCmpt")
        objCommon.synchronize()
    }
    
    //Is Email Verification Complete
    func getIsEmailVerifiCmpt() -> Bool {
        return objCommon.bool(forKey: "IsEmailVerifiCmpt")
    }
    func setIsEmailVerifiCmpt(text: Bool) {
        objCommon.set(text, forKey: "IsEmailVerifiCmpt")
        objCommon.synchronize()
    }
    
    //Your Goal
    func getUserGoal() -> String {
        if objCommon.string(forKey: "UserGoal") != nil {
            return objCommon.string(forKey: "UserGoal")!
        } else {
            return ""
        }
    }
    func setUserGoal(text: String) {
        objCommon.set(text, forKey: "UserGoal")
        objCommon.synchronize()
    }
    //Your Activeness
    func getUserActiveness() -> String {
        if objCommon.string(forKey: "UserActiveness") != nil {
            return objCommon.string(forKey: "UserActiveness")!
        } else {
            return ""
        }
    }
    func setUserActiveness(text: String) {
        objCommon.set(text, forKey: "UserActiveness")
        objCommon.synchronize()
    }
    //Your weekly goal
    func getUserWeeklyGoal() -> String {
        if objCommon.string(forKey: "UserWeeklyGoal") != nil {
            return objCommon.string(forKey: "UserWeeklyGoal")!
        } else {
            return ""
        }
    }
    func setUserWeeklyGoal(text: String) {
        objCommon.set(text, forKey: "UserWeeklyGoal")
        objCommon.synchronize()
    }
    
    //Your Height
    func getUserHeight() -> String {
        if objCommon.string(forKey: "UserHeight") != nil {
            return objCommon.string(forKey: "UserHeight")!
        } else {
            return ""
        }
    }
    func setUserHeight(text: String) {
        objCommon.set(text, forKey: "UserHeight")
        objCommon.synchronize()
    }
    //Your weight
    func getUserWeight() -> String {
        if objCommon.string(forKey: "UserWeight") != nil {
            return objCommon.string(forKey: "UserWeight")!
        } else {
            return ""
        }
    }
    func setUserWeight(text: String) {
        objCommon.set(text, forKey: "UserWeight")
        objCommon.synchronize()
    }
    //Your Chest
    func getUserChest() -> String {
        if objCommon.string(forKey: "UserChest") != nil {
            return objCommon.string(forKey: "UserChest")!
        } else {
            return ""
        }
    }
    func setUserChest(text: String) {
        objCommon.set(text, forKey: "UserChest")
        objCommon.synchronize()
    }
    //Your Waist
    func getUserWaist() -> String {
        if objCommon.string(forKey: "UserWaist") != nil {
            return objCommon.string(forKey: "UserWaist")!
        } else {
            return ""
        }
    }
    func setUserWaist(text: String) {
        objCommon.set(text, forKey: "UserWaist")
        objCommon.synchronize()
    }
    //Your Hip
    func getUserHip() -> String {
        if objCommon.string(forKey: "UserHip") != nil {
            return objCommon.string(forKey: "UserHip")!
        } else {
            return ""
        }
    }
    func setUserHip(text: String) {
        objCommon.set(text, forKey: "UserHip")
        objCommon.synchronize()
    }
    //Your Goal weight
    func getUserGoalWeight() -> String {
        if objCommon.string(forKey: "UserGoalWeight") != nil {
            return objCommon.string(forKey: "UserGoalWeight")!
        } else {
            return ""
        }
    }
    func setUserGoalWeight(text: String) {
        objCommon.set(text, forKey: "UserGoalWeight")
        objCommon.synchronize()
    }
    
    
    //Fitness band Name
    func getFitnessBandName() -> String {
        if objCommon.string(forKey: "FitnessBandName") != nil {
            return objCommon.string(forKey: "FitnessBandName")!
        } else {
            return ""
        }
    }
    func setFitnessBandName(text: String) {
        objCommon.set(text, forKey: "FitnessBandName")
        objCommon.synchronize()
    }
    //Your User Fitbit Connected
    func getIsFitnessBandConnect() -> Bool {
        return objCommon.bool(forKey: "IsFitnessBandConnect")
    }
    func setIsFitnessBandConnect(text: Bool) {
        objCommon.set(text, forKey: "IsFitnessBandConnect")
        objCommon.synchronize()
    }
    //Fitbit AuthToken
    func getFitbitAuthToken() -> String {
        if objCommon.string(forKey: "FitbitAuthToken") != nil {
            return objCommon.string(forKey: "FitbitAuthToken")!
        } else {
            return ""
        }
    }
    func setFitbitAuthToken(text: String) {
        objCommon.set(text, forKey: "FitbitAuthToken")
        objCommon.synchronize()
    }
    //Fitbit AuthVerifer
    func getFitbitAuthVerifer() -> String {
        if objCommon.string(forKey: "FitbitAuthVerifer") != nil {
            return objCommon.string(forKey: "FitbitAuthVerifer")!
        } else {
            return ""
        }
    }
    func setFitbitAuthVerifer(text: String) {
        objCommon.set(text, forKey: "FitbitAuthVerifer")
        objCommon.synchronize()
    }
    //Fitness Welcome message show
    func getIsFitnessWelcomShow() -> Bool {
        return objCommon.bool(forKey: "IsFitnessWelcomShow")
    }
    func setIsFitnessWelcomShow(text: Bool) {
        objCommon.set(text, forKey: "IsFitnessWelcomShow")
        objCommon.synchronize()
    }
    
    
    //Last Expert List Sync time
    func getLastExpertListSycnTime() -> Double {
        return objCommon.double(forKey: "LastExpertListSycnTime")
    }
    func setLastExpertListSycnTime(text: Double) {
        objCommon.set(text, forKey: "LastExpertListSycnTime")
        objCommon.synchronize()
    }
    //Last Food Log Sync time
    func getLastFoodLogListSycnTime() -> Double {
        return objCommon.double(forKey: "LastFoodLogListSycnTime")
    }
    func setLastFoodLogListSycnTime(text: Double) {
        objCommon.set(text, forKey: "LastFoodLogListSycnTime")
        objCommon.synchronize()
    }
    //Last Plan Log Sync time
    func getLastPlanLogListSycnTime() -> Double {
        return objCommon.double(forKey: "LastPlanLogListSycnTime")
    }
    func setLastPlanLogListSycnTime(text: Double) {
        objCommon.set(text, forKey: "LastPlanLogListSycnTime")
        objCommon.synchronize()
    }
    //Last Activity Log Sync time
    func getLastActivityLogListSycnTime() -> Double {
        return objCommon.double(forKey: "LastActivityLogListSycnTime")
    }
    func setLastActivityLogListSycnTime(text: Double) {
        objCommon.set(text, forKey: "LastActivityLogListSycnTime")
        objCommon.synchronize()
    }
    //Last Log Data Sync time
    func getLastLogDataSycnTime() -> Double {
        return objCommon.double(forKey: "LastLogDataSycnTime")
    }
    func setLastLogDataSycnTime(text: Double) {
        objCommon.set(text, forKey: "LastLogDataSycnTime")
        objCommon.synchronize()
    }
    //Last Log Data Sync Type
    func getLastLogDataSycnType() -> String {
        if objCommon.string(forKey: "LastLogDataSycnType") != nil {
            return objCommon.string(forKey: "LastLogDataSycnType")!
        } else {
            return ""
        }
    }
    func setLastLogDataSycnType(text: String) {
        objCommon.set(text, forKey: "LastLogDataSycnType")
        objCommon.synchronize()
    }
    //Last MyDiary Data Sync time
    func getLastMyDiaryDataSycnTime() -> Double {
        return objCommon.double(forKey: "LastMyDiaryDataSycnTime")
    }
    func setLastMyDiaryDataSycnTime(text: Double) {
        objCommon.set(text, forKey: "LastMyDiaryDataSycnTime")
        objCommon.synchronize()
    }
    //Last Apointment Sync time
    func getLastAppointmentSycnTime() -> Double {
        return objCommon.double(forKey: "LastAppointmentSycnTime")
    }
    func setLastAppointmentSycnTime(text: Double) {
        objCommon.set(text, forKey: "LastAppointmentSycnTime")
        objCommon.synchronize()
    }
    
    //Diat Preference
    func getDiatPref() -> String {
        if objCommon.string(forKey: "DiatPref") != nil {
            return objCommon.string(forKey: "DiatPref")!
        } else {
            return ""
        }
    }
    func setDiatPref(text: String) {
        objCommon.set(text, forKey: "DiatPref")
        objCommon.synchronize()
    }
    //Medical Condition
    func getMedicalCondition() -> String {
        if objCommon.string(forKey: "MedicalCondition") != nil {
            return objCommon.string(forKey: "MedicalCondition")!
        } else {
            return ""
        }
    }
    func setMedicalCondition(text: String) {
        objCommon.set(text, forKey: "MedicalCondition")
        objCommon.synchronize()
    }
    //Food Allergies
    func getFoodAllergies() -> String {
        if objCommon.string(forKey: "FoodAllergies") != nil {
            return objCommon.string(forKey: "FoodAllergies")!
        } else {
            return ""
        }
    }
    func setFoodAllergies(text: String) {
        objCommon.set(text, forKey: "FoodAllergies")
        objCommon.synchronize()
    }
    //Food Ethnicity
    func getFoodEthnicity() -> String {
        if objCommon.string(forKey: "FoodEthnicity") != nil {
            return objCommon.string(forKey: "FoodEthnicity")!
        } else {
            return ""
        }
    }
    func setFoodEthnicity(text: String) {
        objCommon.set(text, forKey: "FoodEthnicity")
        objCommon.synchronize()
    }
    //Body Shape
    func getBodyShape() -> String {
        if objCommon.string(forKey: "BodyShape") != nil {
            return objCommon.string(forKey: "BodyShape")!
        } else {
            return ""
        }
    }
    func setBodyShape(text: String) {
        objCommon.set(text, forKey: "BodyShape")
        objCommon.synchronize()
    }
    //User BMR
    func getUserBMR() -> String {
        if objCommon.string(forKey: "UserBMR") != nil {
            return objCommon.string(forKey: "UserBMR")!
        } else {
            return ""
        }
    }
    func setUserBMR(text: String) {
        objCommon.set(text, forKey: "UserBMR")
        objCommon.synchronize()
    }
    //User BMI
    func getUserBMI() -> String {
        if objCommon.string(forKey: "UserBMI") != nil {
            return objCommon.string(forKey: "UserBMI")!
        } else {
            return ""
        }
    }
    func setUserBMI(text: String) {
        objCommon.set(text, forKey: "UserBMI")
        objCommon.synchronize()
    }
    
    //WakeUpTime
    func getWakeUpTime() -> String {
        if objCommon.string(forKey: "WakeUpTime") != nil {
            return objCommon.string(forKey: "WakeUpTime")!
        } else {
            return ""
        }
    }
    func setWakeUpTime(text: String) {
        objCommon.set(text, forKey: "WakeUpTime")
        objCommon.synchronize()
    }
    //BreakfastTime
    func getBreakfastTime() -> String {
        if objCommon.string(forKey: "BreakfastTime") != nil {
            return objCommon.string(forKey: "BreakfastTime")!
        } else {
            return ""
        }
    }
    func setBreakfastTime(text: String) {
        objCommon.set(text, forKey: "BreakfastTime")
        objCommon.synchronize()
    }
    //LunchTime
    func getLunchTime() -> String {
        if objCommon.string(forKey: "LunchTime") != nil {
            return objCommon.string(forKey: "LunchTime")!
        } else {
            return ""
        }
    }
    func setLunchTime(text: String) {
        objCommon.set(text, forKey: "LunchTime")
        objCommon.synchronize()
    }
    //EveningTeaTime
    func getEveningTeaTime() -> String {
        if objCommon.string(forKey: "EveningTeaTime") != nil {
            return objCommon.string(forKey: "EveningTeaTime")!
        } else {
            return ""
        }
    }
    func setEveningTeaTime(text: String) {
        objCommon.set(text, forKey: "EveningTeaTime")
        objCommon.synchronize()
    }
    //DinnerTime
    func getDinnerTime() -> String {
        if objCommon.string(forKey: "DinnerTime") != nil {
            return objCommon.string(forKey: "DinnerTime")!
        } else {
            return ""
        }
    }
    func setDinnerTime(text: String) {
        objCommon.set(text, forKey: "DinnerTime")
        objCommon.synchronize()
    }
    
    //UserHobbies
    func getUserHobbies() -> String {
        if objCommon.string(forKey: "UserHobbies") != nil {
            return objCommon.string(forKey: "UserHobbies")!
        } else {
            return ""
        }
    }
    func setUserHobbies(text: String) {
        objCommon.set(text, forKey: "UserHobbies")
        objCommon.synchronize()
    }
    //FoodEatOut
    func getFoodEatOut() -> String {
        if objCommon.string(forKey: "FoodEatOut") != nil {
            return objCommon.string(forKey: "FoodEatOut")!
        } else {
            return ""
        }
    }
    func setFoodEatOut(text: String) {
        objCommon.set(text, forKey: "FoodEatOut")
        objCommon.synchronize()
    }
    //Addicted Gadget
    func getAddictedGadget() -> String {
        if objCommon.string(forKey: "AddictedGadget") != nil {
            return objCommon.string(forKey: "AddictedGadget")!
        } else {
            return ""
        }
    }
    func setAddictedGadget(text: String) {
        objCommon.set(text, forKey: "AddictedGadget")
        objCommon.synchronize()
    }
    //Alcohol Intake
    func getAlcoholIntake() -> String {
        if objCommon.string(forKey: "AlcoholIntake") != nil {
            return objCommon.string(forKey: "AlcoholIntake")!
        } else {
            return ""
        }
    }
    func setAlcoholIntake(text: String) {
        objCommon.set(text, forKey: "AlcoholIntake")
        objCommon.synchronize()
    }
    //Smoke Intake
    func getSmokeIntake() -> String {
        if objCommon.string(forKey: "SmokeIntake") != nil {
            return objCommon.string(forKey: "SmokeIntake")!
        } else {
            return ""
        }
    }
    func setSmokeIntake(text: String) {
        objCommon.set(text, forKey: "SmokeIntake")
        objCommon.synchronize()
    }
    //Fruit Diet dry
    func getFruitDietDry() -> String {
        if objCommon.string(forKey: "FruitDietDry") != nil {
            return objCommon.string(forKey: "FruitDietDry")!
        } else {
            return ""
        }
    }
    func setFruitDietDry(text: String) {
        objCommon.set(text, forKey: "FruitDietDry")
        objCommon.synchronize()
    }
    //Fruit Daily
    func getFruitDaily() -> String {
        if objCommon.string(forKey: "FruitDaily") != nil {
            return objCommon.string(forKey: "FruitDaily")!
        } else {
            return ""
        }
    }
    func setFruitDaily(text: String) {
        objCommon.set(text, forKey: "FruitDaily")
        objCommon.synchronize()
    }
    //Veg Daily
    func getVegDaily() -> String {
        if objCommon.string(forKey: "VegDaily") != nil {
            return objCommon.string(forKey: "VegDaily")!
        } else {
            return ""
        }
    }
    func setVegDaily(text: String) {
        objCommon.set(text, forKey: "VegDaily")
        objCommon.synchronize()
    }
    
    //Last Log Data Sync time
    func getLastRecentRecDate() -> String {
        if objCommon.string(forKey: "LastRecentRecDate") != nil {
            return objCommon.string(forKey: "LastRecentRecDate")!
        } else {
            return ""
        }
    }
    func setLastRecentRecDate(text: String) {
        objCommon.set(text, forKey: "LastRecentRecDate")
        objCommon.synchronize()
    }
    
    //Temp Record ID use for any other operations
    func getTempRecordID() -> String {
        if objCommon.string(forKey: "TempRecordID") != nil {
            return objCommon.string(forKey: "TempRecordID")!
        } else {
            return ""
        }
    }
    func setTempRecordID(text: String) {
        objCommon.set(text, forKey: "TempRecordID")
        objCommon.synchronize()
    }
    ///////////End Health Profile Info
    
    
    //////////////////Expert Chat
    //Is Pending chat record
    func getIsPendingChatRec() -> Bool {
        return objCommon.bool(forKey: "IsPendingChatRec")
    }
    func setIsPendingChatRec(text: Bool) {
        objCommon.set(text, forKey: "IsPendingChatRec")
        objCommon.synchronize()
    }
    //Is Pending Read chat record
    func getIsPendingReadChatRec() -> String {
        if objCommon.string(forKey: "IsPendingReadChatRec") != nil {
            return objCommon.string(forKey: "IsPendingReadChatRec")!
        } else {
            return ""
        }
    }
    func setIsPendingReadChatRec(text: String) {
        objCommon.set(text, forKey: "IsPendingReadChatRec")
        objCommon.synchronize()
    }
    //Is Pending Delivered chat record
    func getIsPendingDeliveredChatRec() -> String {
        if objCommon.string(forKey: "IsPendingDeliveredChatRec") != nil {
            return objCommon.string(forKey: "IsPendingDeliveredChatRec")!
        } else {
            return ""
        }
    }
    func setIsPendingDeliveredChatRec(text: String) {
        objCommon.set(text, forKey: "IsPendingDeliveredChatRec")
        objCommon.synchronize()
    }
    //Is Pending Delete chat record
    func getIsPendingDeleteChatRec() -> String {
        if objCommon.string(forKey: "IsPendingDeleteChatRec") != nil {
            return objCommon.string(forKey: "IsPendingDeleteChatRec")!
        } else {
            return ""
        }
    }
    func setIsPendingDeleteChatRec(text: String) {
        objCommon.set(text, forKey: "IsPendingDeleteChatRec")
        objCommon.synchronize()
    }
    
    //Expert Chat ID
    func getExpertChatID() -> String {
        if objCommon.string(forKey: "ExpertChatID") != nil {
            return objCommon.string(forKey: "ExpertChatID")!
        } else {
            return ""
        }
    }
    func setExpertChatID(text: String) {
        objCommon.set(text, forKey: "ExpertChatID")
        objCommon.synchronize()
    }
    ////////////End Expert chat
    
    
    //////////////////Reminder
    //is Water Reminder on
    func getIsWaterRemOn() -> Bool {
        return objCommon.bool(forKey: "IsWaterRemOn")
    }
    func setIsWaterRemOn(text: Bool) {
        objCommon.set(text, forKey: "IsWaterRemOn")
        objCommon.synchronize()
    }
    //Water reminder status
    func getWaterRemValue() -> String {
        if objCommon.string(forKey: "WaterRemValue") != nil {
            return objCommon.string(forKey: "WaterRemValue")!
        } else {
            return "-"
        }
    }
    func setWaterRemValue(text: String) {
        objCommon.set(text, forKey: "WaterRemValue")
        objCommon.synchronize()
    }
    
    //is GreenTea Reminder on
    func getIsGreenTeaRemOn() -> Bool {
        return objCommon.bool(forKey: "IsGreenTeaRemOn")
    }
    func setIsGreenTeaRemOn(text: Bool) {
        objCommon.set(text, forKey: "IsGreenTeaRemOn")
        objCommon.synchronize()
    }
    //GreenTea reminder status
    func getGreenTeaRemValue() -> String {
        if objCommon.string(forKey: "GreenTeaRemValue") != nil {
            return objCommon.string(forKey: "GreenTeaRemValue")!
        } else {
            return "-"
        }
    }
    func setGreenTeaRemValue(text: String) {
        objCommon.set(text, forKey: "GreenTeaRemValue")
        objCommon.synchronize()
    }
    
    //is Walk Reminder on
    func getIsWalkRemOn() -> Bool {
        return objCommon.bool(forKey: "IsWalkRemOn")
    }
    func setIsWalkRemOn(text: Bool) {
        objCommon.set(text, forKey: "IsWalkRemOn")
        objCommon.synchronize()
    }
    //Walk reminder status
    func getWalkRemValue() -> String {
        if objCommon.string(forKey: "WalkRemValue") != nil {
            return objCommon.string(forKey: "WalkRemValue")!
        } else {
            return "-"
        }
    }
    func setWalkRemValue(text: String) {
        objCommon.set(text, forKey: "WalkRemValue")
        objCommon.synchronize()
    }
    
    //is Sleep Reminder on
    func getIsSleepRemOn() -> Bool {
        return objCommon.bool(forKey: "IsSleepRemOn")
    }
    func setIsSleepRemOn(text: Bool) {
        objCommon.set(text, forKey: "IsSleepRemOn")
        objCommon.synchronize()
    }
    //Sleep reminder status
    func getSleepRemValue() -> String {
        if objCommon.string(forKey: "SleepRemValue") != nil {
            return objCommon.string(forKey: "SleepRemValue")!
        } else {
            return "-"
        }
    }
    func setSleepRemValue(text: String) {
        objCommon.set(text, forKey: "SleepRemValue")
        objCommon.synchronize()
    }
    
    //is Food Reminder on
    func getIsFoodRemOn() -> Bool {
        return objCommon.bool(forKey: "IsFoodRemOn")
    }
    func setIsFoodRemOn(text: Bool) {
        objCommon.set(text, forKey: "IsFoodRemOn")
        objCommon.synchronize()
    }
    //Food reminder status
    func getFoodRemValue() -> String {
        if objCommon.string(forKey: "FoodRemValue") != nil {
            return objCommon.string(forKey: "FoodRemValue")!
        } else {
            return "-"
        }
    }
    func setFoodRemValue(text: String) {
        objCommon.set(text, forKey: "FoodRemValue")
        objCommon.synchronize()
    }
    
    //Food Name reminder status
    func getFoodRemName() -> String {
        if objCommon.string(forKey: "FoodRemName") != nil {
            return objCommon.string(forKey: "FoodRemName")!
        } else {
            return "-"
        }
    }
    func setFoodRemName(text: String) {
        objCommon.set(text, forKey: "FoodRemName")
        objCommon.synchronize()
    }
    
    //is Fitness Reminder on
    func getIsFitnessRemOn() -> Bool {
        return objCommon.bool(forKey: "IsFitnessRemOn")
    }
    func setIsFitnessRemOn(text: Bool) {
        objCommon.set(text, forKey: "IsFitnessRemOn")
        objCommon.synchronize()
    }
    //Fitness reminder status
    func getFitnessRemValue() -> String {
        if objCommon.string(forKey: "FitnessRemValue") != nil {
            return objCommon.string(forKey: "FitnessRemValue")!
        } else {
            return "-"
        }
    }
    func setFitnessRemValue(text: String) {
        objCommon.set(text, forKey: "FitnessRemValue")
        objCommon.synchronize()
    }
    
    //is Nutraceuticals Reminder on
    func getIsNutraceuticalsRemOn() -> Bool {
        return objCommon.bool(forKey: "IsNutraceuticalsRemOn")
    }
    func setIsNutraceuticalsRemOn(text: Bool) {
        objCommon.set(text, forKey: "IsNutraceuticalsRemOn")
        objCommon.synchronize()
    }
    //Fitness reminder status
    func getNutraceuticalsRemValue() -> String {
        if objCommon.string(forKey: "NutraceuticalsRemValue") != nil {
            return objCommon.string(forKey: "NutraceuticalsRemValue")!
        } else {
            return "-"
        }
    }
    func setNutraceuticalsRemValue(text: String) {
        objCommon.set(text, forKey: "NutraceuticalsRemValue")
        objCommon.synchronize()
    }
    
    //is Pending Reminder
    func getIsPendingReminder() -> Bool {
        return objCommon.bool(forKey: "IsPendingReminder")
    }
    func setIsPendingReminder(text: Bool) {
        objCommon.set(text, forKey: "IsPendingReminder")
        objCommon.synchronize()
    }
    
    ////////////End Reminder
    
    
    /////Daily Goals Valuues
    func getDailySteps() -> String {
        if objCommon.string(forKey: "DailySteps") != nil {
            return objCommon.string(forKey: "DailySteps")!
        } else {
            return "10000"
        }
    }
    func setDailySteps(text: String) {
        objCommon.set(text, forKey: "DailySteps")
        objCommon.synchronize()
    }
    //Your User Dialy Cal
    func getDailyCalTake() -> String {
        if objCommon.string(forKey: "DailyCalTake") != nil {
            return objCommon.string(forKey: "DailyCalTake")!
        } else {
            return "2000"
        }
    }
    func setDailyCalTake(text: String) {
        objCommon.set(text, forKey: "DailyCalTake")
        objCommon.synchronize()
    }
    //Your User Dialy Cal Burn
    func getDailyCalBurn() -> String {
        if objCommon.string(forKey: "DailyCalBurn") != nil {
            return objCommon.string(forKey: "DailyCalBurn")!
        } else {
            return "2000"
        }
    }
    func setDailyCalBurn(text: String) {
        objCommon.set(text, forKey: "DailyCalBurn")
        objCommon.synchronize()
    }
    //Daily Distance
    func getDailyDistance() -> String {
        if objCommon.string(forKey: "DailyDistance") != nil {
            return objCommon.string(forKey: "DailyDistance")!
        } else {
            return "8"
        }
    }
    func setDailyDistance(text: String) {
        objCommon.set(text, forKey: "DailyDistance")
        objCommon.synchronize()
    }
    //Daily Water
    func getDailyWater() -> String {
        if objCommon.string(forKey: "DailyWater") != nil {
            return objCommon.string(forKey: "DailyWater")!
        } else {
            return "4000"
        }
    }
    func setDailyWater(text: String) {
        objCommon.set(text, forKey: "DailyWater")
        objCommon.synchronize()
    }
    //Daily Sleep
    func getDailySleep() -> String {
        if objCommon.string(forKey: "DailySleep") != nil {
            return objCommon.string(forKey: "DailySleep")!
        } else {
            return "8"
        }
    }
    func setDailySleep(text: String) {
        objCommon.set(text, forKey: "DailySleep")
        objCommon.synchronize()
    }
    
    //Timeline Time
    func getTimelineFromDate() -> String {
        if objCommon.string(forKey: "TimelineFromDate") != nil {
            return objCommon.string(forKey: "TimelineFromDate")!
        } else {
            return ""
        }
    }
    func setTimelineFromDate(text: String) {
        objCommon.set(text, forKey: "TimelineFromDate")
        objCommon.synchronize()
    }
    
    func getTimelineToDate() -> String {
        if objCommon.string(forKey: "TimelineToDate") != nil {
            return objCommon.string(forKey: "TimelineToDate")!
        } else {
            return ""
        }
    }
    func setTimelineToDate(text: String) {
        objCommon.set(text, forKey: "TimelineToDate")
        objCommon.synchronize()
    }
    
    //Timeline Type
    func getTimelineType() -> String {
        if objCommon.string(forKey: "TimelineType") != nil {
            return objCommon.string(forKey: "TimelineType")!
        } else {
            return ""
        }
    }
    func setTimelineType(text: String) {
        objCommon.set(text, forKey: "TimelineType")
        objCommon.synchronize()
    }
    
    func getTimelineFilter() -> String {
        if objCommon.string(forKey: "TimelineFilter") != nil {
            return objCommon.string(forKey: "TimelineFilter")!
        } else {
            return "Routine,Food,Sleep,Water,UserInfo"
        }
    }
    func setTimelineFilter(text: String) {
        objCommon.set(text, forKey: "TimelineFilter")
        objCommon.synchronize()
    }
    ///////////End Daily Goals
    
    
    //////////////Purchase plan
    //Is Premium Plan Active
    func getIsPremiumPlanActive() -> Bool {
        return objCommon.bool(forKey: "IsPremiumPlanActive")
    }
    func setIsPremiumPlanActive(text: Bool) {
        objCommon.set(text, forKey: "IsPremiumPlanActive")
        objCommon.synchronize()
    }
    //Plan Voucher Key
    func getPlanVoucherKey() -> String {
        if objCommon.string(forKey: "PlanVoucherKey") != nil {
            return objCommon.string(forKey: "PlanVoucherKey")!
        } else {
            return ""
        }
    }
    func setPlanVoucherKey(text: String) {
        objCommon.set(text, forKey: "PlanVoucherKey")
        objCommon.synchronize()
    }
    //Plan Temp Voucher Key
    func getTempPlanVoucherKey() -> String {
        if objCommon.string(forKey: "TempPlanVoucherKey") != nil {
            return objCommon.string(forKey: "TempPlanVoucherKey")!
        } else {
            return ""
        }
    }
    func setTempPlanVoucherKey(text: String) {
        objCommon.set(text, forKey: "TempPlanVoucherKey")
        objCommon.synchronize()
    }
    //Plan Voucher Start Date
    func getPlanStartDate() -> Double {
        return objCommon.double(forKey: "PlanStartDate")
    }
    func setPlanStartDate(text: Double) {
        objCommon.set(text, forKey: "PlanStartDate")
        objCommon.synchronize()
    }
    //Plan Voucher End Date
    func getPlanEndDate() -> Double {
        return objCommon.double(forKey: "PlanEndDate")
    }
    func setPlanEndDate(text: Double) {
        objCommon.set(text, forKey: "PlanEndDate")
        objCommon.synchronize()
    }
    
    //Plan ID
    func getPlanID() -> String {
        if objCommon.string(forKey: "PlanID") != nil {
            return objCommon.string(forKey: "PlanID")!
        } else {
            return ""
        }
    }
    func setPlanID(text: String) {
        objCommon.set(text, forKey: "PlanID")
        objCommon.synchronize()
    }
    //Plan Name
    func getPlanName() -> String {
        if objCommon.string(forKey: "PlanName") != nil {
            return objCommon.string(forKey: "PlanName")!
        } else {
            return ""
        }
    }
    func setPlanName(text: String) {
        objCommon.set(text, forKey: "PlanName")
        objCommon.synchronize()
    }
    //Plan Duration
    func getPlanDuration() -> String {
        if objCommon.string(forKey: "PlanDuration") != nil {
            return objCommon.string(forKey: "PlanDuration")!
        } else {
            return ""
        }
    }
    func setPlanDuration(text: String) {
        objCommon.set(text, forKey: "PlanDuration")
        objCommon.synchronize()
    }
    //Plan TotalAmount
    func getPlanTotalAmount() -> String {
        if objCommon.string(forKey: "PlanTotalAmount") != nil {
            return objCommon.string(forKey: "PlanTotalAmount")!
        } else {
            return ""
        }
    }
    func setPlanTotalAmount(text: String) {
        objCommon.set(text, forKey: "PlanTotalAmount")
        objCommon.synchronize()
    }
    
    //Plan Request ID
    func getPlanRequestID() -> String {
        if objCommon.string(forKey: "PlanRequestID") != nil {
            return objCommon.string(forKey: "PlanRequestID")!
        } else {
            return ""
        }
    }
    func setPlanRequestID(text: String) {
        objCommon.set(text, forKey: "PlanRequestID")
        objCommon.synchronize()
    }
    //Plan Request ID
    func getPlanBankRefNum() -> String {
        if objCommon.string(forKey: "PlanBankRefNum") != nil {
            return objCommon.string(forKey: "PlanBankRefNum")!
        } else {
            return ""
        }
    }
    func setPlanBankRefNum(text: String) {
        objCommon.set(text, forKey: "PlanBankRefNum")
        objCommon.synchronize()
    }
    //Plan Coupon Code
    func getPlanCouponCode() -> String {
        if objCommon.string(forKey: "PlanCouponCode") != nil {
            return objCommon.string(forKey: "PlanCouponCode")!
        } else {
            return ""
        }
    }
    func setPlanCouponCode(text: String) {
        objCommon.set(text, forKey: "PlanCouponCode")
        objCommon.synchronize()
    }
    //Plan Features List
    func getPlanFeaturesList() -> String {
        if objCommon.string(forKey: "PlanFeaturesList") != nil {
            return objCommon.string(forKey: "PlanFeaturesList")!
        } else {
            return ""
        }
    }
    func setPlanFeaturesList(text: String) {
        objCommon.set(text, forKey: "PlanFeaturesList")
        objCommon.synchronize()
    }
    //////////////End Purchase Plan
    
    
    //////////Notification Setting
    //Is Noti Diet
    func getIsNotifyDiet() -> Bool {
        return objCommon.bool(forKey: "IsNotifyDiet")
    }
    func setIsNotifyDiet(text: Bool) {
        objCommon.set(text, forKey: "IsNotifyDiet")
        objCommon.synchronize()
    }
    //Is Noti Activity
    func getIsNotifyActivity() -> Bool {
        return objCommon.bool(forKey: "IsNotifyActivity")
    }
    func setIsNotifyActivity(text: Bool) {
        objCommon.set(text, forKey: "IsNotifyActivity")
        objCommon.synchronize()
    }
    //Is Noti Reminder
    func getIsNotifyReminder() -> Bool {
        return objCommon.bool(forKey: "IsNotifyReminder")
    }
    func setIsNotifyReminder(text: Bool) {
        objCommon.set(text, forKey: "IsNotifyReminder")
        objCommon.synchronize()
    }
    //Is Noti Expert Chat
    func getIsNotifyExpertChat() -> Bool {
        return objCommon.bool(forKey: "IsNotifyExpertChat")
    }
    func setIsNotifyExpertChat(text: Bool) {
        objCommon.set(text, forKey: "IsNotifyExpertChat")
        objCommon.synchronize()
    }
    //Is Noti News Letter
    func getIsNotifyNewsLetter() -> Bool {
        return objCommon.bool(forKey: "IsNotifyNewsLetter")
    }
    func setIsNotifyNewsLetter(text: Bool) {
        objCommon.set(text, forKey: "IsNotifyNewsLetter")
        objCommon.synchronize()
    }
    //Is Noti promo code
    func getIsNotifyPromoCode() -> Bool {
        return objCommon.bool(forKey: "IsNotifyPromoCode")
    }
    func setIsNotifyPromoCode(text: Bool) {
        objCommon.set(text, forKey: "IsNotifyPromoCode")
        objCommon.synchronize()
    }
    ////////end Notification Setting
   
    //know about
    func getKnowAbout() -> String {
        if objCommon.string(forKey: "KnowAbout") != nil {
            return objCommon.string(forKey: "KnowAbout")!
        } else {
            return ""
        }
    }
    func setKnowAbout(text: String) {
        objCommon.set(text, forKey: "KnowAbout")
        objCommon.synchronize()
    }
    
    //know about
    func getOtherSource() -> String {
        if objCommon.string(forKey: "OtherSource") != nil {
            return objCommon.string(forKey: "OtherSource")!
        } else {
            return ""
        }
    }
    func setOtherSource(text: String) {
        objCommon.set(text, forKey: "OtherSource")
        objCommon.synchronize()
    }
}
