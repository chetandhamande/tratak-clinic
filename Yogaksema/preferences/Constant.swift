//
//  Constant.swift
//  Salk
//
//  Created by Chetan  on 13/06/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

public struct Constant {
    
    struct FolderNames {
        static let imageExtension = ".jpeg"
        static let imageExtensionjpg = ".jpeg"
        static let imageExtensionPng = ".png"
        static let imageExtensionOgg = ".ogg"
        
        static let mainFolderName = "salk"
        static let folderUserProfile = "userProfile"
        static let folderCatche = "Catche"
        static let folderPromocode = "Promocode"
        static let folderExperts = "Experts"
        static let folderDocuWallet = "DocuWallet"
    }
    
    struct AccountMng {
        static let MethodLogin = "Login"
        static let MethodRegister = "Register"
        static let MethodRegisterInCompany = "UrlRegisterInCompany"
        static let MethodRegisterGCMKey = "RegisterGCMKey"
        static let MethodRegisterFitnessCheck = "RegisterFitnessCheck"
        static let MethodForgotPassword = "ForgotPassword"
        static let MethodForgotPasswordNewSet = "ForgotPasswordNewSet"
        static let MethodVerifyCorpEmail = "VerifyCorpEmail"
        static let MethodVerifyEmail = "VerifyEmail"
        static let MethodVerifyEmailCmpt = "VerifyEmailCmpt"
        static let MethodVerifyMobile = "VerifyMobile"
        static let MethodSplashScreen = "SplashScreen"
        static let MethodUpdateProfile = "UpdateProfile"
        static let MethodSyncProfile = "SyncProfile"
        
        static let UrlLogin = "auth/APIRequestManager/login"
        static let UrlRegister = "auth/APIRequestManager/registerPatient"
        static let UrlRegisterInCompany = "auth/APIRequestManager/regInCompany"
        static let UrlVerifyEmail = "verifyEmail.php"
        static let UrlVerifyEmailCmpt = "updateEmailVerify.php"
        static let UrlSplashScreen = "onSplashScreen.php"
        //static let UrlVerifyMobile = "auth/APIRequestManager/verifyUser"
        static let UrlVerifyMobile = "auth/APIRequestManager/verifyUser"
        static let UrlVerifyCorpEmail = "auth/APIRequestManager/verifyCorporateUser"
        static let URLUpdateProfile = "auth/APIRequestManager/updatePatientInfo"
        static let URLSyncProfile = "patient/APIRequestManager/syncPatientDetails"
        static let UrlRegisterFitnessCheck = "patient/APIRequestManager/syncPatientDetails"
        static let UrlForgotPassword = "patient/APIRequestManager/syncPatientDetails"
        static let UrlForgotPasswordNewSet = "patient/APIRequestManager/syncPatientDetails"
        
    }
    
    struct General {
        static let MethodFeedback = "Feedback"
        static let UrlFeedback = "getFeedback.php"
    }

    struct BookAppointment {
        static let methodLoadSpecialList = "loadSpecialityList"
        static let URLLoadSpecialList = "doctor/APIRequestManager/listSpeciality"
        
        static let methodLoadDoctorList = "loadDoctorList"
        static let URLLoadDoctorList = "doctor/APIRequestManager/listDoctor"
        
        static let methodLoadAppointSlot = "loadAppointSlot"
        static let URLLoadAppointSlot = "doctor/APIRequestManager/listAppointmentSlot"
        
        static let methodLoadAppointList = "LoadAppointList"
        static let urlLoadAppointList = "doctor/APIRequestManager/listAppointment"
        
        static let methodAddAppoint = "addAppointment"
        static let URLAddAppoint = "doctor/APIRequestManager/addAppointment"
                
        static let methodAppointCancel = "appointCancel"
        static let URLAppointCancel = "doctor/APIRequestManager/cancelAppointment"
        
        static let methodAppointResched = "appointResched"
        static let URLAppointResched = "doctor/APIRequestManager/editAppointment"
    }
    
    struct Purchase {
        static let MethodSyncPlan = "SyncPlan"
        static let URLSyncPlan = "licence/APIRequestManager/sendPlan"
        
        static let MethodSyncOffers = "SyncOffers"
        static let UrlSyncOffers = "licence/APIRequestManager/sendPromocode"
        
        static let MethodSyncCorpList = "SyncCorpList"
        static let URLSyncCorpList = "patient/APIRequestManager/syncCorporateList"
        
        static let MethodAddPlanEnquiry = "AddPlanEnquiry"
        static let URLAddPlanEnquiry = "licence/APIRequestManager/addCorporatePlanEnquiry"
        
        static let MethodAddNonCorpPlanEnquiry = "AddNonCorpPlanEnquiry"
        static let URLAddNonCorpPlanEnquiry = "licence/APIRequestManager/addNonCorporatePlanEnquiry"
        
        static let methodLoadPatientLicList = "LoadPatientLicList"
        static let URLLoadPatientLicList = "licence/APIRequestManager/patientLicenceList"
        
        static let MethodRazorPlaceOrder = "RazorPlaceOrder"
        static let UrlRazorPlaceOrder = "payment/APIRequestManager/placeOrder"
        
        static let MethodRazorPaymentDone = "RazorPaymentDone"
        static let UrlRazorPaymentDone = "payment/APIRequestManager/paymentConfirm"
    }
    
    struct Fitness {
        static let MethodDownloadFoodLog = "DownloadFoodLog"
        static let MethodDownloadFoodContainer = "DownloadFoodContainer"
        static let UrlDownloadFoodLog = "foodDetails.php"
        static let UrlDownloadFoodContainer = "containerDetails.php"
        static let MethodDownloadActivityLog = "DownloadActivityLog"
        static let UrlDownloadActivityLog = "activityDetails.php"
        static let MethodScheduleList = "ScheduleList"
        static let UrlScheduleList = "sendSchedule.php"
        static let MethodSyncTargetVal = "SyncTargetVal"
        static let UrlSyncTargetVal = "sendTargetValue.php"
        
        static let MethodDeleteFoodRec = "DeleteFoodRec"
        static let UrlDeleteFoodRec = "deleteEatingFood.php"
        static let MethodSyncFitnessLog = "SyncFitnessLog"
        static let UrlSyncPendingFoodLog = "getFoodDetails.php"
        static let UrlSyncPendingActivityLog = "getActivityDetails.php"
        static let UrlSyncPendingPadometerLog = "getPadometerDetails.php"
        static let UrlSyncPendingSleepLog = "getSleepDetails.php"
        static let UrlSyncPendingWaterLog = "getWaterDetails.php"
        static let MethodSyncRoutingTrack = "SyncRoutingTrack"
        static let UrlSyncRoutingTrack = "getSelectedOption.php"
        static let MethodSyncMyDiary = "SyncMyDiary"
        static let UrlSyncMyDiary = "patient/APIRequestManager/syncPrescription"
        
        //<!--Reminder -->
        static let MethodSendReminder = "SendReminder"
        static let UrlSendReminder = "getReminder.php"
        static let MethodSyncReminder = "SyncReminder"
        static let UrlSyncReminder = "syncReminder.php"
        
        //<!-- Fitness Timeline -->
        static let MethodSyncTLSleep = "SyncTLSleep"
        static let UrlSyncTLSleep = "syncSleep.php"
        static let MethodSyncTLWater = "SyncTLWater"
        static let UrlSyncTLWater = "syncWater.php"
        static let MethodSyncTLPedometer = "SyncTLPedometer"
        static let UrlSyncTLPedometer = "syncPadometer.php"
        static let MethodSyncTLFood = "SyncTLFood"
        static let UrlSyncTLFood = "syncEatingFood.php"
        static let MethodSyncTimeline = "SyncTimeline"
        static let UrlSyncTimeline = "syncTimeline.php"
        static let MethodSyncSummeryTimeline = "syncSummeryTimeline"
        static let UrlSyncSummeryTimeline = "syncSummeryTimeline.php"
    }
    
    struct ExpertChat {
        static let MethodSyncExpertChat = "SyncExpertChat"
        static let MethodSyncExpertUser = "SyncExpertUser"
        static let UrlSyncExpertChat = "patient/APIRequestManager/sendChat"
        static let UrlSyncExpertUser = "sendExpertList.php"
        static let MethodUploadAttachment = "UploadAttachment"
        
        static let MethodSyncChatMessage = "SyncChatMessage"
        static let UrlSyncChatMessage = "patient/APIRequestManager/syncChat"
        static let MethodSyncChatStatus = "SyncChatStatus"
        static let UrlSyncChatStatus = "patient/APIRequestManager/chatSeenStatus"
    }
    
    struct DocumentWallet {
        static let methodDocumentWallet = "DocumentWallet"
        static let urlDocumentWallet = "getDocumentWallet.php"
        static let methodSyncDocumentWallet = "SyncDocumentWallet"
        static let urlSyncDocumentWallet = "syncDocumentWallet.php"
        static let methodDocWallMissFile = "DocWallMissFile"
        static let methodDocumentWalletDelete = "DocumentWalletDelete"
        static let urlDocumentWalletDelete = "deleteDocumentWallet.php"
    }
    
    
    struct Broadcast {
        static let Msg_Broadcast_ACTION = "Broadcast"
    }
    
}
