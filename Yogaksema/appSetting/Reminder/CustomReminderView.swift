//
//  demo.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

@IBDesignable class CustomReminderView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCounter: UILabel!
    
    @IBOutlet weak var switchRem: UISwitch!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    var arrMainList = [ArrReminder]()
    let ReminderTimeCellIdentifier = "ReminderTimeCell"
    var offscreenCells = Dictionary<String, UICollectionViewCell>()
    
    var view: UIView!
    var classRef:AnyObject!
    var currType: String = ""
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        SalkRippleEffect.SalkRippleEffect(mainView)
        SalkRippleEffect.SalkRippleEffect(btnAdd)
        
        btnAdd.setImage(UIImage().imageWithImage(UIImage(named: "ic_mode_edit_black_36dp.png")!, scaledToSize: CGSize(width: 20, height: 20)), for: .normal)
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        let nib = UINib(nibName: ReminderTimeCellIdentifier, bundle: nil)
        mainCollectionView.register(nib, forCellWithReuseIdentifier: ReminderTimeCellIdentifier)
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomReminderView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func refreshItemList() {
        mainCollectionView.reloadData()
        mainCollectionView.isHidden = false
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ReminderTimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: ReminderTimeCellIdentifier, for: indexPath) as! ReminderTimeCell
        
        cell.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        // Make sure layout subviews
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //open vehical details list
        
        //self.moveToDetailController(indexPath.row)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var cellWidth = mainCollectionView.frame.width
        
        // Use fake cell to calculate height
        let reuseIdentifier = ReminderTimeCellIdentifier
        var cell: ReminderTimeCell? = self.offscreenCells[reuseIdentifier] as? ReminderTimeCell
        if cell == nil {
            cell = Bundle.main.loadNibNamed(ReminderTimeCellIdentifier, owner: self, options: nil)![0] as? ReminderTimeCell
            self.offscreenCells[reuseIdentifier] = cell
        }
        
        cell!.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        
        if arrMainList[indexPath.row].details.isEmpty {
            cellWidth = cellWidth / 4
        } else {
            cellWidth = cellWidth / 3
        }
        
        
        cell!.bounds = CGRect(x: 0, y: 0, width: cellWidth, height: cell!.bounds.height)
        cell!.contentView.bounds = cell!.bounds
        
        // Layout subviews, this will let labels on this cell to set preferredMaxLayoutWidth
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        
        var size = cell!.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        // Still need to force the width, since width can be smalled due to break mode of labels
        size.width = cellWidth
        return size
    }
    
    @IBAction func onCheckedChanged(_ sender: UISwitch) {
        (classRef as! ReminderVC).onSwitchChange(isCheck: sender.isOn, currType: currType, arrMainList: arrMainList)
    }
    
    @IBAction func onEditReminder(_ sender: UIButton) {
        (classRef as! ReminderVC).onEditReminder(currType: currType, title: self.lblTitle.text!)
    }
}
