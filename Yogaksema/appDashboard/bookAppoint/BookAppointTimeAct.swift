//
//  BookAppointTimeAct.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async
import DatePickerDialog
import DropDown

private class ArrTime {
    var fromTime: String = "", toTime = "";
    var fromTimeLong: Double = 0, toTimeLong: Double = 0
}

class BookAppointTimeAct: UIViewController {

    @IBOutlet weak var txtDocName: UILabel!
    @IBOutlet weak var txtSpeciality: UILabel!
    
    @IBOutlet weak var txtSelectDate: UIButton!
    
    @IBOutlet weak var linMainPanel: UIScrollView!
    @IBOutlet weak var txtNotAvail: UILabel!
    
    @IBOutlet weak var linMorMainPanel: UIView!
    @IBOutlet weak var linANMainPanel: CustomAppointTimePanel!
    @IBOutlet weak var linEveMainPanel: CustomAppointTimePanel!
    @IBOutlet weak var linNightMainPanel: CustomAppointTimePanel!
    
    @IBOutlet weak var MorheaderView: UIView!
    @IBOutlet weak var MorfooterView: UIView!
    @IBOutlet weak var MorlblDay: UILabel!
    @IBOutlet weak var MorlblTime: UILabel!
    @IBOutlet weak var MorimgDay: UIImageView!
    //@IBOutlet weak var MorbtnTime: KPDropMenu!
    @IBOutlet weak var MorbtnNext: UIButton!
    
    @IBOutlet weak var ANheaderView: UIView!
    @IBOutlet weak var ANfooterView: UIView!
    @IBOutlet weak var ANlblDay: UILabel!
    @IBOutlet weak var ANlblTime: UILabel!
    @IBOutlet weak var ANimgDay: UIImageView!
    //@IBOutlet weak var ANbtnTime: KPDropMenu!
    @IBOutlet weak var ANbtnNext: UIButton!
    
    let chooseDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDropDown,
        ]
    }()
    
    
    private let loading = CustomLoading()
    
    var bundle: NSMutableDictionary! = nil

    private var arrTimes: Array<ArrTime>! = nil
    private var listMornTimes: Array<String>! = nil, listAfterTimes: Array<String>! = nil, listEvnTimes: Array<String>! = nil, listNightTimes: Array<String>! = nil
    
    private var sp_id: String = "", doc_id = "", speciality = "", name = "", aptID = "",
    datePre = "", fromTimePre = "", toTimePre = "";
    private var dateLong: Double = 0;
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        sp_id = JsonParser.getJsonValueString(bundle, valueForKey: "sp_id")
        doc_id = JsonParser.getJsonValueString(bundle, valueForKey: "doc_id")
        name = JsonParser.getJsonValueString(bundle, valueForKey: "name")
        speciality = JsonParser.getJsonValueString(bundle, valueForKey: "speciality")
        
        //for reschedule appointment
        aptID = JsonParser.getJsonValueString(bundle, valueForKey: "aptID")
        datePre = JsonParser.getJsonValueString(bundle, valueForKey: "date")
        fromTimePre = JsonParser.getJsonValueString(bundle, valueForKey: "fromTime")
        toTimePre = JsonParser.getJsonValueString(bundle, valueForKey: "toTime")
        
        prepareNavigationItem()
        init1()
        
        linMainPanel.isHidden = true
        linMorMainPanel.isHidden = true
        
        setupDefaultDropDown()
//        dropDowns.forEach { $0.dismissMode = .onTap }
//        dropDowns.forEach { $0.direction = .any }
    }
    
   
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Select time".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    private func init1() {
        txtDocName.text = name
        txtSpeciality.text = speciality
        
        //call default of current date
        let df = DateFormatter()
        
        if aptID.isEmpty {
            dateLong = ConvertionClass().currentTime()
        } else {
            df.dateFormat = "dd MMM, yyyy"
            guard let date = df.date(from: datePre) else {
                return
            }
            dateLong = ConvertionClass().conDateToLong(date)
        }
        df.dateFormat = "dd MMM, yyyy"
        let dateStr = ConvertionClass().conLongToDate(dateLong, dateFormat: df)
        
        configureSelectButton(txtSelectDate, setString: dateStr)
        
        getRecordServer();
    }
    
    @IBAction func txtSelectDateClick(_ sender: UIButton) {
        var currLong = ConvertionClass().currentTime()
        
        if dateLong != 0 {
            currLong = dateLong
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        
        let date = ConvertionClass().conLongToDate(currLong, dateFormat: formatter)
        
        DatePickerDialog().show(title: date, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd MMM, yyyy"
                
                self.dateLong = ConvertionClass().conDateToLong(dt)
                self.configureSelectButton(self.txtSelectDate, setString: formatter.string(from: dt))
                
                self.getRecordServer()
            }
        }
    }
    
    private func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        //        label.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(CustomColor.textBlackColor(), for: .normal)
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loading.loading())
            
            DashboardRequestManager().getRequest(Constant.BookAppointment.methodLoadAppointSlot, dict: createJsonAppointSlot(), classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    // create JSON values to send it to server
    func createJsonAppointSlot() -> NSMutableDictionary {
        
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = ConvertionClass().conLongToDate(dateLong, dateFormat: df)
        
        data = ["osType":"iOS",
                "patientID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "companyID": appUserInfo.getCompanyID(),
                "specialityID": sp_id,
                "doctorID": doc_id,
                "date": date,
                "appointTimeFrom": "00:00:00",
                "appointTimeTo": "23:59:59"
        ]
        
        Print.printLog("Sending Json==\(data)")
        
        return data
    }
    
    public func successResponse(_ respJson: Data) throws {
        loading.removeFromSuperview()
        
        arrTimes = [ArrTime]()
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: respJson, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            if Int(JsonParser.getJsonValueString(dict, valueForKey: "code")) == 200 {
                let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                let dataArray = JsonParser.getJsonValueMutableArray(dataJson, valueForKey: "str")
                
                for i in 0 ..< dataArray.count {
                    guard let subJson = dataArray.object(at: i) as? NSMutableDictionary else {
                        continue
                    }
                    
                    let temp = ArrTime();
                    
                    temp.fromTime = JsonParser.getJsonValueString(subJson, valueForKey: "from")
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd"
                    let fromDate = ConvertionClass().conLongToDate(dateLong, dateFormat: df) + " " + temp.fromTime
                    df.dateFormat = "yyyy-MM-dd hh:mm a"
                    if df.date(from: fromDate) != nil {
                        temp.fromTimeLong = ConvertionClass().conDateToLong(df.date(from: fromDate)!)
                    }

            
                    temp.toTime = JsonParser.getJsonValueString(subJson, valueForKey: "to")
                    df.dateFormat = "yyyy-MM-dd"
                    let toDate = ConvertionClass().conLongToDate(dateLong, dateFormat: df) + " " + temp.toTime
                    df.dateFormat = "yyyy-MM-dd hh:mm a"
                    if df.date(from: toDate) != nil {
                        temp.toTimeLong = ConvertionClass().conDateToLong(df.date(from: toDate)!)
                    }

                    arrTimes.append(temp);
                }
            }
        } catch _ { }
        
        if arrTimes.count > 0 {
            linMainPanel.isHidden = false
            txtNotAvail.isHidden = true
            designLayout()
        }else{
            linMainPanel.isHidden = true
            txtNotAvail.isHidden = false
        }
    }
    
    //Method for design layout
    private func designLayout() {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let tempDate12 = ConvertionClass().conLongToDate(dateLong, dateFormat: df) + " " + "12:00 PM"
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        let long12 = ConvertionClass().conDateToLong(df.date(from: tempDate12)!)
        
        df.dateFormat = "yyyy-MM-dd"
        let tempDate4 = ConvertionClass().conLongToDate(dateLong, dateFormat: df) + " " + "04:00 PM"
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        let long4 = ConvertionClass().conDateToLong(df.date(from: tempDate4)!)
        
        df.dateFormat = "yyyy-MM-dd"
        let tempDate8 = ConvertionClass().conLongToDate(dateLong, dateFormat: df) + " " + "08:00 PM"
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        let long8 = ConvertionClass().conDateToLong(df.date(from: tempDate8)!)
        
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        let tempcurrTime = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df)
        let currTime = ConvertionClass().conDateToLong(df.date(from: tempcurrTime)!)

        listAfterTimes = [String]()
        listEvnTimes = [String]()
        listMornTimes = [String]()
        listNightTimes = [String]()
    
        for i in 0 ..< arrTimes.count {
            if arrTimes[i].fromTimeLong < currTime {
                continue
            }
            if arrTimes[i].fromTimeLong < long12 {
                listMornTimes.append(arrTimes[i].fromTime + "  -  " + arrTimes[i].toTime)
                
            } else if arrTimes[i].fromTimeLong >= long12 && arrTimes[i].fromTimeLong < long4 {
                listAfterTimes.append(arrTimes[i].fromTime + "  -  " + arrTimes[i].toTime)
                
            } else if arrTimes[i].fromTimeLong >= long4 && arrTimes[i].fromTimeLong < long8 {
                listEvnTimes.append(arrTimes[i].fromTime + "  -  " + arrTimes[i].toTime)
            
            } else if arrTimes[i].fromTimeLong >= long8 {
                listEvnTimes.append(arrTimes[i].fromTime + "  -  " + arrTimes[i].toTime)
                
            }
        }
        
        if(listMornTimes.count > 0) {
            linMorMainPanel.isHidden = false
            MorlblDay.text = "Morning"
            MorlblTime.text = "before 12 pm"
        }
        if(listAfterTimes.count > 0) {
            linANMainPanel.isHidden = false
            linANMainPanel.lblDay.text = "Afternoon"
            linANMainPanel.lblTime.text = "12 - 4 pm"
            
        }
        if(listEvnTimes.count > 0) {
            linEveMainPanel.isHidden = false
            linEveMainPanel.lblDay.text = "Evening"
            linEveMainPanel.lblTime.text = "4 - 8 pm"
        }
        if(listNightTimes.count > 0) {
            linNightMainPanel.isHidden = false
            linNightMainPanel.lblDay.text = "Night"
            linNightMainPanel.lblTime.text = "after 8 pm"
            linNightMainPanel.btnTime.target(forAction: #selector(nightPopup), withSender: self)
        }
    
        if(listMornTimes.count > 0) {
            showDurationTimeSlot("Morn");
        } else if(listAfterTimes.count > 0) {
            showDurationTimeSlot("AN");
        } else if(listEvnTimes.count > 0) {
            showDurationTimeSlot("Eve");
        } else if(listNightTimes.count > 0) {
            showDurationTimeSlot("Night");
        }
//        durationPopup(MorbtnTime, mListOption: listMornTimes)
        durationPopup(linANMainPanel.btnTime, mListOption: listAfterTimes)
        durationPopup(linEveMainPanel.btnTime, mListOption: listEvnTimes)
        durationPopup(linNightMainPanel.btnTime, mListOption: listNightTimes)
    }
    
    private func showDurationTimeSlot(_ clickType: String) {
        MorfooterView.isHidden = true
        linANMainPanel.footerView.isHidden = true
        linEveMainPanel.footerView.isHidden = true
        linNightMainPanel.footerView.isHidden = true
    
        if(clickType.equalsIgnoreCase("Morn")) {
            MorfooterView.isHidden = false
        } else if(clickType.equalsIgnoreCase("AN")) {
            linANMainPanel.footerView.isHidden = false
        } else if(clickType.equalsIgnoreCase("Eve")) {
            linEveMainPanel.footerView.isHidden = false
        } else if(clickType.equalsIgnoreCase("Night")) {
            linNightMainPanel.footerView.isHidden = false
        }
    }
    
    @objc func nightPopup() {
        chooseDropDown.show()
    }
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    private func durationPopup(_ button: UIButton, mListOption: [String]) {
        chooseDropDown.anchorView = button
        chooseDropDown.bottomOffset = CGPoint(x: 0, y: linNightMainPanel.btnTime.bounds.height)
        chooseDropDown.dataSource = mListOption
        chooseDropDown.selectionAction = { [weak self] (index, item) in
            button.setTitle(item, for: .normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
