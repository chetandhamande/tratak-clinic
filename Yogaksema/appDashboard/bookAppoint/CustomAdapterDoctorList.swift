//
//  CustomAdapterDoctorList.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomAdapterDoctorList: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var textHead: UILabel!
    @IBOutlet weak var txtDetails: UILabel!
    @IBOutlet weak var btnBook: UIButton!
    
    private var mValues: Array<ArrDoctor>! = nil
    private var classRef: AnyObject! = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func onBindViewHolder(_ arr: [ArrDoctor], position: Int, classRef: AnyObject) {
        
        self.classRef = classRef
        btnBook.tag = position
        mValues = arr
        let user: ArrDoctor = mValues[position]
        
        if user.listLayoutIndex == 0 {
            //Add view to each list
            
            textHead.text = user.fname + " " + user.lname
            
            if user.speciality.isEmpty {
                txtDetails.isHidden = true
            } else {
                txtDetails.isHidden = false
                txtDetails.text = user.speciality
            }
            
        }
    }
    
    @IBAction func bookBtnClick(sender: UIButton) {
        
        let temp = mValues[sender.tag]
        
        let intent = NSMutableDictionary()
        intent.setValue(temp.speciality_id, forKey: "sp_id")
        intent.setValue(temp.doctorID, forKey: "doc_id")
        intent.setValue(temp.speciality, forKey: "speciality")
        intent.setValue(temp.fname + " " + temp.lname, forKey: "name")
        
        if classRef.isKind(of: DoctorsListAct.self) {
            (classRef as! DoctorsListAct).moveToBookAppointTimeAct(intent)
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

