//
//  DashboardJsonCreator.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class DashboardJsonCreator {
    
    var err: AutoreleasingUnsafeMutablePointer<NSError?>? = nil

    func storeDefaultJson(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
        appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
       
    }
    
    // create JSON values to send it to server
    func putJsonDefualt() throws -> Data {
        
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        data = ["osType":"iOS",
                "by":"PATIENT",
                "patientID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "companyID": appUserInfo.getCompanyID()
        ]
        
        Print.printLog("Sending Json==\(data)")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeSyncPlanJson(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
        appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
        
        if JsonParser.getJsonValueInt(dict, valueForKey: "code") == 200 {
            
            let dataArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
            
            if dataArray.count > 0 {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                datasource.TruncatePurchasePlanList()
                
                for i in 0 ..< dataArray.count {
                    guard let subJson = dataArray[i] as? NSDictionary else {
                        continue
                    }
                    let planID = JsonParser.getJsonValueString(subJson, valueForKey: "planID")
                    let planName = JsonParser.getJsonValueString(subJson, valueForKey: "planName")
                    let perMonth = JsonParser.getJsonValueString(subJson, valueForKey: "perMonth")
                    let details = JsonParser.getJsonValueString(subJson, valueForKey: "details")
                    let features = JsonParser.getJsonValueString(subJson, valueForKey: "features")
                    let longDesc = JsonParser.getJsonValueString(subJson, valueForKey: "longDesc")
                    let imageName = JsonParser.getJsonValueString(subJson, valueForKey: "imageName")
                    
                    let durationprice = JsonParser.getJsonValueMutableArray(subJson, valueForKey: "durationprice")
                    guard let data = try? JSONSerialization.data(withJSONObject: durationprice, options: []) else {
                        continue
                    }
                    let durationStr = String(data: data, encoding: String.Encoding.utf8)
                    
                    datasource.InsertPurchasePlanList(id: planID, name: planName, price: perMonth, comboPlan: durationStr!, comboPrice: "", details: details, feature: features, longDesc: longDesc, imageName: imageName)
                }
                
                datasource.closeDatabase()
                
                appPrefs.setLastPlanLogListSycnTime(text: ConvertionClass().currentTime())
            }
        }
    }
    
    func storeJsonLoadLicence(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
        appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
        
        if JsonParser.getJsonValueInt(dict, valueForKey: "code") == 200 {
            
            let dataArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
            
            if dataArray.count > 0 {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                datasource.TruncateMySubscriptionList()
                
                for i in 0 ..< dataArray.count {
                    guard let subJson = dataArray[i] as? NSDictionary else {
                        continue
                    }
                    let planName = JsonParser.getJsonValueString(subJson, valueForKey: "planName")
                    let duration = JsonParser.getJsonValueString(subJson, valueForKey: "duration")
                    let key = JsonParser.getJsonValueString(subJson, valueForKey: "key")
                    let startDate = JsonParser.getJsonValueString(subJson, valueForKey: "startDate")
                    let endDate = JsonParser.getJsonValueString(subJson, valueForKey: "endDate")
                    let measure = JsonParser.getJsonValueString(subJson, valueForKey: "measure")
                    let features = JsonParser.getJsonValueString(subJson, valueForKey: "features")
                    let companyID = JsonParser.getJsonValueString(subJson, valueForKey: "companyID")
                    let companyName = JsonParser.getJsonValueString(subJson, valueForKey: "companyName")
                    let licStatus = "1"
                    
                    datasource.InsertMySubscriptionTable(planName: planName, planDura: duration, key: key, fromDate: startDate, toDate: endDate, measure: measure, features: features, cmpID: companyID, cmpName: companyName, status: licStatus)
                    
                    if(companyID == appPrefs.getCompanyID()) {
                        appPrefs.setIsPremiumPlanActive(text: true)
                    }
                }
                
                datasource.closeDatabase()
            }
        }
    }
    
}
