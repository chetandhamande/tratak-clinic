//
//  ArrTimeline.swift
//  Salk
//
//  Created by Chetan  on 07/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class ArrTimeline {
        
    var points:TimelinePoint? = TimelinePoint()
    var color:UIColor = UIColor.black, headColor:UIColor = PrimaryColor.colorPrimary()
    var title:String = "", lineInfo: String = "", thumbnail: String = "", values: String = "0", units: String = ""
    var dbTimeID:Double = 0.0
    var dbTimeIDStr:String = ""
}
