//
//  FitnessBandConnectVC.swift
//  Salk
//
//  Created by Chetan  on 13/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material

class FitnessBandConnectVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    fileprivate var arrMainList = [ArrFitnessBand]()
    let CustomAdaFitnessBandIdentifier = "CustomAdaFitnessBandCell"
    var offscreenCells = Dictionary<String, UICollectionViewCell>()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        let nib = UINib(nibName: CustomAdaFitnessBandIdentifier, bundle: nil)
        mainCollectionView.register(nib, forCellWithReuseIdentifier: CustomAdaFitnessBandIdentifier)
        
        self.prepareNavigationItem()
        self.loadBandList()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Apps and Devices"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CustomAdaFitnessBandCell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomAdaFitnessBandIdentifier, for: indexPath) as! CustomAdaFitnessBandCell
        
        cell.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        // Make sure layout subviews
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //open vehical details list
        
        self.moveToDetailController(indexPath.row)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let orientation = UIApplication.shared.statusBarOrientation
        var cellWidth = mainCollectionView.frame.width
        
        // Use fake cell to calculate height
        let reuseIdentifier = CustomAdaFitnessBandIdentifier
        var cell: CustomAdaFitnessBandCell? = self.offscreenCells[reuseIdentifier] as? CustomAdaFitnessBandCell
        if cell == nil {
            cell = Bundle.main.loadNibNamed(CustomAdaFitnessBandIdentifier, owner: self, options: nil)![0] as? CustomAdaFitnessBandCell
            self.offscreenCells[reuseIdentifier] = cell
        }
        
        cell!.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        if traitCollection.horizontalSizeClass == .regular || traitCollection.verticalSizeClass == .regular
        {
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                if(orientation == .portrait) {
                    cellWidth = (cellWidth - 1) / 2
                } else {
                    cellWidth = (cellWidth - 5) / 3
                }
            } else {
                if(orientation == .portrait) {
                } else {
                    cellWidth = (cellWidth - 1) / 2
                }
            }
        } else {
            if(orientation == .portrait) {
            } else {
                cellWidth = (cellWidth - 1) / 2
            }
        }
        
        cell!.bounds = CGRect(x: 0, y: 0, width: cellWidth, height: cell!.bounds.height)
        cell!.contentView.bounds = cell!.bounds
        
        // Layout subviews, this will let labels on this cell to set preferredMaxLayoutWidth
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        
        var size = cell!.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        // Still need to force the width, since width can be smalled due to break mode of labels
        size.width = cellWidth
        return size
    }
    
    func moveToDetailController(_ index: Int) {
        let user = arrMainList[index]
        
        if user.bandName == "Health" {
            let a = AppleFitConnectVC(nibName: "AppleFitConnectVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
            
        } else if user.bandName == "Fitbit" {
            //let a = AppleFitConnectVC(nibName: "AppleFitConnectVC", bundle: nil)
            //self.navigationController?.pushViewController(a, animated: true)
        }
    }
}

extension FitnessBandConnectVC {
    fileprivate func loadBandList() {
        
        arrMainList = [ArrFitnessBand]()
        
        var temp = ArrFitnessBand()
        temp.bandName = "Health"
        temp.bandImage = "apple_fit.png"
        arrMainList.append(temp)
        
//        temp = ArrFitnessBand()
//        temp.bandName = "Fitbit"
//        temp.bandImage = "fitbit_icon.png"
//        arrMainList.append(temp)
//        
//        temp = ArrFitnessBand()
//        temp.bandName = "SHealth"
//        temp.bandImage = "shealth_logo.png"
//        arrMainList.append(temp)
        
        mainCollectionView.reloadData()
        mainCollectionView.isHidden = false
    }
}
