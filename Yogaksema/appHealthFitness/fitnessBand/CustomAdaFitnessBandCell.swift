//
//  CustomAdaFitnessBandCell.swift
//  Salk
//
//  Created by Chetan  on 13/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomAdaFitnessBandCell: UICollectionViewCell {

    @IBOutlet weak var backgroundCellView: UIView!
    
    @IBOutlet weak var lblBandName: UILabel!
    @IBOutlet weak var imgBandImage: UIImageView!
    @IBOutlet weak var imgIsSelected: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        SalkRippleEffect.SalkRippleEffect(backgroundCellView)        
    }

    func onBindViewHolder(_ featureList: [ArrFitnessBand], position: Int, classRef: AnyObject)
    {
        let user: ArrFitnessBand = featureList[position]
        
        self.lblBandName.text = user.bandName
        
        if user.isSelected {
            self.imgIsSelected.isHidden = false
        } else {
            self.imgIsSelected.isHidden = true
        }
        
        self.imgBandImage.image = UIImage(named: user.bandImage)
    }

}
