class FitnessRequestManager {
    
    func getRequest(_ methodName: String, classRef: AnyObject!) {
        getRequest(methodName, dict: nil, classRef: classRef)
    }
    
    func getRequest(_ mMethod: String, dict: NSMutableDictionary?, classRef: AnyObject!) {
        var serverIP = SalkProjectConstant().Serveradd
        var url = ""
        
        if (mMethod == Constant.Fitness.MethodDownloadFoodLog) {
            url = Constant.Fitness.UrlDownloadFoodLog
        } else if (mMethod == Constant.Fitness.MethodDownloadFoodContainer) {
            url = Constant.Fitness.UrlDownloadFoodContainer
        } else if (mMethod == Constant.Fitness.MethodDownloadActivityLog) {
            url = Constant.Fitness.UrlDownloadActivityLog
        } else if (mMethod == Constant.Fitness.MethodScheduleList) {
            url = Constant.Fitness.UrlScheduleList
        } else if (mMethod == Constant.Fitness.MethodSyncTargetVal) {
            url = Constant.Fitness.UrlSyncTargetVal
        } else if (mMethod == Constant.Fitness.MethodSyncFitnessLog) {
            let appPrefs = MySharedPreferences()
            if(appPrefs.getLastLogDataSycnType() == "food") {
                url = Constant.Fitness.UrlSyncPendingFoodLog
            } else if(appPrefs.getLastLogDataSycnType() == "activity") {
                url = Constant.Fitness.UrlSyncPendingActivityLog
            } else if(appPrefs.getLastLogDataSycnType() == "water") {
                url = Constant.Fitness.UrlSyncPendingWaterLog
            } else if(appPrefs.getLastLogDataSycnType() == "pedometer") {
                url = Constant.Fitness.UrlSyncPendingPadometerLog
            } else if(appPrefs.getLastLogDataSycnType() == "sleep") {
                url = Constant.Fitness.UrlSyncPendingSleepLog
            }
        } else if (mMethod == Constant.ExpertChat.MethodSyncExpertChat) {
            serverIP = SalkProjectConstant().ServeraddCI
            url = Constant.ExpertChat.UrlSyncExpertChat
        } else if (mMethod == Constant.ExpertChat.MethodSyncExpertUser) {
            url = Constant.ExpertChat.UrlSyncExpertUser
        } else if (mMethod == Constant.Fitness.MethodSyncRoutingTrack) {
            url = Constant.Fitness.UrlSyncRoutingTrack
        } else if (mMethod == Constant.Fitness.MethodSyncMyDiary) {
            serverIP = SalkProjectConstant().ServeraddCI
            url = Constant.Fitness.UrlSyncMyDiary
        } else if (mMethod == Constant.ExpertChat.MethodSyncChatMessage) {
            serverIP = SalkProjectConstant().ServeraddCI
            url = Constant.ExpertChat.UrlSyncChatMessage
        } else if (mMethod == Constant.Fitness.MethodSyncTLSleep) {
            url = Constant.Fitness.UrlSyncTLSleep
        } else if (mMethod == Constant.Fitness.MethodSyncTLWater) {
            url = Constant.Fitness.UrlSyncTLWater
        } else if (mMethod == Constant.Fitness.MethodSyncTLPedometer) {
            url = Constant.Fitness.UrlSyncTLPedometer
        } else if (mMethod == Constant.Fitness.MethodSyncTLFood) {
            url = Constant.Fitness.UrlSyncTLFood
        } else if (mMethod == Constant.Fitness.MethodDeleteFoodRec) {
            url = Constant.Fitness.UrlDeleteFoodRec
        } else if (mMethod == Constant.ExpertChat.MethodSyncChatStatus) {
            serverIP = SalkProjectConstant().ServeraddCI
            url = Constant.ExpertChat.UrlSyncChatStatus
        } else if (mMethod == Constant.Fitness.MethodSendReminder) {
            url = Constant.Fitness.UrlSendReminder
        } else if (mMethod == Constant.Fitness.MethodSyncReminder) {
            url = Constant.Fitness.UrlSyncReminder
        } else if (mMethod == Constant.Fitness.MethodSyncTimeline) {
            url = Constant.Fitness.UrlSyncTimeline
        } else if (mMethod == Constant.Fitness.MethodSyncSummeryTimeline) {
            url = Constant.Fitness.UrlSyncSummeryTimeline
        } else if (mMethod == Constant.DocumentWallet.methodDocumentWallet) {
            url = Constant.DocumentWallet.urlDocumentWallet
        } else if (mMethod == Constant.DocumentWallet.methodSyncDocumentWallet) {
            url = Constant.DocumentWallet.urlSyncDocumentWallet
        } else if (mMethod == Constant.DocumentWallet.methodDocumentWalletDelete) {
            url = Constant.DocumentWallet.urlDocumentWalletDelete
        }
        
        serverIP += url
        
        ////Call For HTTP Connection
        let httpObj = FitnessHTTPConnection()
        httpObj.sendServerRequest(serverIP, mMethod: mMethod, classRef:classRef, dict: dict)
    }
    
}
