//
//  Print.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class Print {
    static func printLog(_ string: String) {
        if SalkProjectConstant().isDebugMode {
            print(string)
        }
    }
}
