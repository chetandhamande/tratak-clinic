//
//  CardView.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

@IBDesignable

//class SalkCardView: UIView {
//
//    @IBInspectable var corRadius: CGFloat = 5
//
//    @IBInspectable var shadowOffsetWidth: Int = 5
//    @IBInspectable var shadowOffsetHeight: Int = 2
//    @IBInspectable var shadoColor: UIColor? = UIColor.black
//    @IBInspectable var shadoOpacity: Float = 0.5
//
//    override func layoutSubviews() {
//
//        self.backgroundColor = UIColor(colorLiteralRed: 228.0/255.0, green: 228.0/255.0, blue: 228.0/255.0, alpha: 0.5)
//
//        layer.cornerRadius = corRadius
//        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: corRadius)
//
//        layer.borderColor = UIColor.lightGray.cgColor
//        layer.borderWidth = 0.2
//        layer.masksToBounds = true
//        layer.shadowColor = shadoColor?.cgColor
//        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
//        layer.shadowOpacity = shadoOpacity
//        layer.shadowPath = shadowPath.cgPath
//        layer.shadowRadius = corRadius
//    }
//}

class SalkCardView: UIView {
    /// The corner radius of the `ShadowView`, inspectable in Interface Builder
    @IBInspectable var corRadius: CGFloat = 5.0 {
        didSet {
            self.updateProperties()
        }
    }
    /// The shadow color of the `ShadowView`, inspectable in Interface Builder
    @IBInspectable var shadoColor: UIColor = UIColor.black {
        didSet {
            self.updateProperties()
        }
    }
    /// The shadow offset of the `ShadowView`, inspectable in Interface Builder
    @IBInspectable var shadoOffset: CGSize = CGSize(width: 0.0, height: 2) {
        didSet {
            self.updateProperties()
        }
    }
    /// The shadow radius of the `ShadowView`, inspectable in Interface Builder
    @IBInspectable var shadoRadius: CGFloat = 4.0 {
        didSet {
            self.updateProperties()
        }
    }
    /// The shadow opacity of the `ShadowView`, inspectable in Interface Builder
    @IBInspectable var shadoOpacity: Float = 0.5 {
        didSet {
            self.updateProperties()
        }
    }
    
    /**
     Masks the layer to it's bounds and updates the layer properties and shadow path.
     */
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.masksToBounds = false
        
        self.updateProperties()
        self.updateShadowPath()
    }
    
    /**
     Updates all layer properties according to the public properties of the `ShadowView`.
     */
    fileprivate func updateProperties() {
        self.layer.cornerRadius = self.corRadius
        self.layer.shadowColor = self.shadoColor.cgColor
        self.layer.shadowOffset = self.shadoOffset
        self.layer.shadowRadius = self.shadoRadius
        self.layer.shadowOpacity = self.shadoOpacity
    }
    
    /**
     Updates the bezier path of the shadow to be the same as the layer's bounds, taking the layer's corner radius into account.
     */
    fileprivate func updateShadowPath() {
        self.layer.shadowPath = UIBezierPath(roundedRect: layer.bounds, cornerRadius: layer.cornerRadius).cgPath
    }
    
    /**
     Updates the shadow path everytime the views frame changes.
     */
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.updateShadowPath()
    }
}
