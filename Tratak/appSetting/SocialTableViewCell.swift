//
//  SocialTableViewCell.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class SocialTableViewCell: UITableViewCell {
    
    @IBOutlet weak var followTitleLabel: UILabel!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var youtubeButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
        
        let mysharedObj = MySharedPreferences()
        
        followTitleLabel.text = "followUS".localized
        followTitleLabel.textColor = CustomColor.textBlackColor()
        followTitleLabel.font = UIFont.systemFont(ofSize: 14)
        
        versionLabel.text = "app_name".applocalized + " " + mysharedObj.getAppCurrentVersionCodeLong()
        versionLabel.textColor = CustomColor.textBlackColor()
        versionLabel.font = UIFont.systemFont(ofSize: 13)
        
        if "link_facebook".applocalized == "NA" {
            facebookButton.isHidden = true
        } else {
            self.socialButton(facebookButton, imageName: "facebook.png", colorName: CustomColor.facebookColor())
        }
        
        if "link_twitter".applocalized == "NA" {
            twitterButton.isHidden = true
        } else {
            self.socialButton(twitterButton, imageName: "twitter.png", colorName: CustomColor.twitterColor())
        }
        
        if "link_googlePlus".applocalized == "NA" {
            googleButton.isHidden = true
        } else {
            self.socialButton(googleButton, imageName: "googleplus.png", colorName: CustomColor.googleColor())
        }
        
        if "link_youtube".applocalized == "NA" {
            youtubeButton.isHidden = true
        } else {
            self.socialButton(youtubeButton, imageName: "you.png", colorName: CustomColor.youtubeColor())
        }
    }
    
    func socialButton(_ getButton: UIButton, imageName: String, colorName: UIColor) -> UIButton {
        
        let refreshImage: UIImage = UIImage().imageWithImage(UIImage(named: imageName)!, scaledToSize: CGSize(width: 35, height: 35))
        
        getButton.setImage(refreshImage, for: UIControlState())
        getButton.backgroundColor = colorName
        getButton.setTitle("", for: UIControlState())
        getButton.layer.cornerRadius = 25
        getButton.layer.masksToBounds = true
        
        return getButton
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func facebookButtonClick(sender: UIButton) {
        let url = URL(string: "link_facebook".applocalized)
        UIApplication.shared.openURL(url!)
    }
    
    @IBAction func twitterButtonClick(sender: UIButton) {
        let url = URL(string: "link_twitter".applocalized)
        UIApplication.shared.openURL(url!)
    }
    
    @IBAction func googleButtonClick(sender: UIButton) {
        let url = URL(string: "link_googlePlus".applocalized)
        UIApplication.shared.openURL(url!)
    }
    
    @IBAction func youtubeButtonClick(sender: UIButton) {
        let url = URL(string: "link_youtube".applocalized)
        UIApplication.shared.openURL(url!)
    }
}
