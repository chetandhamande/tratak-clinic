//
//  FeedbackViewController.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class FeedbackViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var feedbackTextView : UITextView!
    @IBOutlet weak var feedbackHintLabel : UILabel!
    @IBOutlet weak var feedbackButton : UIButton!
    
    var feedbackText: String?
    var alertObj = SweetAlert()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        feedbackTextView.delegate = self
        
        self.viewConfiguration()
        
        self.prepareNavigationItem()
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func viewConfiguration() {
        feedbackTextView.layer.cornerRadius = 3
        feedbackTextView.layer.borderWidth = 1
        feedbackTextView.layer.borderColor = PrimaryColor.colorPrimary().cgColor
        feedbackTextView.text = "feedbackHint".localized
        feedbackTextView.textColor = CustomColor.textGrayColor()
        feedbackTextView.font = UIFont.systemFont(ofSize: 16)
        
        feedbackHintLabel.text = "feedbackNote".localized
        feedbackHintLabel.textColor = CustomColor.textBlackColor()
        feedbackHintLabel.font = UIFont.systemFont(ofSize: 15)
        
        CustomButton().createButton("feedbackSend".localized, buttonID: feedbackButton)
    }
    
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "helpMenuFeedback".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
    }
    
    func performBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        feedbackTextView.text = ""
        feedbackTextView.textColor = CustomColor.textBlackColor()
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if feedbackTextView.text.lengthOfBytes(using: String.Encoding.utf8) == 0 {
            feedbackTextView.textColor = CustomColor.textGrayColor()
            feedbackTextView.text = "feedbackHint".localized
            feedbackTextView.resignFirstResponder()
        } else {
            feedbackText=feedbackTextView.text
        }
    }
    
    @IBAction func feedbackButtonClick(_ sender: UIButton) {
        
        self.dismissKeyboard()
        
        let messageText = feedbackTextView.text.trim()
        
        if messageText.length() <= 0 {
            alertObj.showAlert("Oops...", subTitle: "missFeedback".localized, style: AlertStyle.warning)
        } else {
            //set values in pref and send request to server
            let appPrefs = MySharedPreferences()
            appPrefs.setUserFeedbackMsg(text: messageText)
            
            //check for internet and serverlink connectin
            let mCheckInternet = CheckInternetConnection().isCheckInternetConnection()
            if mCheckInternet {
                //here send request to server
                UserRegistationRequestManager().getRequest(Constant.General.MethodFeedback, classRef: self)
            } else {
                //show message for error of internet not conn
                
                alertObj.showAlert("Oops...", subTitle: "noInternet".localized, style: AlertStyle.warning)
            }
        }
    }
    
    func testSuccess() {
        SweetAlert().showAlert("Success", subTitle: "Thank you for your feedback", style: .success, buttonTitle:"OK", buttonColor: PrimaryColor.colorPrimary()) { (isOtherButton) -> Void in
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func testFaliuer() {
        alertObj.showAlert("Oops...", subTitle: "ServerNotWork".localized, style: AlertStyle.warning)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
