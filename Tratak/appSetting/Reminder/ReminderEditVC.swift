//
//  ReminderEditVC.swift
//  Salk
//
//  Created by Chetan  on 23/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import DatePickerDialog

class ReminderEditVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var type = "", titleName = ""
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    var arrMainList = [ArrReminder]()
    let ReminderTimeEditCellIdentifier = "ReminderTimeEditCell"
    var offscreenCells = Dictionary<String, UICollectionViewCell>()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareNavigationItem()
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        let nib = UINib(nibName: ReminderTimeEditCellIdentifier, bundle: nil)
        mainCollectionView.register(nib, forCellWithReuseIdentifier: ReminderTimeEditCellIdentifier)
        
        self.loadAllRecord()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = titleName + " Reminder"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        if (type == "food" || type == "fitness") {
            //dont show add option
        } else {
            let submitImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_add_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
            let submitButton = UIBarButtonItem(image: submitImage, style: .plain, target: self, action: #selector(self.performSubmit))
            
            navigationItem.rightBarButtonItems = [submitButton]
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ReminderTimeEditCell = collectionView.dequeueReusableCell(withReuseIdentifier: ReminderTimeEditCellIdentifier, for: indexPath) as! ReminderTimeEditCell
        
        cell.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        // Make sure layout subviews
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //open vehical details list
        
        //self.moveToDetailController(indexPath.row)
        
        if arrMainList[indexPath.row].details.isEmpty {
            onAddModifyTime(index: indexPath.row, timeVal: arrMainList[indexPath.row].title)
        } else {
            onAddModifyTime(index: indexPath.row, timeVal: arrMainList[indexPath.row].details)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var cellWidth = mainCollectionView.frame.width
        
        // Use fake cell to calculate height
        let reuseIdentifier = ReminderTimeEditCellIdentifier
        var cell: ReminderTimeEditCell? = self.offscreenCells[reuseIdentifier] as? ReminderTimeEditCell
        if cell == nil {
            cell = Bundle.main.loadNibNamed(ReminderTimeEditCellIdentifier, owner: self, options: nil)![0] as? ReminderTimeEditCell
            self.offscreenCells[reuseIdentifier] = cell
        }
        
        cell!.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        
        
        cellWidth = cellWidth - 8
        
        
        cell!.bounds = CGRect(x: 0, y: 0, width: cellWidth, height: cell!.bounds.height)
        cell!.contentView.bounds = cell!.bounds
        
        // Layout subviews, this will let labels on this cell to set preferredMaxLayoutWidth
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        
        var size = cell!.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        // Still need to force the width, since width can be smalled due to break mode of labels
        size.width = cellWidth
        return size
    }
}

extension ReminderEditVC {
    
    @objc fileprivate func performSubmit() {
        onAddModifyTime(index: 0, timeVal: "")
    }
    
    fileprivate func loadAllRecord() {
        let appPref = MySharedPreferences()
        
        if(type == "walk") {
            arrMainList = [ArrReminder]()
            let temp = appPref.getWalkRemValue().components(separatedBy: ",")
            for i in 0 ..< temp.count {
                let tempVal = ArrReminder()
                tempVal.title = temp[i]
                arrMainList.append(tempVal)
            }
        } else if(type == "sleep") {
            arrMainList = [ArrReminder]()
            let temp = appPref.getSleepRemValue().components(separatedBy: ",")
            for i in 0 ..< temp.count {
                let tempVal = ArrReminder()
                tempVal.title = temp[i]
                arrMainList.append(tempVal)
            }
        } else if(type == "food") {
            arrMainList = [ArrReminder]()
            let temp = appPref.getFoodRemValue().components(separatedBy: ",")
            let tempName = appPref.getFoodRemName().components(separatedBy: ",")
            for i in 0 ..< temp.count {
                let tempVal = ArrReminder()
                tempVal.title = tempName[i]
                tempVal.details = temp[i]
                arrMainList.append(tempVal)
            }
        } else if(type == "fitness") {
            arrMainList = [ArrReminder]()
            let temp = appPref.getFitnessRemValue().components(separatedBy: ",")
            for i in 0 ..< temp.count {
                let tempVal = ArrReminder()
                tempVal.title = i == 0 ? "Diet" : "Activity"
                tempVal.details = temp[i]
                arrMainList.append(tempVal)
            }
        } else if(type == "water") {
            arrMainList = [ArrReminder]()
            let temp = appPref.getWaterRemValue().components(separatedBy: ",")
            for i in 0 ..< temp.count {
                let tempVal = ArrReminder()
                tempVal.title = temp[i]
                arrMainList.append(tempVal)
            }
        }
        
        mainCollectionView.reloadData()
    }
    
    
    func onAddModifyTime(index: Int, timeVal: String) {
        //get current Date
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        
        var currDate = Date()
        if(index > 0 && !timeVal.isEmpty && timeVal != "--") {
            currDate = df.date(from: timeVal)!
        }
        
        DatePickerDialog().show(title: "DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: currDate,  datePickerMode: .time) {
            (date) -> Void in
            
            if date == nil { return } //skip on cancel button
            
            
            let df = DateFormatter()
            df.dateFormat = "hh:mm a"
            let selTimeSet = df.string(from: date!)
            
            
            var temp = "", temp1 = ""
            if(self.type == "food" || self.type == "fitness")
            {
                for i in 0 ..< self.arrMainList.count {
                    temp += temp.isEmpty ? self.arrMainList[i].title : "," + self.arrMainList[i].title
                    
                    if (i == index) {
                        temp1 += temp1.isEmpty ? selTimeSet : "," + selTimeSet
                    } else {
                        temp1 += temp1.isEmpty ? self.arrMainList[i].details : "," + self.arrMainList[i].details
                    }
                }
            } else {
                let tempVal = ArrReminder()
                tempVal.title = selTimeSet
                self.arrMainList.append(tempVal)
                
                for i in 0 ..< self.arrMainList.count {
                    if self.arrMainList[i].details.isEmpty {
                        temp += temp.isEmpty ? self.arrMainList[i].title : "," + self.arrMainList[i].title
                    } else {
                        temp += temp.isEmpty ? self.arrMainList[i].title : "," + self.arrMainList[i].title
                        
                        temp1 += temp1.isEmpty ? self.arrMainList[i].details : "," + self.arrMainList[i].details
                    }
                }
                
                temp = self.getSortingReminder(temp: temp)
            }
            
            
            let appPref = MySharedPreferences()
            if(self.type == "walk") {
                appPref.setWalkRemValue(text: temp)
            } else if(self.type == "sleep") {
                appPref.setSleepRemValue(text: temp)
            } else if(self.type == "food") {
                appPref.setFoodRemValue(text: temp1)
                appPref.setFoodRemName(text: temp)
            } else if(self.type == "fitness") {
                appPref.setFitnessRemValue(text: temp1)
            } else if(self.type == "water") {
                appPref.setWaterRemValue(text: temp)
            }
            self.loadAllRecord()
            
            appPref.setIsPendingReminder(text: true)
        }
    }
    
    func onDeleteTime(index: Int) {
        
        if(arrMainList.count <= 1) { return }
        
        SweetAlert().showAlert("Warning", subTitle: "Are you sure, you want to remove reminder time? ", style: AlertStyle.none, buttonTitle:"Remove", buttonColor: PrimaryColor.colorPrimary(), otherButtonTitle:  "btnCancel".localized, otherButtonColor: UIColor.colorFromRGB(0xD0D0D0) ) { (isOtherButton) -> Void in
            
            if isOtherButton == true {
                var temp = "", temp1 = ""
                if(self.type == "food" || self.type == "fitness")
                {
                    for i in 0 ..< self.arrMainList.count {
                        
                        temp += temp.isEmpty ? self.arrMainList[i].title : "," + self.arrMainList[i].title
                        
                        if (i == index) {
                            temp1 += temp1.isEmpty ? "--" : "," + "--"
                        } else {
                            temp1 += temp1.isEmpty ? self.arrMainList[i].details : "," + self.arrMainList[i].details
                        }
                    }
                } else {
                    for i in 0 ..< self.arrMainList.count {
                        if i == index { continue }
                        
                        temp += temp.isEmpty ? self.arrMainList[i].title : "," + self.arrMainList[i].title
                    }
                    
                    temp = self.getSortingReminder(temp: temp)
                }
                
                
                let appPref = MySharedPreferences()
                if(self.type == "walk") {
                    appPref.setWalkRemValue(text: temp)
                } else if(self.type == "sleep") {
                    appPref.setSleepRemValue(text: temp)
                } else if(self.type == "food") {
                    appPref.setFoodRemValue(text: temp1)
                    appPref.setFoodRemName(text: temp)
                } else if(self.type == "fitness") {
                    appPref.setFitnessRemValue(text: temp1)
                } else if(self.type == "water") {
                    appPref.setWaterRemValue(text: temp)
                }
                self.loadAllRecord()
                
                appPref.setIsPendingReminder(text: true)
            }
        }
    }
    
    fileprivate func getSortingReminder(temp: String) -> String {
        //sort by time
        var tempStr = temp.components(separatedBy: ",")
        var tempReminder = [String]()
        
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        
        for i in 0 ..< tempStr.count {
            let time:Double = ConvertionClass().conDateToLong(df.date(from: tempStr[i])!)
        
            var isAdd:Bool = false
            if(tempReminder.count == 0) {
                tempReminder.append(tempStr[i])
                isAdd = true
            }
            
            for j in 0 ..< tempReminder.count {
                let preTime:Double = ConvertionClass().conDateToLong(df.date(from: tempReminder[j])!)
                
                if (time < preTime) {
                    tempReminder.insert(tempStr[i], at: j)
                    isAdd = true
                    break
                }
            }
            
            if(!isAdd) {
                tempReminder.append(tempStr[i])
            }
            
        }
        
        var tempReturn = ""
        for i in 0 ..< tempReminder.count {
            tempReturn += tempReturn.isEmpty ? tempReminder[i] : "," + tempReminder[i]
        }
    
        return tempReturn
    }
    
}

