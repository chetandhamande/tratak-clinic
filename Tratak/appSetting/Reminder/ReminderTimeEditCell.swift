//
//  ReminderTimeEditCell.swift
//  Salk
//
//  Created by Chetan  on 23/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class ReminderTimeEditCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    var classRef:AnyObject!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func onBindViewHolder(_ featureList: [ArrReminder], position: Int, classRef: AnyObject)
    {
        self.classRef = classRef
        
        let user: ArrReminder = featureList[position]
        
        self.lblTitle.text = user.title
        self.lblDetails.text = user.details
        self.btnDelete.tag = position
    }

    @IBAction func onEditReminder(_ sender: UIButton) {
        (classRef as! ReminderEditVC).onDeleteTime(index: sender.tag)
    }
    
}
