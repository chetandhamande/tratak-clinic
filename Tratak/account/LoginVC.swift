//
//  LoginVC.swift
//  Salk
//
//  Created by Chetan  on 13/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material

class LoginVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var txtEmail: MKTextField!
    @IBOutlet weak var txtMobileNo: MKTextField!
    @IBOutlet weak var txtDrCode: MKTextField!
    
    @IBOutlet weak var stackDrCode: UIStackView!
    
    @IBOutlet weak var lblDrCode: UILabel!
    @IBOutlet weak var lblPrivacyAgree: UILabel!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    fileprivate var loadingObj = CustomLoading()
    fileprivate var loginType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnLogin.backgroundColor = PrimaryColor.colorPrimaryDark()
        
        self.prepareNavigationItem()
        self.checkUserDefaultData()
        
        lblPrivacyAgree.attributedText = "By signing in you agree to our <a href=\"http://www.salk.healthcare/salksite/legal.php\">Privacy Policy</a> and <a href=\"http://www.salk.healthcare/salksite/legal.php\">Terms of Service</a>.".html2AttributedString
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onClicLabel(sender:)))
        lblPrivacyAgree.isUserInteractionEnabled = true
        lblPrivacyAgree.addGestureRecognizer(tap)
        
        let touch = UITapGestureRecognizer(target: self, action:#selector(LoginVC.dismissKeyboard))
        self.view.addGestureRecognizer(touch)
    }
    
    func onClicLabel(sender:UITapGestureRecognizer) {
        let urlString = "http://salk.healthcare/salksite/legal.php"
        
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    /// Prepares the navigationItem.
    private func prepareNavigationItem() {
        navigationItem.titleLabel.text = "app_name".applocalized
        navigationItem.titleLabel.textColor = UIColor.black
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func checkUserDefaultData() {
        configureTextField(txtEmail, placeHolder: "Email Address")
        configureTextField(txtMobileNo, placeHolder: "Mobile Number")
        
        if ("app_refCode".applocalized.uppercased() == "") {
            stackDrCode.isHidden = false
            
            configureTextField(txtDrCode, placeHolder: "Doctor Reference Code")
            txtDrCode.returnKeyType = UIReturnKeyType.go
        } else {
            stackDrCode.isHidden = true
            txtDrCode.text = "app_refCode".applocalized.uppercased()
        }
    }
    
    func configureTextField(_ textField : UITextField, placeHolder: String) {
        textField.delegate = self
        
        if textField == txtDrCode {
            textField.returnKeyType = UIReturnKeyType.done
        } else {
            textField.returnKeyType = UIReturnKeyType.next
        }
        
        //textField.layer.borderColor = UIColor.clear.cgColor
        textField.placeholder = placeHolder
        textField.backgroundColor = UIColor.white
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.autocapitalizationType = .words
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            return txtDrCode.becomeFirstResponder()
        } else if textField == txtMobileNo {
            return txtEmail.becomeFirstResponder()
        } else {
            scrollView.contentOffset = CGPoint.zero;
            return textField.resignFirstResponder()
        }
    }
    
    @IBAction func onClickLogin(sender: UIButton) {
    
        self.dismissKeyboard()
        
        let email = txtEmail.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let mobileNo = txtMobileNo.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let refCodeStr = txtDrCode.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        loginType = "";
        if (!email.isEmpty) {
            loginType = "EMAIL"
        }
        if (!mobileNo.isEmpty) {
            loginType = "MOBILE"
        }
        
        if loginType.isEmpty {
            showSweetDialog(title: "Oops...", message: "Missing Mobile number")
        } else if (refCodeStr.isEmpty) {
            showSweetDialog(title: "Oops...", message: "Missing Dr Reference code")
        } else {
            
            let appPrefs = MySharedPreferences()
            if(loginType == "EMAIL") {
                appPrefs.setEmailID(text: email)
            } else if(loginType == "MOBILE") {
                appPrefs.setMobile(text: "+91\(mobileNo)")
                appPrefs.setCountryCode(text: "+91")
            }
            appPrefs.setTempDrRefCode(text: refCodeStr.uppercased())
            appPrefs.setLoginType(text: loginType)
            
            var message = ""
            if(loginType == "MOBILE") {
                message = "We will be verifying your mobile number\n+91\(mobileNo)\nIs this OK, or would you like to edit the number?"
            } else if(loginType == "EMAIL") {
                message = "We will be verifying your email address\n\(email)\nIs this OK, or would you like to edit the email address?"
            }
            
            let alertObj = SweetAlert()
            alertObj.showAlert("", subTitle: message, style: AlertStyle.none, buttonTitle:"OK", buttonColor: PrimaryColor.colorPrimary(), otherButtonTitle:  "btnCancel".localized, otherButtonColor: UIColor.colorFromRGB(0xD0D0D0) ) { (isOtherButton) -> Void in
                
                if isOtherButton == true {
                    // check login on server
                    let isInternet = CheckInternetConnection().isCheckInternetConnection()
                    if isInternet {
                        self.view.addSubview(self.loadingObj.loading())
                        UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodLogin, classRef: self)
                    } else {
                        self.showSweetDialog(title: "Oops...", message: "noInternet".localized)
                    }
                }
            }
        }
    }
    
    @IBAction func onClickSignUp(sender: UIButton) {
        let regView = RegistrationVC(nibName: "RegistrationVC", bundle: nil)
        self.navigationController?.pushViewController(regView, animated: true)
    }
    
    @IBAction func onClickForgotPass(sender: UIButton) {
        let regView = ForgotPasswordVC(nibName: "ForgotPasswordVC", bundle: nil)
        self.navigationController?.pushViewController(regView, animated: true)
    }
    
    private func showSweetDialog(title: String, message: String) {
        SweetAlert().showOnlyAlert(title, subTitle: message, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    func moveToMainViewController() {
        
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        appPref.setIsNeedBuildApplication(text: true) //defualt build on to configure app
        
        //close current registration form and restart app
        let mainObj = MainViewController()
        mainObj.startVerification(method: Constant.AccountMng.MethodLogin)
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        showSweetDialog(title: "Oops...12", message: appPref.getErrorMessage())
    }

}
