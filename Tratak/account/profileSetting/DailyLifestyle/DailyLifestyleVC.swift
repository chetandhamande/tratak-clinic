//
//  DailyLifestyleVC.swift
//  Salk
//
//  Created by Chetan on 07/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class DailyLifestyleVC: UIViewController {
    
    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var timePicker: UIDatePicker!

    @IBOutlet weak var objDaily: DailyLifestyleView!
    @IBOutlet weak var objHabbits: DietaryHabbitsView!
    @IBOutlet weak var objper: PersonalHabbitsView!

    fileprivate var loadingObj = CustomLoading()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mainScrollView.backgroundColor = CustomColor.background()
        timePicker.isHidden = true
        timePicker.backgroundColor = CustomColor.grayLight()
        UIApplication.shared.keyWindow!.bringSubview(toFront: timePicker)
        
        init1()
        prepareNavigationItem()
        getRecordServer()
    }
    
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviDailyLifestyle".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
    }
    
    func init1() {
        let tap = UITapGestureRecognizer(target: self, action:#selector(MedicalHistoryVC.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        objDaily.btnWakeUp.addTarget(self, action: #selector(btnWakeUp), for: .touchUpInside)
        objDaily.btnBreakfast.addTarget(self, action: #selector(btnBreakfast), for: .touchUpInside)
        objDaily.btnLunch.addTarget(self, action: #selector(btnLunch), for: .touchUpInside)
        objDaily.btnEveTea.addTarget(self, action: #selector(btnEveTea), for: .touchUpInside)
        objDaily.btnDinner.addTarget(self, action: #selector(btnDinner), for: .touchUpInside)
        objDaily.btnSleep.addTarget(self, action: #selector(btnSleep), for: .touchUpInside)
        
        refreshParameters()
    }
    
    public func refreshParameters() {
    
        var smoking_since = "", alcohol = "", eatout_preferences = "",
        eat_fruits_daily = "", diet_dry_fruits = "", eat_vegetables = "";
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
        let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
            + " WHERE " + datasource.dbGroupType + " = '"
            + "assGrpDailyLifestyle".localized + "' ")
        if cursor != nil {
            while cursor!.next() {
                cursor!.string(forColumnIndex: 0)
                if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("wake_up") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objDaily.configureSelectButton(objDaily.btnWakeUp, setString:  cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("breakfast") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objDaily.configureSelectButton(objDaily.btnBreakfast, setString:  cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("lunch") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objDaily.configureSelectButton(objDaily.btnLunch, setString:  cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("evening_snack") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objDaily.configureSelectButton(objDaily.btnEveTea, setString:  cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("dinner") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objDaily.configureSelectButton(objDaily.btnDinner, setString:  cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("sleep") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objDaily.configureSelectButton(objDaily.btnSleep, setString:  cursor!.string(forColumnIndex: 1))
                    }
                }else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("likes") {
                    objper.editHobbies.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("alcohol") {
                    alcohol = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("smoking_since") {
                    smoking_since = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("eatout_preferences") {
                    eatout_preferences = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("eat_fruits_daily") {
                    eat_fruits_daily = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("diet_dry_fruits") {
                    diet_dry_fruits = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("eat_vegetables") {
                    eat_vegetables = cursor!.string(forColumnIndex: 1)
                }
            }
            cursor?.close()
        }
        datasource.closeDatabase()
    
        if eat_fruits_daily.equals(objHabbits.fruitYes.titleLabel!.text!) {
            objHabbits.fruitYes.isSelected = true
        }
        if eat_fruits_daily.equals(objHabbits.fruitNo.titleLabel!.text!) {
            objHabbits.fruitNo.isSelected = true
        }
        
        if diet_dry_fruits.equals(objHabbits.dietDryYes.titleLabel!.text!) {
            objHabbits.dietDryYes.isSelected = true
        }
        if diet_dry_fruits.equals(objHabbits.dietDryNo.titleLabel!.text!) {
            objHabbits.dietDryNo.isSelected = true
        }
       
        if eat_vegetables.equals(objHabbits.vegDailyYes.titleLabel!.text!) {
            objHabbits.vegDailyYes.isSelected = true
        }
        if eat_vegetables.equals(objHabbits.vegDailyNo.titleLabel!.text!) {
            objHabbits.vegDailyNo.isSelected = true
        }
    
        if smoking_since.equals(objper.smokeYes.titleLabel!.text!) {
            objper.smokeYes.isSelected = true
        }
        if smoking_since.equals(objper.smokeNo.titleLabel!.text!) {
            objper.smokeNo.isSelected = true
        }
        
        if alcohol.equals(objper.alcoholYes.titleLabel!.text!) {
            objper.alcoholYes.isSelected = true
        }
        if alcohol.equals(objper.alcoholNo.titleLabel!.text!) {
            objper.alcoholNo.isSelected = true
        }
        
//        if objHabbits.eatDaily.titleLabel!.text!) {
//            objHabbits.vegDailyYes.isSelected = true
//        }
//        if eat_vegetables.equals(objHabbits.vegDailyNo.titleLabel!.text!) {
//            objHabbits.vegDailyNo.isSelected = true
//        }
//
//    radioBtn = (RadioButton) findViewById(R.id.smokeNo);
//    if(.equals(radioBtn.getText()))
//    rdgSmoke.check(R.id.smokeNo);
//    radioBtn = (RadioButton) findViewById(R.id.smokeDaily);
//    if(smoking_since.equals(radioBtn.getText())) {
//    rdgSmoke.check(R.id.smokeYes);
//    rdgSmokeSub.check(R.id.smokeDaily);
//    }
//    radioBtn = (RadioButton) findViewById(R.id.smokeWeekly);
//    if(smoking_since.equals(radioBtn.getText())) {
//    rdgSmoke.check(R.id.smokeYes);
//    rdgSmokeSub.check(R.id.smokeWeekly);
//    }
//    radioBtn = (RadioButton) findViewById(R.id.smokeMonth);
//    if(smoking_since.equals(radioBtn.getText())) {
//    rdgSmoke.check(R.id.smokeYes);
//    rdgSmokeSub.check(R.id.smokeMonth);
//    }
//    radioBtn = (RadioButton) findViewById(R.id.smokeRear);
//    if(smoking_since.equals(radioBtn.getText())) {
//    rdgSmoke.check(R.id.smokeYes);
//    rdgSmokeSub.check(R.id.smokeRear);
//    }
//
//    radioBtn = (RadioButton) findViewById(R.id.alcoholNo);
//    if(alcohol.equals(radioBtn.getText()))
//    rdgAlcohol.check(R.id.alcoholNo);
//    radioBtn = (RadioButton) findViewById(R.id.alcoholDaily);
//    if(alcohol.equals(radioBtn.getText())) {
//    rdgAlcohol.check(R.id.alcoholYes);
//    rdgAlcoholSub.check(R.id.alcoholDaily);
//    }
//    radioBtn = (RadioButton) findViewById(R.id.alcoholWeekly);
//    if(alcohol.equals(radioBtn.getText())) {
//    rdgAlcohol.check(R.id.alcoholYes);
//    rdgAlcoholSub.check(R.id.alcoholWeekly);
//    }
//    radioBtn = (RadioButton) findViewById(R.id.alcoholMonth);
//    if(alcohol.equals(radioBtn.getText())) {
//    rdgAlcohol.check(R.id.alcoholYes);
//    rdgAlcoholSub.check(R.id.alcoholMonth);
//    }
//    radioBtn = (RadioButton) findViewById(R.id.alcoholRear);
//    if(alcohol.equals(radioBtn.getText())) {
//    rdgAlcohol.check(R.id.alcoholYes);
//    rdgAlcoholSub.check(R.id.alcoholRear);
//    }
    
        if eatout_preferences.equals(objHabbits.eatDaily.titleLabel!.text!) {
            objHabbits.eatDaily.isSelected = true
        }
        if eatout_preferences.equals(objHabbits.eatWeekly.titleLabel!.text!) {
            objHabbits.eatWeekly.isSelected = true
        }
        if eatout_preferences.equals(objHabbits.eatMonth.titleLabel!.text!) {
            objHabbits.eatMonth.isSelected = true
        }
        if eatout_preferences.equals(objHabbits.eatRear.titleLabel!.text!) {
            objHabbits.eatRear.isSelected = true
        }
    }
    
    private func performModifyValue(_ defaultTime: String) {
        var defaultTime = defaultTime
        let df = DateFormatter()
        if defaultTime.isEmpty {
            df.dateFormat = "yyyy-MM-dd hh:mm a"
            defaultTime = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df)
        } else {
            df.dateFormat = "yyyy-MM-dd"
            defaultTime = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df) + " " + defaultTime;
        }
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        let defaultTimeLong = ConvertionClass().conDateToLong(df.date(from: defaultTime)!)
    
    //show time selector popup
        timePicker.isHidden = false
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.setDate(df.date(from: defaultTime)!, animated: true)
        timePicker.addTarget(self, action: #selector(startTimeDiveChanged(sender:)), for: UIControlEvents.valueChanged)
    }
    
    @objc func startTimeDiveChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        
        let time = formatter.string(from: sender.date)
        if sender.tag == 1 {
            objDaily.configureSelectButton(objDaily.btnWakeUp, setString: time)
        } else if sender.tag == 2 {
            objDaily.configureSelectButton(objDaily.btnBreakfast, setString: time)
        } else if sender.tag == 3 {
            objDaily.configureSelectButton(objDaily.btnLunch, setString: time)
        } else if sender.tag == 4 {
            objDaily.configureSelectButton(objDaily.btnEveTea, setString: time)
        } else if sender.tag == 5 {
            objDaily.configureSelectButton(objDaily.btnDinner, setString: time)
        } else if sender.tag == 6 {
            objDaily.configureSelectButton(objDaily.btnSleep, setString: time)
        }
    }

    @objc func performNextOpr() {
        dismissKeyboard()
        
        if objDaily.btnWakeUp.titleLabel!.text!.isEmpty && objDaily.btnWakeUp.titleLabel!.text!.equals("HH:MM") {
            SweetAlert().showAlert("popupAlert".localized, subTitle: "Missing Wakeup time", style: .warning)
        } else if objDaily.btnBreakfast.titleLabel!.text!.isEmpty && objDaily.btnBreakfast.titleLabel!.text!.equals("HH:MM") {
            SweetAlert().showAlert("popupAlert".localized, subTitle: "Missing Breakfast time", style: .warning)
        } else if objDaily.btnLunch.titleLabel!.text!.isEmpty && objDaily.btnLunch.titleLabel!.text!.equals("HH:MM") {
            SweetAlert().showAlert("popupAlert".localized, subTitle: "Missing Lunch time", style: .warning)
        } else if objDaily.btnEveTea.titleLabel!.text!.isEmpty && objDaily.btnEveTea.titleLabel!.text!.equals("HH:MM") {
            SweetAlert().showAlert("popupAlert".localized, subTitle: "Missing Evening time", style: .warning)
        } else if objDaily.btnDinner.titleLabel!.text!.isEmpty && objDaily.btnDinner.titleLabel!.text!.equals("HH:MM") {
            SweetAlert().showAlert("popupAlert".localized, subTitle: "Missing Dinner time", style: .warning)
        } else {
            var smoking_since = "", alcohol = "", eatout_preferences = "",
            eat_fruits_daily = "", diet_dry_fruits = "", eat_vegetables = "";
            
           
            eat_fruits_daily = objHabbits.eat_fruits_daily
            diet_dry_fruits = objHabbits.diet_dry_fruits
            eat_vegetables = objHabbits.eat_vegetables
            eatout_preferences = objHabbits.eatout_preferences
            
            smoking_since = objper.smoking_since
            alcohol = objper.alcohol
            
//            try {
//            int selectedId = rdgSmoke.getCheckedRadioButtonId();
//            RadioButton radioButton;
//            if(selectedId == R.id.smokeNo) {
//            radioButton = (RadioButton) findViewById(selectedId);
//            } else {
//            selectedId = rdgSmokeSub.getCheckedRadioButtonId();
//            radioButton = (RadioButton) findViewById(selectedId);
//            }
//            smoking_since = radioButton.titleLabel!.text!;
//            } catch (Exception e) {}
            
//            try {
//            int selectedId = rdgAlcohol.getCheckedRadioButtonId();
//            RadioButton radioButton;
//            if(selectedId == R.id.alcoholNo) {
//            radioButton = (RadioButton) findViewById(selectedId);
//            } else {
//            selectedId = rdgAlcoholSub.getCheckedRadioButtonId();
//            radioButton = (RadioButton) findViewById(selectedId);
//            }
//            alcohol = radioButton.titleLabel!.text!;
//            } catch (Exception e) {}
            
            let datasource = DBOperation()
            datasource.openDatabase(true)
            datasource.UpdateAssessmentItemTable(paramName: "wake_up", paramVal: objDaily.btnWakeUp.titleLabel!.text!, groupType: "assGrpDailyLifestyle".localized)
            datasource.UpdateAssessmentItemTable(paramName: "breakfast", paramVal: objDaily.btnBreakfast.titleLabel!.text!, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "lunch", paramVal: objDaily.btnLunch.titleLabel!.text!, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "evening_snack", paramVal: objDaily.btnEveTea.titleLabel!.text!, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "dinner", paramVal: objDaily.btnDinner.titleLabel!.text!, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "sleep", paramVal: objDaily.btnSleep.titleLabel!.text!, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "likes", paramVal: objper.editHobbies.text!, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "smoking_since", paramVal: smoking_since, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "alcohol", paramVal: alcohol, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "eatout_preferences", paramVal: eatout_preferences, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "eat_fruits_daily", paramVal: eat_fruits_daily, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "diet_dry_fruits", paramVal: diet_dry_fruits, groupType: "assGrpDailyLifestyle".localized);
            datasource.UpdateAssessmentItemTable(paramName: "eat_vegetables", paramVal: eat_vegetables, groupType: "assGrpDailyLifestyle".localized);
            datasource.closeDatabase();
            
            // call for getting Register user
            let checkConn = CheckInternetConnection()
            if checkConn.isCheckInternetConnection() {
                self.view.addSubview(loadingObj.loading())
                let appPrefs = MySharedPreferences()
                appPrefs.setUpdateReqType(text: "assGrpDailyLifestyle".localized)
                UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
                //call for update old UI on server, only for RightLiving
                if(appPrefs.getDrRefCode().equalsIgnoreCase("RLIVING")) {
                    //                    new ConnRequestManager().getRequestManager(mContext,
                    //                            mContext.getString(R.string.MethodRegisterFitness),
                    //                            DailyLifestyleAct.this);
                }
            } else {
                SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
            }
        }
    }
    
    private func getRecordServer() {
    // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPrefs = MySharedPreferences();
            appPrefs.setUpdateReqType(text: "assGrpDailyLifestyle".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
            //call for update old UI on server, only for RightLiving
            if(appPrefs.getDrRefCode().equalsIgnoreCase("RLIVING")) {
                //                    new ConnRequestManager().getRequestManager(mContext,
                //                            mContext.getString(R.string.MethodRegisterFitness),
                //                            DailyLifestyleAct.this);
            }
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @objc func btnWakeUp() {
        timePicker.tag = 1
        var time = objDaily.btnWakeUp.titleLabel!.text!
        if time.equals("HH:MM") {
            time = ""
        }
        performModifyValue(time)
    }
    @objc func btnBreakfast() {
        timePicker.tag = 2
        var time = objDaily.btnBreakfast.titleLabel!.text!
        if time.equals("HH:MM") {
            time = ""
        }
        performModifyValue(time)
    }
    @objc func btnLunch() {
        timePicker.tag = 3
        var time = objDaily.btnLunch.titleLabel!.text!
        if time.equals("HH:MM") {
            time = ""
        }
        performModifyValue(time)
    }
    @objc func btnEveTea() {
        timePicker.tag = 4
        var time = objDaily.btnEveTea.titleLabel!.text!
        if time.equals("HH:MM") {
            time = ""
        }
        performModifyValue(time)
    }
    @objc func btnDinner() {
        timePicker.tag = 5
        var time = objDaily.btnDinner.titleLabel!.text!
        if time.equals("HH:MM") {
            time = ""
        }
        performModifyValue(time)
    }
    
    @objc func btnSleep() {
        timePicker.tag = 6
        var time = objDaily.btnSleep.titleLabel!.text!
        if time.equals("HH:MM") {
            time = ""
        }
        performModifyValue(time)
    }
    
    @objc func dismissKeyboard() {
        timePicker.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
