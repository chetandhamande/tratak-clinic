//
//  BasicInfoView.swift
//  Salk
//
//  Created by Chetan on 05/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
//import ScalePicker
import Async

class BasicInfoView: UIView {
    
    @IBOutlet weak var backCardView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    
    @IBOutlet weak var lblBloodGrp: UILabel!
    @IBOutlet weak var btnBlood: UIButton!
    @IBOutlet weak var imgBlood: UIImageView!
    
    @IBOutlet weak var lblHeight: UILabel!
//    @IBOutlet weak var txtHeight: UILabel!
    @IBOutlet weak var btnHeight: UIButton!
    @IBOutlet weak var imgHeight: UIImageView!
    @IBOutlet weak var txtHeight: MKTextField!
//    @IBOutlet weak var scaleHeight: ScalePicker!
    
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var txtWeight: MKTextField!
    @IBOutlet weak var btnWeight: UIButton!
    @IBOutlet weak var imgWeight: UIImageView!
    
    @IBOutlet weak var lblLifestyle: UILabel!
    @IBOutlet weak var btnLifestyle: UIButton!
    @IBOutlet weak var imgLifestyle: UIImageView!
    
    @IBOutlet weak var lblBMI: UILabel!
    @IBOutlet weak var txtBMI: UILabel!
    
    @IBOutlet weak var lblBMR: UILabel!
    @IBOutlet weak var txtBMR: UILabel!
    
    @IBOutlet weak var lblFat: UILabel!
    @IBOutlet weak var txtFat: MKTextField!
    @IBOutlet weak var viewFat: UIView!
    
    @IBOutlet weak var lblWater: UILabel!
    @IBOutlet weak var txtWater: MKTextField!
    @IBOutlet weak var viewWater: UIView!
    
    @IBOutlet weak var lblLBM: UILabel!
    @IBOutlet weak var txtLBM: MKTextField!
    @IBOutlet weak var viewLBM: UIView!
    
    @IBOutlet weak var lblBM: UILabel!
    @IBOutlet weak var txtBM: MKTextField!
    @IBOutlet weak var viewBM: UIView!
    
    @IBOutlet weak var lblVF: UILabel!
    @IBOutlet weak var txtVF: MKTextField!
    @IBOutlet weak var viewVF: UIView!
    
    @IBOutlet weak var lblMA: UILabel!
    @IBOutlet weak var txtMA: MKTextField!
    @IBOutlet weak var viewMA: UIView!
    
    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        self.viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "BasicInfoView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        headerView.backgroundColor = CustomColor.blackTransparent1()
        
//        scale(scaleView: scaleHeight)
        
        viewFat.isHidden = true
        
        viewWater.isHidden = true
        viewLBM.isHidden = true
        viewBM.isHidden = true
        viewVF.isHidden = true
        viewMA.isHidden = true
        
        configureTextField(txtHeight, placeholder: "Current height")
        configureTextField(txtWeight, placeholder: "Current weight")
        configureTextField(txtFat, placeholder: "Body Fat(%)")
        configureTextField(txtWater, placeholder: "Body Water(%)")
        configureTextField(txtLBM, placeholder: "Lean Body Mass")
        configureTextField(txtBM, placeholder: "Bone Mass")
        configureTextField(txtVF, placeholder: "Visceral Fat(%)")
        configureTextField(txtMA, placeholder: "Metabolic Age")
        
        configureLabel(lblBloodGrp, setString: "Blood Group")
        configureLabel(lblHeight, setString: "Height")
        configureLabel(lblWeight, setString: "Weight")
        configureLabel(lblLifestyle, setString: "Lifestyle")
        configureLabel(lblBMI, setString: "BMI")
        configureLabel(lblBMR, setString: "BMR")
        configureLabel(txtBMI, setString: "")
        configureLabel(txtBMR, setString: "")
        configureLabel(lblFat, setString: "Fat(%)")
        configureLabel(lblWater, setString: "Water(%)")
        configureLabel(lblLBM, setString: "Lean Body Mass")
        configureLabel(lblBM, setString: "Bone Mass")
        configureLabel(lblVF, setString: "Visceral Fat")
        configureLabel(lblMA, setString: "Metabolic Age")
        
                
        btnHeight.setTitleColor(CustomColor.textBlackColor(), for: .normal)
        btnWeight.setTitleColor(CustomColor.textBlackColor(), for: .normal)
        
        configureButton(btnBlood, setString: "")
        configureButton(btnLifestyle, setString: "")
    }
    
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.keyboardType = .decimalPad
        textField.returnKeyType = .next
    }
    
    func configureLabel(_ label: UILabel, setString: String) {
        label.text = setString
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = CustomColor.darker_gray()
    }
    
    func configureButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        //        label.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(CustomColor.textGrayColor(), for: .normal)
        refreshView()
    }
    
    func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
//        label.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(CustomColor.textBlackColor(), for: .normal)
        refreshView()
    }
    
    func refreshView() {
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
    
//    func scale(scaleView: ScalePicker) {
//        scaleView.minValue = 0
//        scaleView.maxValue = 10
//        scaleView.numberOfTicksBetweenValues = 2
//        scaleView.spaceBetweenTicks = 2.0
//        scaleView.showTickLabels = true
////        scaleView.delegate = self
//        scaleView.snapEnabled = true
//        scaleView.bounces = true
//        scaleView.tickColor = UIColor.black
//        scaleView.centerArrowImage = UIImage(named: "ic_arrow_drop_down_black_24dp.png")
//        scaleView.gradientMaskEnabled = true
//        scaleView.blockedUI = true
//        scaleView.sidePadding = 10.0
//        scaleView.title = "Height"
//        scaleView.showCurrentValue = true
//        scaleView.trackProgress = true
//        scaleView.invertProgress = true
//        scaleView.progressColor = UIColor.green
//        
//        
////        scaleView.values = [32, 40, 50, 64, 80, 100, 125, 160, 200, 250, 320, 400, 500, 640, 800, 1000, 1250, 1600]
//        
//        scaleView.valueFormatter = {(value: CGFloat) -> NSAttributedString in
//            let attrs = [NSForegroundColorAttributeName: UIColor.white,
//                         NSFontAttributeName: UIFont.systemFont(ofSize: 12.0)]
//            
//            let text = "\(value)" + " auto"
//            let attrText = NSMutableAttributedString(string: text, attributes: attrs)
//            
//            if let range = text.range(of: "auto") {
//                let rangeValue = text.nsRange(fromRange: range)
//                
//                attrText.addAttribute(NSForegroundColorAttributeName, value:UIColor.orange, range:rangeValue)
//            }
//            
//            return attrText
//        }
//        
//        Async.main(after: 0.1, {_ in
//            scaleView.setInitialCurrentValue(value: 0)
//        })
//    }
}

extension BasicInfoView {
    
//    func didChangeScaleValue(picker: ScalePicker, value: CGFloat) {
//        txtHeight.text = "\(value)"
//    }
}
