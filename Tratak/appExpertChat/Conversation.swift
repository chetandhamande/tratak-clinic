//
//  Conversation.swift
//  SwiftExample
//
//  Created by Dan Leonard on 5/11/16.
//  Copyright © 2016 MacMeDan. All rights reserved.
//

import Foundation

struct Conversation {
    let firstName: String?
    let lastName: String?
    let preferredName: String?
    let smsNumber: String
    let id: String?
    let latestMessage: String?
    let isRead: Bool
    
    var expertName = "", expertID = "", expertCatID = "",
    msgID = "", subMsg = "", dateTime = "", totalUnread = "0", inbox = "1", qualification = "",
    msgType = "", fileName = ""
    var listLayoutIndex:Int = 0
    var isChecked:Bool = false
}
