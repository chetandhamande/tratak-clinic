//
//  MainViewController.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController

class MainViewController: UIViewController {

    @IBOutlet weak var lblAppName: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden =  true
        
        //Status bar style and visibility
        UIApplication.shared.isStatusBarHidden = true
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = PrimaryColor.colorPrimaryBackgroud()
        lblAppName.text = "app_name".applocalized
    
        self.getAppBasicInformation()
        
        moveToDecideAppCondition()
    }
    
    /*
     * Check for status of application run
     */
    private func checkingStatusOfApp() -> Int {
        var status:Int = 0;
    
        // 0 for show splash screen
        // 1 for open dashboard activity
        // 2 for open dashboard without login
        // 3 for open build application
        // 4 for open Accept Disclaimer page
    
        let appPref = MySharedPreferences()
        if (appPref.getUserID() > 0) {
            status = 1
    
            if(appPref.getIsNeedBuildApplication()) {
				status = 3
            }
        }
    
        return status
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func moveToDecideAppCondition() {
        let status = checkingStatusOfApp()
        if status == 0 {
            perform(#selector(startSalkLibrary), with: nil, afterDelay: 1)
        } else if status == 3 {
            perform(#selector(moveToBuildAppViewController), with: nil, afterDelay: 1)
        } else if status == 1 {
            //check for need to upgrade db
            let appPref = MySharedPreferences()
            if appPref.getPreviousDBVersion() != SalkProjectConstant().dbVersion {
                let dbOpr = DBOperation()
                dbOpr.openDatabase(true)
                dbOpr.closeDatabase()
                
                //call build app, to reload all data
                perform(#selector(moveToBuildAppViewController), with: nil, afterDelay: 1)
            } else {
                perform(#selector(moveToMainViewController), with: nil, afterDelay: 1)
            }
        }
    }
    
    func startSalkLibrary() {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let userRegistationViewObj = SplashScreenVC(nibName: "SplashScreenVC", bundle: nil)
        let navController = UINavigationController(rootViewController: userRegistationViewObj)
        
        appDelegate.window!.rootViewController = navController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func startLogin() {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let userRegistationViewObj = LoginVC(nibName: "LoginVC", bundle: nil)
        let navController = AppNavigationController(rootViewController: userRegistationViewObj)
        
        appDelegate.window!.rootViewController = navController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func startRegistration() {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let userRegistationViewObj = RegistrationVC(nibName: "RegistrationVC", bundle: nil)
        let navController = AppNavigationController(rootViewController: userRegistationViewObj)
        
        appDelegate.window!.rootViewController = navController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func startVerification(method: String) {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let userRegistationViewObj = VerificationViewController(nibName: "VerificationViewController", bundle: nil)
        userRegistationViewObj.type = method
        let navController = AppNavigationController(rootViewController: userRegistationViewObj)
        
        appDelegate.window!.rootViewController = navController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func moveToBuildAppViewController() {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let userRegistationViewObj = BuildSalkViewController(nibName: "BuildSalkViewController", bundle: nil)
        let navController = AppNavigationController(rootViewController: userRegistationViewObj)
        
        appDelegate.window!.rootViewController = navController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func moveToMainViewController() {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if CheckInternetConnection().isCheckInternetConnection() == true {
            let mySharedObj = MySharedPreferences()
            let mobileNo = mySharedObj.getMobile()
            let myJID = mobileNo + "_" + mySharedObj.getCompanyID() + "_" + SalkProjectConstant().XMPPKeyWord + "@" + SalkProjectConstant().XMPPLink
            let myPassword = XMPPPasswordAlgo().getXMPPUserPassword(mobileNo)
            //Print.printLog("Mobile : \(mobileNo)  Password : \(myPassword) ")
            
            OneChat.start(true, delegate: nil) { (stream, error) -> Void in
                if let _ = error {
                    print("errors from appdelegate")
                } else {
                    print("Yayyyy")
                }
            }
            
            OneChat.sharedInstance.connect(username: myJID, password: myPassword!) { (stream, error) -> Void in
                if let _ = error {
                    Print.printLog("XMPP Stream : \(stream) ")
                    Print.printLog("XMPP Error : \(error) ")
                    
                    let alertController = UIAlertController(title: "Sorry", message: "An error occured: \(error)", preferredStyle: UIAlertControllerStyle.alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { (UIAlertAction) -> Void in
                        //do something
                    }))
                    self.present(alertController, animated: true, completion: nil)
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
            //check for APN Set on server, if not then call updateProfile to update
            let appPref = MySharedPreferences()
            if(!appPref.getIsAPNUpdateOnServer() && !appPref.getAPNToken().isEmpty) {
                appPref.setUpdateReqType(text: "secondaryInfo")
                UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
            }
            
        }
        
        let first = MainDashboardActivity(nibName: "MainDashboardActivity", bundle: nil)
        let nav1 = AppNavigationController(rootViewController: first)
        let tab1 = RAMAnimatedTabBarItem()
        tab1.animation = RAMBounceAnimation()
        tab1.iconColor = CustomColor.grayLight()
        tab1.title = "Dashboard"
        tab1.image = UIImage().imageWithImage(UIImage(named: "dashboard.png")!, scaledToSize: CGSize(width: 24, height: 24))
        tab1.selectedImage = UIImage().imageWithImage(UIImage(named: "dashboard.png")!, scaledToSize: CGSize(width: 24, height: 24))
        nav1.tabBarItem = tab1

        let second = FitnessPanelTabLayoutVC(nibName: "FitnessPanelTabLayoutVC", bundle: nil)
        let nav2 = AppNavigationController(rootViewController: second)
        let tab2 = RAMAnimatedTabBarItem()
        tab2.animation = RAMBounceAnimation()
        tab2.iconColor = CustomColor.grayLight()
        tab2.title = "Daily Routine"
        tab2.image = UIImage().imageWithImage(UIImage(named: "daily_routine.png")!, scaledToSize: CGSize(width: 24, height: 24))
        tab2.selectedImage = UIImage().imageWithImage(UIImage(named: "daily_routine.png")!, scaledToSize: CGSize(width: 24, height: 24))
        nav2.tabBarItem = tab2
        
        let third = MainExpertVC(nibName: "MainExpertVC", bundle: nil)
        let nav3 = AppNavigationController(rootViewController: third)
        let tab3 = RAMAnimatedTabBarItem(title: "Expert Chat", image: UIImage().imageWithImage(UIImage(named: "expert_chat.png")!, scaledToSize: CGSize(width: 24, height: 24)), selectedImage: UIImage().imageWithImage(UIImage(named: "expert_chat.png")!, scaledToSize: CGSize(width: 24, height: 24)))
        tab3.animation = RAMBounceAnimation()
        tab3.iconColor = CustomColor.grayLight()
        nav3.tabBarItem = tab3
        
        let forth = SettingTableView(nibName: "SettingTableView", bundle: nil)
        let nav4 = AppNavigationController(rootViewController: forth)
        let tab4 = RAMAnimatedTabBarItem(title: "Setting", image: UIImage().imageWithImage(UIImage(named: "settings.png")!, scaledToSize: CGSize(width: 24, height: 24)), selectedImage: UIImage().imageWithImage(UIImage(named: "settings.png")!, scaledToSize: CGSize(width: 24, height: 24)))
        tab4.animation = RAMBounceAnimation()
        tab4.iconColor = CustomColor.grayLight()
        nav4.tabBarItem = tab4
        
        let fifth = BlogDashboardVC(nibName: "BlogDashboardVC", bundle: nil)
        let nav5 = AppNavigationController(rootViewController: fifth)
        let tab5 = RAMAnimatedTabBarItem(title: "Health Feed", image: UIImage().imageWithImage(UIImage(named: "blog.png")!, scaledToSize: CGSize(width: 24, height: 24)), selectedImage: UIImage().imageWithImage(UIImage(named: "blog.png")!, scaledToSize: CGSize(width: 24, height: 24)))
        tab5.animation = RAMBounceAnimation()
        tab5.iconColor = CustomColor.grayLight()
        nav5.tabBarItem = tab5
        
        let tabs = RAMAnimatedTabBarController(viewControllers: [nav1, nav2, nav3, nav5, nav4])
        tabs.changeSelectedColor(PrimaryColor.colorPrimary(), iconSelectedColor: PrimaryColor.colorPrimary())
        tabs.selectedIndex = 0
        appDelegate.window!.rootViewController = tabs
        appDelegate.window?.makeKeyAndVisible()
    }
    
    private func getAppBasicInformation() {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        
        Print.printLog(path)
        
        let df = DateFormatter()
        df.dateFormat = "Z"
        let date = Date()
        let tz = df.string(from: date)
        
        let mySharedObj = MySharedPreferences()
        mySharedObj.setTimeZone(tz.substringWithRange(0, end: 3) + ":" + tz.substringWithRange(3, end: 5))
        
        let tzz = TimeZone.autoupdatingCurrent
        mySharedObj.setTimeZoneID(tzz.identifier)
        
        let myDevice = UIDevice.current
        let appVersionCode = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        
        let appBundle = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
        //let applicationName = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleDisplayName")
        
        let deviceOSVersion = myDevice.systemVersion
        
        mySharedObj.setApplicationID("app_id".applocalized)
        mySharedObj.setMobileCurrentVersionCode(deviceOSVersion.substring(to: deviceOSVersion.characters.index(deviceOSVersion.startIndex, offsetBy: 4)))
        mySharedObj.setAppCurrentVersionCode(appBundle)
        mySharedObj.setAppCurrentVersionCodeLong(appVersionCode)
        mySharedObj.setMobileModel(myDevice.modelName)
        mySharedObj.setDeviceID(text: myDevice.identifierForVendor!.uuidString)
        mySharedObj.setMobileCountryCodeNumber(text: "+91")
                
        if SalkProjectConstant().isDebugMode {
            // debug only code
            mySharedObj.setDeviceID(text: "0000000000")
        } else {
            // release only code
            mySharedObj.setDeviceID(text: myDevice.identifierForVendor!.uuidString)
        }
    }

}
