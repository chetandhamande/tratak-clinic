//
//  CaloriesTimelineTVC.swift
//  Salk
//
//  Created by Chetan  on 09/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class CaloriesTimelineTVC: UITableViewController {
        
    fileprivate var arrMainList = [ArrTimelines]()
    fileprivate var mRefreshControl: UIRefreshControl!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let timelineTableViewCellNib = UINib(nibName: "TimelineTableViewCell", bundle: Bundle(for: TimelineTableViewCell.self))
        self.tableView.register(timelineTableViewCellNib, forCellReuseIdentifier: "TimelineTableViewCell")
        
        self.tableView.estimatedRowHeight = 300
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.prepareNavigationItem()
        
        self.loadValues()
        
        //pull to refresh
        mRefreshControl = UIRefreshControl()
        mRefreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        mRefreshControl.addTarget(self, action: #selector(self.handleRefresh), for: UIControlEvents.valueChanged)
        
        self.tableView.addSubview(mRefreshControl)
    }
    
    func handleRefresh() {
        let isInternet = CheckInternetConnection().isCheckInternetConnection()
        if (isInternet) {
            FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncTLPedometer, classRef: self)
        } else {
            self.stopOnResponse()
        }
    }
    
    func refreshOnResponse() {
        mRefreshControl.endRefreshing()
        
        loadValues()
        self.tableView.reloadData()
    }
    
    func stopOnResponse() {
        mRefreshControl.endRefreshing()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Calories Burn"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let settingImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_settings_black_24dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        let settingButton = UIBarButtonItem(image: settingImage, style: .plain, target: self, action: #selector(self.loadValues))
        
        let addNewImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_add_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        let addNewButton = UIBarButtonItem(image: addNewImage, style: .plain, target: self, action: #selector(self.showAppsAndDevices))
        
        navigationItem.rightBarButtonItems = [addNewButton, settingButton]
    }
    
    func showAppsAndDevices() {
        let a = FitnessBandConnectVC(nibName: "FitnessBandConnectVC", bundle: nil)
        self.navigationController?.pushViewController(a, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return arrMainList.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard let sectionData:ArrTimelines = arrMainList[section] else {
            return 0
        }
        return sectionData.arrTimeline.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return String(describing: arrMainList[section].headerTitle)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimelineTableViewCell", for: indexPath) as! TimelineTableViewCell
        
        // Configure the cell...
        guard let sectionData:ArrTimelines = arrMainList[indexPath.section] else
        {
            return cell
        }
        
        let arrTimeline = sectionData.arrTimeline[indexPath.row]
        
        let timelinePoint = arrTimeline.points
        let timelineBackColor = arrTimeline.color
        let title = arrTimeline.title
        let description = arrTimeline.values + "  " + arrTimeline.units
        let lineInfo = arrTimeline.lineInfo
        //let thumbnail = arrTimeline.thumbnail
        
        var timelineFrontColor = UIColor.clear
        if (indexPath.row > 0) {
            timelineFrontColor = sectionData.arrTimeline[indexPath.row - 1].color
        }
        cell.timelinePoint = timelinePoint!
        cell.timeline.frontColor = timelineFrontColor
        cell.timeline.backColor = timelineBackColor
        cell.titleLabel.text = title
        cell.descriptionLabel.text = description
        cell.descriptionLabel.font = UIFont.boldSystemFont(ofSize: 16)
        cell.descriptionLabel.textColor = CustomColor.dark_gray()
        cell.lineInfoLabel.text = lineInfo
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        guard let sectionData = data[indexPath.section] else {
        //            return
        //        }
    }
}

extension CaloriesTimelineTVC {
    func loadValues() {
        
        arrMainList = [ArrTimelines]()
        
        let appPref = MySharedPreferences()
        let unitValue = "of  " + appPref.getDailyCalBurn() + " calories"
        
        var headerIndex: Int = 0
        let temp = ArrTimelines()
        temp.headerTitle = "This Week"
        arrMainList.append(temp)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        var arrList = [ArrTimeline]()
        
        for day in 0 ..< 91 {
            let calendar = Calendar.current
            let currDate = calendar.date(byAdding: .day, value: -day, to: Date())
            
            dateFormatter.dateFormat = "EEE"
            let dayStr = dateFormatter.string(from: currDate!)
            
            if(dayStr.uppercased() == "SUN") {
                arrMainList[headerIndex].arrTimeline = arrList
                arrList = [ArrTimeline]()
                
                headerIndex += 1
                
                let temp = ArrTimelines()
                
                //find Week Date
                let weekDate = calendar.date(byAdding: .day, value: -(6 + day), to: Date())
                
                dateFormatter.dateFormat = "MMM dd"
                temp.headerTitle = dateFormatter.string(from: weekDate!)
                
                dateFormatter.dateFormat = "MMM dd,  yyyy"
                temp.headerTitle += " - " + dateFormatter.string(from: currDate!)
                
                arrMainList.append(temp)
            }
            
            //for Days
            let tempSub = ArrTimeline()
            if(day == 0) {
                tempSub.title = "Today"
            } else if(day < 7) {
                dateFormatter.dateFormat = "EEE"
                tempSub.title = dateFormatter.string(from: currDate!)
            } else {
                dateFormatter.dateFormat = "dd MMM"
                tempSub.title = dateFormatter.string(from: currDate!)
            }
            
            tempSub.values = "0"
            tempSub.units = unitValue
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            tempSub.dbTimeIDStr = dateFormatter.string(from: currDate!)
                        
            arrList.append(tempSub)
        }
        
        //update last record
        arrMainList[headerIndex].arrTimeline = arrList
        
        //remove this week, if no date avaialble
        if arrMainList[0].arrTimeline.count == 0 {
            arrMainList.remove(at: 0)
        }
        
        
        //getting record from database
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let sql = "SELECT * from " + datasource.CaloriesRecordLogList_tlb
            + " ORDER BY " + datasource.dbDateTime + " desc LIMIT 90 "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let time = cursor!.string(forColumnIndex: 1)!
                
                for i in 0 ..< arrMainList.count {
                    for j in 0 ..< arrMainList[i].arrTimeline.count {
                        if(arrMainList[i].arrTimeline[j].dbTimeIDStr.uppercased() == time.uppercased())
                        {
                            arrMainList[i].arrTimeline[j].values = String(format: "%.1f", cursor!.double(forColumnIndex: 0))
                            break
                        }
                    }
                }
            }
            cursor!.close()
        }
        datasource.closeDatabase()
    }
}
