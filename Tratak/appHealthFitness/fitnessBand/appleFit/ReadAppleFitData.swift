//
//  ReadAppleFitData.swift
//  Salk
//
//  Created by Chetan  on 16/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class ReadAppleFitData {
    
    let healthManager:HealthManager = HealthManager()
    
    func readFitnessData() {
     
        //read steps data
        healthManager.retrieveStepCount { (result) -> Void in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateTime = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter)
            
            self.updateDBRecord(type: "steps", dateTime: dateTime, value: String(result), extraData: "")
        }
        
        //read Calory burn data
        healthManager.retrieveEnergyBurnCount { (result) -> Void in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateTime = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter)
            
            self.updateDBRecord(type: "calories", dateTime: dateTime, value: String(result), extraData: "")
        }
        
        //read Distance Walk data
        healthManager.retrieveDistanceWalkCount { (result) -> Void in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateTime = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter)
            
            self.updateDBRecord(type: "distance", dateTime: dateTime, value: String(result), extraData: "")
        }
        
        //read Water data
        healthManager.retrieveWaterCount { (result) -> Void in

            if result > 0 {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                var dateTime = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter))" + " 00:00:01 am";
                
                dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                dateTime = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime)!))"
                
                self.updateDBRecord(type: "water", dateTime: dateTime, value: String(result), extraData: "")
            }
        }
        
        //Read Sleep data
        healthManager.retrieveSleepCount { (sleepTotal, fromToTime) -> Void in
            
            if sleepTotal > 0 && !fromToTime.isEmpty {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                var dateTime = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter))" + " 00:00:01 am";
                
                dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                dateTime = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime)!))"
                
                self.updateDBRecord(type: "sleep", dateTime: dateTime, value: String(sleepTotal), extraData: fromToTime)
            }
        }
    }
    
    fileprivate func updateDBRecord(type: String, dateTime: String, value: String, extraData: String?)
    {        
        let datasource = DBOperation()
        datasource.openDatabase(true)
        
        if(type == "steps") {
            datasource.UpdatePedometerRecordLogList(steps: value, dateTime: dateTime, isServerSync: "0")
        } else if(type == "calories") {
            datasource.UpdateCaloriesRecordLogList(steps: value, dateTime: dateTime, isServerSync: "0")
        } else if(type == "distance") {
            var distance:Double = Double(value)!
            distance /= 1000
            datasource.UpdateDistanceRecordLogList(steps: String(format: "%.1f", distance), dateTime: dateTime, isServerSync: "0")
        } else if(type == "water") {
            var water:Double = Double(value)!
            water *= 1000
            
            datasource.InsertWaterTrackList(currentValue: String(format: "%.0f", water), dateTime: dateTime, isServerSync: "0", isDelete: "0")
            
        } else if(type == "sleep") {
            datasource.InsertSleepTrackList(currentValue: value, sleepValue: extraData!, dateTime: dateTime, quality: "3", isServerSync: "0")
        }
        
        datasource.closeDatabase()
        
        
        // Fire the broadcast with intent packaged
        let dict = ["resultCode":1,
                    "udpateType":"padometerUpdate"
            ] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
    }
    
}
