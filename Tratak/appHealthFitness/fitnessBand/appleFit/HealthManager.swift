//
//  HealthManager.swift
//  HKTutorial
//
//  Created by ernesto on 18/10/14.
//  Copyright (c) 2014 raywenderlich. All rights reserved.
//

import Foundation
import HealthKit
import Toast_Swift

class HealthManager {
    
    let healthKitStore:HKHealthStore = HKHealthStore()
    
    func requestPermissions(completion: @escaping ((_ success:Bool, _ error:Error?) -> Void))
    {
        let healthKitTypesToRead : Set<HKSampleType> = [
            HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!,
            HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.appleExerciseTime)!,
            HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!,
            HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!,
            HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.dietaryWater)!,
            HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!
            ]
        
        let healthKitTypesToWrite: Set<HKSampleType> = []
        
        if !HKHealthStore.isHealthDataAvailable() {
            print("Error: HealthKit is not available in this Device")
            //completion(false, error)
            return
        }
        
        healthKitStore.requestAuthorization(toShare: healthKitTypesToWrite, read: healthKitTypesToRead)
        {
            (success, error) -> Void in completion(success, error)
        }
    }
    
    func readMostRecentSample(sampleType:HKSampleType , completion: ((HKSample?, NSError?) -> Void)!)
    {
        
        // 1. Build the Predicate
        let past = NSDate.distantPast as NSDate
        let now   = NSDate()
        let mostRecentPredicate = HKQuery.predicateForSamples(withStart: past as Date, end:now as Date, options: .strictStartDate)
        
        // 2. Build the sort descriptor to return the samples in descending order
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        // 3. we want to limit the number of samples returned by the query to just 1 (the most recent)
        let limit = 1
        
        // 4. Build samples query
        let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: mostRecentPredicate, limit: limit, sortDescriptors: [sortDescriptor])
        { (sampleQuery, results, error ) -> Void in
            
            if error != nil {
                completion(nil, error as NSError?)
                return;
            }
            
            // Get the first sample
            let mostRecentSample = results?.first as? HKQuantitySample
            
            // Execute the completion closure
            if completion != nil {
                completion(mostRecentSample,nil)
            }
        }
        // 5. Execute the Query
        self.healthKitStore.execute(sampleQuery)
    }
    
    
//    if let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height) {
//        let date = Date()
//        let quantity = HKQuantity(unit: HKUnit.inch(), doubleValue: height!)
//        let sample = HKQuantitySample(type: type, quantity: quantity, start: date, end: date)
//        self.healthKitStore.save(sample, withCompletion: { (success, error) in
//            print("Saved \(success), error \(error)")
//        })
//    }
    
    func retrieveStepCount(completion: @escaping (_ stepRetrieved: Double) -> Void)
    {        
        //   Define the Step Quantity Type
        let stepsCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
        
        //   Get the start of the day
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let newDate = cal.startOfDay(for: date)
        
        //  Set the Predicates & Interval
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: Date(), options: .strictStartDate)
        var interval = DateComponents()
        interval.day = 1
        
        
        let query = HKStatisticsQuery(quantityType: stepsCount!, quantitySamplePredicate: predicate, options: .cumulativeSum) { (_, result, error) in
            var resultCount = 0.0
            
            guard let result = result else {
                Print.printLog("Failed to fetch steps = \(error?.localizedDescription ?? "N/A")")
                completion(resultCount)
                return
            }
            
            if let sum = result.sumQuantity() {
                resultCount = sum.doubleValue(for: HKUnit.count())
            }
            
            DispatchQueue.main.async {
                completion(resultCount)
            }
        }
        
        healthKitStore.execute(query)
    }
    
    func retrieveEnergyBurnCount(completion: @escaping (_ stepRetrieved: Double) -> Void)
    {
        let engBurnCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)
        
        //   Get the start of the day
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let newDate = cal.startOfDay(for: date)
        
        //  Set the Predicates & Interval
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: Date(), options: .strictStartDate)
        var interval = DateComponents()
        interval.day = 1
        
        
        let query = HKStatisticsQuery(quantityType: engBurnCount!, quantitySamplePredicate: predicate, options: .cumulativeSum) { (_, result, error) in
            var resultCount = 0.0
            
            guard let result = result else {
                Print.printLog("Failed to fetch steps = \(error?.localizedDescription ?? "N/A")")
                completion(resultCount)
                return
            }
            
            if let sum = result.sumQuantity() {
                resultCount = sum.doubleValue(for: HKUnit.kilocalorie())
            }
            
            DispatchQueue.main.async {
                completion(resultCount)
            }
        }
        
        healthKitStore.execute(query)
    }
    
    func retrieveDistanceWalkCount(completion: @escaping (_ stepRetrieved: Double) -> Void)
    {
        let distWalkCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)
        
        //   Get the start of the day
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let newDate = cal.startOfDay(for: date)
        
        //  Set the Predicates & Interval
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: Date(), options: .strictStartDate)
        var interval = DateComponents()
        interval.day = 1
        
        
        let query = HKStatisticsQuery(quantityType: distWalkCount!, quantitySamplePredicate: predicate, options: .cumulativeSum) { (_, result, error) in
            var resultCount = 0.0
            
            guard let result = result else {
                Print.printLog("Failed to fetch steps = \(error?.localizedDescription ?? "N/A")")
                completion(resultCount)
                return
            }
            
            if let sum = result.sumQuantity() {
                resultCount = sum.doubleValue(for: HKUnit.meter())
            }
            
            DispatchQueue.main.async {
                completion(resultCount)
            }
        }
        
        healthKitStore.execute(query)
    }
    
    func retrieveWaterCount(completion: @escaping (_ stepRetrieved: Double) -> Void)
    {
        let waterCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.dietaryWater)
        
        //   Get the start of the day
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let newDate = cal.startOfDay(for: date)
        
        //  Set the Predicates & Interval
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: Date(), options: .strictStartDate)
        var interval = DateComponents()
        interval.day = 1
        
        
        let query = HKStatisticsQuery(quantityType: waterCount!, quantitySamplePredicate: predicate, options: .cumulativeSum) { (_, result, error) in
            var resultCount = 0.0
            
            guard let result = result else {
                Print.printLog("Failed to fetch steps = \(error?.localizedDescription ?? "N/A")")
                completion(resultCount)
                return
            }
            
            if let sum = result.sumQuantity() {
                resultCount = sum.doubleValue(for: HKUnit.liter())
            }
            
            DispatchQueue.main.async {
                completion(resultCount)
            }
        }
        
        healthKitStore.execute(query)
    }
    
    func retrieveSleepCount(completion: @escaping (_ sleepTotal: Int, _ fromToTime: String) -> Void)
    {
        
        //   Get the start of the day
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let newDate = cal.startOfDay(for: date)
        
        //  Set the Predicates & Interval
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: Date(), options: .strictStartDate)
        var interval = DateComponents()
        interval.day = 1
        
        
        // first, we define the object type we want
        if let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis) {
            
            // Use a sortDescriptor to get the recent data first
            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
            
            // we create our query with a block completion to execute
            let query = HKSampleQuery(sampleType: sleepType, predicate: predicate, limit: 10, sortDescriptors: [sortDescriptor]) { (query, tmpResult, error) -> Void in
                
                if error != nil {
                    return
                }
                
                if let result = tmpResult {
                    var totalDiff: Int = 0
                    var timeValue = ""
                    for item in result {
                        if let sample = item as? HKCategorySample {
                            var diff: Int = Int(ConvertionClass().conDateToLong(sample.endDate) - ConvertionClass().conDateToLong(sample.startDate))
                            diff /= 60
                            
                            totalDiff += diff
                            
                            let df = DateFormatter()
                            df.dateFormat = "hh:mm a"
                            timeValue = df.string(from: sample.startDate) + "," + df.string(from: sample.endDate)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        completion(totalDiff, timeValue)
                    }
                }
            }
            
            healthKitStore.execute(query)
        }
    }
}
