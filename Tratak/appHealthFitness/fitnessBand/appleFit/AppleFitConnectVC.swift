//
//  AppleFitConnectVC.swift
//  Salk
//
//  Created by Chetan  on 16/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material
import Async

class AppleFitConnectVC: UIViewController {

    @IBOutlet weak var btnSubmit: UIButton!
    
    let healthManager:HealthManager = HealthManager()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareNavigationItem()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Health"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        btnSubmit.cornerRadius = 5
        
        let appPref = MySharedPreferences()
        if (appPref.getIsFitnessBandConnect() && appPref.getFitnessBandName() == "Health")
        {
            btnSubmit.setTitle("Disconnect", for: UIControlState.normal)
        } else {
            btnSubmit.setTitle("Connect", for: UIControlState.normal)
        }
    }
   
    @IBAction func performConnetFit(button: UIButton) {
        
        if (btnSubmit.titleLabel?.text == "Connect") {
            authorizeHealthKit()
        } else {
            let appPref = MySharedPreferences()
            appPref.setIsFitnessBandConnect(text: false)
            appPref.setFitnessBandName(text: "")
            
            btnSubmit.setTitle("Connect", for: UIControlState.normal)
        }
    }

    fileprivate func authorizeHealthKit()
    {
        healthManager.requestPermissions { (authorized, error) -> Void in
            if authorized {
                let appPref = MySharedPreferences()
                appPref.setIsFitnessBandConnect(text: true)
                appPref.setFitnessBandName(text: "Health")
                
                Async.main({
                    self.btnSubmit.setTitle("Disconnect", for: UIControlState.normal)
                })
            }
        }
    }
}
