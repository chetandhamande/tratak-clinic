//
//  FoodSelectorVC.swift
//  Salk
//
//  Created by Salk on 24/11/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class FoodSelectorVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblTotalCalories: UILabel!
    @IBOutlet weak var lblTotalSize: UILabel!
    @IBOutlet weak var lblService: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var pickerSize: UIPickerView!
    @IBOutlet weak var pickerServing: UIPickerView!
    
    var foodID = "", foodName = "", currDateTime = "", scheduleName = "", recID = "", preSizeStr = "", prevContIDStr = ""
    var selSize = "", selServing = ""
    var selServingIndex  = 0
    
    var sizeItems = [String]()
    var servingItems = [String]()
    var arrServing = [String]()
    var arrContainerID = [String]()
    var arrContUint = [String]()
    var arrContValue = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationItem()
        loadPickerOption()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = foodName
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
    }
    
    fileprivate func loadPickerOption() {
        
        lblFoodName.text = foodName
        
        //size
        for i in 0 ..< 103 {
            if i == 0 {
                sizeItems.append("0.25")
            } else if i == 1 {
                sizeItems.append("0.5")
            } else if i == 2 {
                sizeItems.append("0.75")
            } else {
                sizeItems.append("\(i - 2)")
            }
        }
        
        //load food releted information
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        var sql = "SELECT * FROM " + datasource.FoodRecordList_tlb + " WHERE " + datasource.dbFoodID + " = '" + foodID + "' "
        
        var totalCalories = "", containerIDStr = ""
        var cursor = datasource.selectRecords(sql)
        if cursor != nil {
            if cursor!.next() {
                totalCalories = cursor!.string(forColumnIndex: 2)
                containerIDStr = cursor!.string(forColumnIndex: 7)
            }
            cursor!.close()
        }
        
        //getting container list
        if !containerIDStr.isEmpty {
            sql = "SELECT * FROM " + datasource.FoodContainerList_tlb + " WHERE " + datasource.dbContainID + " IN (\(containerIDStr))"
            cursor = datasource.selectRecords(sql)
            if cursor != nil {
                while cursor!.next() {
                    arrContainerID.append(cursor!.string(forColumnIndex: 0))
                    arrServing.append(cursor!.string(forColumnIndex: 1))
                    arrContValue.append(cursor!.string(forColumnIndex: 2))
                    arrContUint.append(cursor!.string(forColumnIndex: 3))
                }
                cursor!.close()
            }
        }
        datasource.closeDatabase()
        
        if arrContainerID.count == 0 {
            self.navigationController?.popViewController(animated: true)
        } else {
            lblTotalSize.text = totalCalories.isEmpty ? "0 gram" : "\(totalCalories) grams"
            lblTotalCalories.text = totalCalories.isEmpty ? "0 cal" : "\(totalCalories) cal"
            
            var tempServingIndex = 0, tempSizeIndex = 3
            if(!recID.isEmpty) {
                for i in 0 ..< sizeItems.count {
                    if sizeItems[i] == preSizeStr {
                        tempSizeIndex = i
                        break
                    }
                }
                
                for i in 0 ..< arrContainerID.count {
                    if arrContainerID[i] == prevContIDStr {
                        tempServingIndex = i
                        break
                    }
                }
            }
            
            selSize = sizeItems[tempSizeIndex]
            selServing = arrServing[tempServingIndex]
            lblService.text = "\(selSize) \(selServing)"
            
            pickerSize.dataSource = self
            pickerSize.delegate = self
            pickerSize.tag = 1
           
            pickerServing.dataSource = self
            pickerServing.delegate = self
            pickerServing.tag = 2
            
            pickerSize.selectRow(tempSizeIndex, inComponent: 0, animated: false)
            pickerServing.selectRow(tempServingIndex, inComponent: 0, animated: false)
            
            do {
                try gettingServeInfo()
            } catch {}
        }
    }
    
    @IBAction func perfromSubmit(sender: UIButton) {
        
        if !foodID.isEmpty {
            let selContID = arrContainerID[selServingIndex]
            
            let datasource = DBOperation()
            datasource.openDatabase(true)
            
            if(recID.isEmpty) {
                datasource.InsertFoodRecordLogList(foodID: foodID, quanti: selSize, dateTime: currDateTime, containID: selContID, isRoutineRec: "0", schedule: scheduleName, isServerSync: "0", serverID: "0")
            } else {
                datasource.UpdateFoodRecordLogList(recIDs: recID, foodID: foodID, quanti: selSize, containID: selContID)
            }
            datasource.closeDatabase()
            
            // Fire the broadcast with intent packaged
            var dict = ["resultCode":1,
                        "udpateType":"sendPendingData"
                ] as [String : Any]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
            
            dict = ["resultCode":1,
                    "udpateType":"foodRef"
                ] as [String : Any]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
        }
        
        //self.view.makeToast("Record Added", duration: 2, position: CGPoint(x: self.view.bounds.size.width / 2.0, y: (self.view.bounds.size.height - 100.0)))
        self.navigationController?.popViewController(animated: true)
    }
}

extension FoodSelectorVC {
    // DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return sizeItems.count
        } else {
            return arrServing.count
        }
    }
    
    // Delegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return sizeItems[row]
        } else {
            return arrServing[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            selSize = sizeItems[row]
        } else {
            selServing = arrServing[row]
            selServingIndex = row
        }
        
        lblService.text = "\(selSize) \(selServing)"
        
        do {
            try gettingServeInfo()
        } catch {}
    }
    
    fileprivate func gettingServeInfo() throws {
        
        if arrContValue[selServingIndex].trim() == "" {
            lblTotalSize.text = "0"
        } else {
            let value: Double = Double(arrContValue[selServingIndex])!
            let quant: Double = Double(selSize)!
            lblTotalSize.text = "\(String(format: "%.1f", (value * quant))) \(arrContUint[selServingIndex])"
        }
    }
}
