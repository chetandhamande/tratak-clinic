//
//  ArrFitnessTypeData.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

public class ArrFitnessTypeData {
    public var type: String = "" , typeData = ""
    public var typeIcon = ""
}
