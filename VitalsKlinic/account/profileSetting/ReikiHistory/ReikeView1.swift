//
//  ReikeView1.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import DLRadioButton

class ReikeView1: UIView {

    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var rdgReikiCaYes: DLRadioButton!
    @IBOutlet weak var rdgReikiCaNo: DLRadioButton!
    @IBOutlet weak var rdgReikiSessionYes: DLRadioButton!
    @IBOutlet weak var rdgReikiSessionNo: DLRadioButton!
    
    var underPhy: String = "", reikiSession = ""
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ReikeView1", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        //        headerView.backgroundColor = PrimaryColor.blackTransparent1()
        //
        //        lblHeading.text = "Major Medical Complaints"
        
        
        createRadioButton(rdgReikiCaYes, title: "Yes")
        createRadioButton(rdgReikiCaNo, title: "No")
        
        createRadioButton(rdgReikiSessionYes, title: "Yes")
        createRadioButton(rdgReikiSessionNo, title: "No")
    }
    
    private func createRadioButton(_ button: DLRadioButton, title: String) {
        button.indicatorColor = CustomColor.dark_gray()
        button.setTitle(title, for: .normal)
        button.setTitleColor(CustomColor.dark_gray(), for: .normal)
        button.isIconSquare = false
        button.isMultipleSelectionEnabled = false
    }
    
    @objc @IBAction private func radioButtonClickPhy(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            underPhy = radioButton.selected()!.titleLabel!.text!
        }
    }
    
    @objc @IBAction private func radioButtonClickSession(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            reikiSession = radioButton.selected()!.titleLabel!.text!
        }
    }
}


