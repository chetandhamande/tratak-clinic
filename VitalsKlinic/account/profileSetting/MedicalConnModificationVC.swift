//
//  MedicalConnModificationVC.swift
//  Salk
//
//  Created by Chetan  on 16/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material

class MedicalConnModificationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mainTableView: UITableView!
    
    fileprivate var arrMainList = [ArrSetting]()
    let CustomProfileSettingIdentifier = "CustomProfileSettingTVC"
    
    fileprivate var loadingObj = CustomLoading()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareNavigationItem()
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        
        self.loadList()
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviMedicalCond".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let submitImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_agree_colored.png")!, scaledToSize: CGSize(width: 20, height: 20))
        let submitButton = UIBarButtonItem(image: submitImage, style: .plain, target: self, action: #selector(self.performSubmit))
        
        navigationItem.rightBarButtonItems = [submitButton]
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var settingCell: CustomProfileSettingTVC! = mainTableView.dequeueReusableCell(withIdentifier: CustomProfileSettingIdentifier) as? CustomProfileSettingTVC
        
        if settingCell == nil {
            mainTableView.register(UINib(nibName: CustomProfileSettingIdentifier, bundle: nil), forCellReuseIdentifier: CustomProfileSettingIdentifier)
            settingCell = tableView.dequeueReusableCell(withIdentifier: CustomProfileSettingIdentifier) as? CustomProfileSettingTVC
        }
        
        settingCell!.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        return settingCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        arrMainList[indexPath.row].isCheck = !arrMainList[indexPath.row].isCheck
        if arrMainList[indexPath.row].heading == "Other"
        {
            var isVail: Bool = false
            var index: Int = 0
            for i in 0 ..< arrMainList.count {
                if(arrMainList[i].heading.isEmpty) {
                    isVail = true
                    index = i
                    break
                }
            }
            
            if arrMainList[indexPath.row].isCheck {
                if(!isVail) {
                    let temp = ArrSetting()
                    temp.heading = ""
                    arrMainList.append(temp)
                }
            } else {
                if(isVail) {
                    arrMainList.remove(at: index)
                }
            }
        }
        
        mainTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
}

extension MedicalConnModificationVC {
    fileprivate func loadList() {
        
        let appPref = MySharedPreferences()
        let medicalPref = appPref.getMedicalCondition()
        
        var temp = ArrSetting()
        temp.heading = "Diabetes"
        arrMainList.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Hypertension"
        arrMainList.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Thyroid"
        arrMainList.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Insomania"
        arrMainList.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Stress"
        arrMainList.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Hypotension"
        arrMainList.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Other"
        arrMainList.append(temp)
        
        for i in 0 ..< arrMainList.count {
            if medicalPref.contains(arrMainList[i].heading) {
               arrMainList[i].isCheck = true
            }
            
            if arrMainList[i].heading == "Other" && arrMainList[i].isCheck {
                arrMainList[i].isCheck = true
                
                temp = ArrSetting()
                temp.heading = ""
                
                let index = medicalPref.indexOf(",") + 1
                temp.otherData = medicalPref.substringFromIndex(index)
                arrMainList.append(temp)
            }
        }
        
        mainTableView.reloadData()
    }
    
    @objc fileprivate func performSubmit() {
        
        self.dismissKeyboard()
    
        var selItem = ""
        for i in 0 ..< arrMainList.count {
            if arrMainList[i].isCheck {
                selItem += selItem.isEmpty ? arrMainList[i].heading : "," + arrMainList[i].heading
            }
        }
        
        if selItem.contains("Other") {
            for i in 0 ..< arrMainList.count {
                if arrMainList[i].heading.isEmpty {
                    selItem += "," + arrMainList[i].otherData
                    break
                }
            }
        }
        
        
        let isInternet = CheckInternetConnection().isCheckInternetConnection()
        if isInternet {
            let appPrefs = MySharedPreferences()
            
            appPrefs.setMedicalCondition(text: selItem)
            
            self.view.addSubview(loadingObj.loading())
            
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            showSweetDialog(title: "Oops...", message: "noInternet".localized)
        }
        
    }
    
    fileprivate func showSweetDialog(title: String, message: String) {
        SweetAlert().showOnlyAlert(title, subTitle: message, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        showSweetDialog(title: "Oops...", message: appPref.getErrorMessage())
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showOnlyAlert("Success", subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
}
