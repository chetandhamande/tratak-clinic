//
//  FoodPreferenceModificationVC.swift
//  Salk
//
//  Created by Chetan  on 16/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material

class FoodPreferenceModificationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mainTableView: UITableView!
    
    fileprivate var arrMainList = [ArrSettingSections]()
    let CustomProfileSettingIdentifier = "CustomProfileSettingTVC"
    
    fileprivate var loadingObj = CustomLoading()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareNavigationItem()
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        
        self.loadList()
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviFoodHabit".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let submitImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_agree_colored.png")!, scaledToSize: CGSize(width: 20, height: 20))
        let submitButton = UIBarButtonItem(image: submitImage, style: .plain, target: self, action: #selector(self.performSubmit))
        
        navigationItem.rightBarButtonItems = [submitButton]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrMainList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainList[section].arrSetting.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return arrMainList[section].heading
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var settingCell: CustomProfileSettingTVC! = mainTableView.dequeueReusableCell(withIdentifier: CustomProfileSettingIdentifier) as? CustomProfileSettingTVC
        
        if settingCell == nil {
            mainTableView.register(UINib(nibName: CustomProfileSettingIdentifier, bundle: nil), forCellReuseIdentifier: CustomProfileSettingIdentifier)
            settingCell = tableView.dequeueReusableCell(withIdentifier: CustomProfileSettingIdentifier) as? CustomProfileSettingTVC
        }
        
        settingCell!.onBindViewHolder(arrMainList[indexPath.section].arrSetting, position: indexPath.row, classRef: self)
        
        return settingCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if arrMainList[indexPath.section].heading == "Diet Preference" {
            arrMainList[indexPath.section].arrSetting[indexPath.row].isCheck = true
            
            for j in 0 ..< arrMainList[indexPath.section].arrSetting.count {
                if j != indexPath.row {
                    arrMainList[indexPath.section].arrSetting[j].isCheck = false
                }
            }
        } else if arrMainList[indexPath.section].heading == "Ethnicity" || arrMainList[indexPath.section].heading == "Food Allergy ?"
        {
            arrMainList[indexPath.section].arrSetting[indexPath.row].isCheck = !arrMainList[indexPath.section].arrSetting[indexPath.row].isCheck
            
            if arrMainList[indexPath.section].arrSetting[indexPath.row].heading.contains("Other")
            {
                var index: Int = 0
                var isVail: Bool = false
                for i in 0 ..< arrMainList[indexPath.section].arrSetting.count {
                    if(arrMainList[indexPath.section].arrSetting[i].heading.isEmpty)
                    {
                        isVail = true
                        index = i
                        break
                    }
                }
                
                if arrMainList[indexPath.section].arrSetting[indexPath.row].isCheck
                {
                    if(!isVail) {
                        let temp = ArrSetting()
                        temp.heading = ""
                        arrMainList[indexPath.section].arrSetting.append(temp)
                    }
                } else {
                    if(isVail) {
                        arrMainList[indexPath.section].arrSetting.remove(at: index)
                    }
                }
            }
        }
        
        mainTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
}

extension FoodPreferenceModificationVC {
    
    fileprivate func loadList() {
        
        //Diet Preference
        var arrSetting = [ArrSetting]()
        var temp = ArrSetting()
        temp.heading = "Vegetarian"
        temp.isRadio = true
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Non Vegetarian"
        temp.isRadio = true
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Eggetarian"
        temp.isRadio = true
        arrSetting.append(temp)
        
        var tempSection = ArrSettingSections()
        tempSection.heading = "Diet Preference"
        tempSection.arrSetting = arrSetting
        arrMainList.append(tempSection)
        
        
        //Food Allergy
        arrSetting = [ArrSetting]()
        temp = ArrSetting()
        temp.heading = "Dairy Product"
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Nut"
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Seafood"
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Egg"
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Wheat"
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Poultry"
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Other (Specify)"
        arrSetting.append(temp)
        
        tempSection = ArrSettingSections()
        tempSection.heading = "Food Allergy ?"
        tempSection.arrSetting = arrSetting
        arrMainList.append(tempSection)
        
        
        
        //Food Allergy
        arrSetting = [ArrSetting]()
        temp = ArrSetting()
        temp.heading = "South Indian"
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "North Indian"
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "North East Indian"
        arrSetting.append(temp)
        
        temp = ArrSetting()
        temp.heading = "Other (Specify)"
        arrSetting.append(temp)
        
        tempSection = ArrSettingSections()
        tempSection.heading = "Ethnicity"
        tempSection.arrSetting = arrSetting
        arrMainList.append(tempSection)
        
        let appPref = MySharedPreferences()
        let foodAllergies = appPref.getFoodAllergies()
        let foodEthnicity = appPref.getFoodEthnicity()
        let dietPref = appPref.getDiatPref()
        
        for i in 0 ..< arrMainList.count {
            for j in 0 ..< arrMainList[i].arrSetting.count {
                if arrMainList[i].heading == "Food Allergy ?" {
                    if foodAllergies.contains(arrMainList[i].arrSetting[j].heading)
                    {
                        arrMainList[i].arrSetting[j].isCheck = true
                        
                        if arrMainList[i].arrSetting[j].heading.contains("Other") && arrMainList[i].arrSetting[j].isCheck
                        {
                            arrMainList[i].arrSetting[j].isCheck = true
                            
                            temp = ArrSetting()
                            temp.heading = ""
                            
                            let index = foodAllergies.indexOf(",") + 1
                            temp.otherData = foodAllergies.substringFromIndex(index)
                            arrMainList[i].arrSetting.append(temp)
                        }
                    }
                } else if arrMainList[i].heading == "Ethnicity" {
                    if foodEthnicity.contains(arrMainList[i].arrSetting[j].heading)
                    {
                        arrMainList[i].arrSetting[j].isCheck = true
                        
                        if arrMainList[i].arrSetting[j].heading.contains("Other") && arrMainList[i].arrSetting[j].isCheck
                        {
                            arrMainList[i].arrSetting[j].isCheck = true
                            
                            temp = ArrSetting()
                            temp.heading = ""
                            
                            let index = foodEthnicity.indexOf(",") + 1
                            temp.otherData = foodEthnicity.substringFromIndex(index)
                            arrMainList[i].arrSetting.append(temp)
                        }
                    }
                } else if arrMainList[i].heading == "Diet Preference" {
                    if dietPref == arrMainList[i].arrSetting[j].heading
                    {
                        arrMainList[i].arrSetting[j].isCheck = true
                    }
                }
            }
        }
        
        mainTableView.reloadData()
    }
    
    @objc fileprivate func performSubmit() {
        
        self.dismissKeyboard()        
        
        var selDiat = "", selAllergy = "", selEthnicity = ""
        for i in 0 ..< arrMainList.count {
            if arrMainList[i].heading == "Food Allergy ?" {
                
                for j in 0 ..< arrMainList[i].arrSetting.count {
                    if arrMainList[i].arrSetting[j].isCheck {
                        selAllergy += selAllergy.isEmpty ? arrMainList[i].arrSetting[j].heading : "," + arrMainList[i].arrSetting[j].heading
                    }
                }
                
                if selAllergy.contains("Other") {
                    for j in 0 ..< arrMainList[i].arrSetting.count {
                        if arrMainList[i].arrSetting[j].heading.isEmpty {
                            selAllergy += "," + arrMainList[i].arrSetting[j].otherData
                            break
                        }
                    }
                }
                
            } else if arrMainList[i].heading == "Ethnicity" {
                for j in 0 ..< arrMainList[i].arrSetting.count {
                    if arrMainList[i].arrSetting[j].isCheck {
                        selEthnicity += selEthnicity.isEmpty ? arrMainList[i].arrSetting[j].heading : "," + arrMainList[i].arrSetting[j].heading
                    }
                }
                
                if selEthnicity.contains("Other") {
                    for j in 0 ..< arrMainList[i].arrSetting.count {
                        if arrMainList[i].arrSetting[j].heading.isEmpty {
                            selEthnicity += "," + arrMainList[i].arrSetting[j].otherData
                            break
                        }
                    }
                }
            } else if arrMainList[i].heading == "Diet Preference" {
                for j in 0 ..< arrMainList[i].arrSetting.count {
                    if arrMainList[i].arrSetting[j].isCheck {
                        selDiat = arrMainList[i].arrSetting[j].heading
                    }
                }
            }
        }
       
        
        let isInternet = CheckInternetConnection().isCheckInternetConnection()
        if isInternet {
            let appPrefs = MySharedPreferences()
            
            appPrefs.setDiatPref(text: selDiat)
            appPrefs.setFoodAllergies(text: selAllergy)
            appPrefs.setFoodEthnicity(text: selEthnicity)
            
            self.view.addSubview(loadingObj.loading())
            
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            showSweetDialog(title: "Oops...", message: "noInternet".localized)
        }
        
    }
    
    fileprivate func showSweetDialog(title: String, message: String) {
        SweetAlert().showOnlyAlert(title, subTitle: message, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        showSweetDialog(title: "Oops...", message: appPref.getErrorMessage())
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showOnlyAlert("Success", subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
}
