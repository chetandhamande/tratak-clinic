//
//  ArrSetting.swift
//  Salk
//
//  Created by Chetan  on 16/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class ArrSetting {
    var heading = "", otherData = ""
    var isCheck: Bool = false, isRadio: Bool = false
}
