//
//  StressHistoryVC.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class StressHistoryVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var objPhy: PysicalStressView!
    @IBOutlet weak var objMen: MentalStressView!
    @IBOutlet weak var objEmo: EmotionalStressView!
    @IBOutlet weak var objSoc: SocialStressView!
    @IBOutlet weak var objRel: ReligiousStressView!
    
    fileprivate var loadingObj = CustomLoading()
    fileprivate let arr = ["Day", "Week", "Month", "Year"]
    fileprivate let arrGrade = ["None", "Mild", "Moderate", "Severe"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mainScrollView.backgroundColor = CustomColor.background()
        
        picker.isHidden = true
        picker.backgroundColor = CustomColor.grayLight()
        picker.showsSelectionIndicator = true
        self.picker.delegate = self
        self.picker.dataSource = self
        UIApplication.shared.keyWindow!.bringSubview(toFront: picker)
        
        prepareNavigationItem()
        
        init1();
        
        
        //getting Latest record
        getRecordServer();
    }
    
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviStressHist".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
        
    }
    
    private func init1() {
        let tap = UITapGestureRecognizer(target: self, action:#selector(StressHistoryVC.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        objPhy.btnPhyMonth.addTarget(self, action: #selector(btnPhyMonth), for: .touchUpInside)
        objPhy.btnPhyGrade.addTarget(self, action: #selector(btnPhyGrade), for: .touchUpInside)
        
        objMen.btnMenMonth.addTarget(self, action: #selector(btnMenMonth), for: .touchUpInside)
        objMen.btnMenGrade.addTarget(self, action: #selector(btnMenGrade), for: .touchUpInside)
        
        objEmo.btnEmoMonth.addTarget(self, action: #selector(btnEmoMonth), for: .touchUpInside)
        objEmo.btnEmoGrade.addTarget(self, action: #selector(btnEmoGrade), for: .touchUpInside)
        
        objSoc.btnSocMonth.addTarget(self, action: #selector(btnSocMonth), for: .touchUpInside)
        objSoc.btnSocGrade.addTarget(self, action: #selector(btnSocGrade), for: .touchUpInside)
        
        objRel.btnRelMonth.addTarget(self, action: #selector(btnRelMonth), for: .touchUpInside)
        objRel.btnRelGrade.addTarget(self, action: #selector(btnRelGrade), for: .touchUpInside)
        
        refreshParameters()
    }
    
    func refreshParameters() {
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
            + " WHERE " + datasource.dbGroupType + " = '" + "assGrpStressHistory".localized + "' ")
        if cursor != nil {
            while cursor!.next() {
                
                if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("physical_stress_duration") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objPhy.editPhyDuration.text = duraArr[0]
                        objPhy.configureSelectButton(objPhy.btnPhyMonth, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("physical_stress_source") {
                    objPhy.editPhySource.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("physical_stress_cause") {
                    objPhy.editPhyCause.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("physical_stress_trigger") {
                    objPhy.editPhyTrigger.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("physical_stress_grade") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objPhy.configureSelectButton(objPhy.btnPhyGrade, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("physical_stress_rate") {
                    objPhy.editPhyRate.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("mental_stress_duration") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objMen.editMenDuration.text = duraArr[0]
                        objMen.configureSelectButton(objMen.btnMenMonth, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("mental_stress_source") {
                    objMen.editMenSource.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("mental_stress_cause") {
                    objMen.editMenCause.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("mental_stress_trigger") {
                    objMen.editMenTrigger.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("mental_stress_grade") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objMen.configureSelectButton(objMen.btnMenGrade, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("mental_stress_rate") {
                    objMen.editMenRate.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("emotional_stress_duration") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objEmo.editEmoDuration.text = duraArr[0]
                        objEmo.configureSelectButton(objEmo.btnEmoMonth, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("emotional_stress_source") {
                    objEmo.editEmoSource.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("emotional_stress_cause") {
                    objEmo.editEmoCause.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("emotional_stress_trigger") {
                    objEmo.editEmoTrigger.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("emotional_stress_grade") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objEmo.configureSelectButton(objEmo.btnEmoGrade, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("emotional_stress_rate") {
                    objEmo.editEmoRate.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("social_stress_duration") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objSoc.editSocDuration.text = duraArr[0]
                        objSoc.configureSelectButton(objSoc.btnSocMonth, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("social_stress_source") {
                    objSoc.editSocSource.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("social_stress_cause") {
                    objSoc.editSocCause.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("social_stress_trigger") {
                    objSoc.editSocTrigger.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("social_stress_grade") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objSoc.configureSelectButton(objSoc.btnSocGrade, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("social_stress_rate") {
                    objSoc.editSocRate.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("religious_stress_duration") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objRel.editRelDuration.text = duraArr[0]
                        objRel.configureSelectButton(objRel.btnRelMonth, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("religious_stress_source") {
                    objRel.editRelSource.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("religious_stress_cause") {
                    objRel.editRelCause.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("religious_stress_trigger") {
                    objRel.editRelTrigger.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("religious_stress_grade") {
                    if !cursor!.string(forColumnIndex: 1).isEmpty {
                        objRel.configureSelectButton(objRel.btnRelGrade, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("religious_stress_rate") {
                    objRel.editRelRate.text = cursor!.string(forColumnIndex: 1)
                }
            }
            cursor?.close()
            datasource.closeDatabase()
        }
    }

    @objc func performNextOpr() {
        dismissKeyboard()
        
        let datasource = DBOperation()
        datasource.openDatabase(true)
        
        if !objPhy.editPhyDuration.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "physical_stress_duration", paramVal: objPhy.editPhyDuration.text! + " " + objPhy.btnPhyMonth.titleLabel!.text!, groupType: "assGrpStressHistory".localized)
        }
        datasource.UpdateAssessmentItemTable(paramName: "physical_stress_source", paramVal: objPhy.editPhySource.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "physical_stress_cause", paramVal: objPhy.editPhyCause.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "physical_stress_trigger", paramVal: objPhy.editPhyTrigger.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "physical_stress_grade", paramVal: objPhy.btnPhyGrade.titleLabel!.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "physical_stress_rate", paramVal: objPhy.editPhyRate.text!, groupType: "assGrpStressHistory".localized)
        
        if !objMen.editMenDuration.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "mental_stress_duration", paramVal: objMen.editMenDuration.text! + " " + objMen.btnMenMonth.titleLabel!.text!, groupType: "assGrpStressHistory".localized)
        }
        datasource.UpdateAssessmentItemTable(paramName: "mental_stress_source", paramVal: objMen.editMenSource.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "mental_stress_cause", paramVal: objMen.editMenCause.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "mental_stress_trigger", paramVal: objMen.editMenTrigger.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "mental_stress_grade", paramVal: objMen.btnMenGrade.titleLabel!.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "mental_stress_rate", paramVal: objMen.editMenRate.text!, groupType: "assGrpStressHistory".localized)
        
        if !objEmo.editEmoDuration.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_duration", paramVal: objEmo.editEmoDuration.text! + " " + objEmo.btnEmoMonth.titleLabel!.text!, groupType: "assGrpStressHistory".localized)
        }
        datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_source", paramVal: objEmo.editEmoSource.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_cause", paramVal: objEmo.editEmoCause.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_trigger", paramVal: objEmo.editEmoTrigger.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_grade", paramVal: objEmo.btnEmoGrade.titleLabel!.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_rate", paramVal: objEmo.editEmoRate.text!, groupType: "assGrpStressHistory".localized)
        
        if !objSoc.editSocDuration.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "social_stress_duration", paramVal: objSoc.editSocDuration.text! + " " + objSoc.btnSocMonth.titleLabel!.text!, groupType: "assGrpStressHistory".localized)
        }
        datasource.UpdateAssessmentItemTable(paramName: "social_stress_source", paramVal: objSoc.editSocSource.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "social_stress_cause", paramVal: objSoc.editSocCause.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "social_stress_trigger", paramVal: objSoc.editSocTrigger.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "social_stress_grade", paramVal: objSoc.btnSocGrade.titleLabel!.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "social_stress_rate", paramVal: objSoc.editSocRate.text!, groupType: "assGrpStressHistory".localized)
        
        if !objRel.editRelDuration.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "religious_stress_duration", paramVal: objRel.editRelDuration.text! + " " + objRel.btnRelMonth.titleLabel!.text!, groupType: "assGrpStressHistory".localized)
        }
        datasource.UpdateAssessmentItemTable(paramName: "religious_stress_source", paramVal: objRel.editRelSource.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "religious_stress_cause", paramVal: objRel.editRelCause.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "religious_stress_trigger", paramVal: objRel.editRelTrigger.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "religious_stress_grade", paramVal: objRel.btnRelGrade.titleLabel!.text!, groupType: "assGrpStressHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "religious_stress_rate", paramVal: objRel.editRelRate.text!, groupType: "assGrpStressHistory".localized)
        
        datasource.closeDatabase();
        
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loadingObj.loading())
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpStressHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    private func getRecordServer() {
    // call for getting Register user
        
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpStressHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @objc func btnPhyMonth() {
        picker.isHidden = false
        picker.tag = 1
        picker.reloadAllComponents()
    }
    @objc func btnPhyGrade() {
        picker.isHidden = false
        picker.tag = 2
        picker.reloadAllComponents()
    }
    
    @objc func btnMenMonth() {
        picker.isHidden = false
        picker.tag = 3
        picker.reloadAllComponents()
    }
    @objc func btnMenGrade() {
        picker.isHidden = false
        picker.tag = 4
        picker.reloadAllComponents()
    }
    
    @objc func btnEmoMonth() {
        picker.isHidden = false
        picker.tag = 5
        picker.reloadAllComponents()
    }
    @objc func btnEmoGrade() {
        picker.isHidden = false
        picker.tag = 6
        picker.reloadAllComponents()
    }
    
    @objc func btnSocMonth() {
        picker.isHidden = false
        picker.tag = 7
        picker.reloadAllComponents()
    }
    @objc func btnSocGrade() {
        picker.isHidden = false
        picker.tag = 8
        picker.reloadAllComponents()
    }
    
    @objc func btnRelMonth() {
        picker.isHidden = false
        picker.tag = 9
        picker.reloadAllComponents()
    }
    @objc func btnRelGrade() {
        picker.isHidden = false
        picker.tag = 10
        picker.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 || pickerView.tag == 3 || pickerView.tag == 5 || pickerView.tag == 7 {
            return arr.count
        } else {
            return arrGrade.count
        }
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 || pickerView.tag == 3 || pickerView.tag == 5 || pickerView.tag == 7 {
            return arr[row]
        } else {
            return arrGrade[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            self.objPhy.configureSelectButton(self.objPhy.btnPhyMonth, setString: arr[row])
        } else if pickerView.tag == 2 {
            self.objPhy.configureSelectButton(self.objPhy.btnPhyGrade, setString: arrGrade[row])
        } else if pickerView.tag == 3 {
            self.objMen.configureSelectButton(self.objMen.btnMenMonth, setString: arr[row])
        } else if pickerView.tag == 4 {
            self.objMen.configureSelectButton(self.objMen.btnMenGrade, setString: arrGrade[row])
        } else if pickerView.tag == 5 {
            self.objEmo.configureSelectButton(self.objEmo.btnEmoMonth, setString: arr[row])
        } else if pickerView.tag == 6 {
            self.objEmo.configureSelectButton(self.objEmo.btnEmoGrade, setString: arrGrade[row])
        } else if pickerView.tag == 7 {
            self.objSoc.configureSelectButton(self.objSoc.btnSocMonth, setString: arr[row])
        } else if pickerView.tag == 8 {
            self.objSoc.configureSelectButton(self.objSoc.btnSocGrade, setString: arrGrade[row])
        } else if pickerView.tag == 9 {
            self.objRel.configureSelectButton(self.objRel.btnRelMonth, setString: arr[row])
        } else if pickerView.tag == 10 {
            self.objRel.configureSelectButton(self.objRel.btnRelGrade, setString: arrGrade[row])
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        picker.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
