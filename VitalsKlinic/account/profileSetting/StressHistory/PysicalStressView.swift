//
//  PysicalStressView.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class PysicalStressView: UIView {

    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var editPhyDuration: MKTextField!
    @IBOutlet weak var editPhySource: MKTextField!
    @IBOutlet weak var editPhyCause: MKTextField!
    @IBOutlet weak var editPhyTrigger: MKTextField!
    @IBOutlet weak var editPhyRate: MKTextField!
    
    @IBOutlet weak var btnPhyMonth: UIButton!
    @IBOutlet weak var btnPhyGrade: UIButton!
    
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        self.viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PysicalStressView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
//        headerView.backgroundColor = PrimaryColor.blackTransparent1()
//
//        lblHeading.text = "Major Medical Complaints"
        
        configureTextField(editPhyDuration, placeholder: "")
        configureTextField(editPhySource, placeholder: "")
        configureTextField(editPhyCause, placeholder: "")
        configureTextField(editPhyTrigger, placeholder: "")
        configureTextField(editPhyRate, placeholder: "")
        
        configureButton(btnPhyMonth, setString: "Month")
        configureButton(btnPhyGrade, setString: "None")
    }
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
        
        if textField == editPhyDuration  {
            textField.keyboardType = .decimalPad
        }
    }
    
    func configureLabel(_ label: UILabel, setString: String) {
        label.text = setString
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = CustomColor.darker_gray()
    }
    
    func configureButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textGrayColor(), for: UIControlState.normal)
    }
    
    func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textBlackColor(), for: UIControlState.normal)
        
        refreshView()
    }
    
    func refreshView() {
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }

}
