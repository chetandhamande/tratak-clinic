//
//  PersonalityHistoryVC.swift
//  Salk
//
//  Created by Chetan on 01/01/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class PersonalityHistoryVC: UIViewController {

    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var objPsy: PsychologicalView!
    @IBOutlet weak var objYog: YogicView!
    @IBOutlet weak var objAyur: AyurvedicView!
    @IBOutlet weak var objPer: PersonalView!
    
    fileprivate var loadingObj = CustomLoading()
    
    private var editOther: String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        mainScrollView.backgroundColor = CustomColor.background()
        
        prepareNavigationItem()
        
        refreshParameters()
        
        //getting Latest record
        getRecordServer()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviPersonalityHist".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
    }
    
    @objc private func performNextOpr() {
                
        let datasource = DBOperation()
        datasource.openDatabase(true)
        
        var psy_asses = ""
        if objPsy.chkEthenicity1.isSelected {
            psy_asses = objPsy.chkEthenicity1.titleLabel!.text!
        }
        if objPsy.chkEthenicity2.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objPsy.chkEthenicity2.titleLabel!.text!
            }else {
                psy_asses = psy_asses + "," + objPsy.chkEthenicity2.titleLabel!.text!
            }
        }
        
        if objPsy.chkEthenicity3.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objPsy.chkEthenicity3.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objPsy.chkEthenicity3.titleLabel!.text!
            }
        }
        
        if objPsy.chkEthenicity4.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objPsy.chkEthenicity4.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objPsy.chkEthenicity4.titleLabel!.text!
            }
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "psychological_assessment", paramVal: psy_asses, groupType: "assGrpPersonality".localized)
        
        psy_asses = ""
        if objYog.chkEthenicity1.isSelected {
            psy_asses = objYog.chkEthenicity1.titleLabel!.text!
        }
        if objYog.chkEthenicity2.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objYog.chkEthenicity2.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objYog.chkEthenicity2.titleLabel!.text!
            }
        }
        
        if objYog.chkEthenicity3.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objYog.chkEthenicity3.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objYog.chkEthenicity3.titleLabel!.text!
            }
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "yogic_analysis", paramVal: psy_asses, groupType: "assGrpPersonality".localized)
        
        psy_asses = ""
        if objAyur.chkEthenicity1.isSelected {
            psy_asses = objAyur.chkEthenicity1.titleLabel!.text!
        }
        
        if objAyur.chkEthenicity2.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objAyur.chkEthenicity2.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objAyur.chkEthenicity2.titleLabel!.text!
            }
        }
        
        if objAyur.chkEthenicity3.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objAyur.chkEthenicity3.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objAyur.chkEthenicity3.titleLabel!.text!
            }
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "ayurvedic_body", paramVal: psy_asses, groupType: "assGrpPersonality".localized)
        
        psy_asses = ""
        if objPer.chkEthenicity1.isSelected {
            psy_asses = objPer.chkEthenicity1.titleLabel!.text!
        }
        if objPer.chkEthenicity2.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objPer.chkEthenicity2.titleLabel!.text!
            }else {
                psy_asses = psy_asses + "," + objPer.chkEthenicity2.titleLabel!.text!
            }
        }
        
        if objPer.chkEthenicity3.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objPer.chkEthenicity3.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objPer.chkEthenicity3.titleLabel!.text!
            }
        }
        
        if objPer.chkEthenicity4.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objPer.chkEthenicity4.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objPer.chkEthenicity4.titleLabel!.text!
            }
        }
        
        if objPer.chkEthenicity5.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objPer.chkEthenicity5.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objPer.chkEthenicity5.titleLabel!.text!
            }
        }
        
        if objPer.chkEthenicity6.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objPer.chkEthenicity6.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objPer.chkEthenicity6.titleLabel!.text!
            }
        }
        
        if objPer.chkEthenicity7.isSelected {
            if psy_asses.isEmpty {
                psy_asses = objPer.chkEthenicity7.titleLabel!.text!
            } else {
                psy_asses = psy_asses + "," + objPer.chkEthenicity7.titleLabel!.text!
            }
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "personal_temperament", paramVal: psy_asses, groupType: "assGrpPersonality".localized)
        
        datasource.UpdateAssessmentItemTable(paramName: "personal_temperament_other", paramVal: editOther, groupType: "assGrpPersonality".localized)
        
        datasource.closeDatabase()
        
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpPersonality".localized)
            
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func refreshParameters() {
        var psy_asses: String = "",yogic_ana = "", ayur_body = "", pers_temp = ""
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
        let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
    + " WHERE " + datasource.dbGroupType + " = '" + "assGrpPersonality".localized + "' ")
    
        if cursor != nil {
            while cursor!.next() {
                if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("psychological_assessment") {
                    psy_asses = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("yogic_analysis") {
                    yogic_ana = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("ayurvedic_body") {
                    ayur_body = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("personal_temperament") {
                    pers_temp = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("personal_temperament_other") {
                    editOther = cursor!.string(forColumnIndex: 1)
                }
            }
            cursor!.close()
        }
        datasource.closeDatabase()
    
        if !psy_asses.trimmed.isEmpty {
            var psy_assesArr = psy_asses.split(",")
    
            for i in 0 ..< psy_assesArr.count {
                if psy_assesArr[i].equals(objPsy.chkEthenicity1.titleLabel!.text!) {
                    objPsy.chkEthenicity1.isSelected = true
                }else if  psy_assesArr[i].equals(objPsy.chkEthenicity2.titleLabel!.text!) {
                    objPsy.chkEthenicity2.isSelected = true
                }else if  psy_assesArr[i].equals(objPsy.chkEthenicity3.titleLabel!.text!) {
                    objPsy.chkEthenicity3.isSelected = true
                }else if  psy_assesArr[i].equals(objPsy.chkEthenicity4.titleLabel!.text!) {
                    objPsy.chkEthenicity4.isSelected = true
                }
            }
        }
    
        if !yogic_ana.trimmed.isEmpty {
            var yogic_anaArr = yogic_ana.split(",")
            
            for i in 0 ..< yogic_anaArr.count {
                if yogic_anaArr[i].equals(objYog.chkEthenicity1.titleLabel!.text!) {
                    objYog.chkEthenicity1.isSelected = true
                } else if yogic_anaArr[i].equals(objYog.chkEthenicity2.titleLabel!.text!) {
                    objYog.chkEthenicity2.isSelected = true
                } else if yogic_anaArr[i].equals(objYog.chkEthenicity3.titleLabel!.text!) {
                    objYog.chkEthenicity3.isSelected = true
                }
            }
        }
    
        if !ayur_body.trimmed.isEmpty {
            var ayur_bodyArr = ayur_body.split(",")
            
            for i in 0 ..< ayur_bodyArr.count {
                if ayur_bodyArr[i].equals(objAyur.chkEthenicity1.titleLabel!.text!) {
                    objAyur.chkEthenicity1.isSelected = true
                } else if ayur_bodyArr[i].equals(objAyur.chkEthenicity2.titleLabel!.text!) {
                    objAyur.chkEthenicity2.isSelected = true
                } else if ayur_bodyArr[i].equals(objAyur.chkEthenicity3.titleLabel!.text!) {
                    objAyur.chkEthenicity3.isSelected = true
                }
            }
        }
        
        if !pers_temp.trimmed.isEmpty {
            var pers_tempArr = pers_temp.split(",")
            
            for i in 0 ..< pers_tempArr.count {
                if pers_tempArr[i].equals(objPer.chkEthenicity1.titleLabel!.text!) {
                    objPer.chkEthenicity1.isSelected = true
                } else if pers_tempArr[i].equals(objPer.chkEthenicity2.titleLabel!.text!) {
                    objPer.chkEthenicity2.isSelected = true
                } else if pers_tempArr[i].equals(objPer.chkEthenicity3.titleLabel!.text!) {
                    objPer.chkEthenicity3.isSelected = true
                } else if pers_tempArr[i].equals(objPer.chkEthenicity4.titleLabel!.text!) {
                    objPer.chkEthenicity4.isSelected = true
                } else if pers_tempArr[i].equals(objPer.chkEthenicity5.titleLabel!.text!) {
                    objPer.chkEthenicity5.isSelected = true
                } else if pers_tempArr[i].equals(objPer.chkEthenicity6.titleLabel!.text!) {
                    objPer.chkEthenicity6.isSelected = true
                } else if pers_tempArr[i].equals(objPer.chkEthenicity7.titleLabel!.text!) {
                    objPer.chkEthenicity7.isSelected = true
                }
                
                if !editOther.trimmed.equals("") {
                    objPer.chkEthenicity8.isSelected = true
                }
            }
        }
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpPersonality".localized)
            
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
