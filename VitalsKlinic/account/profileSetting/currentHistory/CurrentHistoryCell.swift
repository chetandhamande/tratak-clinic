//
//  CurrentHistoryCell.swift
//  Salk
//
//  Created by Chetan on 02/01/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CurrentHistoryCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var editMBrand: MKTextField!
    @IBOutlet weak var editMGeneric: MKTextField!
    @IBOutlet weak var editMdose: MKTextField!
    @IBOutlet weak var editMduration: MKTextField!
    
    @IBOutlet weak var btnMmg: UIButton!
    @IBOutlet weak var btnMtimes: UIButton!
    @IBOutlet weak var btnMDay: UIButton!
    
    @IBOutlet weak var picker: UIPickerView!

    var classRefs: AnyObject! = nil
    
    let arrMg = ["Mg", "Gram"]
    let arrTimes = ["1Times", "2Times", "3Times", "4Times",
                    "5Times", "6Times", "7Times", "8Times"]
    let arrDay = ["Day", "Week", "Month", "Year"]
    
    var type = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewConfiguration()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    private func viewConfiguration() {
        configureTextField(editMBrand, placeholder: "Brand Name")
        configureTextField(editMGeneric, placeholder: "Generic Name")
        configureTextField(editMdose, placeholder: "Dose")
        configureTextField(editMduration, placeholder: "Duration")
        
        configureButton(btnMmg, setString: "mg")
        configureButton(btnMtimes, setString: "1Times")
        configureButton(btnMDay, setString: "Day")
        
        picker.isHidden = true
        picker.backgroundColor = CustomColor.grayLight()
        picker.showsSelectionIndicator = true
        
        btnMmg.addTarget(self, action: #selector(btnMmg1), for: .touchUpInside)
        btnMtimes.addTarget(self, action: #selector(btnMtimes1), for: .touchUpInside)
        btnMDay.addTarget(self, action: #selector(btnMDay1), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(CurrentHistoryCell.dismissView))
        tap.cancelsTouchesInView = false
        self.addGestureRecognizer(tap)
    }
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
        
        if textField == editMduration || textField == editMdose {
            textField.keyboardType = .decimalPad
        }
    }
    
    func configureLabel(_ label: UILabel, setString: String) {
        label.text = setString
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = CustomColor.darker_gray()
    }
    
    func configureButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        //        label.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(CustomColor.textGrayColor(), for: .normal)
        refreshView()
    }
    
    func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        //        label.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(CustomColor.textBlackColor(), for: .normal)
        refreshView()
    }
    
    func configureCell(_ classRef: AnyObject) {
        self.classRefs = classRef
        
        picker!.delegate = self
        picker!.dataSource = self
    }
    
    func setBrandName(_ brandName: String){
        editMBrand.text = brandName
    }
    func getBrandName() -> String {
        return  editMBrand.text!.trimmed
    }
    
    func setGenericName(_ genericName: String){
        editMGeneric.text = genericName
    }
    func getGenericName() ->  String{
        return  editMGeneric.text!.trimmed
    }
    
    func setDose(_ dose: String) {
        if !dose.trimmed.isEmpty {
            let doseArr = dose.split(" ")
            editMdose.text = doseArr[0]
            configureSelectButton(btnMmg, setString: doseArr[1])
            configureSelectButton(btnMtimes, setString: doseArr[2])
        }
    }
    func getDose() -> String {
    
        return  editMdose.text!.trimmed + " "
            + btnMmg.titleLabel!.text!.trimmed + " "
            + btnMtimes.titleLabel!.text!.trimmed
    }
    
    
    
    func setDuration(_ dura: String){
        if !dura.trimmed.isEmpty {
            let duraArr = dura.split(" ")
            editMduration.text = duraArr[0]
            configureSelectButton(btnMDay, setString: duraArr[1])
        }
    }
    
    func getDuration() -> String {
        return  editMduration.text!.trimmed + " "
        + btnMDay.titleLabel!.text!.trimmed
    }
    
    func refreshView() {
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
    
    @objc func btnMmg1() {
        picker.isHidden = false
//        UIApplication.shared.keyWindow!.bringSubview(toFront: picker)
        type = "mg"
        picker.reloadAllComponents()
    }
    @objc func btnMtimes1() {
        picker.isHidden = false
//        UIApplication.shared.keyWindow!.bringSubview(toFront: picker)
        type = "times"
        picker.reloadAllComponents()
    }
    @objc func btnMDay1() {
        picker.isHidden = false
//        UIApplication.shared.keyWindow!.bringSubview(toFront: picker)
        type = "day"
        picker.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if type == "mg" {
            return arrMg.count
        } else if type == "times" {
            return arrTimes.count
        } else if type == "day" {
            return arrDay.count
        } else {
            return 0
        }
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if type == "mg" {
            return arrMg[row]
        } else if type == "times" {
            return arrTimes[row]
        } else if type == "day" {
            return arrDay[row]
        } else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if type == "mg" {
            configureSelectButton(btnMmg, setString: arrMg[row])
        } else if type == "times" {
            configureSelectButton(btnMtimes, setString: arrTimes[row])
        } else if type == "day" {
            configureSelectButton(btnMDay, setString: arrDay[row])
        }
    }
    
    @objc func dismissView() {
        picker.isHidden = true
    }
}
