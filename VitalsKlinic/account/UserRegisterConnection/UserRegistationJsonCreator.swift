class UserRegistationJsonCreator {
    
    var err: AutoreleasingUnsafeMutablePointer<NSError?>? = nil
        
    func storeDefaultJson(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
        } catch {
            appPrefs.setErrorCode(text: 0)
            appPrefs.setErrorMessage(text: "ServerNotWork".lowercased())
        }
    }
    
    func putJsonLogin() -> Data {
        
        let appUserInfo = MySharedPreferences()
        
        var refCode = appUserInfo.getTempDrRefCode()
        if appUserInfo.getTempDrRefCode().isEmpty {
            refCode = "123DEV"
        }
        
        let data: Dictionary<String, String> =
            ["email": appUserInfo.getEmailID(),
             "mobileNo": appUserInfo.getMobile(),
             "deviceID": appUserInfo.getDeviceID(),
             "osType": "IOS",
             "loginType": appUserInfo.getLoginType(),
             "refCode": refCode
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
    
        return json;
    }
    
    func storeLoginJson(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
            if appPrefs.getErrorCode() == 200 {
                let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                   appPrefs.setVerificationCode(text: JsonParser.getJsonValueString(dataJson, valueForKey: "text_OTP"))
                appPrefs.setDrRefCode(text: JsonParser.getJsonValueString(dataJson, valueForKey: "refCode"))
                
                
                let userDetailJson = JsonParser.getJsonValueMutableDictionary(dataJson, valueForKey: "patientInfo")
                
                appPrefs.setCompanyID(text: JsonParser.getJsonValueString(userDetailJson, valueForKey: "companyID"))
                
                appPrefs.setIsProfileHealthCmpt(text: true)
                appPrefs.setFirstName(text: JsonParser.getJsonValueString(userDetailJson, valueForKey: "fName"))
                appPrefs.setLastName(text: JsonParser.getJsonValueString(userDetailJson, valueForKey: "lName"))
                appPrefs.setTempUserID(text: Int(JsonParser.getJsonValueString(userDetailJson, valueForKey: "patientID"))!)
                
                appPrefs.setOtherSource(text: JsonParser.getJsonValueString(userDetailJson, valueForKey: "other_source"))
                appPrefs.setKnowAbout(text: JsonParser.getJsonValueString(userDetailJson, valueForKey: "know_about"))
                
                appPrefs.setDOB(text: JsonParser.getJsonValueString(userDetailJson, valueForKey: "dob"))
                appPrefs.setGender(text: JsonParser.getJsonValueString(userDetailJson, valueForKey: "gender"))
                appPrefs.setEmailID(text: JsonParser.getJsonValueString(userDetailJson, valueForKey: "email"))
                appPrefs.setMobile(text: JsonParser.getJsonValueString(userDetailJson, valueForKey: "phone"))
                
                var verifyFlag = JsonParser.getJsonValueString(userDetailJson, valueForKey: "verifyFlag")
                if(verifyFlag == "null" || verifyFlag == "na")
                {
                    verifyFlag = "0"
                }
                appPrefs.setIsEmailVerifiCmpt(text: verifyFlag == "0" ? false : true)
                
                //get licence info of user
                let licInfo = JsonParser.getJsonValueMutableDictionary(userDetailJson, valueForKey: "licInfo")
                let voucherID = JsonParser.getJsonValueString(licInfo, valueForKey: "key")
                var startDate = JsonParser.getJsonValueString(licInfo, valueForKey: "startDate")
                var endDate = JsonParser.getJsonValueString(licInfo, valueForKey: "endDate")
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                if !startDate.isEmpty {
                    startDate = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: startDate)!))"
                    appPrefs.setPlanStartDate(text: Double(startDate)!)
                }
                if !endDate.isEmpty {
                    endDate = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: endDate)!))"
                    appPrefs.setPlanEndDate(text: Double(endDate)!)
                }
                
                appPrefs.setPlanVoucherKey(text: voucherID)
                appPrefs.setIsPremiumPlanActive(text: true)
                appPrefs.setPlanDuration(text: appPrefs.getPlanDuration() + " month");
                
                
                let planName = JsonParser.getJsonValueString(licInfo, valueForKey: "planName")
                let duration = JsonParser.getJsonValueString(licInfo, valueForKey: "duration")
                let measure = JsonParser.getJsonValueString(licInfo, valueForKey: "measure")
                
                appPrefs.setPlanName(text: planName)
                
                if(measure == "d") {
                    appPrefs.setPlanDuration(text: duration + " days")
                } else if(measure == "m") {
                    appPrefs.setPlanDuration(text: duration + " month")
                } else if(measure == "y") {
                    appPrefs.setPlanDuration(text: duration + " year")
                }
                
                appPrefs.setPlanFeaturesList(text: JsonParser.getJsonValueString(licInfo, valueForKey: "features"))
                
            } else if appPrefs.getErrorCode() == 202 {
                //for register new user
                let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                appPrefs.setVerificationCode(text: JsonParser.getJsonValueString(dataJson, valueForKey: "text_OTP"))
                appPrefs.setRegUserType(text: JsonParser.getJsonValueString(dataJson, valueForKey: "regUserType"))
                appPrefs.setUpdateReqType(text: Constant.AccountMng.MethodRegister)
            } else if appPrefs.getErrorCode() == 203 {
                //for register in company user, already register, switching
                let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                appPrefs.setVerificationCode(text: JsonParser.getJsonValueString(dataJson, valueForKey: "text_OTP"))
                appPrefs.setRegUserType(text: JsonParser.getJsonValueString(dataJson, valueForKey: "regUserType"))
                appPrefs.setUpdateReqType(text: Constant.AccountMng.MethodRegisterInCompany)
                
                let patientInfo = JsonParser.getJsonValueMutableDictionary(dataJson, valueForKey: "patientInfo")
                appPrefs.setTempUserID(text: Int(JsonParser.getJsonValueString(patientInfo, valueForKey: "patientID"))!)
                appPrefs.setFirstName(text: JsonParser.getJsonValueString(patientInfo, valueForKey: "fName"))
                appPrefs.setLastName(text: JsonParser.getJsonValueString(patientInfo, valueForKey: "lName"))
                
                if(appPrefs.getEmailID().isEmpty) {
                    appPrefs.setEmailID(text: JsonParser.getJsonValueString(patientInfo, valueForKey: "email"))
                }
                if(appPrefs.getMobile().isEmpty) {
                    appPrefs.setMobile(text: JsonParser.getJsonValueString(patientInfo, valueForKey: "mobileNo"))
                }
                
                appPrefs.setDOB(text: JsonParser.getJsonValueString(patientInfo, valueForKey: "dob"))
                appPrefs.setGender(text: JsonParser.getJsonValueString(patientInfo, valueForKey: "gender"))
            }
        } catch {
            appPrefs.setErrorCode(text: 0)
            appPrefs.setErrorMessage(text: "ServerNotWork".lowercased())
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonRegister() -> Data {
    
        let appUserInfo = MySharedPreferences()
    
        var refCode = appUserInfo.getTempDrRefCode()
        if appUserInfo.getTempDrRefCode().isEmpty {
            refCode = "123DEV"
        }
        
        var data = NSMutableDictionary()
        data =
            ["patientID": appUserInfo.getTempUserID(),
             "fName": appUserInfo.getFirstName(),
             "lName": appUserInfo.getLastName(),
             "osType": "IOS",
             "email": appUserInfo.getEmailID(),
             "countryCode": appUserInfo.getCountryCode(),
             "mobileNo": appUserInfo.getMobile(),
             "refCode": refCode,
             
             "gender": appUserInfo.getGender(),
             "dob": appUserInfo.getDOB(),
             
             "manufacturer": "Apple",
             "model": appUserInfo.getMobileModel(),
             "osVersion": "10.0.1",
             "deviceID": appUserInfo.getDeviceID()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeRegisterJson(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
            if appPrefs.getErrorCode() == 200 {
                let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                appPrefs.setCompanyID(text: JsonParser.getJsonValueString(dataJson, valueForKey: "companyID"))
                appPrefs.setUserID(text: Int(JsonParser.getJsonValueString(dataJson, valueForKey: "patientID"))!)
                
                appPrefs.setDrRefCode(text: appPrefs.getTempDrRefCode())
            }
        } catch {}
    }
    
    // create JSON values to send it to server
    func putJsonUpdateProfile() -> Data {
        
        let appUserInfo = MySharedPreferences()
        
        var data = NSMutableDictionary()
        data =
            ["patientID": appUserInfo.getTempUserID(),
             "refCode": appUserInfo.getDrRefCode(),
             "osType": "IOS",
             "companyID": appUserInfo.getCompanyID(),
             "updateType": appUserInfo.getUpdateReqType(),
             "manufacturer": "Apple",
             "model": appUserInfo.getMobileModel(),
             "osVersion": "10.0.1",
             "deviceID": appUserInfo.getDeviceID()
         ]
        if appUserInfo.getUpdateReqType().equalsIgnoreCase("secondaryInfo") {
            data.setValue(appUserInfo.getAPNToken(), forKey: "apn")
            
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpPrimaryInfo".localized) {
            data.setValue(appUserInfo.getFirstName(), forKey: "fName")
            data.setValue(appUserInfo.getLastName(), forKey: "lName")
            data.setValue(appUserInfo.getGender(), forKey: "gender")
            data.setValue(appUserInfo.getDOB(), forKey: "dob")
            data.setValue(appUserInfo.getEmailID(), forKey: "email")
            
            data.setValue(appUserInfo.getKnowAbout(), forKey: "know_about")
            data.setValue(appUserInfo.getOtherSource(), forKey: "other_source")
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpBasicInfo".localized) {
            let datasource = DBOperation();
            datasource.openDatabase(false)
            let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
                + " WHERE " + datasource.dbGroupType + " = '" + appUserInfo.getUpdateReqType() + "' ")
            
            if cursor != nil {
                while cursor!.next() {
                    data.setValue(cursor!.string(forColumnIndex: 1), forKey: cursor!.string(forColumnIndex: 0))
                }
            }
            
            cursor?.close();
            datasource.closeDatabase();
            
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpMedicalHistory".localized) {
            
            let datasource = DBOperation();
            datasource.openDatabase(false)
            let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
                + " WHERE " + datasource.dbGroupType + " = '" + appUserInfo.getUpdateReqType() + "' ")
            
            if cursor != nil {
                while cursor!.next() {
                    data.setValue(cursor!.string(forColumnIndex: 1), forKey: cursor!.string(forColumnIndex: 0))
                }
            }
            cursor?.close();
            datasource.closeDatabase();

        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpFamilyHistory".localized) {
            
            let datasource = DBOperation();
            datasource.openDatabase(false)
            let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
                + " WHERE " + datasource.dbGroupType + " = '" + appUserInfo.getUpdateReqType() + "' ")
            
            if cursor != nil {
                while cursor!.next() {
                    data.setValue(cursor!.string(forColumnIndex: 1), forKey: cursor!.string(forColumnIndex: 0))
                }
            }
            cursor?.close();
            datasource.closeDatabase();
            
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpPersonalHistory".localized) {
            
            let datasource = DBOperation();
            datasource.openDatabase(false)
            let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
                + " WHERE " + datasource.dbGroupType + " = '" + appUserInfo.getUpdateReqType() + "' ")
            
            if cursor != nil {
                while cursor!.next() {
                    data.setValue(cursor!.string(forColumnIndex: 1), forKey: cursor!.string(forColumnIndex: 0))
                }
            }
            cursor?.close();
            datasource.closeDatabase();
            
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpMenstrualHistory".localized) {
            
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpStressHistory".localized) {
            let datasource = DBOperation();
            datasource.openDatabase(false)
            let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
                + " WHERE " + datasource.dbGroupType + " = '" + appUserInfo.getUpdateReqType() + "' ")
            
            if cursor != nil {
                while cursor!.next() {
                    data.setValue(cursor!.string(forColumnIndex: 1), forKey: cursor!.string(forColumnIndex: 0))
                }
            }
            cursor?.close();
            datasource.closeDatabase();
            
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpProfessionalHistory".localized) {
            
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpPainHistory".localized) {
            
            let datasource = DBOperation();
            datasource.openDatabase(false)
            let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
                + " WHERE " + datasource.dbGroupType + " = '" + appUserInfo.getUpdateReqType() + "' ")
            
            if cursor != nil {
                while cursor!.next() {
                    data.setValue(cursor!.string(forColumnIndex: 1), forKey: cursor!.string(forColumnIndex: 0))
                }
            }
            cursor?.close();
            datasource.closeDatabase();
            
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpPersonality".localized) {
            let datasource = DBOperation();
            datasource.openDatabase(false)
            let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
                + " WHERE " + datasource.dbGroupType + " = '" + appUserInfo.getUpdateReqType() + "' ")
            
            if cursor != nil {
                while cursor!.next() {
                    data.setValue(cursor!.string(forColumnIndex: 1), forKey: cursor!.string(forColumnIndex: 0))
                }
            }
            cursor?.close();
            datasource.closeDatabase();
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpDailyLifestyle".localized) {
            let datasource = DBOperation();
            datasource.openDatabase(false)
            let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
                + " WHERE " + datasource.dbGroupType + " = '" + appUserInfo.getUpdateReqType() + "' ")
            
            if cursor != nil {
                while cursor!.next() {
                    data.setValue(cursor!.string(forColumnIndex: 1), forKey: cursor!.string(forColumnIndex: 0))
                }
            }
            cursor?.close();
            datasource.closeDatabase();
        } else if appUserInfo.getUpdateReqType().equalsIgnoreCase("assGrpYogaHistory".localized) {
            let datasource = DBOperation();
            datasource.openDatabase(false)
            let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
                + " WHERE " + datasource.dbGroupType + " = '" + appUserInfo.getUpdateReqType() + "' ")
            
            if cursor != nil {
                while cursor!.next() {
                    data.setValue(cursor!.string(forColumnIndex: 1), forKey: cursor!.string(forColumnIndex: 0))
                }
            }
            cursor?.close();
            datasource.closeDatabase();
        }
        
        Print.printLog("send Json : \(data)")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeJsonUpdateProfile(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
            if appPrefs.getErrorCode() == 200 {
                if appPrefs.getUpdateReqType().equalsIgnoreCase("secondaryInfo") {
                    appPrefs.setIsAPNUpdateOnServer(true)
                }
            }
        } catch {
            appPrefs.setErrorCode(text: 0)
            appPrefs.setErrorMessage(text: "ServerNotWork".lowercased())
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonVerifyCorpEmail() -> Data {
        
        let appUserInfo = MySharedPreferences()
        
        var refCode = appUserInfo.getTempDrRefCode()
        if appUserInfo.getTempDrRefCode().isEmpty {
            refCode = "123DEV"
        }
        
        var data = NSMutableDictionary()
        data =
            ["refCode": refCode,
             "osType": "IOS",
             "email": appUserInfo.getEmailID(),
             "manufacturer": "Apple",
             "model": appUserInfo.getMobileModel(),
             "osVersion": "10.0.1",
             "deviceID": appUserInfo.getDeviceID()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeVerifyCorpEmail(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
            if appPrefs.getErrorCode() == 200 {
                let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                appPrefs.setVerificationCode(text: JsonParser.getJsonValueString(dataJson, valueForKey: "text_OTP"))
                appPrefs.setLoginType(text: "EMAIL")
            }
        } catch {}
    }
    
    // create JSON values to send it to server
    func putJsonVerifyMobileEmail() -> Data {
        
        let appUserInfo = MySharedPreferences()
        
        var refCode = appUserInfo.getTempDrRefCode()
        if appUserInfo.getTempDrRefCode().isEmpty {
            refCode = "123DEV"
        }
        
        var data = NSMutableDictionary()
        data =
            ["refCode": refCode,
             "osType": "IOS",
             "email": appUserInfo.getEmailID(),
             "mobileNo": appUserInfo.getMobile(),
             "patientID": appUserInfo.getTempUserID(),
             "loginType": appUserInfo.getLoginType(),
             "manufacturer": "Apple",
             "model": appUserInfo.getMobileModel(),
             "osVersion": "10.0.1",
             "deviceID": appUserInfo.getDeviceID()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeVerifyMobileEmail(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
            if appPrefs.getErrorCode() == 200 {
                let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                appPrefs.setVerificationCode(text: JsonParser.getJsonValueString(dataJson, valueForKey: "text_OTP"))
            }
        } catch {}
    }
    
    // create JSON values to send it to server
    func putJsonRegistrationCheck() -> Data {
        
        let appUserInfo = MySharedPreferences()
        
        var refCode = appUserInfo.getTempDrRefCode()
        if appUserInfo.getTempDrRefCode().isEmpty {
            refCode = "123DEV"
        }
        
        var data = NSMutableDictionary()
        data =
            ["refCode": refCode,
             "osType": "IOS",
             "email": appUserInfo.getEmailID(),
             "manufacturer": "Apple",
             "model": appUserInfo.getMobileModel(),
             "osVersion": "10.0.1",
             "deviceID": appUserInfo.getDeviceID(),
             "companyID": appUserInfo.getCompanyID(),
                "name": appUserInfo.getFirstName()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsonRegistrationCheck(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
        } catch {}
    }

    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsonForgotPassword(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
            if appPrefs.getErrorCode() == 200 {
                appPrefs.setVerifyOTP(text: JsonParser.getJsonValueString(dict, valueForKey: "OTP"))
                appPrefs.setUserID(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "userID"))!)
            }
            
        } catch {}
    }
    
    // create JSON values to send it to server
    func putJsonForgotPasswordNewSet() -> Data {
        
        let appUserInfo = MySharedPreferences()
        
        var refCode = appUserInfo.getTempDrRefCode()
        if appUserInfo.getTempDrRefCode().isEmpty {
            refCode = "123DEV"
        }
        
        var data = NSMutableDictionary()
        data =
            ["refCode": refCode,
             "osType": "IOS",
             "email": appUserInfo.getEmailID(),
             "manufacturer": "Apple",
             "model": appUserInfo.getMobileModel(),
             "osVersion": "10.0.1",
             "companyID": appUserInfo.getCompanyID(),
             "userID": appUserInfo.getUserID(),
             "phonebookID": appUserInfo.getPhonebookID()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsonVerifyEmail(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
            if appPrefs.getErrorCode() == 200 {
                appPrefs.setVerifyOTP(text: JsonParser.getJsonValueString(dict, valueForKey: "OTP"))
            }
            
        } catch {}
    }
    
    // create JSON values to send it to server
    func putJsonSplashScreen() -> Data {
        
        let appUserInfo = MySharedPreferences()
        
        var refCode = appUserInfo.getTempDrRefCode()
        if appUserInfo.getTempDrRefCode().isEmpty {
            refCode = "123DEV"
        }
        
        var data = NSMutableDictionary()
        data =
            ["refCode": refCode,
             "osType": "IOS",
             "email": appUserInfo.getEmailID(),
             "manufacturer": "Apple",
             "model": appUserInfo.getMobileModel(),
             "osVersion": "10.0.1",
             "companyID": appUserInfo.getCompanyID(),
             "userID": appUserInfo.getUserID(),
             "phonebookID": appUserInfo.getPhonebookID(),
             "deviceID": appUserInfo.getDeviceID(),
             "key": appUserInfo.getPlanVoucherKey()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeJsonSplashScreen(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
            if appPrefs.getErrorCode() == 200 {
                
                //licence check
                if !appPrefs.getPlanVoucherKey().isEmpty {
                    let licStatus = JsonParser.getJsonValueString(dict, valueForKey: "licStatus")
                    if licStatus.equalsIgnoreCase("0") {
                        //                    CustomAdapterNavigDrawer.expirePlan(appPrefs);
                    }
                }
                
                //check server app version
                let versionCode = Int(JsonParser.getJsonValueString(dict, valueForKey: "versionCode"))
                appPrefs.setServerCurrentVersionCode(text: versionCode!)
                
                //get server device id
                
                var deviceID = JsonParser.getJsonValueString(dict, valueForKey: "deviceID")
                if deviceID.equalsIgnoreCase("NA") || deviceID.equalsIgnoreCase("null") {
                    deviceID = ""
                }
                
                appPrefs.setServerPhoneDeviceID(text: deviceID)
            }
            
        } catch {}
    }
    
    
    
    // create JSON values to send it to server
    func putJsonUpdateEmailVerify() -> Data {
        
        let appUserInfo = MySharedPreferences()
        
        var refCode = appUserInfo.getTempDrRefCode()
        if appUserInfo.getTempDrRefCode().isEmpty {
            refCode = "123DEV"
        }
        
        var data = NSMutableDictionary()
        data =
            ["refCode": refCode,
             "osType": "IOS",
             "manufacturer": "Apple",
             "model": appUserInfo.getMobileModel(),
             "osVersion": "10.0.1",
             "companyID": appUserInfo.getCompanyID(),
             "userID": appUserInfo.getUserID(),
             "phonebookID": appUserInfo.getPhonebookID(),
             "verifyFlag": "1"
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
        // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsstoreJsonUpdateEmailVerifyonVerifyEmail(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
            if appPrefs.getErrorCode() == 200 {
                appPrefs.setIsEmailVerifiCmpt(text: true)
            }
            
        } catch {}
    }
    
    func putJsonSyncProfile() -> Data {
        
        let appUserInfo = MySharedPreferences()
        
        var refCode = appUserInfo.getTempDrRefCode()
        if appUserInfo.getTempDrRefCode().isEmpty {
            refCode = "123DEV"
        }
        
        var data = NSMutableDictionary()
        data =
            ["refCode": refCode,
             "osType": "IOS",
             "manufacturer": "Apple",
             "model": appUserInfo.getMobileModel(),
             "osVersion": "10.0.1",
             "companyID": appUserInfo.getCompanyID(),
             "deviceID": appUserInfo.getDeviceID(),
             "phonebookID": appUserInfo.getPhonebookID(),
             "verifyFlag": "1",
             "phonebookID": appUserInfo.getPhonebookID(),
             "updateType": appUserInfo.getUpdateReqType(),
             "patientID": appUserInfo.getUserID()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    
    func storeJsonSyncProfile(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
            
            if appPrefs.getErrorCode() == 200 {
                if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpPrimaryInfo".localized) {
                    
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpBasicInfo".localized) {
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    datasource.UpdateAssessmentItemTable(paramName: "blood_group", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "blood_group"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "body_shape", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "body_shape"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "bone_mass", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "bone_mass"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "fat", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "fat"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "water", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "water"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "metabolic_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "metabolic_age"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "height", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "height"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "weight", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "weight"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "hip", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "hip"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "waist", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "waist"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "chest", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "chest"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "visceral_fat", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "visceral_fat"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "lifestyle", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "lifestyle"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "lean_body_mass", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "lean_body_mass"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "waist", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "waist"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "bmi", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "bmi"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "bmr", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "bmr"), groupType: appPrefs.getUpdateReqType())

                    datasource.closeDatabase()
                    
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpMedicalHistory".localized) {
                    
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    
                    //`updateType`,`companyID`, `patientID`,
                    datasource.UpdateAssessmentItemTable(paramName: "major_complaint1", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "major_complaint1"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "major_since1", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "major_since1"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "major_complaint2", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "major_complaint2"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "major_since2", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "major_since2"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "major_complaint3", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "major_complaint3"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "major_since3", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "major_since3"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "major_complaint4", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "major_complaint4"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "major_since4", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "major_since4"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "minor_complaint1", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "minor_complaint1"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "minor_since1", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "minor_since1"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "minor_complaint2", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "minor_complaint2"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "minor_since2", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "minor_since2"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "minor_complaint3", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "minor_complaint3"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "minor_since3", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "minor_since3"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "minor_complaint4", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "minor_complaint4"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "minor_since4", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "minor_since4"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "past_illness", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "past_illness"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "prev_hospitalization", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "prev_hospitalization"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "surgery", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "surgery"), groupType: appPrefs.getUpdateReqType())
                
                    datasource.closeDatabase();
                    
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpFamilyHistory".localized) {
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    datasource.UpdateAssessmentItemTable(paramName: "disease1", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "disease1"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "disease2", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "disease2"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "disease3", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "disease3"), groupType: appPrefs.getUpdateReqType())
                    
                    datasource.closeDatabase();
                    
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpPersonalHistory".localized) {
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    datasource.UpdateAssessmentItemTable(paramName: "diet_preference", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "diet_preference"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "ethnicity", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "ethnicity"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "eth_other_Details", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "eth_other_Details"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "bowel_movement", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "bowel_movement"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "bowel_other_details", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "bowel_other_details"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "micturition", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "micturition"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "appetite_digestion", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "appetite_digestion"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_food1", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_food1"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_food_since1", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_food_since1"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_food2", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_food2"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_food_since2", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_food_since2"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_food3", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_food3"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_food_since3", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_food_since3"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_food4", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_food4"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_food_since4", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_food_since4"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_food5", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_food5"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_food_since5", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_food_since5"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_drug1", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_drug1"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_drug_since1", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_drug_since1"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_drug2", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_drug2"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_drug_since2", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_drug_since2"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_drug3", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_drug3"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_drug_since3", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_drug_since3"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_drug4", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_drug4"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_drug_since4", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_drug_since4"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_drug5", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_drug5"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "allergy_drug_since5", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "allergy_drug_since5"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "family_type", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "family_type"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "household_people", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "household_people"), groupType: appPrefs.getUpdateReqType())
                    
                    datasource.closeDatabase();
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpMenstrualHistory".localized) {
                    
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    datasource.UpdateAssessmentItemTable(paramName: "menstruation_status", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "menstruation_status"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "menopause_since", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "menopause_since"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "frequency", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "frequency"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "last_menstruation", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "last_menstruation"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "painful_menstruation", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "painful_menstruation"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "excess_flow", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "excess_flow"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "missed_periods", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "missed_periods"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "menstrual_syndrome", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "menstrual_syndrome"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "pregnancy_status", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "pregnancy_status"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "abortions", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "abortions"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children_count", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children_count"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children1_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children1_age"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children2_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children2_age"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children3_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children3_age"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children4_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children4_age"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children5_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children5_age"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children6_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children6_age"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children7_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children7_age"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children8_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children8_age"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children9_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children9_age"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "children10_age", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "children10_age"), groupType: appPrefs.getUpdateReqType())
                
                    datasource.closeDatabase()
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpStressHistory".localized) {
                    
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    datasource.UpdateAssessmentItemTable(paramName: "physical_stress_duration", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "physical_stress_duration"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "physical_stress_source", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "physical_stress_source"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "physical_stress_cause", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "physical_stress_cause"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "physical_stress_trigger", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "physical_stress_trigger"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "physical_stress_grade", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "physical_stress_grade"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "physical_stress_rate", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "physical_stress_rate"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "mental_stress_duration", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "mental_stress_duration"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "mental_stress_source", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "mental_stress_source"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "mental_stress_cause", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "mental_stress_cause"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "mental_stress_trigger", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "mental_stress_trigger"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "mental_stress_grade", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "mental_stress_grade"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "mental_stress_rate", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "mental_stress_rate"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_duration", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "emotional_stress_duration"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_source", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "emotional_stress_source"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_cause", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "emotional_stress_cause"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_trigger", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "emotional_stress_trigger"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "emotional_stress_grade", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "emotional_stress_grade"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "social_stress_duration", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "social_stress_duration"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "social_stress_source", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "social_stress_source"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "social_stress_cause", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "social_stress_cause"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "social_stress_trigger", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "social_stress_trigger"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "social_stress_grade", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "social_stress_grade"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "social_stress_rate", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "social_stress_rate"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "religious_stress_duration", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "religious_stress_duration"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "religious_stress_source", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "religious_stress_source"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "religious_stress_cause", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "religious_stress_cause"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "religious_stress_trigger", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "religious_stress_trigger"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "religious_stress_grade", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "religious_stress_cause"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "religious_stress_rate", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "religious_stress_trigger"), groupType: appPrefs.getUpdateReqType())
                    
                    datasource.closeDatabase();
                    
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpProfessionalHistory".localized) {
                    
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpPainHistory".localized) {
                    
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    datasource.UpdateAssessmentItemTable(paramName: "aches_severity", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "aches_severity"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "aches_since", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "aches_since"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "head_neck_severity", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "head_neck_severity"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "head_neck_since", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "head_neck_since"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "upper_limb_severity", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "upper_limb_severity"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "upper_limb_since", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "upper_limb_since"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "lower_limb_severity", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "lower_limb_severity"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "lower_limb_since", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "lower_limb_since"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "chest_severity", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "chest_severity"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "chest_since", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "chest_since"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "back_severity", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "back_severity"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "back_since", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "back_since"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "abdomen_severity", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "abdomen_severity"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "abdomen_since", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "abdomen_since"), groupType: appPrefs.getUpdateReqType())
                    
                    datasource.closeDatabase();
                    
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpPersonality".localized) {
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    
                    datasource.UpdateAssessmentItemTable(paramName: "psychological_assessment", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "psychological_assessment"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "yogic_analysis", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "yogic_analysis"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "ayurvedic_body", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "ayurvedic_body"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "personal_temperament", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "personal_temperament"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "personal_temperament_other", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "personal_temperament_other"), groupType: appPrefs.getUpdateReqType())
                   
                    datasource.closeDatabase()
                    
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpDailyLifestyle".localized) {
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    
                    datasource.UpdateAssessmentItemTable(paramName: "wake_up", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "wake_up"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "breakfast", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "breakfast"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "lunch", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "lunch"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "evening_snack", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "evening_snack"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "dinner", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "dinner"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "sleep", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "sleep"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "smoking_since", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "smoking_since"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "alcohol", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "alcohol"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "eatout_preferences", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "eatout_preferences"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "eat_fruits_daily", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "eat_fruits_daily"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "diet_dry_fruits", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "diet_dry_fruits"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "eat_vegetables", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "eat_vegetables"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "likes", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "likes"), groupType: appPrefs.getUpdateReqType())
                    
                    datasource.closeDatabase();
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpReikiHistory".localized) {
                    
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    
                    datasource.UpdateAssessmentItemTable(paramName: "under_physician", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "under_physician"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "reiki_session", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "reiki_session"), groupType: appPrefs.getUpdateReqType())

                    datasource.UpdateAssessmentItemTable(paramName: "last_session", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "last_session"), groupType: appPrefs.getUpdateReqType())

                    datasource.UpdateAssessmentItemTable(paramName: "previous_session", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "previous_session"), groupType: appPrefs.getUpdateReqType())

                    datasource.UpdateAssessmentItemTable(paramName: "concern_area", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "concern_area"), groupType: appPrefs.getUpdateReqType())

                    datasource.UpdateAssessmentItemTable(paramName: "sensitive_perfumes", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "sensitive_perfumes"), groupType: appPrefs.getUpdateReqType())

                    datasource.UpdateAssessmentItemTable(paramName: "sensitive_touch", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "sensitive_touch"), groupType: appPrefs.getUpdateReqType())

                    datasource.closeDatabase();
                    
                } else if appPrefs.getUpdateReqType().equalsIgnoreCase("assGrpYogaHistory".localized) {
                    
                    let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    
                    datasource.UpdateAssessmentItemTable(paramName: "learning_yoga", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "learning_yoga"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "practiced_before", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "practiced_before"), groupType: appPrefs.getUpdateReqType())
                    datasource.UpdateAssessmentItemTable(paramName: "current_injuries", paramVal: JsonParser.getJsonValueString(dataJson, valueForKey: "current_injuries"), groupType: appPrefs.getUpdateReqType())
                    
                    datasource.closeDatabase()
                    
                }
                    
            }
            
        } catch {}
    }
    
    // create JSON values to send it to server
    func putJsonFeedback() -> Data {
        
        let appUserInfo = MySharedPreferences()
        
        let data: Dictionary<String, String> =
            ["userID": "\(appUserInfo.getUserID())",
             "deviceID": appUserInfo.getDeviceID(),
             "osType": "IOS",
             "feedbackMsg": appUserInfo.getUserFeedbackMsg(),
             "companyID": appUserInfo.getCompanyID()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json;
    }
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeFeedbackJson(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            appPrefs.setErrorCode(text: 0)
            appPrefs.setErrorCode(text: Int(JsonParser.getJsonValueString(dict, valueForKey: "code"))!)
            appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
        } catch {
            appPrefs.setErrorCode(text: 0)
            
        }
    }
}
