//
//  RegistrationVC.swift
//  Salk
//
//  Created by Chetan  on 13/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import BEMCheckBox

class RegistrationVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var txtEmail: MKTextField!
    @IBOutlet weak var txtMobileNo: MKTextField!
    @IBOutlet weak var txtFirstName: MKTextField!
    @IBOutlet weak var txtLastName: MKTextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var stackEmail: UIStackView!
    @IBOutlet weak var stackMobile: UIStackView!
    @IBOutlet weak var stackCorpUser: UIStackView!
    
    @IBOutlet weak var switchCorpUser: BEMCheckBox!
    
    fileprivate var loadingObj = CustomLoading()
    fileprivate var prevLoginType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSubmit.backgroundColor = PrimaryColor.colorPrimaryDark()

        self.prepareNavigationItem()
        
        let appPref = MySharedPreferences()
        txtEmail.text = appPref.getEmailID()
        txtMobileNo.text = appPref.getMobile()
        txtFirstName.text = appPref.getFirstName()
        txtLastName.text = appPref.getLastName()
        
        initCheckBox(switchCorpUser)
        if(appPref.getRegUserType().caseInsensitiveCompare("ALL") == .orderedSame) {
            stackCorpUser.isHidden = false
        } else {
            stackCorpUser.isHidden = true
            
            if(appPref.getRegUserType().caseInsensitiveCompare("Corporate") == .orderedSame) {
                switchCorpUser.setOn(true, animated: true)
            }
        }
        
        prevLoginType = appPref.getLoginType()
        if(prevLoginType == "EMAIL") {
            stackEmail.isHidden = true
            stackMobile.isHidden = false
            appPref.setIsMobileVerifiCmpt(text: false)
        } else {
            stackEmail.isHidden = false
            stackMobile.isHidden = true
            appPref.setIsMobileVerifiCmpt(text: true);
        }
        
        let touch = UITapGestureRecognizer(target: self, action:#selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(touch)
    }
    
    fileprivate func initCheckBox(_ checkBox: BEMCheckBox) {
        
        checkBox.onAnimationType = BEMAnimationType.bounce
        checkBox.offAnimationType = BEMAnimationType.bounce
        checkBox.boxType = .square
        checkBox.lineWidth = 2
        checkBox.animationDuration = 0.5
        
        checkBox.tintColor = CustomColor.grayLight()
        checkBox.onTintColor = PrimaryColor.colorPrimary()
        checkBox.onFillColor = PrimaryColor.colorPrimary()
        checkBox.onCheckColor = UIColor.white
    }
    
    /// Prepares the navigationItem.
    private func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Create Profile"
        navigationItem.titleLabel.textColor = UIColor.black
        navigationItem.titleLabel.textAlignment = .left
        
        let backImage: UIImage = Icon.arrowBack!
        let backButton = IconButton()
        backButton.tintColor = UIColor.black
        backButton.setImage(backImage, for: UIControlState.normal)
        backButton.setImage(backImage, for: UIControlState.highlighted)
        backButton.addTarget(self, action: #selector(self.navBackButtunClick), for: .touchUpInside)
        
        navigationItem.leftViews = [backButton]
    }
    
    func navBackButtunClick() {
        let mainCon = MainViewController()
        mainCon.startLogin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    private func showSweetDialog(title: String, message: String) {
        SweetAlert().showOnlyAlert(title, subTitle: message, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    @IBAction func onClickSubmit(sender: UIButton) {
     
        self.dismissKeyboard()
        
        let email = txtEmail.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let mobileNo = txtMobileNo.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let fName = txtFirstName.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let lName = txtLastName.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if (email.isEmpty) {
            showSweetDialog(title: "Oops...", message: "Missing Email")
        } else if (fName.isEmpty) {
            showSweetDialog(title: "Oops...", message: "Missing First Name")
        } else if (lName.isEmpty) {
            showSweetDialog(title: "Oops...", message: "Missing Last Name")
        } else if (mobileNo.isEmpty) {
            showSweetDialog(title: "Oops...", message: "Missing Mobile Number")
        } else {
            
            let appPrefs = MySharedPreferences()
            appPrefs.setEmailID(text: email)
            appPrefs.setFirstName(text: fName)
            appPrefs.setLastName(text: lName)
            appPrefs.setCountryCode(text: "+91")
            
            if(mobileNo.starts(with: "+91")) {
                appPrefs.setMobile(text: mobileNo)
            } else {
                appPrefs.setMobile(text: "+91\(mobileNo)")
            }
            
            // check login on server
            let isInternet = CheckInternetConnection().isCheckInternetConnection()
            if isInternet {
                
                if(switchCorpUser.on) {
                    self.view.addSubview(self.loadingObj.loading())
                    UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodVerifyCorpEmail, classRef: self)
                } else {
                    if(prevLoginType == "EMAIL") {
                        appPrefs.setLoginType(text: "MOBILE")
                        
                        self.view.addSubview(self.loadingObj.loading())
                        UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodVerifyMobile, classRef: self)
                    } else {
                        self.view.addSubview(self.loadingObj.loading())
                        UserRegistationRequestManager().getRequest(appPrefs.getUpdateReqType(), classRef: self)
                    }
                }
            } else {
                self.showSweetDialog(title: "Oops...", message: "noInternet".localized)
            }
        }
    }
    
                             
    func stopProgressBar() {
        loadingObj.removeFromSuperview()
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        showSweetDialog(title: "Oops...", message: appPref.getErrorMessage())
    }
    
}
