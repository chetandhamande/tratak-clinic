//
//  CustomAdapterBookAppointList.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomAdapterBookAppointList: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var txt_aptStatus: UILabel!
    @IBOutlet weak var txt_timeRemain: UILabel!
    @IBOutlet weak var txt_special: UILabel!
    @IBOutlet weak var textHead: UILabel!
    @IBOutlet weak var txtAptTime: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnReSched: UIButton!
    
    private var mValues: Array<ArrAppointDetail>! = nil
    private var classRef: AnyObject! = nil
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    func onBindViewHolder(_ arrMainList: Array<ArrAppointDetail>, position: Int, classRef: AnyObject) {
    
        mValues = arrMainList
        self.classRef = classRef
        btnReSched.tag = position
        btnCancel.tag = position
        
        let user = mValues[position]
        if user.listLayoutIndex == 0 {
            //Add view to each list
            
            textHead.text = user.doctorName
            txt_special.text = user.speciality
            txtAptTime.text = user.date + ", " + user.appointTimeFrom
            
            if !user.timeDiff.contains("-") {
                txt_timeRemain.text = "in " + user.timeDiff
            } else {
                txt_timeRemain.text = "Closed"
            }
        }
    }
    
    //Code for reschedule
    @IBAction func bntReSchedClick(sender: UIButton) {
        
        let temp: ArrAppointDetail = mValues[sender.tag]
        
        let intent = NSMutableDictionary()
        intent.setValue(temp.specialityID, forKey: "sp_id")
        intent.setValue(temp.doctorID, forKey: "doc_id")
        intent.setValue(temp.doctorName, forKey: "name")
        intent.setValue(temp.speciality, forKey: "speciality")
        
        intent.setValue(temp.date, forKey: "date")
        intent.setValue(temp.appointTimeFrom, forKey: "fromTime")
        intent.setValue(temp.appointTimeTo, forKey: "toTime")
        intent.setValue(temp.aptID, forKey: "aptID")
        
        (classRef as! BookAppointListAct).moveToBookAppointTimeAct(intent)
    }
    
    //Code for cancel
    @IBAction func bntCancelClick(sender: UIButton) {
        let temp: ArrAppointDetail = mValues[sender.tag]
        //Code for reschedule
        let intent = NSMutableDictionary()
        intent.setValue(temp.doctorName, forKey: "name")
        intent.setValue(temp.speciality, forKey: "speciality")
        intent.setValue(temp.timeDiff, forKey: "timeDiff")
        
        intent.setValue(temp.date, forKey: "date")
        intent.setValue(temp.appointTimeFrom, forKey: "fromTime")
        intent.setValue(temp.appointTimeTo, forKey: "toTime")
        intent.setValue(temp.aptID, forKey: "aptID")
        
        (classRef as! BookAppointListAct).moveToBookAppointCancelAct(intent)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
