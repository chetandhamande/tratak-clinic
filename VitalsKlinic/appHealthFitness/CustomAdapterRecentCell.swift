//
//  CustomAdapterRecentCell.swift
//  Salk
//
//  Created by Chetan  on 01/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import SnapKit
import Material

class CustomAdapterRecentCell: UICollectionViewCell {

    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var customView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.masksToBounds = true
    }

}

extension CustomAdapterRecentCell {
    
    func onBindViewHolder(_ featureList: [ArrRecentAct], position: Int, classRef: AnyObject)
    {
        classRefs = classRef
        
        let user: ArrRecentAct = featureList[position]
        
        if user.layoutType == 0 {
            let headView = DailyRouteHeaderCell(frame: self.bounds)
            self.addSubview(headView)
            
            headView.textTypeName.text = user.details
            headView.textSubName.text = user.time
            headView.textDetails.text = user.heading
            
        } else if user.layoutType == 1 || user.layoutType == 2 {
            //Add view to each list
            let routeView = DailyRouteCell(frame: self.bounds)
            self.addSubview(routeView)
            
            routeView.textTypeName.text = user.heading
            routeView.textSubName.text = user.time
            routeView.textDetails.attributedText = user.details.html2AttributedString
            
            if user.imgResouceID != "" {
                routeView.imgDone.image = UIImage(named: user.imgResouceID)
            }
            
            if user.isTodaysDone {
//                routeView.imgDailyRoute.isHidden = false
                routeView.imgDone.image = UIImage(named: "verified_icon.png")
            } else {
//                routeView.imgDailyRoute.isHidden = true
            }
            //if(user.imgBackgourID != 0)
            //viewHolder.imgBackgroud.setImageResource(user.imgBackgourID);
        }
    }
}
