//
//  VideoPopupVC.swift
//  Salk
//
//  Created by Chetan  on 04/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import YouTubePlayer
import RAMAnimatedTabBarController

class VideoPopupVC: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    
    @IBOutlet var videoPlayer: YouTubePlayerView!
    
    @IBOutlet weak var mainCollectionView: UITableView!
    let CustomVideoIdentifier = "CustomVideoCell"
    var arrMainList = [ArrActivity]()
    
    var optionID = "", mainType = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareNavigationItem()
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Routine Details"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        navigationItem.detailLabel.text = mainType
        navigationItem.detailLabel.textColor = Color.lightGray
        navigationItem.detailLabel.textAlignment = .left
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var settingCell: CustomVideoCell! = mainCollectionView.dequeueReusableCell(withIdentifier: CustomVideoIdentifier) as? CustomVideoCell
        
        if settingCell == nil {
            mainCollectionView.register(UINib(nibName: CustomVideoIdentifier, bundle: nil), forCellReuseIdentifier: CustomVideoIdentifier)
            settingCell = tableView.dequeueReusableCell(withIdentifier: CustomVideoIdentifier) as? CustomVideoCell
        }
        
        settingCell!.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        return settingCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        performClickOpr(indexPath.row, tableCell: tableView.cellForRow(at: indexPath)!)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 72
    }
    
    
    fileprivate func loadData() {
        
        arrMainList = [ArrActivity]()
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
    
        var sql = ""
        if(mainType == "food") {
            sql = "SELECT " + datasource.dbOptionID + ", " + datasource.dbOptionName
                + ", " + datasource.dbIsChecked
                + ", (Select group_concat(" + datasource.dbActID + ", ';') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as optionVal "
                + ", (Select group_concat(" + datasource.dbQuantity + ", ';') "
                + " FROM "+datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as quaVal "
                + ", (Select group_concat(" + datasource.dbContainID + ", ';') "
                + " FROM "+datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE "+datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as containVal "
                + ", rD." + datasource.dbRecDesc + ", rDS." + datasource.dbVideoLink
                + ", rDS." + datasource.dbDays
                + " FROM " + datasource.recDashboardList_tlb + " rD, " + datasource.recDashboardSubList_tlb + " rDS "
                + " WHERE rD." + datasource.recID + " = rDS." + datasource.recID
                + " AND " + datasource.dbOptionID + " = '" + optionID + "' "
                + " GROUP BY " + datasource.dbOptionID
                + " ORDER BY " + datasource.dbRecName
        } else {
            sql = "SELECT " + datasource.dbOptionID + ", " + datasource.dbOptionName
                + ", " + datasource.dbIsChecked
                + ", (Select group_concat(" + datasource.dbActID + ", ';') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as optionVal "
                + ", (Select group_concat(" + datasource.dbQuantity + ", ';') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as quaVal "
                + ", rD." + datasource.dbRecDesc
                + ", (Select group_concat(" + datasource.dbVideoLink + ", ';') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as video "
                + ", rDS." + datasource.dbDays
                + " FROM " + datasource.recDashboardList_tlb + " rD, " + datasource.recDashboardSubList_tlb + " rDS "
                + " WHERE rD." + datasource.recID + " = rDS." + datasource.recID
                + " AND " + datasource.dbOptionID + " = '" + optionID + "' "
                + " GROUP BY " + datasource.dbOptionID
                + " ORDER BY " + datasource.dbRecName
        }
    
        var optionName = "", details = "", quantitys = "", videoLink = "", days = ""
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                optionName = cursor!.string(forColumnIndex: 1)
                
                var tempVal = cursor!.string(forColumnIndex: 3)!
                let actName = tempVal.components(separatedBy: ";")
                
                tempVal = cursor!.string(forColumnIndex: 4)
                let quantity = tempVal.components(separatedBy: ";")
                
                var val = ""
                if(mainType.uppercased() == "FOOD") {
                    let contain = cursor!.string(forColumnIndex: 6).components(separatedBy: ";")
                    details = cursor!.string(forColumnIndex: 6)
                    videoLink = cursor!.string(forColumnIndex: 7)
                    days = cursor!.string(forColumnIndex: 8)
                    
                    for i in 0 ..< actName.count {
                        var temp = ""
                        if i < actName.count {
                            temp = actName[i]
                        }
                        if i < quantity.count {
                            temp = actName[i] + " - " + quantity[i]
                        }
                        if i < contain.count {
                            temp = actName[i] + " - " + quantity[i] + " " + contain[i]
                        }
                        
                        val += val.isEmpty ? temp : ", " + temp
                    }
                } else {
                    details = cursor!.string(forColumnIndex: 5)
                    videoLink = cursor!.string(forColumnIndex: 6)
                    days = cursor!.string(forColumnIndex: 7)
                    
                    for i in 0 ..< actName.count {
                        var temp = ""
                        if i < actName.count {
                            temp = actName[i] + " min"
                            
                        }
                        if i < quantity.count {
                            temp = actName[i] + " - " + quantity[i] + " min"
                        }
                        
                        val += val.isEmpty ? temp : ", " + temp
                    }
                    
                }
                quantitys = val
            }
            cursor!.close()
        }
        datasource.closeDatabase()
    
    
        //show list items and its video link
        let heading = quantitys.components(separatedBy: ";")
        let videos = videoLink.components(separatedBy: ";")
        
        for i in 0 ..< heading.count {
            let temp = ArrActivity()
            temp.recSubName = heading[i]
            temp.videoLink = videos[i]
            arrMainList.append(temp)
        }

        if(arrMainList.count > 0) {
            mainCollectionView.reloadData()
            
            playVideo(videoLink: arrMainList[0].videoLink, title: arrMainList[0].recSubName)
        }
        
        lblHeading.text = optionName
        lblNote.attributedText = details.html2AttributedString
        
    }
    
    func playVideo(videoLink: String, title: String) {
    
        navigationItem.detailLabel.text = title
        navigationItem.detailLabel.textColor = Color.lightGray
        navigationItem.detailLabel.textAlignment = .left
    
        // Load video from YouTube URL
        let myVideoURL = NSURL(string: videoLink)
        videoPlayer.loadVideoURL(myVideoURL! as URL)
    }
    
}
