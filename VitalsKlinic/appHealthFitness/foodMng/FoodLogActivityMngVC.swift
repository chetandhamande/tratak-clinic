//
//  FoodLogActivityMngVC.swift
//  Salk
//
//  Created by Chetan  on 11/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class FoodLogActivityMngVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    fileprivate var backDateButton: UIBarButtonItem? = nil, nextDateButton: UIBarButtonItem? = nil
    
    fileprivate var currentDayDate = "0"
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    fileprivate var arrMainList = [ArrFoodSchedule]()
    let CustomFoodLogCellIdentifier = "CustomFoodLogCell"
    var offscreenCells = Dictionary<String, UICollectionViewCell>()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
        
        if currentDayDate != "0" {
            loadCurrentRecord()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        let nib = UINib(nibName: CustomFoodLogCellIdentifier, bundle: nil)
        mainCollectionView.register(nib, forCellWithReuseIdentifier: CustomFoodLogCellIdentifier)
        
        prepareNavigationItem()
        findDateNLoadRecord(isNextDay: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Today"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let backDateImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_chevron_left_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        backDateButton = UIBarButtonItem(image: backDateImage, style: .plain, target: self, action: #selector(self.findBackDate))
        
        let nextDateImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_chevron_right_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        nextDateButton = UIBarButtonItem(image: nextDateImage, style: .plain, target: self, action: #selector(self.findNextDate))
        
        navigationItem.rightBarButtonItems = [nextDateButton!, backDateButton!]
        
        self.mainCollectionView.backgroundColor = CustomColor.background()
        self.view.backgroundColor = CustomColor.background()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CustomFoodLogCell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomFoodLogCellIdentifier, for: indexPath) as! CustomFoodLogCell
        
        cell.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        // Make sure layout subviews
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //open vehical details list
        
        let user = arrMainList[indexPath.row]
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        let dateTimeLong = ConvertionClass().conDateToLong(df.date(from: (currentDayDate + " " + user.time))!)
        
        let a = SearchFoodVC(nibName: "SearchFoodVC", bundle: nil)
        a.scheduleName = user.scheduleName
        a.currDateTime = "\(dateTimeLong)"
        self.navigationController?.pushViewController(a, animated: true)
    }    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let orientation = UIApplication.shared.statusBarOrientation
        var cellWidth = mainCollectionView.frame.width
        
        // Use fake cell to calculate height
        let reuseIdentifier = CustomFoodLogCellIdentifier
        var cell: CustomFoodLogCell? = self.offscreenCells[reuseIdentifier] as? CustomFoodLogCell
        if cell == nil {
            cell = Bundle.main.loadNibNamed(CustomFoodLogCellIdentifier, owner: self, options: nil)![0] as? CustomFoodLogCell
            self.offscreenCells[reuseIdentifier] = cell
        }
        
        cell!.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        if traitCollection.horizontalSizeClass == .regular || traitCollection.verticalSizeClass == .regular
        {
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                if(orientation == .portrait) {
                    cellWidth = (cellWidth - 1) / 2
                } else {
                    cellWidth = (cellWidth - 5) / 3
                }
            } else {
                if(orientation == .portrait) {
                } else {
                    cellWidth = (cellWidth - 1) / 2
                }
            }
        } else {
            if(orientation == .portrait) {
            } else {
                cellWidth = (cellWidth - 1) / 2
            }
        }
        
        cell!.bounds = CGRect(x: 0, y: 0, width: cellWidth, height: cell!.bounds.height)
        cell!.contentView.bounds = cell!.bounds
        
        // Layout subviews, this will let labels on this cell to set preferredMaxLayoutWidth
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        
        var size = cell!.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        // Still need to force the width, since width can be smalled due to break mode of labels
        size.width = cellWidth
        return size
    }
}

extension FoodLogActivityMngVC {
    @objc fileprivate func findBackDate() {
        findDateNLoadRecord(isNextDay: false)
    }
    @objc fileprivate func findNextDate() {
        findDateNLoadRecord(isNextDay: true)
    }
    
    fileprivate func findDateNLoadRecord(isNextDay: Bool) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if(currentDayDate == "0") {
            currentDayDate = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter))"
        } else {
            
            let dateTimeStr = currentDayDate + " 12:00:10 AM"
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            let dateTimeLong = ConvertionClass().conDateToLong(dateFormatter.date(from: dateTimeStr)!)
            
            var value:Double = Double(dateTimeLong)
            if(isNextDay) {
                value += (60 * 60 * 24); // day
            } else {
                value -= (60 * 60 * 24); // day
            }
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            currentDayDate = "\(ConvertionClass().conLongToDate(value, dateFormat: dateFormatter))"
        }
        
        
        var selDate = currentDayDate
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let currDate = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter))"
        
        if(selDate == currDate) {
            selDate = "Today"
            
            nextDateButton?.isEnabled = false
        } else {
            nextDateButton?.isEnabled = true
            
            let dateTimeStr = currentDayDate + " 12:00:10 AM"
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            let dateTimeLong = ConvertionClass().conDateToLong(dateFormatter.date(from: dateTimeStr)!)
            
            dateFormatter.dateFormat = "dd MMMM"
            selDate = ConvertionClass().conLongToDate(dateTimeLong, dateFormat: dateFormatter)
            
        }
        navigationItem.titleLabel.text = selDate
        
        loadCurrentRecord()
        
    }
    
    
    func loadCurrentRecord() {
        
        arrMainList = [ArrFoodSchedule]()
        
        let df = DateFormatter()
        df.dateFormat = "hh:mm:ss a"
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        var sql = "SELECT * FROM " + datasource.ScheduleList_tlb + " WHERE UPPER(" + datasource.dbRouteName + ") = 'DIET' " + " ORDER BY CAST(" + datasource.dbTime + " as INTEGER) "
        
        var cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let cdd = ArrFoodSchedule()
                cdd.scheduleName = cursor!.string(forColumnIndex: 2)
                cdd.arrFoodList = [ArrFoodLog]()
                cdd.totalCal = 0
                cdd.imgResouceName = FoodScheduleType.getScheduleDefaultImage(type: cdd.scheduleName)
                cdd.time = ConvertionClass().conLongToDate(cursor!.double(forColumnIndex: 1), dateFormat: df)
                
                arrMainList.append(cdd)
            }
            cursor!.close()
        }
        
        
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        //load details of food
        for i in 0 ..< arrMainList.count {
            let tempSchedule:ArrFoodSchedule = arrMainList[i]

            let fromDateTimeLong = ConvertionClass().conDateToLong(df.date(from: currentDayDate + " 12:00:10 AM")!)
            let toDateTimeLong = ConvertionClass().conDateToLong(df.date(from: currentDayDate + " 11:59:00 PM")!)
            
            sql = "SELECT FL.\(datasource.dbFoodID), \(datasource.dbFoodName), \(datasource.dbCalories), \(datasource.dbCalories), \(datasource.dbQuantity), (SELECT \(datasource.dbContainerName) FROM \(datasource.FoodContainerList_tlb) WHERE \(datasource.dbContainID) = FL.\(datasource.dbContainID)), \(datasource.recID), FL.\(datasource.dbContainID), \(datasource.dbDateTime), FL.\(datasource.dbServerID)  from \(datasource.FoodRecordList_tlb) as F, \(datasource.FoodRecordLogList_tlb) as FL WHERE F.\(datasource.dbFoodID) = FL.\(datasource.dbFoodID) AND \(datasource.dbDateTime) >= \(fromDateTimeLong) AND \(datasource.dbDateTime) <= \(toDateTimeLong) AND UPPER(\(datasource.dbRouteName)) = '\(tempSchedule.scheduleName.uppercased())' "

            cursor = datasource.selectRecords(sql)
            
            var arrFoodList = [ArrFoodLog]()
            if cursor != nil {
                while cursor!.next() {
                    let temp = ArrFoodLog()
                    temp.foodName = cursor!.string(forColumnIndex: 1)
                    temp.actCaloriesPerGram = cursor!.string(forColumnIndex: 2)
                    temp.actCaloriesTotal = cursor!.string(forColumnIndex: 3)
                    temp.quantity = cursor!.string(forColumnIndex: 4)
                    temp.serveIn = cursor!.string(forColumnIndex: 5)
                    temp.serveInID = cursor!.string(forColumnIndex: 7)
                    temp.recID = cursor!.string(forColumnIndex: 6)
                    temp.foodID = cursor!.string(forColumnIndex: 0)
                    temp.dateTime = cursor!.string(forColumnIndex: 8)
                    temp.serverID = cursor!.string(forColumnIndex: 9)
                    
                    let val = Double(temp.actCaloriesTotal)! * Double(temp.quantity)!
                    temp.actCaloriesTotal = String(format: "%.1f", val)
                    arrFoodList.append(temp)
                }
                cursor!.close()
            }
            arrMainList[i].arrFoodList = arrFoodList
            Print.printLog("Food array==== \(arrFoodList)")
            
            var totalCal:Double = 0
            for j in 0 ..< arrFoodList.count {
                totalCal += Double(arrFoodList[j].actCaloriesTotal)!
            }
            arrMainList[i].totalCal = Int(totalCal)
        }
        datasource.closeDatabase()
        
        mainCollectionView.reloadData()
        mainCollectionView.isHidden = false
    }
}

