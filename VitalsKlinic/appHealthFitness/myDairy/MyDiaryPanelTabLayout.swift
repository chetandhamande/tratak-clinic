//
//  MyDiaryPanelTabLayout.swift
//  Salk
//
//  Created by Chetan on 08/01/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import MaterialControls
import RAMAnimatedTabBarController

class MyDiaryPanelTabLayout: UIViewController, MDTabBarViewControllerDelegate {
    
    fileprivate var vcDiet = MyDiaryDiatFragment()
    fileprivate var vcDashboard = MyDiaryActivityFragment()
    
    fileprivate var tabBarViewController = MDTabBarViewController()
    
    fileprivate var loadingObj = CustomLoading()
    fileprivate let mySharedObj = MySharedPreferences()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    func initContent() {
        tabBarViewController = MDTabBarViewController(delegate: self)
        
        var names = [String]()
        names = ["Diet", "Activity"]
        
        tabBarViewController.setItems(names)
        tabBarViewController.tabBar.backgroundColor = Color.white
        tabBarViewController.tabBar.textColor = Color.black
        tabBarViewController.tabBar.indicatorColor = Color.black
        
        vcDiet = MyDiaryDiatFragment(nibName: "MyDiaryDiatFragment", bundle: nil)
        vcDashboard = MyDiaryActivityFragment(nibName: "MyDiaryActivityFragment", bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        self.initContent()
        
        self.addChildViewController(tabBarViewController)
        self.view.addSubview(tabBarViewController.view)
        tabBarViewController.didMove(toParentViewController: self)
        let controllerView : UIView = tabBarViewController.view
        
        let rootTopLayoutGuide = self.topLayoutGuide
        let rootBottomLayoutGuide = self.bottomLayoutGuide
        
        let viewsDictionary = ["rootTopLayoutGuide": rootTopLayoutGuide, "rootBottomLayoutGuide": rootBottomLayoutGuide, "controllerView": controllerView] as [String : Any]
        
        let verticalConstraints : [AnyObject] = NSLayoutConstraint.constraints(withVisualFormat: "V:[rootTopLayoutGuide][controllerView][rootBottomLayoutGuide]",                                                                                               options: NSLayoutFormatOptions(rawValue: 0),                                                                                               metrics: nil,                                                                                               views:viewsDictionary as [String : AnyObject])
        self.view.addConstraints(verticalConstraints as AnyObject as! [NSLayoutConstraint])
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[controllerView]|",                                                                                   options: NSLayoutFormatOptions(rawValue: 0),                                                                            metrics: nil, views: viewsDictionary as [String : AnyObject])
        self.view.addConstraints(horizontalConstraints)
        
        tabBarViewController.selectedIndex = 1
        
        self.prepareNavigationItem()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "My Diary"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let timelineImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_refresh_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        let timelineButton = UIBarButtonItem(image: timelineImage, style: .plain, target: self, action: #selector(self.openTimeline))
        
        navigationItem.rightBarButtonItems = [timelineButton]
        
        self.navigationController?.navigationBar.hideBottomHairline()
    }
    
    @objc fileprivate func openTimeline() {
        performSyncMyDiary()
    }
    
    func tabBarViewController(_ viewController: MDTabBarViewController, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
            return vcDiet
        } else {
            return vcDashboard
        }
    }
    
    func imageWithImage(_ image: UIImage!, scaledToSize: CGSize!) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(scaledToSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: scaledToSize.width, height: scaledToSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    private func performSyncMyDiary() {
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loadingObj.loading())
            
            let appPref = MySharedPreferences()
            appPref.setTempRecordID(text: "diet")
            
            FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncMyDiary, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func refreshMyDiaryList() {
        loadingObj.removeFromSuperview()
        if tabBarViewController.selectedIndex == 0 {
            vcDiet.onLoad()
        } else {
            vcDashboard.onLoad()
        }
    }
    
    func failGettingInfo() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

