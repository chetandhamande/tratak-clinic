//
//  MyDiaryActivityFragment.swift
//  Salk
//
//  Created by Chetan on 08/01/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import SnapKit
import Async

class MyDiaryActivityFragment: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    var arrMainList = [ArrMyDiary]()
    
    let CustomeAdpaterMyDiaryIdentifer = "CustomeAdpaterMyDiary"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        mainTableView.backgroundColor = CustomColor.background()
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        
        mainTableView.estimatedRowHeight = 100
        mainTableView.rowHeight = UITableViewAutomaticDimension
        
        mainTableView.translatesAutoresizingMaskIntoConstraints = false

        self.mainTableView.isHidden = true
        self.loading.isHidden = false
        loading.startAnimating()
        
        Async.main(after: 0.1) {
            self.mainTableView.isHidden = false
            self.loading.isHidden = true
        }
        onLoad()
    }
    
    func onLoad() {
        arrMainList = [ArrMyDiary]()
        
        //find list of diet
        
        var daysList = [String]()
        var daysStrList = [String]()
        
        daysStrList.append("Today")
        daysStrList.append("Tomorrow")
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        
        var currDaysVal: String = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df).uppercased()
        daysList.append(currDaysVal)
        currDaysVal = ConvertionClass().conLongToDate(ConvertionClass().currentTime() + (60 * 60 * 24), dateFormat: df).uppercased()
        
        daysList.append(currDaysVal)
        
        for i in  2 ..< 7 {
            df.dateFormat = "yyyy-MM-dd"
            currDaysVal = ConvertionClass().conLongToDate(ConvertionClass().currentTime() + Double(60 * 60 * 24 * i), dateFormat: df).uppercased()
            daysList.append(currDaysVal)
            
            df.dateFormat = "dd MMM, yyyy"
            daysStrList.append(ConvertionClass().conLongToDate(ConvertionClass().currentTime() + Double(60 * 60 * 24 * i), dateFormat: df).uppercased())
        }
        
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        var type = "", typeFood = "";
        var orderBy = "CASE "
        var sql = "SELECT * FROM " + datasource.ScheduleList_tlb
            + " WHERE UPPER(" + datasource.dbRouteName + ") IN ('FITNESS', 'THERAPY') "
            + " ORDER BY CAST(" + datasource.dbTime + " as INTEGER) "
        
        var index = 0
        var cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("Food") {
                    typeFood += typeFood.isEmpty ? "'" + cursor!.string(forColumnIndex: 2) + "'" : ",'" + cursor!.string(forColumnIndex: 2) + "'";
                } else {
                    type += type.isEmpty ? "'" + cursor!.string(forColumnIndex: 2) + "'" : ",'" + cursor!.string(forColumnIndex: 2) + "'"
                }
                orderBy += " WHEN rd." + datasource.dbRecType + " = '" + cursor!.string(forColumnIndex: 2) + "' then " + "\(index)"
                index += 1
            }
            cursor!.close()
        }
        orderBy += " END "
        
        for j in 0 ..< daysList.count {
            sql = "SELECT " + datasource.dbOptionID + ", " + datasource.dbOptionName
                + ", " + datasource.dbIsChecked
                + ", (Select group_concat(" + datasource.dbActID + ", ',') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as optionVal "
                + ", (Select group_concat(" + datasource.dbQuantity + ", ',') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as quaVal "
                + ", rD." + datasource.dbRecDesc + ", rDS." + datasource.dbVideoLink
                + ", rDS." + datasource.dbDays + ", rD." + datasource.dbRecType
                + " FROM " + datasource.recDashboardList_tlb + " rD, " + datasource.recDashboardSubList_tlb + " rDS "
                + " WHERE rD." + datasource.recID + " = rDS." + datasource.recID
                + " AND UPPER(" + datasource.dbRecType + ") IN (" + type.uppercased() + ") "
                + " AND UPPER(" + datasource.dbDays + ") like '%" + daysList[j] + "%' "
                + " GROUP BY " + datasource.dbOptionID
                + " ORDER BY " + datasource.dbRecName + " ASC, rD." + datasource.dbRecType + " ASC "
            
            
            let myDiaryTemp: ArrMyDiary = ArrMyDiary()
            myDiaryTemp.day = daysList[j]
            myDiaryTemp.dayStr = daysStrList[j]
            
            var tempFitnessArr = [ArrFitnessTypeData]()
            var preType = "", typeData = ""
            var tempFitness = ArrFitnessTypeData()
            
            cursor = datasource.selectRecords(sql)
            if cursor != nil {
                while cursor!.next() {
                    guard let recType = cursor!.string(forColumnIndex: 8) else {
                        continue
                    }
                    if preType.isEmpty {
                        preType = recType
                        tempFitness = ArrFitnessTypeData()
                        typeData = ""
                        tempFitness.type = recType
                        tempFitness.typeIcon = FoodScheduleType.getScheduleDefaultImage(type: recType)
                    }
                    
                    if !preType.equalsIgnoreCase(recType) {
                        tempFitness.typeData = typeData
                        tempFitnessArr.append(tempFitness)
                        
                        preType = recType
                        tempFitness = ArrFitnessTypeData()
                        typeData = ""
                        tempFitness.type = recType
                        tempFitness.typeIcon = FoodScheduleType.getScheduleDefaultImage(type: recType)
                    }
                    
                    guard let tempVal = cursor!.string(forColumnIndex: 3) else {
                        continue
                    }
                    let actName = tempVal.split(",")
                    
                    guard let tempVal1 = cursor!.string(forColumnIndex: 4) else {
                        continue
                    }
                    let quantity = tempVal1.split(",")
                    
                    var val = ""
                    for i in 0 ..< actName.count {
                        var temp = ""
                        if i < actName.count {
                            temp = actName[i] + " min"
                            
                        }
                        if i < quantity.count {
                            temp = actName[i] + " - " + quantity[i] + " min"
                        }
                        
                        val += val.isEmpty ? temp : ", " + temp
                    }
                    
                    if typeData.isEmpty {
                        typeData += "<b>" + cursor!.string(forColumnIndex: 1) + "</b> <br> " + val
                    } else {
                        typeData += "<br><br> <b>" + cursor!.string(forColumnIndex: 1) + "</b> <br> " + val
                    }
                }
                cursor!.close()
            }
            //add last record
            if !typeData.isEmpty {
                tempFitness.typeData = typeData
                tempFitnessArr.append(tempFitness)
            }
            
            //getting pre Workout and post workout
            sql = "SELECT " + datasource.dbOptionID + ", " + datasource.dbOptionName
                + ", " + datasource.dbIsChecked
                + ", (Select group_concat(" + datasource.dbActID + ", ',') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as optionVal "
                + ", (Select group_concat(" + datasource.dbQuantity + ", ',') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as quaVal "
                + ", (Select group_concat(" + datasource.dbContainID + ", ',') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as containVal "
                + ", rD." + datasource.dbRecDesc + ", rDS." + datasource.dbVideoLink
                + ", rDS." + datasource.dbDays + ", rD." + datasource.dbRecType
                + " FROM " + datasource.recDashboardList_tlb + " rD, " + datasource.recDashboardSubList_tlb + " rDS "
                + " WHERE rD." + datasource.recID + " = rDS." + datasource.recID
                + " AND UPPER(" + datasource.dbRecType + ") IN (" + typeFood.uppercased() + ") "
                + " AND UPPER(" + datasource.dbDays + ") like '%" + daysList[j] + "%' "
                + " GROUP BY " + datasource.dbOptionID
                + " ORDER BY " + orderBy + ", " + datasource.dbRecName
            
            preType = ""
            typeData = ""
            tempFitness = ArrFitnessTypeData()
            
            cursor = datasource.selectRecords(sql)
            if cursor != nil {
                while cursor!.next() {
                    guard let recType = cursor!.string(forColumnIndex: 9) else {
                        continue
                    }
                    if preType.isEmpty {
                        preType = recType
                        tempFitness = ArrFitnessTypeData()
                        typeData = ""
                        tempFitness.type = recType
                        tempFitness.typeIcon = FoodScheduleType.getScheduleDefaultImage(type: recType)
                    }
                    
                    if !preType.equalsIgnoreCase(recType) {
                        tempFitness.typeData = typeData
                        tempFitnessArr.append(tempFitness)
                        
                        preType = recType
                        tempFitness = ArrFitnessTypeData()
                        typeData = ""
                        tempFitness.type = recType
                        tempFitness.typeIcon = FoodScheduleType.getScheduleDefaultImage(type: recType)
                    }
                    
                    guard let tempVal = cursor!.string(forColumnIndex: 3) else {
                        continue
                    }
                    let actName = tempVal.split(",")
                    
                    guard let tempVal1 = cursor!.string(forColumnIndex: 4) else {
                        continue
                    }
                    let quantity = tempVal1.split(",")
                    
                    var val = ""
                    let contain = cursor!.string(forColumnIndex: 5).split(",")
                    
                    for i in 0 ..< actName.count {
                        var temp = ""
                        if i < actName.count {
                            temp = actName[i]
                        }
                        if i < quantity.count {
                            temp = actName[i] + " - " + quantity[i]
                        }
                        if i < contain.count {
                            temp = actName[i] + " - " + quantity[i] + " " + contain[i]
                        }
                        
                        val += val.isEmpty ? temp : ", " + temp
                    }

                    if typeData.isEmpty {
                        typeData += "<b>" + cursor!.string(forColumnIndex: 1) + "</b> <br> " + val
                    } else {
                        typeData += "<br><br> <b>" + cursor!.string(forColumnIndex: 1) + "</b> <br> " + val
                    }
                }
                cursor!.close()
            }
            //add last record
            if !typeData.isEmpty {
                tempFitness.typeData = typeData
                tempFitnessArr.append(tempFitness)
            }
            
            myDiaryTemp.typeDataList = tempFitnessArr
            arrMainList.append(myDiaryTemp)
        }
        datasource.closeDatabase()
        mainTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var currCell: CustomeAdpaterMyDiary!  = mainTableView.dequeueReusableCell(withIdentifier: CustomeAdpaterMyDiaryIdentifer) as? CustomeAdpaterMyDiary
        
        if currCell == nil {
            mainTableView.register(UINib(nibName: CustomeAdpaterMyDiaryIdentifer, bundle: nil), forCellReuseIdentifier: CustomeAdpaterMyDiaryIdentifer)
            currCell = mainTableView.dequeueReusableCell(withIdentifier: CustomeAdpaterMyDiaryIdentifer) as? CustomeAdpaterMyDiary
        }
        
        currCell.CustomeAdpaterMyDiary(arrMainList, position: indexPath.row, classRef: self)
        
        return currCell
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

