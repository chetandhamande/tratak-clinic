//
//  CustomeSubAdpaterMyDiary.swift
//  Salk
//
//  Created by Chetan on 10/01/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomeSubAdpaterMyDiary: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
