//
//  Util.swift

import Foundation

class Util {
    
    class func removeUnwantedSymbols(_ values :String) -> String {
        
        var output = "";
        for char in values.utf8 {
            if char >= 65 && char <= 90 {
                output += ("\(char)")
            } else if char >= 97 && char <= 122 {
                output += ("\(char)")
            }
        }
        return output;
    }
    
    class func convertStringToDictionary(_ text: String) -> NSMutableDictionary? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSMutableDictionary
                return json
            } catch {
                Print.printLog("Something went wrong")
            }
        }
        return nil
    }
    
    class func convertDictionaryToString(_ data: NSMutableDictionary) -> String? {
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            return NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        } catch {
            Print.printLog("Something went wrong")
        }
        
        return ""
    }
}

public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(pvc)
            } else {
                return vc
            }
        }
    }
}


