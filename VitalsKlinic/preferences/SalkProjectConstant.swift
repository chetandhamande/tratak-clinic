//
//  SalkProjectConstant.swift
//  Salk
//
//  Created by Chetan  on 14/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class SalkProjectConstant: NSObject {   
    
    public let isDebugMode: Bool = true
    
    public let userName: String = "sapi"
    public let password: String = "Serving@world/-"
    
    public let XMPPLink = "im.salk.healthcare"
    public let XMPPPort: UInt16 = 5222
    public let XMPPKeyWord = "salk"
    
    //databse Version Number
    public let dbVersion: Int = 2
    
    
    //Server Informations
    public let Serveradd: String = "http://www.salk.healthcare/doctor/api_v1/";
    public let ServeraddFTP: String = "http://www.salk.healthcare/doctor/";
    public let ServeraddCI: String = "http://api.salk.healthcare/index.php/patient/api/v2/";
    //public let ServeraddCI: String = "http://salk.tech/salkAPI/index.php/patient/api/v2/";
    public let ServeraddFTPCI: String = "http://file.salk.healthcare/";
    
    public let ServeraddExpertImg: String = "http://file.salk.healthcare/assets/savefiles/doctor/profile/"
    public let ServerFitnessProfileImg: String = "http://file.salk.healthcare/assets/savefiles/patient/profile/"
    public let ServerFitnessChatAttach = "http://file.salk.healthcare/assets/savefiles/"
    
}
