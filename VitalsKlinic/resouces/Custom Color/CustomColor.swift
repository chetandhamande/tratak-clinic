//
//  CustomColor.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomColor: NSObject {
    
    class func light_gray() -> UIColor {
        return UIColor(rgba: "#c2c7cc")
    }
    class func dark_gray() -> UIColor {
        return UIColor(rgba: "#3d454c")
    }
    class func dark_gray_pressed() -> UIColor {
        return UIColor(rgba: "#373e44")
    }
    class func darker_gray() -> UIColor {
        return UIColor(rgba: "#1f2226")
    }
    class func black_semi_transparent() -> UIColor {
        return UIColor(rgba: "#000000B2")
    }
    class func background() -> UIColor {
        return UIColor(rgba: "#e5e5e5")
    }
    class func half_black() -> UIColor {
        return UIColor(rgba: "#808080")
    }
    class func white() -> UIColor {
        return UIColor(rgba: "#fafafa")
    }
    class func white_pressed() -> UIColor {
        return UIColor(rgba: "#f1f1f1")
    }
    class func pink() -> UIColor {
        return UIColor(rgba: "#e91e63")
    }
    class func pink_pressed() -> UIColor {
        return UIColor(rgba: "#ec407a")
    }
    class func blue_semi_transparent() -> UIColor {
        return UIColor(rgba: "#5677fc80")
    }
    class func blue_semi_transparent_pressed() -> UIColor {
        return UIColor(rgba: "#738ffe80")
    }
    class func green_typing() -> UIColor {
        return UIColor(rgba: "#20CD15")
    }
    
    //<!-- Menu Color -->
    class func menuRed() -> UIColor {
        return UIColor(rgba: "#ff7a5f")
    }
    class func menuSkyBlue() -> UIColor {
        return UIColor(rgba: "#2aaff4")
    }
    class func menuYellow() -> UIColor {
        return UIColor(rgba: "#f9991c")
    }
    class func menuLightGreen() -> UIColor {
        return UIColor(rgba: "#7ddb71")
    }
    class func menuLightViolet() -> UIColor {
        return UIColor(rgba: "#9382d4")
    }
    class func menuDarkViolet() -> UIColor {
        return UIColor(rgba: "#4e5fda")
    }
    
    class func whiteShade() -> UIColor {
        return UIColor(rgba: "#f2f2f2FF")
    }
    
    class func colorAccentFaint() -> UIColor {
        return UIColor(rgba: "#98CFFCFF")
    }
    
    class func colorSalk() -> UIColor {
        return UIColor(rgba: "#00ccd3FF")
    }
    class func colorSalkDark() -> UIColor {
        return UIColor(rgba: "#008d93FF")
    }
    class func colorSalkContrast() -> UIColor {
        return UIColor(rgba: "#333d4bFF")
    }
    
    class func rb_checked() -> UIColor {
        return UIColor(rgba: "#4CAF50FF")
    }
    class func rb_unchecked() -> UIColor {
        return UIColor(rgba: "#6D6D6DFF")
    }
    class func rb_disable() -> UIColor {
        return UIColor(rgba: "#B0B0B0FF")
    }
    class func tpi_checked() -> UIColor {
        return UIColor(rgba: "#FFFFFFFF")
    }
    class func tpi_unchecked() -> UIColor {
        return UIColor(rgba: "#FFFFFF99")
    }
    class func et_divider_focused() -> UIColor {
        return UIColor(rgba: "")
    }//return UIColor(rgba: "")color/colorControlActivated
    class func et_divider_not_focused() -> UIColor {
        return UIColor(rgba: "#6D6D6DFF")
    }
    class func et_divider_disable() -> UIColor {
        return UIColor(rgba: "#C1C1C1FF")
    }
    
    class func tranparentBlue() -> UIColor {
        return UIColor(rgba: "#79b9e15c")
    }
    
    class func textBlackColor() -> UIColor {
        return UIColor(rgba: "#000000")
    }
    class func textGrayColor() -> UIColor {
        return UIColor(rgba: "#D6D6D6FF")
    }
    class func whiteTransparentCC() -> UIColor {
        return UIColor(rgba: "#FFFFFFCC")
    }
    class func whiteTransparent99() -> UIColor {
        return UIColor(rgba: "#FFFFFF99")
    }
    class func whiteTransparent77() -> UIColor {
        return UIColor(rgba: "#FFFFFF77")
    }
    class func whiteTransparent55() -> UIColor {
        return UIColor(rgba: "#FFFFFF55")
    }
    class func textGrayLineColor() -> UIColor {
        return UIColor(rgba: "#e5e5e5")
    }
    class func grayBackground() -> UIColor {
        return UIColor(rgba: "#e2e2e2")
    }
    class func grayLight() -> UIColor {
        return UIColor(rgba: "#B2B2B2")
    }
    class func transparent() -> UIColor {
        return UIColor(rgba: "#00000000")
    }
    class func blackTransparent55() -> UIColor {
        return UIColor(rgba: "#000000DD")
    }
    
    class func facebookColor() -> UIColor {
        return UIColor(rgba: "#0B53A6")
    }
    class func twitterColor() -> UIColor {
        return UIColor(rgba: "#2AA3D8")
    }
    class func googleColor() -> UIColor {
        return UIColor(rgba: "#DB5331")
    }
    class func youtubeColor() -> UIColor {
        return UIColor(rgba: "#cc181e")
    }
    class func facebookPressColor() -> UIColor {
        return UIColor(rgba: "#003E7C")
    }
    class func twitterPressColor() -> UIColor {
        return UIColor(rgba: "#117CC5")
    }
    class func googlePressColor() -> UIColor {
        return UIColor(rgba: "#BE3E2E")
    }
  
    // <!-- Flurocent color -->
    class func fluorescentColor1() -> UIColor {
        return UIColor(rgba: "#FF17B8")
    }
    class func fluorescentColor2() -> UIColor {
        return UIColor(rgba: "#FF9D01")
    }
    class func fluorescentColor3() -> UIColor {
        return UIColor(rgba: "#FF3300")
    }
    class func fluorescentColor4() -> UIColor {
        return UIColor(rgba: "#FFF205")
    }
    class func fluorescentColor5() -> UIColor {
        return UIColor(rgba: "#E70545")
    }
    class func fluorescentColor6() -> UIColor {
        return UIColor(rgba: "#7F15E9")
    }
    class func fluorescentColor7() -> UIColor {
        return UIColor(rgba: "#070DBB")
    }
    class func fluorescentColor8() -> UIColor {
        return UIColor(rgba: "#00FFF4")
    }
    class func fluorescentColor9() -> UIColor {
        return UIColor(rgba: "#7CFF01")
    }
    class func fluorescentColor10() -> UIColor {
        return UIColor(rgba: "#BEF71F")
    }
    
    // <!-- Button color menu -->
    class func buttonColor1() -> UIColor {
        return UIColor(rgba: "#991037")
    }
    class func buttonColor2() -> UIColor {
        return UIColor(rgba: "#079C90")
    }
    class func buttonColor3() -> UIColor {
        return UIColor(rgba: "#0773D7")
    }
    class func buttonColor4() -> UIColor {
        return UIColor(rgba: "#5E8667")
    }
    
    // <!-- Popup Colors -->
    class func overlay_light() -> UIColor {
        return UIColor(rgba: "#F8F8F8")
    }
    
    // <!-- Chat Messager -->
    class func chatBackground() -> UIColor {
        return UIColor(rgba: "#e1ffc7")
    }
    // <!-- Send Msg color #-->
    class func chatBorder() -> UIColor {
        return UIColor(rgba: "#B3C833")
    }
    class func chatBackground1() -> UIColor {
        return UIColor(rgba: "#FFFFFF")
    }
    // <!-- Receive Msg color -->
    class func lightGreenColor() -> UIColor {
        return UIColor(rgba: "#00AE43")
    }
    class func lightGreenPressColor() -> UIColor {
        return UIColor(rgba: "#009E43")
    }
    class func lightBlueColor() -> UIColor {
        return UIColor(rgba: "#8FB1F3")
    }
    
    class func mainGreenColor() -> UIColor {
        return UIColor(rgba: "#6CAB36FF")
    }
    class func mainYellowColor() -> UIColor {
        return UIColor(rgba: "#EAC900")
    }
    class func mainYellowColorDark() -> UIColor {
        return UIColor(rgba: "#C0A500")
    }
    
    class func blackTransparent1() -> UIColor {
        return UIColor(rgba: "#00000030")
    }
    class func blackTransparent2() -> UIColor {
        return UIColor(rgba: "#00000020")
    }
    class func blackTransparent4() -> UIColor {
        return UIColor(rgba: "#00000060")
    }
    class func blackTransparent3() -> UIColor {
        return UIColor(rgba: "#000000A0")
    }
    class func blackTransparent5() -> UIColor {
        return UIColor(rgba: "#000000D0")
    }
    class func blackTransparent6() -> UIColor {
        return UIColor(rgba: "#0000000A")
    }
    class func blackTransparent7() -> UIColor {
        return UIColor(rgba: "#00000045")
    }
    class func cyenColor() -> UIColor {
        return UIColor(rgba: "#AFECEB")
    }
    
    class func popupColorSystem() -> UIColor {
        return UIColor(rgba: "#EEEEEE")
    }
    class func locRequestSent() -> UIColor {
        return UIColor(rgba: "#E24929FF")
    }
    class func locRequestWaiting() -> UIColor {
        return UIColor(rgba: "#F2AF3AFF")
    }
    class func locRequestSend() -> UIColor {
        return UIColor(rgba: "#279B5EFF")
    }
    
    //red
    class func google1() -> UIColor {
        return UIColor(rgba: "#D8433CFF")
    }
    //orange
    class func google2() -> UIColor {
        return UIColor(rgba: "#F2AF3AFF")
    }
    //blue
    class func google3() -> UIColor {
        return UIColor(rgba: "#3F8AF8FF")
    }
    //green
    class func google4() -> UIColor {
        return UIColor(rgba: "#279B5EFF")
    }
    
    class func indicatorRed() -> UIColor {
        return UIColor(rgba: "#AA0000FF")
    }
    
    class func switchShade() -> UIColor {
        return UIColor(rgba: "#2c6499")
    }
    
    
    //Features Color
    class func water() -> UIColor {
        return UIColor(rgba: "#5BCDCCFF")
    }
    class func food() -> UIColor {
        return UIColor(rgba: "#078B94FF")
    }
    class func activity() -> UIColor {
        return UIColor(rgba: "#F23957FF")
    }
    class func sleep() -> UIColor {
        return UIColor(rgba: "#3C8CDEFF")
    }
    class func calories() -> UIColor {
        return UIColor(rgba: "#F4A625FF")
    }
    class func distance() -> UIColor {
        return UIColor(rgba: "#E84C38FF")
    }
    
    
    
}
