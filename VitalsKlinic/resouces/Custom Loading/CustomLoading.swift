//
//  CustomLoading.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import MaterialControls

class CustomLoading: UIView {

    func loading() -> UIView {
        self.frame = UIScreen.main.bounds
        self.backgroundColor = CustomColor.black_semi_transparent()
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 30, y: (self.frame.size.height/2)-60, width: self.frame.size.width-60, height: 120)
        loadingView.layer.cornerRadius = 3
        loadingView.backgroundColor = CustomColor.white()
        self.addSubview(loadingView)
        
        let loadingIndicator = MDProgress(frame: CGRect(x: loadingView.frame.size.width / 2 - 30, y: 10, width: 60, height: 60))
        loadingIndicator.progressColor = PrimaryColor.colorPrimary()
        loadingIndicator.progressStyle = MDProgressStyle(rawValue: 0)!
        loadingIndicator.progressType = MDProgressType(rawValue: 0)!
        
        loadingView.addSubview(loadingIndicator)
        
        let loadingLabel: UILabel = UILabel()
        loadingLabel.frame = CGRect(x: 0, y: loadingIndicator.frame.size.height + 20, width: loadingView.frame.size.width, height: 30)
        loadingLabel.text = "Loading..."
        
        loadingLabel.font = UIFont.systemFont(ofSize: 20)
        loadingLabel.textAlignment = NSTextAlignment.center
        loadingView.addSubview(loadingLabel)
        
        return self;
    }
}
