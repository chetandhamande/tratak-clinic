//
//  CustomButton.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    func createButton(_ buttonName: String, buttonID: UIButton) -> UIButton {
        buttonID.setTitle(buttonName, for: UIControlState())
        buttonID.titleLabel!.textAlignment = NSTextAlignment.center
        buttonID.titleLabel!.font = UIFont.systemFont(ofSize: 16)
        buttonID.setTitleColor(CustomColor.white(), for: UIControlState())
        buttonID.backgroundColor = PrimaryColor.colorPrimary()
        buttonID.layer.cornerRadius = 3
        buttonID.tintColor = CustomColor.blue_semi_transparent_pressed()
        return buttonID;
    }
}
