//
//  CustomNavBar.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation
import UIKit
import Material

class CustomNavBar: UIView {
    
    func createNavBarWithSubTitle(_ title: String, subTitle: String, backButton: UIButton, rightBtn1: UIButton, rightBtn2: UIButton?) -> UIView {
        let width: CGFloat = 320
        
        let navBarView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 44))
//        navBarView.backgroundColor = UIColor.clearColor()
        
        let titleLabel = UILabel(frame: CGRect(x: 44, y: 4, width: width - 88, height: 21))
        titleLabel.text = title;
        titleLabel.textAlignment = .left
        titleLabel.textColor = CustomColor.white()
        navBarView.addSubview(titleLabel)
        
        let subTitleLabel = UILabel(frame: CGRect(x: 44, y: 21, width: width - 88, height: 21))
        subTitleLabel.text = subTitle
        subTitleLabel.textAlignment = .left
        subTitleLabel.font = UIFont.systemFont(ofSize: 12)
        subTitleLabel.textColor = CustomColor.white()
        navBarView.addSubview(subTitleLabel)
        
        backButton.frame = CGRect(x: -8,y: 0, width: 44, height: 44)
        navBarView.addSubview(backButton)
        
        rightBtn1.frame = CGRect(x: width - 52, y: 0, width: 44, height: 44)
        navBarView.addSubview(rightBtn1)
        
        if rightBtn2 != nil {
            rightBtn2!.frame = CGRect(x: width - 96, y: 0, width: 44, height: 44)
            navBarView.addSubview(rightBtn2!)
        }
        
        return navBarView;
    }
    
    func createNavBar(_ title: String, backButton: UIButton, rightBtn1: UIButton, rightBtn2: UIButton?) -> UIView {
        let width: CGFloat = 320
        
        let navBarView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 44))
//        navBarView.backgroundColor = UIColor.clearColor()
        
        let titleLabel = UILabel(frame: CGRect(x: 44, y: 0, width: width - 88, height: 44))
        titleLabel.text = title;
        titleLabel.textAlignment = .left
        titleLabel.textColor = CustomColor.white()
        navBarView.addSubview(titleLabel)
        
        backButton.frame = CGRect(x: -8,y: 0, width: 44, height: 44)
        navBarView.addSubview(backButton)
        
        rightBtn1.frame = CGRect(x: width - 52, y: 0, width: 44, height: 44)
        navBarView.addSubview(rightBtn1)
        
        if rightBtn2 != nil {
            rightBtn2!.frame = CGRect(x: width - 96, y: 0, width: 44, height: 44)
            navBarView.addSubview(rightBtn2!)
        }
        
        return navBarView;
    }
    
    
    func createNavBarBackButton(_ title: String, backButton: UIButton) -> UIView {
        let width: CGFloat = 320
        
        let navBarView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 44))
        //        navBarView.backgroundColor = UIColor.clearColor()
        
        let titleLabel = UILabel(frame: CGRect(x: 44, y: 0, width: width - 88, height: 44))
        titleLabel.text = title;
        titleLabel.textAlignment = .left
        titleLabel.textColor = CustomColor.white()
        navBarView.addSubview(titleLabel)
        
        backButton.frame = CGRect(x: -8,y: 0, width: 44, height: 44)
        navBarView.addSubview(backButton)
        
       return navBarView;
    }
}

extension UINavigationBar {
    
    func hideBottomHairline() {
        let navigationBarImageView = hairlineImageViewInNavigationBar(self)
        navigationBarImageView!.isHidden = true
    }
    
    func showBottomHairline() {
        let navigationBarImageView = hairlineImageViewInNavigationBar(self)
        navigationBarImageView!.isHidden = false
    }
    
    fileprivate func hairlineImageViewInNavigationBar(_ view: UIView) -> UIImageView? {
        if view.isKind(of: UIImageView.self) && view.bounds.height <= 1.0 {
            return (view as! UIImageView)
        }
        
        let subviews = (view.subviews as [UIView])
        for subview: UIView in subviews {
            if let imageView: UIImageView = hairlineImageViewInNavigationBar(subview) {
                return imageView
            }
        }
        
        return nil
    }
    
}
