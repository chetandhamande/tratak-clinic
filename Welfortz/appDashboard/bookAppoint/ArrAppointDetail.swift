//
//  ArrAppointDetail.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

public class ArrAppointDetail {
    public var aptID = "",doctorID = "",doctorName = "",specialityID = ""
    ,speciality = "";
    public var aptFor = "";
    public var reminderBy = "";
    public var date = "";
    public var appointTimeFrom = "";
    public var appointTimeTo = "";
    public var details = "";
    public var aptType = "";
    public var name = "";
    public var email = "";
    public var mobileNo = "";
    public var timeDiff = "";
    public var listLayoutIndex: Int = 0
}
