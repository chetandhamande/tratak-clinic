//
//  DashboardRequestManager.swift
//  Salk
//

import Foundation

class DashboardRequestManager {
    
    func getRequest(_ methodName: String, classRef: AnyObject!) {
        getRequest(methodName, dict: nil, classRef: classRef)
    }
    
    func getRequest(_ mMethod: String, dict: NSMutableDictionary?, classRef: AnyObject!) {
        var serverIP = SalkProjectConstant().Serveradd
        var url = ""
        
        if (mMethod == Constant.Purchase.MethodSyncPlan) {
            url = Constant.Purchase.URLSyncPlan
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.Purchase.MethodSyncOffers) {
            url = Constant.Purchase.UrlSyncOffers
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.Purchase.MethodSyncCorpList) {
            url = Constant.Purchase.URLSyncCorpList
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.Purchase.MethodAddPlanEnquiry) {
            url = Constant.Purchase.URLAddPlanEnquiry
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.Purchase.MethodAddNonCorpPlanEnquiry) {
            url = Constant.Purchase.URLAddNonCorpPlanEnquiry
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.BookAppointment.methodLoadSpecialList) {
            url = Constant.BookAppointment.URLLoadSpecialList
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.BookAppointment.methodLoadDoctorList) {
            url = Constant.BookAppointment.URLLoadSpecialList
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.BookAppointment.methodLoadAppointSlot) {
            url = Constant.BookAppointment.URLLoadAppointSlot
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.BookAppointment.methodAddAppoint) {
            url = Constant.BookAppointment.URLAddAppoint
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.BookAppointment.methodLoadAppointList) {
            url = Constant.BookAppointment.urlLoadAppointList
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.BookAppointment.methodAppointCancel) {
            url = Constant.BookAppointment.URLAppointCancel
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.BookAppointment.methodAppointResched) {
            url = Constant.BookAppointment.URLAppointResched
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.Purchase.methodLoadPatientLicList) {
            url = Constant.Purchase.URLLoadPatientLicList
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.Purchase.MethodRazorPlaceOrder) {
            url = Constant.Purchase.UrlRazorPlaceOrder
            serverIP = SalkProjectConstant().ServeraddCI
        } else if (mMethod == Constant.Purchase.MethodRazorPaymentDone) {
            url = Constant.Purchase.UrlRazorPaymentDone
            serverIP = SalkProjectConstant().ServeraddCI
        }
        
        serverIP += url
        
        ////Call For HTTP Connection
        let httpObj = DashboardHTTPConnection()
        httpObj.sendServerRequest(serverIP, mMethod: mMethod, classRef:classRef, dict: dict)
    }
}
