//
//  CustomAdapterPlanList.swift
//  Salk
//
//  Created by Salk on 15/03/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomAdapterPlanList: UICollectionViewCell {
    
    @IBOutlet weak var viewBackground: UIView!
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle0: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func onBindViewHolder(_ featureList: [ArrPlanList], position: Int, classRef: AnyObject)
    {
        let user: ArrPlanList = featureList[position]
        
        self.lblTitle.text = user.planName
        self.lblSubTitle0.text = user.planDetail
        self.imgBackground.image = UIImage(named: "back.jpg")
    }

}
