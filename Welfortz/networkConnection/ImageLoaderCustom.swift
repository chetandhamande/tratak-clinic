//
//  ImageLoaderCustom.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation
import UIKit

class ImageLoaderCustom {
    
//    func downlaodAddressBookPhoto(_ addrID: String, imageName: String, showImage: UIImageView, isRounded: Bool , isThumbImage: Bool) {
//        let homeDir = NSHomeDirectory()
//        let path = homeDir + "Documents/" + "\(Constant.FolderNames.mainFolderName)/\(Constant.FolderNames.folderAddrBook)/\(imageName)"
//    
//        var fileName: String  = addrID.sha1() + "imageExtension".localized
//        fileName += ";" + imageName
//        
//        self.downloadImage(path, imageName: fileName, showImage: showImage, isRounded: isRounded, isThumbImage: isThumbImage)
//    }
    
    //function for download user dp
    func downloadImage(_ fileURL: String, imageName: String, showImage: UIImageView, isRounded: Bool , isThumbImage: Bool)
    {
        let homeDir = NSHomeDirectory()
        let path = homeDir + "Documents/" + "\(Constant.FolderNames.mainFolderName)/\(Constant.FolderNames.folderUserProfile)/\(fileURL)"
        
        if path != "" {
            //check for internet and server link connection
            let mCheckInternet = CheckInternetConnection().isCheckInternetConnection()
            if mCheckInternet {
                //here send request to server
//                new DownloadImageFromServer(mContext, imageName, mContext.getString(R.string.methodAddressBookDownImage),
//                    showImage, isRounded, isThumbImage).execute();
            } else {
                showImage.image = UIImage(named: "ic_person_light_gray_24dp")
            }
        } else {
            //set image
//            Bitmap bmp;
            if isThumbImage {
                showImage
//                bmp = new ConvertCompressImage().decodeSampledBitmapFromFile(fileURL, 40, 40);
//            } else {
//                Uri outputFileUri = Uri.fromFile(file);
//                bmp = BitmapFactory.decodeStream(mContext.getContentResolver().openInputStream(outputFileUri));
//            }
//            
//            if(bmp == null) {
//                showImage.setImageResource(R.drawable.ic_person_light_gray_24dp);
//            } else {
//                if(isRounded) {
//                    showImage.setImageBitmap(new MakeRoundImage().getCroppedBitmap(bmp));
//                } else {
//                    showImage.setImageBitmap(bmp);
//                }
//            }
            }
        }
    }
    
    func downlaodUserProfilePhoto(_ imageName: String, showImage: UIImageView, isRounded: Bool , isThumbImage: Bool) {
        let homeDir = NSHomeDirectory()
        let path = homeDir + "Documents/" + "\(Constant.FolderNames.mainFolderName)/\(Constant.FolderNames.folderUserProfile)/\(imageName)"
        
        self.downloadProfileImage(path, imageName: imageName, showImage: showImage, isRounded: isRounded, isThumbImage: isThumbImage)
    }
    
    //function for download user dp
    func downloadProfileImage(_ fileURL: String, imageName: String, showImage: UIImageView, isRounded: Bool , isThumbImage: Bool) {
        let homeDir = NSHomeDirectory()
        let path = homeDir + "Documents/" + "\(Constant.FolderNames.mainFolderName)/\(Constant.FolderNames.folderUserProfile)/\(fileURL)"
        
        var file = NSDictionary()
        do {
            file = try FileManager.default.attributesOfItem(atPath: path) as NSDictionary
        } catch {
            Print.printLog("not found!")
        }

        if path != "" {
            //check for internet and server link connection
            let mCheckInternet = CheckInternetConnection().isCheckInternetConnection()
            if mCheckInternet {
                //here send request to server
                
                //                new DownloadImageFromServer(mContext, imageName, mContext.getString(R.string.methodAddressBookDownImage),
                //                    showImage, isRounded, isThumbImage).execute();
            } else {
                showImage.image = UIImage(named: "ic_person_light_gray_24dp")
            }
        } else {
            //set image
            let lastModified = file.fileModificationDate()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
            
            let currentTime = dateFormatter.string(from: lastModified!)
            
            let diff = ConvertionClass().currentTime() - ConvertionClass().conDateToLong(dateFormatter.date(from: currentTime)!)
            
//            Date lastModified = new Date(file.lastModified());
//            long diff = System.currentTimeMillis() - lastModified.getTime();
//            diff = diff / (1000 * 60 * 60 * 24); //in days
            
            var isNeedToShow: Bool = true
            if diff > 1 {
                let mCheckInternet = CheckInternetConnection().isCheckInternetConnection()
                if mCheckInternet {
//                        file.delete();
//                    } catch (Exception e) {}
                }
                isNeedToShow = false
//                    new DownloadImageFromServer(mContext, imageName, mContext.getString(R.string.methodPhoneBookDownImage),
//                        showImage, isRounded, isThumbImage).execute();
            }
            
            if isNeedToShow {
//                Bitmap bmp;
                //            if(isThumbImage) {
                //                bmp = new ConvertCompressImage().decodeSampledBitmapFromFile(fileURL, 40, 40);
                //            } else {
                //                Uri outputFileUri = Uri.fromFile(file);
                //                bmp = BitmapFactory.decodeStream(mContext.getContentResolver().openInputStream(outputFileUri));
                //            }
                //
                //            if(bmp == null) {
                //                showImage.setImageResource(R.drawable.ic_person_light_gray_24dp);
                //            } else {
                //                if(isRounded) {
                //                    showImage.setImageBitmap(new MakeRoundImage().getCroppedBitmap(bmp));
                //                } else {
                //                    showImage.setImageBitmap(bmp);
                //                }
                //            }
    
            }
        }
    }
    
    func compressImage(_ image: UIImage, width: CGFloat, height: CGFloat) -> UIImage {
        var actualHeight = image.size.height
        var actualWidth = image.size.width
        
        var imgRatio = actualWidth / actualHeight
        let maxRatio = width / height
        let compressionQuality: CGFloat = 0.5
        
        if actualHeight > height || actualWidth > height {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = height / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = height
            } else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = width / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = width
            } else {
                actualHeight = height
                actualWidth = width
            }
        }
        
        let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData: Data = UIImageJPEGRepresentation(img, compressionQuality)!
        UIGraphicsEndImageContext()
        
        return UIImage(data: imageData)!
    }
}
