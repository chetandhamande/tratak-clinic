//
//  PersonalHistoryVC.swift
//  Salk
//
//  Created by Chetan on 07/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class PersonalHistoryVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var objDiet: DietPreferenceView!
    @IBOutlet weak var objEth: EthinicityView!
    @IBOutlet weak var objBow: BowelMovementView!
    @IBOutlet weak var objFood: FoodAllergyView!
    @IBOutlet weak var objDrug: DrugAllergyView!
    
    fileprivate var loadingObj = CustomLoading()
    
    fileprivate let arr = ["Day", "Week", "Month", "Year"]
    fileprivate var editEthOther = "", editMoveOther = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mainScrollView.backgroundColor = CustomColor.background()
        
        picker.isHidden = true
        picker.backgroundColor = CustomColor.grayLight()
        picker.showsSelectionIndicator = true
        self.picker.delegate = self
        self.picker.dataSource = self
        UIApplication.shared.keyWindow!.bringSubview(toFront: picker)
        
        prepareNavigationItem()
        init1()
        getRecordServer()
    }

    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviPersonalHist".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
    }
    
    private func init1() {
        let tap = UITapGestureRecognizer(target: self, action:#selector(PersonalHistoryVC.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        objFood.btn1.addTarget(self, action: #selector(foodBtn1), for: .touchUpInside)
        objFood.btn2.addTarget(self, action: #selector(foodBtn2), for: .touchUpInside)
        objFood.btn3.addTarget(self, action: #selector(foodBtn3), for: .touchUpInside)
        objFood.btn4.addTarget(self, action: #selector(foodBtn4), for: .touchUpInside)
        objFood.btn5.addTarget(self, action: #selector(foodBtn4), for: .touchUpInside)
        
        objDrug.btn1.addTarget(self, action: #selector(drugBtn1), for: .touchUpInside)
        objDrug.btn2.addTarget(self, action: #selector(drugBtn2), for: .touchUpInside)
        objDrug.btn3.addTarget(self, action: #selector(drugBtn3), for: .touchUpInside)
        objDrug.btn4.addTarget(self, action: #selector(drugBtn4), for: .touchUpInside)
        objDrug.btn5.addTarget(self, action: #selector(drugBtn5), for: .touchUpInside)
        
        refreshParameters()
    }
    
    func refreshParameters() {
        
        var diet_pref = "",ethnicity = "", bowel_mov = ""
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
            + " WHERE " + datasource.dbGroupType + " = '" + "assGrpPersonalHistory".localized + "' ")
        
        if cursor != nil {
            while cursor!.next() {
                if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("diet_preference") {
                    diet_pref = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("ethnicity") {
                    ethnicity = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("eth_other_Details") {
                    editEthOther = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("bowel_movement") {
                    bowel_mov = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("bowel_other_details") {
                    editMoveOther = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("micturition") {
//                    btnMicturition.setTextcursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("appetite_digestion") {
//                    btnAppetite.setTextcursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_food1") {
                    objFood.txtCmpt1.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_food_since1") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        continue
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        if duraArr.count == 1 {
                            objFood.txtDur1.text = "1"
                            objFood.configureSelectButton(objFood.btn1, setString: duraArr[0])
                        } else {
                            objFood.txtDur1.text = duraArr[0]
                            objFood.configureSelectButton(objFood.btn1, setString: duraArr[1])
                        }
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_food2") {
                    objFood.txtCmpt2.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_food_since2") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        continue
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        if duraArr.count == 1 {
                            objFood.txtDur2.text = "1"
                            objFood.configureSelectButton(objFood.btn2, setString: duraArr[0])
                        } else {
                            objFood.txtDur2.text = duraArr[0]
                            objFood.configureSelectButton(objFood.btn2, setString: duraArr[1])
                        }
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_food3") {
                    objFood.txtCmpt3.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_food_since3") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        continue
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        if duraArr.count == 1 {
                            objFood.txtDur3.text = "1"
                            objFood.configureSelectButton(objFood.btn3, setString: duraArr[0])
                        } else {
                            objFood.txtDur3.text = duraArr[0]
                            objFood.configureSelectButton(objFood.btn3, setString: duraArr[1])
                        }
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_food4") {
                    objFood.txtCmpt4.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_food_since4") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        continue
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        if duraArr.count == 1 {
                            objFood.txtDur4.text = "1"
                            objFood.configureSelectButton(objFood.btn4, setString: duraArr[0])
                        } else {
                            objFood.txtDur4.text = duraArr[0]
                            objFood.configureSelectButton(objFood.btn4, setString: duraArr[1])
                        }
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_food5") {
                    objFood.txtCmpt5.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_food_since5") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        continue
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        if duraArr.count == 1 {
                            objFood.txtDur5.text = "1"
                            objFood.configureSelectButton(objFood.btn5, setString: duraArr[0])
                        } else {
                            objFood.txtDur5.text = duraArr[0]
                            objFood.configureSelectButton(objFood.btn5, setString: duraArr[1])
                        }
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_drug1") {
                    objDrug.txtCmpt1.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_drug_since1") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        continue
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        if duraArr.count == 1 {
                            objDrug.txtDur1.text = "1"
                            objDrug.configureSelectButton(objDrug.btn1, setString: duraArr[0])
                        } else {
                            objDrug.txtDur1.text = duraArr[0]
                            objDrug.configureSelectButton(objDrug.btn1, setString: duraArr[1])
                        }
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_drug2") {
                    objDrug.txtCmpt2.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_drug_since2") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        continue
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        if duraArr.count == 1 {
                            objDrug.txtDur2.text = "1"
                            objDrug.configureSelectButton(objDrug.btn2, setString: duraArr[0])
                        } else {
                            objDrug.txtDur2.text = duraArr[0]
                            objDrug.configureSelectButton(objDrug.btn2, setString: duraArr[1])
                        }
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_drug3") {
                    objDrug.txtCmpt3.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_drug_since3") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        continue
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        if duraArr.count == 1 {
                            objDrug.txtDur3.text = "1"
                            objDrug.configureSelectButton(objDrug.btn3, setString: duraArr[0])
                        } else {
                            objDrug.txtDur3.text = duraArr[0]
                            objDrug.configureSelectButton(objDrug.btn3, setString: duraArr[1])
                        }
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_drug4") {
                    objDrug.txtCmpt4.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_drug_since4") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        continue
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        if duraArr.count == 1 {
                            objDrug.txtDur4.text = "1"
                            objDrug.configureSelectButton(objDrug.btn4, setString: duraArr[0])
                        } else {
                            objDrug.txtDur4.text = duraArr[0]
                            objDrug.configureSelectButton(objDrug.btn4, setString: duraArr[1])
                        }
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_drug5") {
                    objDrug.txtCmpt5.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("allergy_drug_since5") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        continue
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        if duraArr.count == 1 {
                            objDrug.txtDur5.text = "1"
                            objDrug.configureSelectButton(objDrug.btn5, setString: duraArr[0])
                        } else {
                            objDrug.txtDur5.text = duraArr[0]
                            objDrug.configureSelectButton(objDrug.btn5, setString: duraArr[1])
                        }
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("family_type") {
//                    btnFamily.setTextcursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("household_people") {
//                    editHousehold.setTextcursor!.string(forColumnIndex: 1)
                }
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        
        if !diet_pref.trimmed.isEmpty {
            objDiet.rdgDiatPref = diet_pref
            if diet_pref.equals(objDiet.btnVeg.titleLabel!.text!) {
                objDiet.btnVeg.isSelected = true
            }
            if diet_pref.equals(objDiet.btnNonVeg.titleLabel!.text!) {
                objDiet.btnNonVeg.isSelected = true
            }
            if diet_pref.equals(objDiet.btnEgg.titleLabel!.text!) {
                objDiet.btnEgg.isSelected = true
            }
        }
        
        if !ethnicity.trimmed.isEmpty {
            let ethArr = ethnicity.split(",")
            
            for i in 0 ..< ethArr.count {
                if ethArr[i].equals(objEth.chkEthenicity1.titleLabel!.text!) {
                    objEth.chkEthenicity1.isSelected = true
                } else if ethArr[i].equals(objEth.chkEthenicity2.titleLabel!.text!) {
                    objEth.chkEthenicity2.isSelected = true
                } else if ethArr[i].equals(objEth.chkEthenicity3.titleLabel!.text!) {
                    objEth.chkEthenicity3.isSelected = true
                } else if ethArr[i].equals(objEth.chkEthenicity4.titleLabel!.text!) {
                    objEth.chkEthenicity4.isSelected = true
                }
            }
        }
        
        if !bowel_mov.trimmed.isEmpty {
            let bowArr = bowel_mov.split(",")
            
            for i in 0 ..< bowArr.count {
                if bowArr[i].equals(objBow.chkMovement1.titleLabel!.text!) {
                    objBow.chkMovement1.isSelected = true
                } else if bowArr[i].equals(objBow.chkMovement2.titleLabel!.text!) {
                    objBow.chkMovement2.isSelected = true
                } else if bowArr[i].equals(objBow.chkMovement3.titleLabel!.text!) {
                    objBow.chkMovement3.isSelected = true
                } else if bowArr[i].equals(objBow.chkMovement4.titleLabel!.text!) {
                    objBow.chkMovement4.isSelected = true
                } else if bowArr[i].equals(objBow.chkMovement5.titleLabel!.text!) {
                    objBow.chkMovement5.isSelected = true
                }
            }
        }
    }
    
    @objc private func performNextOpr() {
        dismissKeyboard()
        
        let datasource = DBOperation()
        datasource.openDatabase(true)
        
        let diet_pref = objDiet.rdgDiatPref
        
        datasource.UpdateAssessmentItemTable(paramName: "diet_preference", paramVal: diet_pref, groupType: "assGrpPersonalHistory".localized)
        
        var ethnicity = "";
        if objEth.chkEthenicity1.isSelected {
            ethnicity = objEth.chkEthenicity1.titleLabel!.text!
        }
        if objEth.chkEthenicity2.isSelected {
            if ethnicity.isEmpty {
                ethnicity = objEth.chkEthenicity2.titleLabel!.text!
            } else {
                ethnicity = ethnicity + "," + objEth.chkEthenicity2.titleLabel!.text!
            }
        }
        if objEth.chkEthenicity3.isSelected {
            if ethnicity.isEmpty {
                ethnicity = objEth.chkEthenicity3.titleLabel!.text!
            } else {
                ethnicity = ethnicity + "," + objEth.chkEthenicity3.titleLabel!.text!
            }
        }
        datasource.UpdateAssessmentItemTable(paramName: "ethnicity", paramVal: ethnicity, groupType: "assGrpPersonalHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "eth_other_Details", paramVal: objEth.chkEthenicity4.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        
        
        var movement = "";
        if objBow.chkMovement1.isSelected {
            movement = objBow.chkMovement1.titleLabel!.text!
        }
        if objBow.chkMovement2.isSelected {
            if movement.isEmpty {
                movement = objBow.chkMovement2.titleLabel!.text!
            }else {
                movement = movement + "," + objBow.chkMovement2.titleLabel!.text!
            }
        }
        if objBow.chkMovement3.isSelected {
            if movement.isEmpty {
                movement = objBow.chkMovement3.titleLabel!.text!
            } else {
                movement = movement + "," + objBow.chkMovement3.titleLabel!.text!
            }
        }
        if objBow.chkMovement4.isSelected {
            if movement.isEmpty {
                movement = objBow.chkMovement4.titleLabel!.text!
            } else {
                movement = movement + "," + objBow.chkMovement4.titleLabel!.text!
            }
        }
        datasource.UpdateAssessmentItemTable(paramName: "bowel_movement", paramVal: movement, groupType: "assGrpPersonalHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "bowel_other_details", paramVal: objBow.chkMovement5.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        
        
//        datasource.UpdateAssessmentItemTable("micturition", btnMicturition.getText().toString(), mContext.getString(R.string.assGrpPersonalHistory));
//        datasource.UpdateAssessmentItemTable("appetite_digestion", btnAppetite.getText().toString(), mContext.getString(R.string.assGrpPersonalHistory));
        
        datasource.UpdateAssessmentItemTable(paramName: "allergy_food1", paramVal: objFood.txtCmpt1.text!, groupType: "assGrpPersonalHistory".localized)
        if !objFood.btn1.titleLabel!.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "allergy_food_since1", paramVal: objFood.txtDur1.text! + " "
                + objFood.btn1.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "allergy_food2", paramVal: objFood.txtCmpt2.text!, groupType: "assGrpPersonalHistory".localized)
        if !objFood.btn2.titleLabel!.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "allergy_food_since2", paramVal: objFood.txtDur2.text! + " "
                + objFood.btn2.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "allergy_food3", paramVal: objFood.txtCmpt3.text!, groupType: "assGrpPersonalHistory".localized)
        if !objFood.btn3.titleLabel!.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "allergy_food_since3", paramVal: objFood.txtDur3.text! + " "
                + objFood.btn3.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "allergy_food4", paramVal: objFood.txtCmpt4.text!, groupType: "assGrpPersonalHistory".localized)
        if !objFood.btn4.titleLabel!.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "allergy_food_since4", paramVal: objFood.txtDur4.text! + " "
                + objFood.btn4.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "allergy_food5", paramVal: objFood.txtCmpt5.text!, groupType: "assGrpPersonalHistory".localized)
        if !objFood.btn5.titleLabel!.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "allergy_food_since5", paramVal: objFood.txtDur5.text! + " "
                + objFood.btn5.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "allergy_drug1", paramVal: objDrug.txtCmpt1.text!, groupType: "assGrpPersonalHistory".localized)
        if !objDrug.btn1.titleLabel!.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "allergy_drug_since1", paramVal: objDrug.txtDur1.text! + " "
                + objDrug.btn1.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "allergy_drug2", paramVal: objDrug.txtCmpt2.text!, groupType: "assGrpPersonalHistory".localized)
        if !objDrug.btn2.titleLabel!.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "allergy_drug_since2", paramVal: objDrug.txtDur2.text! + " "
                + objDrug.btn2.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "allergy_drug3", paramVal: objDrug.txtCmpt3.text!, groupType: "assGrpPersonalHistory".localized)
        if !objDrug.btn3.titleLabel!.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "allergy_drug_since3", paramVal: objDrug.txtDur3.text! + " "
                + objDrug.btn3.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "allergy_drug4", paramVal: objDrug.txtCmpt4.text!, groupType: "assGrpPersonalHistory".localized)
        if !objDrug.btn4.titleLabel!.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "allergy_drug_since4", paramVal: objDrug.txtDur4.text! + " "
                + objDrug.btn4.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "allergy_drug5", paramVal: objDrug.txtCmpt5.text!, groupType: "assGrpPersonalHistory".localized)
        if !objDrug.btn5.titleLabel!.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "allergy_drug_since5", paramVal: objDrug.txtDur5.text! + " "
                + objDrug.btn5.titleLabel!.text!, groupType: "assGrpPersonalHistory".localized)
        }
        
        
//        datasource.UpdateAssessmentItemTable("family_type", btnFamily.getText().toString(), mContext.getString(R.string.assGrpPersonalHistory));
//        datasource.UpdateAssessmentItemTable("household_people", editHousehold.getText().toString(), mContext.getString(R.string.assGrpPersonalHistory));
        
        datasource.closeDatabase()
        
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loadingObj.loading())
            
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpPersonalHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    private func getRecordServer() {
    // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpPersonalHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @objc func foodBtn1() {
        picker.isHidden = false
        picker.tag = 1
        picker.reloadAllComponents()
    }
    @objc func foodBtn2() {
        picker.isHidden = false
        picker.tag = 2
        picker.reloadAllComponents()
    }
    @objc func foodBtn3() {
        picker.isHidden = false
        picker.tag = 3
        picker.reloadAllComponents()
    }
    @objc func foodBtn4() {
        picker.isHidden = false
        picker.tag = 4
        picker.reloadAllComponents()
    }
    @objc func foodBtn5() {
        picker.isHidden = false
        picker.tag = 5
        picker.reloadAllComponents()
    }
    
    @objc func drugBtn1() {
        picker.isHidden = false
        picker.tag = 6
        picker.reloadAllComponents()
    }
    @objc func drugBtn2() {
        picker.isHidden = false
        picker.tag = 7
        picker.reloadAllComponents()
    }
    @objc func drugBtn3() {
        picker.isHidden = false
        picker.tag = 8
        picker.reloadAllComponents()
    }
    @objc func drugBtn4() {
        picker.isHidden = false
        picker.tag = 9
        picker.reloadAllComponents()
    }
    @objc func drugBtn5() {
        picker.isHidden = false
        picker.tag = 10
        picker.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return arr.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            self.objFood.configureSelectButton(self.objFood.btn1, setString: arr[row])
        } else if pickerView.tag == 2 {
            self.objFood.configureSelectButton(self.objFood.btn2, setString: arr[row])
        } else if pickerView.tag == 3 {
            self.objFood.configureSelectButton(self.objFood.btn3, setString: arr[row])
        } else if pickerView.tag == 4 {
            self.objFood.configureSelectButton(self.objFood.btn4, setString: arr[row])
        } else if pickerView.tag == 5 {
            self.objFood.configureSelectButton(self.objFood.btn5, setString: arr[row])
        } else if pickerView.tag == 6 {
            self.objDrug.configureSelectButton(self.objDrug.btn1, setString: arr[row])
        } else if pickerView.tag == 7 {
            self.objDrug.configureSelectButton(self.objDrug.btn2, setString: arr[row])
        } else if pickerView.tag == 8 {
            self.objDrug.configureSelectButton(self.objDrug.btn3, setString: arr[row])
        } else if pickerView.tag == 9 {
            self.objDrug.configureSelectButton(self.objDrug.btn4, setString: arr[row])
        } else if pickerView.tag == 10 {
            self.objDrug.configureSelectButton(self.objDrug.btn5, setString: arr[row])
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        picker.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
