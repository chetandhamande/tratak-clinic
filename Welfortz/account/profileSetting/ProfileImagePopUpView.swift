//
//  ProfileImagePopUpView.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Photos

protocol ProfileImageViewDelegate {
    func didSelectProfileImage(_ controller :ProfileImagePopUpView, image : UIImage)
}


class ProfileImagePopUpView: UIViewController, UIImagePickerControllerDelegate, TOCropViewControllerDelegate {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var gallaryButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var gallaryImageView: UIImageView!
    @IBOutlet weak var cameraImageView: UIImageView!
    @IBOutlet weak var gallaryLabel: UILabel!
    @IBOutlet weak var cameraLabel: UILabel!
    
    var image: UIImage!
    
    var delegate: ProfileImageViewDelegate! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = CustomColor.black_semi_transparent()
        
        profileImageView.image = image
        profileImageView.backgroundColor = CustomColor.white()
        profileImageView.layer.cornerRadius = 3
        
        gallaryImageView.image = UIImage(named: "ic_gallery_picker")
        gallaryLabel.text = "choosePhoto".localized
        gallaryLabel.textColor = CustomColor.white()
        gallaryButton.backgroundColor = CustomColor.blackTransparent5()
        
        cameraImageView.image = UIImage(named: "ic_camera_picker")
        cameraLabel.text = "clickPhoto".localized
        cameraLabel.textColor = CustomColor.white()
        cameraButton.backgroundColor = CustomColor.blackTransparent5()
        
        closeButton.backgroundColor = UIColor.clear
        closeButton.setImage(UIImage(named: "ic_close_48px_128"), for: UIControlState())
        
//        _ = UserRegistationViewController()
    }
    
    func dismissProfileImagePopUpView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
      //  self.layoutImageView()
    }

    @IBAction func gallaryButtonClick(_ sender: UIButton) {
        self.showCropViewController()
        
    }
    
    @IBAction func cameraButtonClick(_ sender: UIButton) {
        self.showCameraViewController()
    }
    
    @IBAction func closeButtonClick(_ sender: UIButton) {
        self.dismissProfileImagePopUpView()
    }
    
    // Show Camera
    func showCameraViewController() {
        let cameraController = UIImagePickerController()
        cameraController.sourceType = UIImagePickerControllerSourceType.camera
        cameraController.allowsEditing = false
        cameraController.delegate = self
        
        self.present(cameraController, animated: true, completion: nil)
    }
    
    // Show Photo library
    func showCropViewController() {
        let photoPickerController = UIImagePickerController()
        photoPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        photoPickerController.allowsEditing = false
        photoPickerController.delegate = self
        self.present(photoPickerController, animated: true, completion: nil)
        
        
        // Get the current authorization state.
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            
        }
            
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
            SweetAlert().showAlert("Allow Access", subTitle: "go to setting", style: .warning)
        }
            
        else if (status == PHAuthorizationStatus.notDetermined) {
            
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    
                }
                    
                else {
                    
                }
            })
        }
            
        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
    }
    
    // Crop Photo Delegate
    func cropViewController(_ cropViewController: TOCropViewController!, didCropTo image: UIImage!, with cropRect: CGRect, angle: Int) {
        self.profileImageView.image = image
        delegate?.didSelectProfileImage(self, image: image)
        
        cropViewController.dismiss(animated: true, completion: nil)
        self.dismissProfileImagePopUpView()
    }
    
    // Select Photo from Photo Library
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
        self.dismiss(animated: true, completion: {
            self.image = image
            let cropController = TOCropViewController(image: image)
            cropController?.delegate = self
            self.present(cropController!, animated: true, completion: nil)
        })
    }
    
    // Cancel Photo Library
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageWithImage(_ image: UIImage!, scaledToSize: CGSize!) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(scaledToSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: scaledToSize.width, height: scaledToSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
