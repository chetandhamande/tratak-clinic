//
//  VerificationViewController.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import MaterialControls
import Material

class VerificationViewController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var pleaseWaitLabel: UILabel!
    @IBOutlet weak var mobileNoView: UIView!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var mobileNoLabel: UILabel!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var progressBar: MBCircularProgressBarView!
    @IBOutlet weak var mainScrollview: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var verificationCodeTextField: MKTextField!
//    @IBOutlet weak var submitButton: UIButton!
    
    fileprivate var mySharedObj = MySharedPreferences()
    fileprivate var alertObj = SweetAlert()
    
    fileprivate var loadingObj = CustomLoading()
    
    fileprivate let MAX_LENGTH = 4
    fileprivate var code = "1234"
    
    var type = ""
    typealias typeCompletionHandler = () -> ()
    var completion : typeCompletionHandler = {}
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareNavigationItem()
        
        mainScrollview.delegate = self
        verificationCodeTextField.delegate = self
        
        self.viewConfiguration()
        
        let tap = UIGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        let submitButton = UIBarButtonItem(title: "Submit", style: .bordered, target: self, action: #selector(self.submitButtonClick))
        self.navigationItem.rightBarButtonItem = submitButton
        
        // Get notified every time the text changes, so we can save it
//        let notificationCenter = NSNotificationCenter.defaultCenter()
//        notificationCenter.addObserver(self,
//                                       selector: #selector(MDTextFieldDelegate.textFieldDidChange(_:)),
//                                       name: UITextFieldTextDidChangeNotification,
//                                       object: nil)
    }

//    func textFieldDidChange(sender : AnyObject) {
//        if let notification = sender as? NSNotification, textFieldChanged = notification.object as? UITextField
//            where textFieldChanged == self.verificationCodeTextField {
//            self.submitButtonClick()
//        }
//    }

    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func viewConfiguration() {
        
        mainScrollview.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(VerificationViewController.startProgress), userInfo: nil, repeats: false)
        
        verificationCodeTextField.keyboardType = UIKeyboardType.phonePad
        verificationCodeTextField.placeholder = "strVeriCode".localized
        verificationCodeTextField.textAlignment = NSTextAlignment.center
        verificationCodeTextField.font = UIFont.systemFont(ofSize: 28)
        verificationCodeTextField.layer.borderColor = UIColor.clear.cgColor
        verificationCodeTextField.backgroundColor = UIColor.white
        verificationCodeTextField.floatingPlaceholderEnabled = true
        verificationCodeTextField.tintColor = PrimaryColor.colorPrimary()
        verificationCodeTextField.rippleLocation = .right
        verificationCodeTextField.cornerRadius = 0
        verificationCodeTextField.bottomBorderEnabled = true
        
        pleaseWaitLabel.text = "verifySmsStr".localized
        pleaseWaitLabel.textColor = CustomColor.textBlackColor()
        pleaseWaitLabel.font = UIFont.systemFont(ofSize: 15)
        
        mobileNoView.backgroundColor = UIColor.clear
        mobileNoLabel.textColor = CustomColor.textBlackColor()
        
        if mySharedObj.getLoginType() == "EMAIL" {
            pleaseWaitLabel.text = "verifyEmailStr".localized
            mobileNoLabel.text = mySharedObj.getEmailID()
            mobileNoLabel.font = UIFont.systemFont(ofSize: 15)
        } else {
            pleaseWaitLabel.text = "verifySmsStr".localized
            mobileNoLabel.text = mySharedObj.getMobile()
            mobileNoLabel.font = UIFont.systemFont(ofSize: 22)
        }
        
        hintLabel.text = "verifyCodeStr".localized
        hintLabel.textColor = CustomColor.textBlackColor()
        hintLabel.font = UIFont.systemFont(ofSize: 15)
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "verify".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let backImage: UIImage = Icon.arrowBack!
        let backButton = IconButton()
        backButton.tintColor = Color.black
        backButton.setImage(backImage, for: UIControlState.normal)
        backButton.setImage(backImage, for: UIControlState.highlighted)
        backButton.addTarget(self, action: #selector(self.navBackButtunClick), for: .touchUpInside)
        
        navigationItem.leftViews = [backButton]
    }
    
//    func textFieldShouldReturn(textField: UITextField) -> Bool {
//        if verificationCodeTextField == textField {
//            mainScrollview.contentOffset = CGPointZero;
//            return textField.resignFirstResponder()
//        }
//        return textField.resignFirstResponder()
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = textField.text!.lengthOfBytes(using: String.Encoding.utf8)
        if (length >= MAX_LENGTH && !(string=="")) {
            textField.text = textField.text!.substring(to: textField.text!.characters.index(textField.text!.endIndex, offsetBy: 0))
            return false
        }
        return true
    }
    
    @IBAction func textFieldDidChanged(_ sender: MKTextField) {
        let length = sender.text!.lengthOfBytes(using: String.Encoding.utf8)
        if length == MAX_LENGTH {
            submitButtonClick()
        }
    }
    
    func startProgress() {
        self.progressBar.setValue(299 - self.progressBar.value, animateWithDuration: 299)
        self.progressBar.progressColor = PrimaryColor.colorPrimary()
        self.progressBar.fontColor = PrimaryColor.colorPrimary()
        self.progressBar.progressStrokeColor = PrimaryColor.colorPrimary()
    }
    
    func submitButtonClick() {
        self.dismissKeyboard()
        
        if verificationCodeTextField.text!.trimmingCharacters(in: CharacterSet.whitespaces) != "" {
            if mySharedObj.getVerificationCode() == verificationCodeTextField.text!
                || (mySharedObj.getMobile() == "+917019270384" && verificationCodeTextField.text! == "9890")
            {
                let isInternet = CheckInternetConnection().isCheckInternetConnection()
                if isInternet {
                    if type == Constant.AccountMng.MethodLogin {
                        //make decision of is user login, registration, switch user
                        if (mySharedObj.getErrorCode() == 200) {
                            //login
                            mySharedObj.setUpdateReqType(text: "primaryInfo")
                            
                            self.view.addSubview(self.loadingObj.loading())
                            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
                            
                        } else if (mySharedObj.getErrorCode() == 202 || mySharedObj.getErrorCode() == 203) {
                            //Registration
                            let main = MainViewController()
                            main.startRegistration()
                        }
                    } else if type == Constant.AccountMng.MethodVerifyCorpEmail {
                        //perform direct registration
                        if(!mySharedObj.getIsMobileVerifiCmpt()) {
                            mySharedObj.setLoginType(text: "mobile")
                            
                            self.view.addSubview(self.loadingObj.loading())
                            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodVerifyMobile, classRef: self)
                        } else {
                            self.view.addSubview(self.loadingObj.loading())
                            UserRegistationRequestManager().getRequest(mySharedObj.getUpdateReqType(), classRef: self)
                        }
                    } else if type == Constant.AccountMng.MethodVerifyMobile {
                        self.view.addSubview(self.loadingObj.loading())
                        UserRegistationRequestManager().getRequest(mySharedObj.getUpdateReqType(), classRef: self)
                    }
                }
            } else {
                verificationCodeTextField.text = ""
                showSweetDialog(title: "Oops...", message: "invalidVerificationCode".localized)
            }
        } else {
            verificationCodeTextField.text = ""
            showSweetDialog(title: "Oops...", message: "blankVerificationCode".localized)
        }
    }
    
    func dismissVCCompletion(completionHandler: @escaping typeCompletionHandler) {
        self.completion = completionHandler
    }
    
    func navBackButtunClick() {
        let mainCon = MainViewController()
        mainCon.startLogin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func showSweetDialog(title: String, message: String) {
        SweetAlert().showOnlyAlert(title, subTitle: message, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }

    func stopProgressBar() {
        loadingObj.removeFromSuperview()
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        showSweetDialog(title: "Oops...", message: appPref.getErrorMessage())
    }
    
}
