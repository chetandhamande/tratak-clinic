
class UserRegistationRequestManager {
    
    func getRequest(_ methodName: String, classRef: AnyObject!) {
        getRequest(methodName, dict: nil, classRef: classRef)
    }
    
    func getRequest(_ methodName: String, dict: NSMutableDictionary?, classRef: AnyObject!) {
        var serverIP = ""
        var url = ""
        
        if methodName == Constant.AccountMng.MethodLogin {
            url = Constant.AccountMng.UrlLogin
        } else if methodName == Constant.AccountMng.MethodRegister {
            url = Constant.AccountMng.UrlRegister
        } else if methodName == Constant.AccountMng.MethodRegisterInCompany {
            url = Constant.AccountMng.UrlRegisterInCompany
        } else if methodName == Constant.AccountMng.MethodUpdateProfile {
            url = Constant.AccountMng.URLUpdateProfile
        } else if methodName == Constant.AccountMng.MethodVerifyCorpEmail {
            url = Constant.AccountMng.UrlVerifyCorpEmail
        } else if methodName == Constant.AccountMng.MethodVerifyMobile {
            url = Constant.AccountMng.UrlVerifyMobile
        } else if methodName == Constant.AccountMng.MethodSyncProfile {
            url = Constant.AccountMng.URLSyncProfile
        } else if methodName.equals(Constant.AccountMng.MethodRegisterFitnessCheck) {
//            url = Constant.AccountMng.UrlRegisterFitnessCheck
        } else if methodName.equals(Constant.AccountMng.MethodForgotPassword) {
//            url = Constant.AccountMng.UrlForgotPassword
        } else if methodName.equals(Constant.AccountMng.MethodForgotPasswordNewSet) {
//            url = Constant.AccountMng.UrlForgotPasswordNewSet
        } else if methodName.equals(Constant.AccountMng.MethodVerifyEmail) {
            url = Constant.AccountMng.UrlVerifyEmail
        } else if methodName.equals(Constant.AccountMng.MethodVerifyEmailCmpt) {
            url = Constant.AccountMng.UrlVerifyEmailCmpt
        } else if methodName.equals(Constant.AccountMng.MethodSplashScreen) {
            url = Constant.AccountMng.UrlSplashScreen
        } else if methodName.equals(Constant.General.MethodFeedback) {
            url = Constant.General.UrlFeedback
        }

        
        serverIP = SalkProjectConstant().ServeraddCI + url
        
        ////Call For HTTP Connection
        let httpObj = UserRegistationHTTPConnection()
        httpObj.sendServerRequest(serverIP, methodName: methodName, classRef:classRef, dict: dict)
    }
    
}
