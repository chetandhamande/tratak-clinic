//
//  TimelineTableVC.swift
//  Salk
//
//  Created by Chetan  on 16/06/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import Async

class TimelineTableVC: UITableViewController {
        
    fileprivate var arrMainList = [ArrTimelineDay]()
    var totalRecMonth = 0
    
    fileprivate var loadingObj = CustomLoading()
    
    let thread1 = DispatchQueue(label: "TimelineTableVC.Thread1", attributes: DispatchQueue.Attributes.concurrent)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appPref = MySharedPreferences()
        appPref.setLastDashboardTypeOpen(text: "Timeline")
        
        let timelineTableViewCellNib = UINib(nibName: "TimelineTableViewCell", bundle: Bundle(for: TimelineTableViewCell.self))
        self.tableView.register(timelineTableViewCellNib, forCellReuseIdentifier: "TimelineTableViewCell")
        
        self.tableView.estimatedRowHeight = 300
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.prepareNavigationItem()
        
//        self.tableView.isHidden = true
        Async.custom(queue: thread1, after: 0.01, {
            self.loadValues()
        }).main(after: 0.01) {
//            self.tableView.isHidden = false
//            self.scrollToTodayDate()
        }
        
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Timeline"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.titleLabel.drawText(in: UIEdgeInsetsInsetRect(navigationItem.titleLabel.frame, UIEdgeInsetsMake(0, 20, 0, 20)))
        
        let todaysDateImg: UIImage = UIImage().imageWithImage(UIImage(named: "ic_today_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        let todayDate = UIBarButtonItem(image: todaysDateImg, style: .plain, target: self, action: #selector(self.scrollToTodayDate))
        
        let refreshImg: UIImage = UIImage().imageWithImage(UIImage(named: "ic_refresh_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        let refresh = UIBarButtonItem(image: refreshImg, style: .plain, target: self, action: #selector(self.getRecordFrmServer))
        
        navigationItem.rightBarButtonItems = [refresh, todayDate]
    }
    
    @objc fileprivate func scrollToTodayDate() {
        var index = self.tableView.numberOfSections
        if(index > 0) {
            index = index / 2
        } else {
            index = 0
        }
        
        let indexPath = NSIndexPath(row: 0, section: index)
        self.tableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: false)
    }
    
    @objc fileprivate func getRecordFrmServer() {
        self.performServerRequest()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return arrMainList.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        guard let sectionData:ArrTimelineDay = arrMainList[section] else {
            return 0
        }
        return sectionData.arrMainData.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return String(describing: arrMainList[section].title)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimelineTableViewCell", for: indexPath) as! TimelineTableViewCell
        
        // Configure the cell...
        guard let sectionData:ArrTimelineDay = arrMainList[indexPath.section] else
        {
            return cell
        }
        
        let arrTimeline = sectionData.arrMainData[indexPath.row]
        
        let timelinePoint = arrTimeline.points
        let timelineBackColor = arrTimeline.color
        let title = arrTimeline.title
        let description = arrTimeline.desc
        let lineInfo = ""
        //let thumbnail = arrTimeline.thumbnail
        
        var timelineFrontColor = UIColor.clear
        if (indexPath.row > 0) {
            timelineFrontColor = sectionData.arrMainData[indexPath.row - 1].color
        }
        cell.bubbleColor = arrTimeline.cardBGColor
        cell.timelinePoint = timelinePoint!
        cell.timeline.frontColor = timelineFrontColor
        cell.timeline.backColor = timelineBackColor
        cell.titleLabel.text = title
        cell.descriptionLabel.text = description
        cell.descriptionLabel.font = UIFont.boldSystemFont(ofSize: 16)
        cell.descriptionLabel.textColor = CustomColor.dark_gray()
        cell.lineInfoLabel.text = lineInfo
        cell.thumbnailImageView.image = UIImage(named: arrTimeline.imageIcon)
                
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}

extension TimelineTableVC {
    fileprivate func performServerRequest() {
        
        let isInternet = CheckInternetConnection().isCheckInternetConnection()
        if (isInternet) {
            let appPref = MySharedPreferences()
            appPref.setTimelineFromDate(text: "2017-06-01")
            appPref.setTimelineToDate(text: "2017-08-31")            
            
            self.view.addSubview(loadingObj.loading())
                
            FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncSummeryTimeline, classRef: self)
        }
    }
    
    func refreshList() {
        stopProgessList()
        
        loadValues()
    }
    
    func stopProgessList() {
        loadingObj.removeFromSuperview()
    }
}

extension TimelineTableVC {
    func loadValues() {
        
        arrMainList = [ArrTimelineDay]()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM"
        let currMonth = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter)
        
        dateFormatter.dateFormat = "yyyy"
        let currYear = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter)
        
        var prevMonth = ""
        for day in -90 ..< 90 {
            let calendar = Calendar.current
            let currDate = calendar.date(byAdding: .day, value: day, to: Date())
            
            dateFormatter.dateFormat = "MMMM"
            var recMonth = dateFormatter.string(from: currDate!)
            if prevMonth != recMonth {
                prevMonth = recMonth
                totalRecMonth += 1
                
                let temp = ArrTimelineDay()
                temp.title = recMonth
                temp.matchDate = ""
                temp.arrMainData = [ArrTimelineData]()
                arrMainList.append(temp)
            }
            
            dateFormatter.dateFormat = "yyyy-MM"
            recMonth = dateFormatter.string(from: currDate!)
            if recMonth == currMonth {
                dateFormatter.dateFormat = "EEE dd"
            } else {
                dateFormatter.dateFormat = "yyyy"
                let recYear = dateFormatter.string(from: currDate!)
                if currYear == recYear {
                    dateFormatter.dateFormat = "EEE dd MMMM"
                } else {
                    dateFormatter.dateFormat = "EEE dd MMMM, yyyy"
                }
            }
            
            let dayStr = dateFormatter.string(from: currDate!)
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let oprDate = dateFormatter.string(from: currDate!)
            
            let temp = ArrTimelineDay()
            temp.title = dayStr
            temp.matchDate = oprDate
            temp.arrMainData = [ArrTimelineData]()
            arrMainList.append(temp)
        }
        
        //finalize total Monthe
        totalRecMonth = totalRecMonth / 2
        totalRecMonth += 1
        
        //getting record from database
        let datasource = DBOperation()
        datasource.openDatabase(false)
        for i in 0 ..< arrMainList.count {
            if !arrMainList[i].matchDate.isEmpty {
                arrMainList[i].arrMainData = self.loadCurrentDateData(datasource: datasource, currSelectedDate: arrMainList[i].matchDate)
            }
        }
        datasource.closeDatabase()
        
        //refresh list elements
        Async.main({
            self.tableView.reloadData()
        })
    }
    
    fileprivate func loadCurrentDateData(datasource: DBOperation, currSelectedDate: String) -> [ArrTimelineData]
    {
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        
        var arrMainList = [ArrTimelineData]()
        
        let appPref = MySharedPreferences()
        var timelineFilter = appPref.getTimelineFilter().uppercased()
        timelineFilter += ",LICALLOT"
    
        let sql = "SELECT * FROM " + datasource.TimelineSummeryList_tlb + " WHERE " + datasource.dbDate + " = '" + currSelectedDate + "' "
    
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let temp = ArrTimelineData()
                
                temp.desc = cursor!.string(forColumnIndex: 2)
                temp.totalSubRec = Int(cursor!.int(forColumnIndex: 3))
                
                let type = cursor!.string(forColumnIndex: 0)!
                if !timelineFilter.contains(type.uppercased()) {
                    continue
                }
                
                if (type.uppercased() == "WATER") {
                    temp.imageIcon = "water_color.png"
                    //temp.padding = Util.dpToPx(2, mContext);
                    temp.cardBGColor = CustomColor.water()
                    temp.title = type
                } else if (type.uppercased() == "FOOD") {
                    temp.imageIcon = "food_color.png"
                    temp.title = type
                    temp.cardBGColor = CustomColor.food()
                } else if (type.uppercased() == "ROUTINE") {
                    temp.imageIcon = "activity_color.png"
                    temp.title = type
                    temp.cardBGColor = CustomColor.activity()
                } else if (type.uppercased() == "SLEEP") {
                    temp.imageIcon = "sleep_color.png"
                    temp.title = type;
                    temp.cardBGColor = CustomColor.sleep()                    
                } else if (type.uppercased() == "USERINFO") {
                    temp.imageIcon = "ic_account_box_black_36dp.png"
                    temp.title = type;
                    temp.cardBGColor = PrimaryColor.colorPrimary()
                } else if (type.uppercased() == "LICALLOT") {
                    temp.imageIcon = "ic_card_membership_black_48dp.png"
                    temp.title = type;
                    temp.cardBGColor = UIColor.black
                }
                
                temp.date = currSelectedDate;
                temp.oriDateTime = cursor!.double(forColumnIndex: 1)
                
                
                temp.time = ConvertionClass().conLongToDate(temp.oriDateTime, dateFormat: df)
                arrMainList.append(temp)
            }
            cursor!.close()
        }
        
        
        if arrMainList.count <= 0 {
            let tempSub = ArrTimelineData()
            tempSub.title = "No record available"
            arrMainList.append(tempSub)
        }
        
        return arrMainList
    }
}
