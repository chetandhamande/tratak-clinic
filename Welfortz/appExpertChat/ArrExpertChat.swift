//
//  ArrExpertChat.swift
//  Salk
//
//  Created by Chetan  on 02/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class ArrExpertChat {
    var expertName = "", expertID = "", expertCatID = "",
    msgID = "", subMsg = "", dateTime = "", totalUnread = "0", inbox = "1", qualification = "",
    msgType = "", fileName = ""
    var listLayoutIndex:Int = 0
    var isChecked:Bool = false
}
