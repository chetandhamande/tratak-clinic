//
//  XMPPMessageBroadcast.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class XMPPMessageBroadcast {
    
    func onHandleIntent(_ message: String, from: String) {
        
        let nType = manageNoticeOperation(message)
        
        // Construct an Intent tying it to the ACTION (arbitrary event namespace)
        if (!nType.isEmpty) {
            let dict = NSMutableDictionary()
            dict.setValue(1, forKey: "resultCode")
            dict.setValue(nType, forKey: "udpateType")
            
            if(nType == "gcmNTypeChat".localized) {
                dict.setValue(message, forKey: "extraData")
            }
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".localized), object: self, userInfo: dict as! [AnyHashable: Any])
        }
    }
    
    //function for manage notice operation
    fileprivate func manageNoticeOperation(_ message: String) -> String {
        
        var nType:String = ""
        
        guard let json = Util.convertStringToDictionary(message) else {
            return ""
        }
        
        //find nType
        nType = JsonParser.getJsonValueString(json, valueForKey: "nType")
        
        do {
            if(nType == "gcmNTypeChat".localized) {
                try PushChatOpr().PerformPushChatOpration(json)
            } else if(nType == "gcmNTypeDailyGoal".localized) {
                try PerformRoutine().performRoutineOpr(json)
            }
        } catch {}
        
        return nType
    }
}
