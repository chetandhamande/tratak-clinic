//
//  ReminderVC.swift
//  
//
//  Created by Chetan  on 19/07/17.
//
//

import UIKit
import Material
import RAMAnimatedTabBarController
import SnapKit

class ReminderVC: UIViewController {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var waterReminder: CustomReminderView!
    @IBOutlet weak var foodReminder: CustomReminderView!
    @IBOutlet weak var walkReminder: CustomReminderView!
    @IBOutlet weak var fitnessReminder: CustomReminderView!
    @IBOutlet weak var sleepReminder: CustomReminderView!
    @IBOutlet weak var nutraceuticalsReminder: CustomReminderView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
        
        //load reminder data n create layout
        self.loadWaterReminders()
        self.loadWalkReminders()
        self.loadSleepReminders()
        self.loadFoodReminders()
        self.loadFitnessReminders()
        self.loadNutraceuticalsReminders()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.prepareNavigationItem()
        self.viewConfiguration()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviReminders".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
    }
    
    fileprivate func viewConfiguration() {
        
        self.view.backgroundColor = UIColor.white
        self.mainScrollView.backgroundColor = UIColor.white
        
        
        foodReminder.lblTitle.text = "Food"
        foodReminder.lblCounter.text = "5 times"
        foodReminder.imgIcon.image = UIImage(named: "food_color.png")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let orientation = UIApplication.shared.statusBarOrientation
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.topMargin.equalTo(0)
                    make.leadingMargin.equalTo(0)
                    make.trailingMargin.equalTo(0)
                    make.bottomMargin.equalTo(0)
                    make.width.equalTo(mainStackView)
                })
            }
        }
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
                
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.width.equalTo(mainStackView)
                })
            }
        }
    }

//    @IBAction func saveOpr(sender: UIButton) {
//        let name = "Demo Test Reminder"
//        var time = timePicker.date
//        let timeInterval = floor(time.timeIntervalSinceReferenceDate/60)*60
//        time = NSDate(timeIntervalSinceReferenceDate: timeInterval) as Date
//        
//        // build notification
//        let notification = UILocalNotification()
//        notification.alertTitle = "Reminder"
//        notification.alertBody = "Don't forget to \(name)!"
//        notification.fireDate = time
//        notification.soundName = UILocalNotificationDefaultSoundName
//        
//        UIApplication.shared.scheduleLocalNotification(notification)
//    }

}

extension ReminderVC {
    
    fileprivate func getCollectionHeight(totalRow: Int) -> Int {
        var height:Int = 170
        
        if(totalRow < 4) {
            height = 170
        } else if(totalRow < 8) {
            height = 170
        } else {
            height = 170
        }
        
        return height
    }
    
    fileprivate func loadWaterReminders() {
        waterReminder.lblTitle.text = "Water"
        waterReminder.imgIcon.image = UIImage(named: "water_color.png")
        //waterReminder.btnAdd.addTarget(self, action: #selector(showAppsAndDevices), for: UIControlEvents.touchUpInside)
        
        //check for empty
        let appPref = MySharedPreferences()
        if (appPref.getWaterRemValue() == "-") {
            let val = "07:00 AM,09:00 AM,11.00 AM,01:00 PM,03:00 PM,05:00 PM,07:00 PM,09:00 PM"
            
            appPref.setWaterRemValue(text: val)
        }
        
        var arrList = [ArrReminder]()
        let temp = appPref.getWaterRemValue().components(separatedBy: ",")
        for i in 0 ..< temp.count {
            let tempVal = ArrReminder()
            tempVal.title = temp[i]
            arrList.append(tempVal)
        }
        
        waterReminder.arrMainList = arrList
        waterReminder.lblCounter.text = "\(temp.count) times"
        waterReminder.view.height = CGFloat(getCollectionHeight(totalRow: temp.count))
        waterReminder.refreshItemList()
        
        waterReminder.classRef = self
        waterReminder.currType = "water"
        waterReminder.switchRem.isOn = appPref.getIsWaterRemOn()
    }
    
    fileprivate func loadWalkReminders() {
        walkReminder.lblTitle.text = "Walk"
        walkReminder.imgIcon.image = UIImage(named: "activity_color.png")
        //walkReminder.btnAdd.addTarget(self, action: #selector(showAppsAndDevices), for: UIControlEvents.touchUpInside)
        
        //check for empty
        let appPref = MySharedPreferences()
        if (appPref.getWalkRemValue() == "-") {
            let val = "07:00 AM,05:30 PM,09:30 PM"
            
            appPref.setWalkRemValue(text: val)
        }
        
        var arrList = [ArrReminder]()
        let temp = appPref.getWalkRemValue().components(separatedBy: ",")
        for i in 0 ..< temp.count {
            let tempVal = ArrReminder()
            tempVal.title = temp[i]
            arrList.append(tempVal)
        }
        
        walkReminder.arrMainList = arrList
        walkReminder.lblCounter.text = "\(temp.count) times"
        walkReminder.view.height = CGFloat(getCollectionHeight(totalRow: temp.count))
        walkReminder.refreshItemList()
        
        walkReminder.classRef = self
        walkReminder.currType = "walk"
        walkReminder.switchRem.isOn = appPref.getIsWalkRemOn()
    }
    
    fileprivate func loadSleepReminders() {
        sleepReminder.lblTitle.text = "Sleep"
        sleepReminder.imgIcon.image = UIImage(named: "sleep_color.png")
        //sleepReminder.btnAdd.addTarget(self, action: #selector(showAppsAndDevices), for: UIControlEvents.touchUpInside)
        
        //check for empty
        let appPref = MySharedPreferences()
        if (appPref.getSleepRemValue() == "-") {
            let val = "10:30 PM"
            
            appPref.setSleepRemValue(text: val)
        }
        
        var arrList = [ArrReminder]()
        let temp = appPref.getSleepRemValue().components(separatedBy: ",")
        for i in 0 ..< temp.count {
            let tempVal = ArrReminder()
            tempVal.title = temp[i]
            arrList.append(tempVal)
        }
        
        sleepReminder.arrMainList = arrList
        sleepReminder.lblCounter.text = "\(temp.count) times"
        sleepReminder.view.height = CGFloat(getCollectionHeight(totalRow: temp.count))
        sleepReminder.refreshItemList()
        
        sleepReminder.classRef = self
        sleepReminder.currType = "sleep"
        sleepReminder.switchRem.isOn = appPref.getIsSleepRemOn()
    }
    
    fileprivate func loadFoodReminders() {
        foodReminder.lblTitle.text = "Food"
        foodReminder.imgIcon.image = UIImage(named: "food_color.png")
        //foodReminder.btnAdd.addTarget(self, action: #selector(showAppsAndDevices), for: UIControlEvents.touchUpInside)
        
        //getting food Schedule from DB
        var scheduleName = [String]()
        var scheduleTime = [String]()
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let sql = "SELECT * FROM " + datasource.ScheduleList_tlb
            + " WHERE UPPER(" + datasource.dbRouteName + ") = 'DIET' "
            + " ORDER BY CAST(" + datasource.dbTime + " as INTEGER) "
        let cursor = datasource.selectRecords(sql)
        
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        
        if cursor != nil {
            while cursor!.next() {
                scheduleName.append(cursor!.string(forColumnIndex: 2))
                
                let val = ConvertionClass().conLongToDate(cursor!.double(forColumnIndex: 1), dateFormat: df)
                scheduleTime.append("\(val)")
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        
        //check for empty
        let appPref = MySharedPreferences()
        if(appPref.getFoodRemName().components(separatedBy: ",").count != scheduleName.count)
        {
            var val = "", nameVal = ""
            for i in 0 ..< scheduleTime.count {
                val += val.isEmpty ? scheduleTime[i] : "," + scheduleTime[i]
                nameVal += nameVal.isEmpty ? scheduleName[i] : "," + scheduleName[i]
            }
            appPref.setFoodRemValue(text: val)
            appPref.setFoodRemName(text: nameVal)
        }

        var arrList = [ArrReminder]()
        let temp = appPref.getFoodRemValue().components(separatedBy: ",")
        let tempName = appPref.getFoodRemName().components(separatedBy: ",")
        for i in 0 ..< temp.count {
            let tempVal = ArrReminder()
            tempVal.title = tempName[i]
            tempVal.details = temp[i]
            arrList.append(tempVal)
        }
        
        foodReminder.arrMainList = arrList
        foodReminder.lblCounter.text = "\(temp.count) times"
        foodReminder.view.height = CGFloat(getCollectionHeight(totalRow: temp.count) + 20)
        foodReminder.refreshItemList()
        
        foodReminder.classRef = self
        foodReminder.currType = "food"
        foodReminder.switchRem.isOn = appPref.getIsFoodRemOn()
    }
    
    fileprivate func loadFitnessReminders() {
        fitnessReminder.lblTitle.text = "Fitness Routine"
        fitnessReminder.imgIcon.image = UIImage(named: "activity_color.png")
        //fitnessReminder.btnAdd.addTarget(self, action: #selector(showAppsAndDevices), for: UIControlEvents.touchUpInside)
        
        //check for empty
        let appPref = MySharedPreferences()
        if (appPref.getFitnessRemValue() == "-") {
            let val = "07:00 PM,07:10 PM"
            
            appPref.setFitnessRemValue(text: val)
        }
        
        var arrList = [ArrReminder]()
        let temp = appPref.getFitnessRemValue().components(separatedBy: ",")
        for i in 0 ..< temp.count {
            let tempVal = ArrReminder()
            tempVal.title = i == 0 ? "Diet" : "Activity"
            tempVal.details = temp[i]
            arrList.append(tempVal)
        }
        
        fitnessReminder.arrMainList = arrList
        fitnessReminder.lblCounter.text = "\(temp.count) times"
        fitnessReminder.view.height = CGFloat(getCollectionHeight(totalRow: temp.count))
        fitnessReminder.refreshItemList()
        
        fitnessReminder.classRef = self
        fitnessReminder.currType = "fitness"
        fitnessReminder.switchRem.isOn = appPref.getIsFitnessRemOn()
    }
    
    fileprivate func loadNutraceuticalsReminders() {
        
        var isNutraceuticalAvail:Bool = false
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let sql = "SELECT * FROM " + datasource.ScheduleList_tlb + " WHERE UPPER(" + datasource.dbRouteName + ") = 'NEUTRACEUTICALS' " + " ORDER BY CAST(" + datasource.dbTime + " as INTEGER) "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            if cursor!.next() {
                isNutraceuticalAvail = true
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        if(!isNutraceuticalAvail) {
            nutraceuticalsReminder.isHidden = true
            return
        } else {
            nutraceuticalsReminder.isHidden = false
        }
        
        nutraceuticalsReminder.lblTitle.text = "Nutraceuticals"
        nutraceuticalsReminder.imgIcon.image = UIImage(named: "food_color.png")
        //nutraceuticalsReminder.btnAdd.addTarget(self, action: #selector(showAppsAndDevices), for: UIControlEvents.touchUpInside)
        
        var arrList = [ArrReminder]()
        
        let appPref = MySharedPreferences()
        let temp = appPref.getNutraceuticalsRemValue().components(separatedBy: ",")
        for i in 0 ..< temp.count {
            let val = temp[i].components(separatedBy: ";")
            
            if(val.count > 1) {
                let tempVal = ArrReminder()
                tempVal.title = val[0]
                tempVal.details = val[1]
                arrList.append(tempVal)
            }
        }
        
        nutraceuticalsReminder.arrMainList = arrList
        nutraceuticalsReminder.lblCounter.text = "\(temp.count) times"
        nutraceuticalsReminder.view.height = CGFloat(getCollectionHeight(totalRow: temp.count))
        nutraceuticalsReminder.refreshItemList()
        
        nutraceuticalsReminder.classRef = self
        nutraceuticalsReminder.currType = "Nutraceuticals"
        nutraceuticalsReminder.switchRem.isOn = appPref.getIsNutraceuticalsRemOn()
    }
    
    func onEditReminder(currType: String, title: String) {
        let reminder = ReminderEditVC(nibName: "ReminderEditVC", bundle: nil)
        reminder.type = currType
        reminder.titleName = title
        self.navigationController?.pushViewController(reminder, animated: true)
    }
    
    func onSwitchChange(isCheck: Bool, currType: String, arrMainList: [ArrReminder]) {
        
        let appPref = MySharedPreferences()
        if(currType == "water") {
            appPref.setIsWaterRemOn(text: isCheck)
            
            for i in 0 ..< arrMainList.count {
                enableDisableNotification(timeStr: arrMainList[i].title, name: currType, isEnable: isCheck, noticeText: "")
            }
        } else if(currType == "sleep") {
            appPref.setIsSleepRemOn(text: isCheck)
            
            for i in 0 ..< arrMainList.count {
                enableDisableNotification(timeStr: arrMainList[i].title, name: currType, isEnable: isCheck, noticeText: "")
            }
        } else if(currType == "walk") {
            appPref.setIsWalkRemOn(text: isCheck)
            
            for i in 0 ..< arrMainList.count {
                enableDisableNotification(timeStr: arrMainList[i].title, name: currType, isEnable: isCheck, noticeText: "")
            }
        } else if(currType == "food") {
            appPref.setIsFoodRemOn(text: isCheck)
            
            for i in 0 ..< arrMainList.count {
                enableDisableNotification(timeStr: arrMainList[i].details, name: currType, isEnable: isCheck, noticeText: arrMainList[i].title)
            }
        } else if(currType == "fitness") {
            appPref.setIsFitnessRemOn(text: isCheck)
            
            for i in 0 ..< arrMainList.count {
                enableDisableNotification(timeStr: arrMainList[i].details, name: currType, isEnable: isCheck, noticeText: arrMainList[i].title)
            }
        } else if(currType == "Nutraceuticals") {
            appPref.setIsNutraceuticalsRemOn(text: isCheck)
            
            for i in 0 ..< arrMainList.count {
                enableDisableNotification(timeStr: arrMainList[i].details, name: currType, isEnable: isCheck, noticeText: arrMainList[i].title)
            }
        }
        
        
        appPref.setIsPendingReminder(text: true)
    }
    
    
    fileprivate func enableDisableNotification(timeStr: String, name: String, isEnable: Bool, noticeText: String) {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df)
        
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        var time = df.date(from: date + " " +  timeStr)
        
        let timeInterval = floor((time?.timeIntervalSinceReferenceDate)!/60) * 60
        time = NSDate(timeIntervalSinceReferenceDate: timeInterval) as Date
        
        if time?.compare(NSDate() as Date) == .orderedAscending {
            // if the time chosen is such that the date is in the past, go to tomorrow
            
            time = NSCalendar.autoupdatingCurrent.date(byAdding: .day, value: 1, to: time!)
        }
        
        // build notification
        let notification = UILocalNotification()
        
        if(name == "walk") {
            notification.alertTitle = "Time to take a short walk! "
            notification.alertBody = "Every steps counts"
        } else if(name == "sleep") {
            notification.alertTitle = "Time to sleep! "
            notification.alertBody = "Every hours of sleep counts"
        } else if(name == "food") {
            notification.alertTitle = "Time to " + noticeText + "!"
            notification.alertBody = "Every record of food counts"
        } else if(name == "fitness") {
            notification.alertTitle = noticeText + " daily routine"
            notification.alertBody = "Be prepare for next " + noticeText + " plan "
        } else if(name == "Nutraceuticals") {
            notification.alertTitle = noticeText + " routine"
            notification.alertBody = "Every record of " + noticeText + " counts "
        } else if(name == "water") {
            notification.alertTitle = "Time to drink a glass of water! " +  noticeText
            notification.alertBody = "Every glass of water counts"
        }
        
        notification.fireDate = time
        notification.repeatInterval = NSCalendar.Unit.day
        notification.soundName = UILocalNotificationDefaultSoundName
        
        if isEnable {
            UIApplication.shared.scheduleLocalNotification(notification)
        } else {
            UIApplication.shared.cancelLocalNotification(notification)
        }
    }
    
}

