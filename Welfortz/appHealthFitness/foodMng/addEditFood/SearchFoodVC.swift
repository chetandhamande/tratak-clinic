//
//  SearchFoodVC.swift
//  Salk
//
//  Created by Salk on 24/11/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class SearchFoodVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchTableView: UICollectionView!
    @IBOutlet weak var searchBarObj: UISearchBar!
    
    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var hideLabel: UILabel!
    
    var currDateTime = "", scheduleName = ""
    
    fileprivate let CellSearchFoodIdentifier = "CellSearchFood"
    var offscreenCells = Dictionary<String, UICollectionViewCell>()
    fileprivate var arrMainList = [ArrFoodLog]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
        
        if searchBarObj.text!.characters.count > 0 {
            self.searchBarObj.text = ""
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchTableView.delegate = self
        searchTableView.dataSource = self
        
        // initilize Food List cell
        let nib = UINib(nibName: CellSearchFoodIdentifier, bundle: nil)
        searchTableView.register(nib, forCellWithReuseIdentifier: CellSearchFoodIdentifier)
        
        searchBarObj.delegate = self
        
        prepareNavigationItem()
        viewConfiguration()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewConfiguration() {
        searchBarObj.becomeFirstResponder()
        
        hideView.isHidden = true
        self.searchTableView.backgroundColor = CustomColor.background()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "action_search".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.characters.count >= 2 || searchText.isEmpty {
            loadFoodList(searchText)
        }
    }
    
    /**
     * Function for refresh list view list adapter
     */
    fileprivate func refreshSearchList() {
        searchTableView.isHidden = true
        
        if arrMainList.count > 0 {
            searchTableView.isHidden = false
            hideView.isHidden = true
        } else if !searchBarObj.text!.isEmpty {
            hideView.isHidden = false
            hideLabel.text = "No result found for '\(searchBarObj.text!)'"
        }
        
        searchTableView.reloadData()
    }
    
    fileprivate func loadFoodList(_ searchText: String) {
        var searchText = searchText
        
        arrMainList = [ArrFoodLog]()
        
        //searchText = searchText.replaceString("'", val2: "")
        
        if !searchText.isEmpty {
            let datasource = DBOperation()
            datasource.openDatabase(false)
            
            let sql = "SELECT * FROM " + datasource.FoodRecordList_tlb + " WHERE " + datasource.dbFoodName + " like '%" + searchText + "%' ORDER BY " + datasource.dbFoodName
            
            let cursor = datasource.selectRecords(sql)
            if cursor != nil {
                while cursor!.next() {
                    let cdd = ArrFoodLog()
                    cdd.foodID = cursor!.string(forColumnIndex: 0)
                    cdd.foodName = cursor!.string(forColumnIndex: 1)
                    cdd.actCaloriesPerGram = cursor!.string(forColumnIndex: 2)
                    arrMainList.append(cdd)
                }
                cursor!.close()
            }
            datasource.closeDatabase()
        }
        
        refreshSearchList()
    }
}

extension SearchFoodVC {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellSearchFood = collectionView.dequeueReusableCell(withReuseIdentifier: CellSearchFoodIdentifier, for: indexPath) as! CellSearchFood
        
        cell.getView(arrMainList: arrMainList, position: indexPath.row)
        
        // Make sure layout subviews
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        // Use fake cell to calculate height
        var cell: CellSearchFood? = self.offscreenCells[CellSearchFoodIdentifier] as? CellSearchFood
        if cell == nil {
            cell = Bundle.main.loadNibNamed(CellSearchFoodIdentifier, owner: self, options: nil)![0] as? CellSearchFood
            self.offscreenCells[CellSearchFoodIdentifier] = cell
        }
        
        cell!.getView(arrMainList: arrMainList, position: indexPath.row)
        cell!.bounds = CGRect(x: 0, y: 0, width: searchTableView.frame.width, height: cell!.bounds.height)
        cell!.contentView.bounds = cell!.bounds
        
        // Layout subviews, this will let labels on this cell to set preferredMaxLayoutWidth
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        
        var size = cell!.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        // Still need to force the width, since width can be smalled due to break mode of labels
        size.width = searchTableView.frame.width
        return size
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.searchTableView.performBatchUpdates(nil, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let user = arrMainList[indexPath.row]
        
        let a = FoodSelectorVC(nibName: "FoodSelectorVC", bundle: nil)
        a.foodID = user.foodID
        a.foodName = user.foodName
        a.scheduleName = scheduleName
        a.currDateTime = currDateTime
        self.navigationController?.pushViewController(a, animated: true)
    }
    
    
}
