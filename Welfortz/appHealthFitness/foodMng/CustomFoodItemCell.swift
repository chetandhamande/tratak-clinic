//
//  CustomFoodItemCell.swift
//  Salk
//
//  Created by Salk on 26/11/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomFoodItemCell: UICollectionViewCell {

    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblServing: UILabel!
    @IBOutlet weak var lblCalories: UILabel!
    
    @IBOutlet weak var btnOption: UIButton!
    
    var classRefs: AnyObject! = nil
    var arrMainList = [ArrFoodLog]()
    var scheduleName = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnOption.setImage(UIImage().imageWithImage(UIImage(named: "ic_more_vert_black_24dp.png")!, scaledToSize: CGSize(width: 20, height: 20)), for: .normal)
    }

    func onBindViewHolder(_ featureList: [ArrFoodLog], position: Int, classRefs: AnyObject!, scheduleName: String)
    {
        self.classRefs = classRefs
        self.arrMainList = featureList
        self.scheduleName = scheduleName
        
        let user: ArrFoodLog = featureList[position]
        
        self.lblFoodName.text = user.foodName
        self.lblServing.text = "\(user.quantity) \(user.serveIn)"
        self.lblCalories.text = "\(user.actCaloriesTotal) cal"
        
        self.btnOption.tag = position
        //
    }
    
    @IBAction func perfromMoreOption(sender: UIButton) {
        
        let user: ArrFoodLog = arrMainList[sender.tag]
        
        let sheet = UIAlertController(title: user.foodName, message: nil, preferredStyle: .actionSheet)
        
        let clearAllAction = UIAlertAction(title: "Edit", style: .default) { (action) in
            self.performEdit(user: user)
        }
        let infoAction = UIAlertAction(title: "Delete", style: .default) { (action) in
            self.performDelete(user: user)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        sheet.addAction(clearAllAction)
        sheet.addAction(infoAction)
        sheet.addAction(cancelAction)
        
        (classRefs as! FoodLogActivityMngVC).present(sheet, animated: true, completion: nil)
    }
    
    fileprivate func performEdit(user: ArrFoodLog) {
        let a = FoodSelectorVC(nibName: "FoodSelectorVC", bundle: nil)
        a.recID = user.recID
        a.foodID = user.foodID
        a.foodName = user.foodName
        a.currDateTime = user.dateTime
        a.preSizeStr = user.quantity
        a.prevContIDStr = user.serveInID
        a.scheduleName = scheduleName
        (classRefs as! FoodLogActivityMngVC).navigationController?.pushViewController(a, animated: true)
    }
    
    fileprivate func performDelete(user: ArrFoodLog) {
        
        if user.serverID.isEmpty || user.serverID == "0" {
            do {
                try self.performDeleteLocalDB(recID: user.recID, isLocal: true)
            } catch {}
        } else {
            let internet:Bool = CheckInternetConnection().isCheckInternetConnection()
            if(internet) {
                let appPref = MySharedPreferences()
                appPref.setTempRecordID(text: user.serverID)
                
                FitnessRequestManager().getRequest(Constant.Fitness.MethodDeleteFoodRec, classRef: self)
            }
        }
    }
    
    func performDeleteLocalDB(recID: String, isLocal: Bool) throws {
        let datasource = DBOperation()
        datasource.openDatabase(true)
        if isLocal {
            datasource.TruncateFoodRecordLogList(recIDs: recID)
        } else {
            datasource.TruncateFoodRecordLogServerIDList(serverID: recID)
        }
        datasource.closeDatabase()
        
        (classRefs as! FoodLogActivityMngVC).loadCurrentRecord()
    }
}
