//
//  MyDiaryHeader.swift
//  Salk
//
//  Created by Chetan on 10/01/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import Foundation

class MyDiaryHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var title: UILabel!
}
