//
//  MySubscriptionVC.swift
//  Salk
//
//  Created by Salk on 27/03/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async

class MySubscriptionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var listview: UITableView!
    @IBOutlet weak var stackNotAvail: UIStackView!
    
    private var arrMainList = [ArrSubscription]()
    private let tableCellIdentifier = "CustomMySubscriptionCell"
    fileprivate var mRefreshControl: UIRefreshControl!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.prepareNavigationItem()
        
        listview.delegate = self
        listview.dataSource = self
        self.listview.separatorStyle = .none
        
        self.view.backgroundColor = CustomColor.background()
        self.listview.backgroundColor = CustomColor.background()
        
        Async.main(after: 0.1, {
            self.loadPlanData()
            if(self.arrMainList.count <= 0) {
                self.refreshButtonClick()
            }
        })
        
        mRefreshControl = UIRefreshControl()
        mRefreshControl.addTarget(self, action: #selector(self.refreshButtonClick), for: UIControlEvents.valueChanged)
        listview.addSubview(mRefreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviMySubscribtion".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    private func loadPlanData() {
        
        arrMainList = [ArrSubscription] ()
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let sql = "Select * " + " from " + datasource.MySubscriptionList_tlb
        
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let temp = ArrSubscription()
                temp.companyName = cursor!.string(forColumnIndex: 7)
                temp.status = cursor!.string(forColumnIndex: 9).equalsIgnoreCase("1")
                temp.planName = cursor!.string(forColumnIndex: 0)
                temp.planDuration = cursor!.string(forColumnIndex: 1)
                temp.voucherKey = cursor!.string(forColumnIndex: 2)
                temp.measure = cursor!.string(forColumnIndex: 5)
                
                temp.fromDate = ConvertionClass().conDateToLong(cursor!.string(forColumnIndex: 3))
                temp.toDate = ConvertionClass().conDateToLong(cursor!.string(forColumnIndex: 4))
                
                arrMainList.append(temp)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        if arrMainList.count == 0 {
            stackNotAvail.isHidden = false
            listview.isHidden = true
        } else {
            stackNotAvail.isHidden = true
            listview.isHidden = false
            listview.reloadData()
        }
    }
    
    @objc private func refreshButtonClick() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            DashboardRequestManager().getRequest(Constant.Purchase.methodLoadPatientLicList, classRef: self)
        } else {
            _ = SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func successRefresh() {
        mRefreshControl.endRefreshing()
        self.loadPlanData()
    }
    
    func mErrDialogue(_ message: String) {
        mRefreshControl.endRefreshing()
        _ = SweetAlert().showAlert("popupAlert".localized, subTitle: message, style: .warning)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var currCell: CustomMySubscriptionCell!  = listview.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? CustomMySubscriptionCell
        
        if currCell == nil {
            listview.register(UINib(nibName: tableCellIdentifier, bundle: nil), forCellReuseIdentifier: tableCellIdentifier)
            currCell = listview.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? CustomMySubscriptionCell
        }
        
        currCell.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        return currCell
    }
}
