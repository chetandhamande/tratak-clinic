//
//  CustomMySubscriptionCell.swift
//  Salk
//
//  Created by Salk on 27/03/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomMySubscriptionCell: UITableViewCell {

    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblVoucherKey: UILabel!
    @IBOutlet weak var lblDaysLeft: UILabel!
    @IBOutlet weak var viewFooter: UIView!
    
    @IBOutlet weak var viewContain: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewFooter.backgroundColor = CustomColor.colorSalkContrast()
        
        self.backgroundView?.backgroundColor = CustomColor.background()
        self.viewContain.backgroundColor = CustomColor.background()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func onBindViewHolder(_ arrMainList: Array<ArrSubscription>, position: Int, classRef: AnyObject)
    {
        let temp: ArrSubscription = arrMainList[position]
        
        lblCompanyName.text = " \(temp.companyName)"
        lblPlanName.text = temp.planName
        lblVoucherKey.text = temp.voucherKey
        
        var measure = ""
        if temp.measure.equalsIgnoreCase("D") {
            measure = "days"
        } else if temp.measure.equalsIgnoreCase("M") {
            if temp.planDuration.equalsIgnoreCase("1") {
                measure = "month"
            } else {
                measure = "months"
            }
        } else if temp.measure.equalsIgnoreCase("Y") {
            if temp.planDuration.equalsIgnoreCase("1") {
                measure = "year"
            } else {
                measure = "years"
            }
        }
        lblDuration.text = "\(temp.planDuration) \(measure)"
        
        lblFromDate.text = "\(ConvertionClass().conLongToDate(temp.fromDate, dateFormat: "dd MMM, yyyy")) "
        lblToDate.text = "Expires on \(ConvertionClass().conLongToDate(temp.toDate, dateFormat: "dd MMM, yyyy"))"
        
        do {
            try getExpireDaysLeft(temp: temp)
        } catch {}
    }
    
    private func getExpireDaysLeft(temp: ArrSubscription) throws {
        
        let expireLeft = ConvertionClass().getDiffFromLicenseExpire(temp.toDate, serverTime: ConvertionClass().currentTime())
        
        let tempArr: [String] = expireLeft.components(separatedBy: " ")
        lblDaysLeft.text = "\(expireLeft) left"
        
        let time: Int = Int(tempArr[0])!
        if (time <= 0) {
            lblDaysLeft.text = "Expired"
            lblDaysLeft.textColor = CustomColor.mainBloodRedColor()
        } else {
            lblDaysLeft.textColor = CustomColor.lightGreenPressColor()
        }
    }
    
}
