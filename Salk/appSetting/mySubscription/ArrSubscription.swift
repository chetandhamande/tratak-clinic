//
//  ArrSubscription.swift
//  Salk
//
//  Created by Salk on 27/03/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import Foundation

class ArrSubscription {
    var planName = "", details = "", planDuration = "", voucherKey = "", measure = "", companyName = ""
    var fromDate: Double = 0, toDate: Double = 0
    var status:Bool = false
}
