//
//  HelpOptionVC.swift
//  Salk
//
//  Created by Salk on 25/03/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async

class HelpOptionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var settingTableView: UITableView!
    
    let optionTableCellIdentifier = "OptionsTableViewCell"
    
    var settingArray : [String?] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingArray = ["naviPrivacyPolicy".localized, "naviTermsofService".localized, "naviPaymentFeesTaxes".localized, "naviSecurityCompliance".localized, "naviRateThis".localized]
        
        settingTableView.delegate = self
        settingTableView.dataSource = self
        settingTableView.reloadData()
        
        prepareNavigationItem()
        
        Async.main(after: 0.1, {
            self.settingTableView.reloadData()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviHelp".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.titleLabel.drawText(in: UIEdgeInsetsInsetRect(navigationItem.titleLabel.frame, UIEdgeInsetsMake(0, 20, 0, 20)))
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var settingCell: OptionsTableViewCell! = settingTableView.dequeueReusableCell(withIdentifier: optionTableCellIdentifier) as? OptionsTableViewCell
        
        if settingCell == nil {
            settingTableView.register(UINib(nibName: optionTableCellIdentifier, bundle: nil), forCellReuseIdentifier: optionTableCellIdentifier)
            settingCell = tableView.dequeueReusableCell(withIdentifier: optionTableCellIdentifier) as? OptionsTableViewCell
        }
        
        settingCell.optionLabel.text = settingArray[indexPath.row]
        settingCell.optionImage.isHidden = true
        settingCell.optionLabel.x = 16
        
        return settingCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if settingArray[indexPath.row] == "naviRateThis".localized {
            UIApplication.shared.openURL(URL(string : "itms-apps://itunes.apple.com/app/id" + "app_id".applocalized)!)
        } else if settingArray[indexPath.row] == "naviPrivacyPolicy".localized {
            self.openDisclaimerLink(urlStr: "https://salk.healthcare/legal.php#privacy-policy")
        } else if settingArray[indexPath.row] == "naviTermsofService".localized {
            self.openDisclaimerLink(urlStr: "https://salk.healthcare/legal.php#terms")
        } else if settingArray[indexPath.row] == "naviPaymentFeesTaxes".localized {
            self.openDisclaimerLink(urlStr: "https://salk.healthcare/legal.php#billing")
        } else if settingArray[indexPath.row] == "naviSecurityCompliance".localized {
            self.openDisclaimerLink(urlStr: "https://salk.healthcare/legal.php#security")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    private func openDisclaimerLink(urlStr: String) {
        let url = URL(string: urlStr)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
}
