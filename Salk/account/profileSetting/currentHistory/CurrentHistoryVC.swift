//
//  CurrentHistoryVC.swift
//  Salk
//
//  Created by Chetan on 02/01/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async

class CurrentHistoryVC: UIViewController {

    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var ObjMView: MedicationView!
    @IBOutlet weak var ObjHView: HerbsView!
    @IBOutlet weak var ObjSView: SupplementsView!
    
    fileprivate var loadingObj = CustomLoading()
    
    private var medPanel1: CurrentHistoryCell! = nil, medPanel2: CurrentHistoryCell! = nil, medPanel3: CurrentHistoryCell! = nil, medPanel4: CurrentHistoryCell! = nil,
    medPanel5: CurrentHistoryCell! = nil, medPanel6: CurrentHistoryCell! = nil, medPanel7: CurrentHistoryCell! = nil, medPanel8: CurrentHistoryCell! = nil
    private var herbPanel1: CurrentHistoryCell! = nil, herbPanel2: CurrentHistoryCell! = nil, herbPanel3: CurrentHistoryCell! = nil, herbPanel4: CurrentHistoryCell! = nil,
    herbPanel5: CurrentHistoryCell! = nil, herbPanel6: CurrentHistoryCell! = nil, herbPanel7: CurrentHistoryCell! = nil, herbPanel8: CurrentHistoryCell! = nil
    private var suplPanel1: CurrentHistoryCell! = nil, suplPanel2: CurrentHistoryCell! = nil, suplPanel3: CurrentHistoryCell! = nil, suplPanel4: CurrentHistoryCell! = nil,
    suplPanel5: CurrentHistoryCell! = nil, suplPanel6: CurrentHistoryCell! = nil, suplPanel7: CurrentHistoryCell! = nil, suplPanel8: CurrentHistoryCell! = nil
    
    var arr = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        mainScrollView.backgroundColor = CustomColor.background()
        
        prepareNavigationItem()
        
        ObjMView.configureView(self)
        ObjHView.configureView(self)
        ObjSView.configureView(self)
        
        Async.main(after: 0.1, {
            self.init1()
        })
        
        //getting Latest record
        getRecordServer()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviCurrentHist".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
    }
        
    private func init1() {
        medPanel1 = ObjMView.currTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! CurrentHistoryCell
        medPanel2 = ObjMView.currTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! CurrentHistoryCell
        medPanel3 = ObjMView.currTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! CurrentHistoryCell
        medPanel4 = ObjMView.currTableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! CurrentHistoryCell
        medPanel5 = ObjMView.currTableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! CurrentHistoryCell
        medPanel6 = ObjMView.currTableView.cellForRow(at: IndexPath(row: 5, section: 0)) as! CurrentHistoryCell
        medPanel7 = ObjMView.currTableView.cellForRow(at: IndexPath(row: 6, section: 0)) as! CurrentHistoryCell
        medPanel8 = ObjMView.currTableView.cellForRow(at: IndexPath(row: 7, section: 0)) as! CurrentHistoryCell
        
        herbPanel1 = ObjHView.currTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! CurrentHistoryCell
        herbPanel2 = ObjHView.currTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! CurrentHistoryCell
        herbPanel3 = ObjHView.currTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! CurrentHistoryCell
        herbPanel4 = ObjHView.currTableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! CurrentHistoryCell
        herbPanel5 = ObjHView.currTableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! CurrentHistoryCell
        herbPanel6 = ObjHView.currTableView.cellForRow(at: IndexPath(row: 5, section: 0)) as! CurrentHistoryCell
        herbPanel7 = ObjHView.currTableView.cellForRow(at: IndexPath(row: 6, section: 0)) as! CurrentHistoryCell
        herbPanel8 = ObjHView.currTableView.cellForRow(at: IndexPath(row: 7, section: 0)) as! CurrentHistoryCell
        
        suplPanel1 = ObjSView.currTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! CurrentHistoryCell
        suplPanel2 = ObjSView.currTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! CurrentHistoryCell
        suplPanel3 = ObjSView.currTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as! CurrentHistoryCell
        suplPanel4 = ObjSView.currTableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! CurrentHistoryCell
        suplPanel5 = ObjSView.currTableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! CurrentHistoryCell
        suplPanel6 = ObjSView.currTableView.cellForRow(at: IndexPath(row: 5, section: 0)) as! CurrentHistoryCell
        suplPanel7 = ObjSView.currTableView.cellForRow(at: IndexPath(row: 6, section: 0)) as! CurrentHistoryCell
        suplPanel8 = ObjSView.currTableView.cellForRow(at: IndexPath(row: 7, section: 0)) as! CurrentHistoryCell
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(PainHistoryVC.dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        refreshParameters()
    }
    
    @objc private func performNextOpr() {
        dismissKeyboard()
        
        let datasource = DBOperation()
        datasource.openDatabase(true)
       
        datasource.UpdateAssessmentItemTable(paramName: "medication_brand_name1", paramVal: medPanel1.getBrandName(), groupType: "assGrpCurrentHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "medication_generic_name1", paramVal: medPanel1.getGenericName(), groupType: "assGrpCurrentHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "medication_dose1", paramVal: medPanel1.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_duration1", paramVal: medPanel1.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_brand_name2", paramVal: medPanel2.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_generic_name2", paramVal: medPanel2.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_dose2", paramVal: medPanel2.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_duration2", paramVal: medPanel2.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_brand_name3", paramVal: medPanel3.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_generic_name3", paramVal: medPanel3.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_dose3", paramVal: medPanel3.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_duration3", paramVal: medPanel3.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_brand_name4", paramVal: medPanel4.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_generic_name4", paramVal: medPanel4.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_dose4", paramVal: medPanel4.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_duration4", paramVal: medPanel4.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_brand_name5", paramVal: medPanel5.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_generic_name5", paramVal: medPanel5.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_dose5", paramVal: medPanel5.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_duration5", paramVal: medPanel5.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_brand_name6", paramVal: medPanel6.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_generic_name6", paramVal: medPanel6.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_dose6", paramVal: medPanel6.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_duration6", paramVal: medPanel6.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_brand_name7", paramVal: medPanel7.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_generic_name7", paramVal: medPanel7.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_dose7", paramVal: medPanel7.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_duration7", paramVal: medPanel7.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_brand_name8", paramVal: medPanel8.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_generic_name8", paramVal: medPanel8.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_dose8", paramVal: medPanel8.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "medication_duration8", paramVal: medPanel8.getDuration(), groupType: "assGrpCurrentHistory".localized);
        
        datasource.UpdateAssessmentItemTable(paramName: "herbs_brand_name1", paramVal: herbPanel1.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_generic_name1", paramVal: herbPanel1.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_dose1", paramVal: herbPanel1.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_duration1", paramVal: herbPanel1.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_brand_name2", paramVal: herbPanel2.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_generic_name2", paramVal: herbPanel2.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_dose2", paramVal: herbPanel2.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_duration2", paramVal: herbPanel2.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_brand_name3", paramVal: herbPanel3.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_generic_name3", paramVal: herbPanel3.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_dose3", paramVal: herbPanel3.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_duration3", paramVal: herbPanel3.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_brand_name4", paramVal: herbPanel4.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_generic_name4", paramVal: herbPanel4.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_dose4", paramVal: herbPanel4.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_duration4", paramVal: herbPanel4.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_brand_name5", paramVal: herbPanel5.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_generic_name5", paramVal: herbPanel5.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_dose5", paramVal: herbPanel5.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_duration5", paramVal: herbPanel5.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_brand_name6", paramVal: herbPanel6.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_generic_name6", paramVal: herbPanel6.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_dose6", paramVal: herbPanel6.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_duration6", paramVal: herbPanel6.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_brand_name7", paramVal: herbPanel7.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_generic_name7", paramVal: herbPanel7.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_dose7", paramVal: herbPanel7.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_duration7", paramVal: herbPanel7.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_brand_name8", paramVal: herbPanel8.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_generic_name8", paramVal: herbPanel8.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_dose8", paramVal: herbPanel8.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "herbs_duration8", paramVal: herbPanel8.getDuration(), groupType: "assGrpCurrentHistory".localized);
        
        datasource.UpdateAssessmentItemTable(paramName: "supplements_brand_name1", paramVal: suplPanel1.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_generic_name1", paramVal: suplPanel1.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_dose1", paramVal: suplPanel1.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_duration1", paramVal: suplPanel1.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_brand_name2", paramVal: suplPanel2.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_generic_name2", paramVal: suplPanel2.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_dose2", paramVal: suplPanel2.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_duration2", paramVal: suplPanel2.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_brand_name3", paramVal: suplPanel3.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_generic_name3", paramVal: suplPanel3.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_dose3", paramVal: suplPanel3.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_duration3", paramVal: suplPanel3.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_brand_name4", paramVal: suplPanel4.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_generic_name4", paramVal: suplPanel4.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_dose4", paramVal: suplPanel4.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_duration4", paramVal: suplPanel4.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_brand_name5", paramVal: suplPanel5.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_generic_name5", paramVal: suplPanel5.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_dose5", paramVal: suplPanel5.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_duration5", paramVal: suplPanel5.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_brand_name6", paramVal: suplPanel6.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_generic_name6", paramVal: suplPanel6.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_dose6", paramVal: suplPanel6.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_duration6", paramVal: suplPanel6.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_brand_name7", paramVal: suplPanel7.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_generic_name7", paramVal: suplPanel7.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_dose7", paramVal: suplPanel7.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_duration7", paramVal: suplPanel7.getDuration(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_brand_name8", paramVal: suplPanel8.getBrandName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_generic_name8", paramVal: suplPanel8.getGenericName(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_dose8", paramVal: suplPanel8.getDose(), groupType: "assGrpCurrentHistory".localized);
        datasource.UpdateAssessmentItemTable(paramName: "supplements_duration8", paramVal: suplPanel8.getDuration(), groupType: "assGrpCurrentHistory".localized);
        
        datasource.closeDatabase();
        
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loadingObj.loading())
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpCurrentHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func refreshParameters() {
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
            + " WHERE " + datasource.dbGroupType + " = '" + "assGrpCurrentHistory".localized + "' ")
        
        if cursor != nil {
            while cursor!.next() {
                if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_brand_name1") {
                    medPanel1.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_generic_name1") {
                    medPanel1.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_dose1") {
                    medPanel1.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_duration1") {
                    medPanel1.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_brand_name2") {
                    medPanel2.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_generic_name2") {
                    medPanel2.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_dose2") {
                    medPanel2.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_duration2") {
                    medPanel2.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_brand_name3") {
                    medPanel3.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_generic_name3") {
                    medPanel3.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_dose3") {
                    medPanel3.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_duration3") {
                    medPanel3.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_brand_name4") {
                    medPanel4.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_generic_name4") {
                    medPanel4.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_dose4") {
                    medPanel4.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_duration4") {
                    medPanel4.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_brand_name5") {
                    medPanel5.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_generic_name5") {
                    medPanel5.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_dose5") {
                    medPanel5.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_duration5") {
                    medPanel5.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_brand_name6") {
                    medPanel6.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_generic_name6") {
                    medPanel6.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_dose6") {
                    medPanel6.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_duration6") {
                    medPanel6.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_brand_name7") {
                    medPanel7.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_generic_name7") {
                    medPanel7.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_dose7") {
                    medPanel7.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_duration7") {
                    medPanel7.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_brand_name8") {
                    medPanel8.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_generic_name8") {
                    medPanel8.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_dose8") {
                    medPanel8.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("medication_duration8") {
                    medPanel8.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_brand_name1") {
                    herbPanel1.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_generic_name1") {
                    herbPanel1.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_dose1") {
                    herbPanel1.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_duration1") {
                    herbPanel1.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_brand_name2") {
                    herbPanel2.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_generic_name2") {
                    herbPanel2.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_dose2") {
                    herbPanel2.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_duration2") {
                    herbPanel2.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_brand_name3") {
                    herbPanel3.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_generic_name3") {
                    herbPanel3.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_dose3") {
                    herbPanel3.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_duration3") {
                    herbPanel3.setDuration(cursor!.string(forColumnIndex: 1))
                }
                else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_brand_name4") {
                    herbPanel4.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_generic_name4") {
                    herbPanel4.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_dose4") {
                    herbPanel4.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_duration4") {
                    herbPanel4.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_brand_name5") {
                    herbPanel5.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_generic_name5") {
                    herbPanel5.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_dose5") {
                    herbPanel5.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_duration5") {
                    herbPanel5.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_brand_name6") {
                    herbPanel6.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_generic_name6") {
                    herbPanel6.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_dose6") {
                    herbPanel6.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_duration6") {
                    herbPanel6.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_brand_name7") {
                    herbPanel7.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_generic_name7") {
                    herbPanel7.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_dose7") {
                    herbPanel7.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_duration7") {
                    herbPanel7.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_brand_name8") {
                    herbPanel8.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_generic_name8") {
                    herbPanel8.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_dose8") {
                    herbPanel8.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("herbs_duration8") {
                    herbPanel8.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_brand_name1") {
                    suplPanel1.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_generic_name1") {
                    suplPanel1.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_dose1") {
                    suplPanel1.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_duration1") {
                    suplPanel1.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_brand_name2") {
                    suplPanel2.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_generic_name2") {
                    suplPanel2.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_dose2") {
                    suplPanel2.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_duration2") {
                    suplPanel2.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_brand_name3") {
                    suplPanel3.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_generic_name3") {
                    suplPanel3.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_dose3") {
                    suplPanel3.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_duration3") {
                    suplPanel3.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_brand_name4") {
                    suplPanel4.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_generic_name4") {
                    suplPanel4.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_dose4") {
                    suplPanel4.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_duration4") {
                    suplPanel4.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_brand_name5") {
                    suplPanel5.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_generic_name5") {
                    suplPanel5.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_dose5") {
                    suplPanel5.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_duration5") {
                    suplPanel5.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_brand_name6") {
                    suplPanel6.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_generic_name6") {
                    suplPanel6.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_dose6") {
                    suplPanel6.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_duration6") {
                    suplPanel6.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_brand_name7") {
                    suplPanel7.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_generic_name7") {
                    suplPanel7.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_dose7") {
                    suplPanel7.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_duration7") {
                    suplPanel7.setDuration(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_brand_name8") {
                    suplPanel8.setBrandName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_generic_name8") {
                    suplPanel8.setGenericName(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_dose8") {
                    suplPanel8.setDose(cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("supplements_duration8") {
                    suplPanel8.setDuration(cursor!.string(forColumnIndex: 1))
                }
            }
            cursor!.close()
            datasource.closeDatabase()
        }
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpCurrentHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)

        medPanel1.picker.isHidden = true
        medPanel2.picker.isHidden = true
        medPanel3.picker.isHidden = true
        medPanel4.picker.isHidden = true
        medPanel5.picker.isHidden = true
        medPanel6.picker.isHidden = true
        medPanel7.picker.isHidden = true
        medPanel8.picker.isHidden = true
        
        herbPanel1.picker.isHidden = true
        herbPanel2.picker.isHidden = true
        herbPanel3.picker.isHidden = true
        herbPanel4.picker.isHidden = true
        herbPanel5.picker.isHidden = true
        herbPanel6.picker.isHidden = true
        herbPanel7.picker.isHidden = true
        herbPanel8.picker.isHidden = true
        
        suplPanel1.picker.isHidden = true
        suplPanel2.picker.isHidden = true
        suplPanel3.picker.isHidden = true
        suplPanel4.picker.isHidden = true
        suplPanel5.picker.isHidden = true
        suplPanel6.picker.isHidden = true
        suplPanel7.picker.isHidden = true
        suplPanel8.picker.isHidden = true
    }
    
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
