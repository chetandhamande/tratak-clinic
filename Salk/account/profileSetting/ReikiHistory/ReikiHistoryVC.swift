//
//  ReikiHistoryVC.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class ReikiHistoryVC: UIViewController {
    
    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var reikeObj1: ReikeView1!

    fileprivate var loadingObj = CustomLoading()
    
    private var selTimeSet: String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mainScrollView.backgroundColor = CustomColor.background()
        prepareNavigationItem()
        
        refreshParameters()
        
        //getting Latest record
        getRecordServer()
    }
    
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviReikiHist".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
    }
    
    @objc func performNextOpr() {
                
        let datasource = DBOperation()
        datasource.openDatabase(true)
        var underPhy: String = "", reikiSession = ""
        
        underPhy = reikeObj1.underPhy
        reikiSession = reikeObj1.reikiSession
        
        datasource.UpdateAssessmentItemTable(paramName: "under_physician", paramVal: underPhy, groupType: "assGrpReikiHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "reiki_session", paramVal: reikiSession, groupType: "assGrpReikiHistory".localized)
        
        var lastSession: String = ""
//        let dateLong = DateTimeCasting.getLongDateFrmString(btnHosession.getText().toString()
//            +" "+btnTime.getText().toString(),"dd MMM yyyy hh mm a");
//        lastSession = DateTimeCasting.getDateStringFrmLong(dateLong);
        
        datasource.UpdateAssessmentItemTable(paramName: "last_session", paramVal: lastSession, groupType: "assGrpReikiHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "previous_session", paramVal: lastSession, groupType: "assGrpReikiHistory".localized) //btnPreSession.getText().toString()
        datasource.UpdateAssessmentItemTable(paramName: "concern_area", paramVal: lastSession, groupType: "assGrpReikiHistory".localized) //editConcern().toString()
        datasource.UpdateAssessmentItemTable(paramName: "sensitive_perfumes", paramVal: lastSession, groupType: "assGrpReikiHistory".localized) //btnFragrances().toString()
        datasource.UpdateAssessmentItemTable(paramName: "sensitive_touch", paramVal: lastSession, groupType: "assGrpReikiHistory".localized) //btnTouch().toString()
        
        datasource.closeDatabase()
        
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loadingObj.loading())
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpReikiHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshParameters() {
        var underPhy: String = "", reikiSession = "", lastSession = ""
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
        let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
            + " WHERE " + datasource.dbGroupType + " = '" + "assGrpReikiHistory".localized + "' ")
        
        if cursor != nil {
            while cursor!.next() {
                if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("under_physician") {
                    underPhy = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("reiki_session") {
                    reikiSession = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("last_session") {
                    lastSession = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("previous_session") {
//                    btnPreSession.setText( cursor.getString(1));
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("concern_area") {
//                    editConcern.setText( cursor.getString(1));
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("sensitive_perfumes") {
//                    btnFragrances.setText( cursor.getString(1));
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("sensitive_touch") {
                    //                    btnTouch.setText( cursor.getString(1));
                }
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        if !underPhy.trimmed.isEmpty {
            reikeObj1.underPhy = underPhy
            if underPhy.equals(reikeObj1.rdgReikiCaYes.titleLabel!.text!) {
                reikeObj1.rdgReikiCaYes.isSelected = true
            } else {
                reikeObj1.rdgReikiCaNo.isSelected = true
            }
        }
        
        if !reikiSession.trimmed.isEmpty {
            reikeObj1.reikiSession = reikiSession
            if reikiSession.equals(reikeObj1.rdgReikiSessionYes.titleLabel!.text!) {
                reikeObj1.rdgReikiSessionYes.isSelected = true
            } else {
                reikeObj1.rdgReikiSessionNo.isSelected = true
            }
        }
        
        if !lastSession.trimmed.isEmpty {
//            try{
//            long dateLong = DateTimeCasting.getLongDateFrmString(lastSession);
//
//            btnHosession.setText(DateTimeCasting.getDateStringFrmLong(dateLong, "dd MMM yyyy"));
//            btnTime.setText(DateTimeCasting.getDateStringFrmLong(dateLong, "hh mm a"));
//            } catch (Exception e){}
        }
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpReikiHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
