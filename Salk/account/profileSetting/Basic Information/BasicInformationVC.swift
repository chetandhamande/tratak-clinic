//
//  BasicInformationVC.swift
//  Salk
//
//  Created by Chetan  on 16/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material
import MaterialControls
import DatePickerDialog

class BasicInformationVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource  {
    
    //    GenderViewDelegate
    
    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var basicObj: BasicInfoView!
    @IBOutlet weak var bodyShapeObj: BodyShapeView!
    @IBOutlet weak var bodyObj: BodyMeasurementView!
    
    
    fileprivate var gender:String = "Male"
    
    fileprivate var loadingObj = CustomLoading()
    
    fileprivate let pickerTitle = ["1", "2", "3", "4", "5"]
    fileprivate let arrLifestyle = ["Little or no activity", "Light activity 1-3 days a week".localized, "Moderate activity 3-5 days a week".localized, "Heavy activity 6-7 days a week".localized, "Very Heavy Activity - Twice a day, extra heavy activities".localized]
    fileprivate let arrBloodGroup = ["O+ve", "O-ve", "A+ve", "A-ve", "B+ve", "B-ve", "AB+ve"]
    fileprivate let arrHeightUnit = ["Ft & In", "cm"]
    fileprivate let arrWeightUnit = ["kg", "Lbs"]
    
    fileprivate var arr = [String]()
    
    fileprivate var dobLongStr:Double = 0
    
    fileprivate var bodyShape: String = "", heightRuler = "0", weightRuler = "0", waistRuler = "0", chestRuler = "0", hipRuler = "0"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainScrollView.backgroundColor = CustomColor.background()
        picker.backgroundColor = CustomColor.grayLight()
        picker.showsSelectionIndicator = true
        self.picker.delegate = self
        self.picker.dataSource = self
        UIApplication.shared.keyWindow!.bringSubview(toFront: picker)
        
        prepareNavigationItem()
        
        init1()
        
        //getting Latest record
        self.getRecordServer()
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(BasicInformationVC.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func init1() {
        
        basicObj.btnLifestyle.addTarget(self, action: #selector(performChangeLifeStyle), for: .touchUpInside)
        basicObj.btnBlood.addTarget(self, action: #selector(performChangeBloodGroup), for: .touchUpInside)
        basicObj.btnHeight.addTarget(self, action: #selector(performChangeHeight), for: .touchUpInside)
        basicObj.btnWeight.addTarget(self, action: #selector(performChangeWeight), for: .touchUpInside)
        
        bodyShapeObj.btnVeryLean.addTarget(self, action: #selector(clickVeryLean), for: .touchUpInside)
        bodyShapeObj.btnLean.addTarget(self, action: #selector(clickLean), for: .touchUpInside)
        bodyShapeObj.btnSlightlyHeavy.addTarget(self, action: #selector(clickSlightlyHeavy), for: .touchUpInside)
        bodyShapeObj.btnHeavy.addTarget(self, action: #selector(clickHeavy), for: .touchUpInside)
        bodyShapeObj.btnPrettyHeavy.addTarget(self, action: #selector(clickPrettyHeavy), for: .touchUpInside)
        
        decideBodyShape()
        refreshParameters()
    }
    
    func decideBodyShape() {
        
        let appPref = MySharedPreferences()
        if appPref.getGender().equalsIgnoreCase("MALE") {
            bodyShapeObj.imgVeryLean.image = UIImage(named: "very_lean.png")!
            bodyShapeObj.imgLean.image = UIImage(named: "lean.png")!
            bodyShapeObj.imgSlightlyHeavy.image = UIImage(named: "slightly_heavy.png")!
            bodyShapeObj.imgPrettyHeavy.image = UIImage(named: "pretty_heavy.png")!
            bodyShapeObj.imgHeavy.image = UIImage(named: "heavy.png")!
            
            bodyShapeObj.refreshView()
        } else {
            bodyShapeObj.lblVeryLean.text = "Slim"
            bodyShapeObj.imgVeryLean.image = UIImage(named: "g_slim.png")!
            bodyShapeObj.lblLean.text = "Average"
            bodyShapeObj.imgLean.image = UIImage(named: "g_average.png")!
            bodyShapeObj.imgPrettyHeavy.image = UIImage(named: "g_pretty_heavy.png")!
            bodyShapeObj.imgHeavy.image = UIImage(named: "g_heavy.png")!
            
            bodyShapeObj.viewSlightlyHeavy.isHidden = true
            bodyShapeObj.refreshView()
        }
        
        setBodyShap(bodyShape)
    }
    
    @objc func clickVeryLean() {
        let appPref = MySharedPreferences()
        if appPref.getGender().equalsIgnoreCase("MALE") {
            setBodyShap("Very lean")
        } else {
            setBodyShap("Slim")
        }
    }
    @objc func clickLean() {
        let appPref = MySharedPreferences()
        if appPref.getGender().equalsIgnoreCase("MALE") {
            setBodyShap("Lean")
        } else {
            setBodyShap("Average")
        }
    }
    @objc func clickSlightlyHeavy() {
        setBodyShap("Slightly Heavy")
    }
    @objc func clickHeavy() {
        setBodyShap("Heavy");
    }
    @objc func clickPrettyHeavy() {
        setBodyShap("Pretty Heavy");
    }
    
    @objc func setBodyShap(_ shape: String) {
        resetBackground()
        bodyShapeObj.lblHeading.text = "Body shape - " + shape
        bodyShape = shape
        
        
        let appPref = MySharedPreferences()
        if appPref.getGender().equalsIgnoreCase("MALE") {
            if shape.equals("Very lean") {
                bodyShapeObj.viewVeryLean.backgroundColor = CustomColor.tranparentBlue()
            } else if shape.equals("Slightly Heavy") {
                bodyShapeObj.viewSlightlyHeavy.backgroundColor = CustomColor.tranparentBlue()
            } else if shape.equals("Pretty Heavy") {
                bodyShapeObj.viewPrettyHeavy.backgroundColor = CustomColor.tranparentBlue()
            } else if shape.equals("Heavy") {
                bodyShapeObj.viewHeavy.backgroundColor = CustomColor.tranparentBlue()
            } else if shape.equals("Lean") {
               bodyShapeObj.viewLean.backgroundColor = CustomColor.tranparentBlue()
            }
            bodyShapeObj.refreshView()
        } else {
            if shape.equals("Slim") {
                bodyShapeObj.viewVeryLean.backgroundColor = CustomColor.tranparentBlue()                
            } else if shape.equals("Average") {
                bodyShapeObj.viewLean.backgroundColor = CustomColor.tranparentBlue()
            } else if shape.equals("Heavy") {
                bodyShapeObj.viewHeavy.backgroundColor = CustomColor.tranparentBlue()
            } else if shape.equals("Pretty Heavy") {
                bodyShapeObj.viewPrettyHeavy.backgroundColor = CustomColor.tranparentBlue()
            }
            bodyShapeObj.refreshView()
        }
    }
    
    private func resetBackground() {
        bodyShapeObj.viewVeryLean.backgroundColor = UIColor.clear
        bodyShapeObj.viewSlightlyHeavy.backgroundColor = UIColor.clear
        bodyShapeObj.viewPrettyHeavy.backgroundColor = UIColor.clear
        bodyShapeObj.viewHeavy.backgroundColor = UIColor.clear
        bodyShapeObj.viewLean.backgroundColor = UIColor.clear
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        picker.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviBasicInfo".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return arrBloodGroup.count
        } else if pickerView.tag == 2 {
            return arrHeightUnit.count
        } else if pickerView.tag == 3 {
            return arrWeightUnit.count
        } else if pickerView.tag == 4 {
            return arrLifestyle.count
        } else {
            return 0
        }
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if pickerView.tag == 1 {
            return arrBloodGroup[row]
        } else if pickerView.tag == 2 {
            return arrHeightUnit[row]
        } else if pickerView.tag == 3 {
            return arrWeightUnit[row]
        } else if pickerView.tag == 4 {
            return arrLifestyle[row]
        } else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            self.basicObj.configureSelectButton(self.basicObj.btnBlood, setString: arrBloodGroup[row])
        } else if pickerView.tag == 2 {
            self.basicObj.configureSelectButton(self.basicObj.btnHeight, setString: arrHeightUnit[row])
        } else if pickerView.tag == 3 {
            self.basicObj.configureSelectButton(self.basicObj.btnWeight, setString: arrWeightUnit[row])
        } else if pickerView.tag == 4 {
            self.basicObj.configureSelectButton(self.basicObj.btnLifestyle, setString: arrLifestyle[row])
        }
    }
    
    fileprivate func showSweetDialog(_ title: String, message: String) {
        SweetAlert().showOnlyAlert(title, subTitle: message, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        showSweetDialog("Oops...", message: appPref.getErrorMessage())
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension BasicInformationVC {
    
    public func refreshParameters() {
        var heightStr = "", weightStr = "", chestStr = "", waistStr = "", hipStr = ""
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let sql = "SELECT * FROM " + datasource.AssessmentList_tlb
            + " WHERE " + datasource.dbGroupType + " = '"
            + "assGrpBasicInfo".localized + "' "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                if cursor!.string(forColumnIndex: 0) == "lifestyle" {
                    basicObj.configureSelectButton(basicObj.btnLifestyle, setString: cursor!.string(forColumnIndex: 1))
                } else if cursor!.string(forColumnIndex: 0) == "blood_group" {
                    if cursor!.string(forColumnIndex: 1).isEmpty {
                         basicObj.configureButton(basicObj.btnBlood, setString: "Blood Group")
                    } else {
                       basicObj.configureSelectButton(basicObj.btnBlood, setString: cursor!.string(forColumnIndex: 1))
                    }
                } else if cursor!.string(forColumnIndex: 0) == "waist" {
                    waistStr = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0) == "chest" {
                    chestStr = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0) == "hip" {
                    hipStr = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0) == "bmi" {
                    basicObj.txtBMI.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0) == "bmr" {
                    basicObj.txtBMR.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0) == "height" {
                    heightStr = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0) == "weight" {
                    weightStr = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0) == "body_shape" {
                    bodyShape = cursor!.string(forColumnIndex: 1)
                }
            }
            cursor!.close()
        }
        datasource.closeDatabase()
     
        basicObj.txtHeight.text = heightStr
        basicObj.txtWeight.text = weightStr
        
        if weightStr != nil && !weightStr.isEmpty && weightStr != "0" {
            let value = weightStr.split(" ")
            basicObj.txtWeight.text = value[0]
            if value.count == 2 {
            basicObj.configureSelectButton(basicObj.btnWeight, setString: value[1])
            }
        } else {
            basicObj.configureButton(basicObj.btnWeight, setString: "kg")
        }
    
        if heightStr != nil && !heightStr.isEmpty && heightStr != "0" {
            let value = heightStr.split(" ")
            basicObj.txtHeight.text = value[0]
            if value.count == 2 {
            basicObj.configureSelectButton(basicObj.btnHeight, setString: value[1])
            }
        } else {
            basicObj.configureButton(basicObj.btnHeight, setString: "cm")
        }
        
        if chestStr != nil && !chestStr.isEmpty && chestStr != "0" {
            let value = chestStr.split(" ")
            bodyObj.txtChest.text = value[0]
            if value.count == 2 {
                bodyObj.configureSelectButton(bodyObj.btnChest, setString: value[1])
            }
        } else {
            bodyObj.configureButton(bodyObj.btnChest, setString: "cm")
        }
        
        if waistStr != nil && !waistStr.isEmpty && waistStr != "0" {
            let value = waistStr.split(" ")
            bodyObj.txtWaist.text = value[0]
            if value.count == 2 {
            bodyObj.configureSelectButton(bodyObj.btnWaist, setString: value[1])
            }
        } else {
            bodyObj.configureButton(bodyObj.btnWaist, setString: "cm")
        }
        
        if hipStr != nil && !hipStr.isEmpty && hipStr != "0" {
            let value = hipStr.split(" ")
            bodyObj.txtHip.text = value[0]
            if value.count == 2 {
            bodyObj.configureSelectButton(bodyObj.btnHip, setString: value[1])
            }
        } else {
            bodyObj.configureButton(bodyObj.btnHip, setString: "cm")
        }
        
        setBodyShap(bodyShape)
    }
    
    private func calculateBMIBMR() {
        
        let txtWeightUnit = basicObj.btnWeight.titleLabel!.text!
        let txtHeightUnit = basicObj.btnHeight.titleLabel!.text!
        
        if txtWeightUnit.isEmpty {
            return
        }
        if txtHeightUnit.isEmpty {
            return
        }
        
        var bmiStr = "", bmrStr = ""
        
        var weight: Double = Double(weightRuler)! // in Kgs
        if txtWeightUnit.equalsIgnoreCase("LB") {
            weight /= 2.2
        }
        
        var height: Double = Double(heightRuler)!; // in cm
        if txtHeightUnit.equalsIgnoreCase("Ft & In") {
            height *= 30.48
        }
        height /= 100; //in meter
        
        let bmi: Double = weight / (height * height)
        
        bmiStr = String(format: "%.2f", bmi)
        basicObj.txtBMI.text = bmiStr
        
        let appPref = MySharedPreferences()
        if appPref.getDOB().isEmpty {
            return
        }
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        let dob = ConvertionClass().conDateToLong(df.date(from: appPref.getDOB())!)
        let df1 = DateFormatter()
        df1.dateFormat = "yyyy"
        
        let df2 = DateFormatter()
        df2.dateFormat = "MM"
        let df3 = DateFormatter()
        df3.dateFormat = "dd"
        
        let age = getAge(year: Int(ConvertionClass().conLongToDate(dob, dateFormat: df1))!, month: Int(ConvertionClass().conLongToDate(dob, dateFormat: df2))!, day: Int(ConvertionClass().conLongToDate(dob, dateFormat: df3))!)
        
        var bmr: Double = 0.0
        if appPref.getGender().equalsIgnoreCase("Male") {
            let a = 9.6 * weight
            let b = 1.8 * (height * 100)
            let c = 4.7 * Double(age)
            
            bmr = 655 + a + b - c
        } else {
            let a = 13.7 * weight
            let b = 5 * (height * 100)
            let c = 6.8 * Double(age)
            
            bmr = 66 + a + b - c
        }
        
        bmrStr = String(format: "%.2f", bmr)
        basicObj.txtBMI.text = "\(bmrStr)"
        
        //update in database
        let datasource = DBOperation()
        datasource.openDatabase(true)
        datasource.UpdateAssessmentItemTable(paramName: "bmi", paramVal: bmiStr, groupType: "assGrpBasicInfo")
        datasource.UpdateAssessmentItemTable(paramName: "bmr", paramVal: bmrStr, groupType: "assGrpBasicInfo")
        datasource.closeDatabase()
    }
    
    private func getAge(year: Int, month: Int, day: Int) -> Int {
        let myDOB = Calendar.current.date(from: DateComponents(year: year, month: month, day: day))!
        return myDOB.age
    }
    
    func performNextOpr() {
        dismissKeyboard()
        
        heightRuler = basicObj.txtHeight.text!
        weightRuler = basicObj.txtWeight.text!
        hipRuler = bodyObj.txtHip.text!
        waistRuler = bodyObj.txtWaist.text!
        chestRuler = bodyObj.txtChest.text!
        
        let bloodGroup = basicObj.btnBlood.titleLabel!.text!
        let lifestyle = basicObj.btnLifestyle.titleLabel!.text!
        let weightUnit = basicObj.btnWeight.titleLabel!.text!
        let heightUnit = basicObj.btnHeight.titleLabel!.text!
        
        let hipUnit = bodyObj.btnHip.titleLabel!.text!
        let waistUnit = bodyObj.btnWaist.titleLabel!.text!
        let chestUnit = bodyObj.btnChest.titleLabel!.text!
        
        if heightRuler.equals("") {
            showSweetDialog("popupAlert", message: "Missing your height")
        } else if weightRuler.equals("") {
            showSweetDialog("popupAlert", message: "Missing your weight")
        } else
            if lifestyle.isEmpty {
                showSweetDialog("popupAlert", message: "Missing your lifestyle")
            } else if bodyShape.isEmpty {
                showSweetDialog("popupAlert", message: "Missing body shape!")
            } else {
                
                let datasource = DBOperation()
                datasource.openDatabase(true)
                datasource.UpdateAssessmentItemTable(paramName: "blood_group", paramVal: bloodGroup, groupType: "assGrpBasicInfo".localized)
                datasource.UpdateAssessmentItemTable(paramName: "body_shape", paramVal: bodyShape, groupType: "assGrpBasicInfo".localized)
                datasource.UpdateAssessmentItemTable(paramName: "height", paramVal: heightRuler + " " + heightUnit, groupType: "assGrpBasicInfo".localized)
                datasource.UpdateAssessmentItemTable(paramName: "weight", paramVal: weightRuler + " " + weightUnit, groupType: "assGrpBasicInfo".localized)
                datasource.UpdateAssessmentItemTable(paramName: "hip", paramVal: hipRuler + " " + hipUnit, groupType: "assGrpBasicInfo".localized)
                datasource.UpdateAssessmentItemTable(paramName: "waist", paramVal: waistRuler + " " + waistUnit, groupType: "assGrpBasicInfo".localized)
                datasource.UpdateAssessmentItemTable(paramName: "chest", paramVal: chestRuler + " " + chestUnit, groupType: "assGrpBasicInfo".localized)
                datasource.UpdateAssessmentItemTable(paramName: "lifestyle", paramVal: lifestyle, groupType: "assGrpBasicInfo".localized)
                
                datasource.closeDatabase()
                
                // call for getting Register user
                let checkConn = CheckInternetConnection()
                if checkConn.isCheckInternetConnection() {
                    
                    self.view.addSubview(loadingObj.loading())
                    
                    let appPref = MySharedPreferences()
                    appPref.setUpdateReqType(text: "assGrpBasicInfo".localized)
                    UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
                    
                    //call for update old UI on server, only for RightLiving
                    if(appPref.getDrRefCode().equalsIgnoreCase("RLIVING")) {
                        //                    new ConnRequestManager().getRequestManager(mContext,
                        //                            mContext.getString(R.string.MethodRegisterFitness),
                        //                            BasicInformationAct.this);
                    }
                } else {
                    SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
                }
        }
    }
    
    @objc func performChangeHeight() {
        picker.isHidden = false
        picker.tag = 2
        picker.reloadAllComponents()
    }
    
    @objc func performChangeWeight() {
        picker.isHidden = false
        picker.tag = 3
        picker.reloadAllComponents()
    }
    
    @objc func performChangeLifeStyle() {
        picker.isHidden = false
        picker.tag = 4
        picker.reloadAllComponents()
    }
    
    @objc func performChangeBloodGroup() {
        picker.isHidden = false
        picker.tag = 1
        picker.reloadAllComponents()
    }
    
    func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpBasicInfo".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        }
    }
}
