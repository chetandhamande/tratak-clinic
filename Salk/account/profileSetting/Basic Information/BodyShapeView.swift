//
//  BodyShapeView.swift
//  Salk
//
//  Created by Chetan on 07/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class BodyShapeView: UIView {

    @IBOutlet weak var backCardView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    
    @IBOutlet weak var viewVeryLean: UIView!
    @IBOutlet weak var imgVeryLean: UIImageView!
    @IBOutlet weak var lblVeryLean: UILabel!
    
    @IBOutlet weak var viewLean: UIView!
    @IBOutlet weak var imgLean: UIImageView!
    @IBOutlet weak var lblLean: UILabel!
    
    @IBOutlet weak var viewSlightlyHeavy: UIView!
    @IBOutlet weak var imgSlightlyHeavy: UIImageView!
    @IBOutlet weak var lblSlightlyHeavy: UILabel!
    
    @IBOutlet weak var viewHeavy: UIView!
    @IBOutlet weak var imgHeavy: UIImageView!
    @IBOutlet weak var lblHeavy: UILabel!
    
    @IBOutlet weak var viewPrettyHeavy: UIView!
    @IBOutlet weak var imgPrettyHeavy: UIImageView!
    @IBOutlet weak var lblPrettyHeavy: UILabel!
    
    @IBOutlet weak var btnVeryLean: UIButton!
    @IBOutlet weak var btnLean: UIButton!
    @IBOutlet weak var btnSlightlyHeavy: UIButton!
    @IBOutlet weak var btnHeavy: UIButton!
    @IBOutlet weak var btnPrettyHeavy: UIButton!
    
    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "BodyShapeView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        
        return view
    }

    func refreshView() {
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
}
