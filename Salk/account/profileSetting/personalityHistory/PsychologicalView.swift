//
//  PsychologicalView.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import DLRadioButton

class PsychologicalView: UIView {

    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var backCardView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    
    @IBOutlet weak var chkEthenicity1: DLRadioButton!
    @IBOutlet weak var chkEthenicity2: DLRadioButton!
    @IBOutlet weak var chkEthenicity3: DLRadioButton!
    @IBOutlet weak var chkEthenicity4: DLRadioButton!

    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PsychologicalView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        headerView.backgroundColor = CustomColor.blackTransparent1()
        
        createRadioButton(chkEthenicity1, title: "Type A")
        createRadioButton(chkEthenicity2, title: "Type B")
        createRadioButton(chkEthenicity3, title: "Type C")
        createRadioButton(chkEthenicity4, title: "Type D")
    }
    
    private func createRadioButton(_ button: DLRadioButton, title: String) {
        button.indicatorColor = CustomColor.dark_gray()
        button.setTitle(title, for: .normal)
        button.setTitleColor(CustomColor.dark_gray(), for: .normal)
        button.isIconSquare = true
        button.isMultipleSelectionEnabled = true
    }
    
    @objc @IBAction private func radioButtonClick(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
        }
    }
}

