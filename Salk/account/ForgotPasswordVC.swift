//
//  ForgotPasswordVC.swift
//  Salk
//
//  Created by Chetan  on 13/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareNavigationItem()
        
    }
    
    /// Prepares the navigationItem.
    private func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Forgot Password"
        navigationItem.titleLabel.textColor = UIColor.black
        navigationItem.titleLabel.textAlignment = .left
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickSubmit(sender: UIButton) {
        
    }
    

}
