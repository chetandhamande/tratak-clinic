//
//  DBTableInfo.swift
//  Salk
//
//  Created by Chetan  on 28/06/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation
import FMDB

class DBTableInfo {
    
    var pathToDatabase: String!
    var database: FMDatabase!
    
    //////Category list table
    let categoryList_tlb = "categoryListTlb"
    
    let dbCatID = "CatID"
    let dbCatName = "CatName"
    let dbImageName = "ImageName"
    
    //City Table List
    func categoryListTableCreate() {
        sqlExecute("create table "
            + categoryList_tlb + "("
            + dbCatID + " text, " + dbCatName + " text, "
            + dbImageName + " text "
            + " )")
    }
    func categoryListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + categoryList_tlb)
        categoryListTableCreate()
    }
    
    //////Sub Category list table
    let subCategoryList_tlb = "subCategoryListTlb"
    
    let dbSubCatID = "SubCatID"
    let dbSubCatName = "SubCatName"
    
    //City Table List
    func subCategoryListTableCreate() {
        sqlExecute("create table "
            + subCategoryList_tlb + "("
            + dbSubCatID + " text, " + dbCatID + " text, "
            + dbSubCatName + " text, " + dbImageName + " text "
                + " )")
    }
    func subCategoryListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + subCategoryList_tlb)
        subCategoryListTableCreate()
    }
    
    
    //////Author list table
    let AuthorList_tlb = "AuthorListTlb"
    
    let dbAuthID = "AuthID"
    let dbAuthName = "AuthName"
    let dbEmail = "Email"
    let dbMobileNo = "MobileNo"
    let dbGender = "Gender"
    let dbLat = "Latitude"
    let dbLng = "Longitude"
    let dbUserAddr = "UserAddr"
    let dbSpecialist = "Specialist"
    let dbFacebookLink = "FacebookLink"
    let dbTwitterLink = "TwitterLink"
    let dbYoutubeLink = "YoutubeLink"
    let dbGoogleLink = "GoogleLink"
    let dbBioAuthor = "BioAuthor"
    
    
    //Author Table List
    func AuthorListTableCreate() {
        sqlExecute("create table "
            + AuthorList_tlb + "("
            + dbAuthID + " text, " + dbAuthName + " text, "
            + dbImageName + " text, " + dbEmail + " text, "
            + dbMobileNo + " text, " + dbGender + " text, "
            + dbLat + " text, " + dbLng + " text, "
            + dbUserAddr + " text, " + dbSpecialist + " text, "
            + dbFacebookLink + " text, " + dbTwitterLink + " text, "
            + dbGoogleLink + " text, " + dbYoutubeLink + " text, "
            + dbBioAuthor + " text "
            + " )")
    }
    func AuthorListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + AuthorList_tlb)
        AuthorListTableCreate()
    }
    
    //////Article list table
    let ArticleList_tlb = "ArticleListTlb"
    
    let dbArticleID = "ArticleID"
    let dbMsgDescri = "MsgDescri"
    let dbMsgTitle = "MsgTitle"
    let dbDateTime = "DateTime"
    let dbIsSaveArti = "IsSaveArti"
    let dbImageKey = "ImageKey"
    let dbPosition = "Position"
    
    
    //Article Table List
    func ArticleListTableCreate() {
        sqlExecute("create table "
            + ArticleList_tlb + "("
            + dbArticleID + " text, " + dbMsgTitle + " text, "
            + dbMsgDescri + " text, " + dbDateTime + " text, "
            + dbImageName + " text, " + dbCatID + " text, "
            + dbAuthID + " text, " + dbIsSaveArti + " text, "
            + dbImageKey + " text, " + dbPosition + " text "
            + " )")
    }
    func ArticleListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + ArticleList_tlb)
        ArticleListTableCreate()
    }
    
    //////Article Page list table
    let ArticlePageList_tlb = "ArticlePageListTlb"
    
    let dbArtiPageIndex = "ArtiPageIndex"
    
    //ArticlePageTable List
    func ArticlePageListTableCreate() {
        sqlExecute("create table "
            + ArticlePageList_tlb + "("
            + dbArticleID + " text, " + dbMsgTitle + " text, "
            + dbMsgDescri + " text, " + dbImageName + " text, "
            + dbArtiPageIndex + " integer, " + dbImageKey + " text "
            + " )")
    }
    func ArticlePageListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + ArticlePageList_tlb)
        ArticlePageListTableCreate()
    }
    
    
    ////////////////////Fitness Health Tracking
    
    //////Water list table
    let WaterTrackList_tlb = "WaterTrackListTlb"
    
    let dbWaterGlass = "WaterGlass"
    let dbIsServerSync = "IsServerSync"
    let dbIsDeleteFlag = "IsDeleteFlag" //1- delete, 0- add
    
    //WaterTrack Table List
    func WaterTrackListTableCreate() {
        sqlExecute("create table "
            + WaterTrackList_tlb + "("
            + dbWaterGlass + " INTEGER, "
            + dbDateTime + " text, " + dbIsServerSync + " text, " + dbIsDeleteFlag + " text"
                + " )")
    }
    func WaterTrackListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + WaterTrackList_tlb)
        WaterTrackListTableCreate()
    }
    
    //////Sleep list table
    let SleepTrackList_tlb = "SleepTrackListTlb"
    
    let dbSleepMinutes = "SleepMinutes"
    let dbSleepValue = "SleepValue"
    let dbSleepQuality = "SleepQuality"
    
    //Sleep Track Table List
    func SleepTrackListTableCreate() {
        sqlExecute("create table "
            + SleepTrackList_tlb + "("
            + dbSleepMinutes + " INTEGER, " + dbSleepValue + " text, "
                + dbDateTime + " text, " + dbIsServerSync + " text, "
            + dbSleepQuality + " text "
            + " )")
    }
    func SleepTrackListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + SleepTrackList_tlb)
        SleepTrackListTableCreate()
    }
    
    //////Sport Activity List
    let SportActivityList_tlb = "SportActivityListTlb"
    let dbActName = "ActName"
    let dbActID = "ActID"
    let dbCalories = "Calories"
    
    //SportActivity Table List
    func SportActivityListTableCreate() {
        sqlExecute("create table "
            + SportActivityList_tlb + "("
            + dbActID + " text, " + dbActName + " text, "
            + dbCalories + " text "
            + " )")
    }
    func SportActivityListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + SportActivityList_tlb)
        SportActivityListTableCreate()
    }
    
    
    //////Sport Activity List
    let SportActivityRecordList_tlb = "SportActivityRecordListTlb"
    
    let recID = "RecID"
    let dbDuration = "Duration"
    let dbActDoType = "ActDoType"
    
    //SportActivity Table List
    func SportActivityRecordListTableCreate() {
        sqlExecute("create table "
            + SportActivityRecordList_tlb + "("
            + dbActID + " text, " + dbDuration + " text, "
            + dbDateTime + " text, " + dbIsServerSync + " text, "
            + dbActDoType + " text, " + recID + " integer primary key autoincrement "
            + " )")
    }
    func SportActivityRecordListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + SportActivityRecordList_tlb)
        SportActivityRecordListTableCreate()
    }
    
    
    ////////Container info
    let FoodContainerList_tlb = "FoodContainerListTlb"
    let dbContainerName = "ContainerName"
    let dbContainID = "ContainID"
    let dbContainValue = "ContainValue"
    let dbContainUnit = "ContainUnit"
    
    //Food Container record Table List
    func FoodContainerListTableCreate() {
        sqlExecute("create table "
            + FoodContainerList_tlb + "("
            + dbContainID + " text, " + dbContainerName + " text, "
            + dbContainValue + " text, " + dbContainUnit + " text "
            + " )")
    }
    func FoodContainerListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + FoodContainerList_tlb)
        FoodContainerListTableCreate()
    }
    /////////End container info
    
    
    //////Food Record List
    let FoodRecordList_tlb = "FoodRecordListTlb"
    let dbFoodName = "FoodName"
    let dbFoodID = "FoodID"
    let dbProtein = "Protein"
    let dbFat = "Fat"
    let dbCarbs = "Carbs"
    let dbFiber = "Fiber"
    
    //Food record Table List
    func FoodRecordListTableCreate() {
        sqlExecute("create table "
            + FoodRecordList_tlb + "("
            + dbFoodID + " text, " + dbFoodName + " text, "
            + dbCalories + " text, " + dbProtein + " text, "
            + dbFat + " text, " + dbCarbs + " text, "
            + dbFiber + " text, " + dbContainID + " text "
            + " )")
    }
    func FoodRecordListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + FoodRecordList_tlb)
        FoodRecordListTableCreate()
    }
    
    
    //////Food Record Log List
    let FoodRecordLogList_tlb = "FoodRecordLogListTlb"
    let dbQuantity = "Quantity"
    let dbISRoutineRec = "ISRoutineRec" //0 - log record, 1 - routine record
    let dbRouteName = "RouteName"  //name of routing line lunch, dinner, breakfast
    
    //Food record Table List
    func FoodRecordLogListTableCreate() {
        sqlExecute("create table "
            + FoodRecordLogList_tlb + "("
            + dbFoodID + " text, " + dbQuantity + " text, "
            + dbDateTime + " text, " + dbIsServerSync + " text, "
            + dbContainID + " text, " + recID + " integer primary key autoincrement, "
            + dbISRoutineRec + " text, " + dbRouteName + " text, "
            + dbServerID + " text "
            + " )")
    }
    func FoodRecordLogListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + FoodRecordLogList_tlb)
        FoodRecordLogListTableCreate()
    }
    
    
    //////Pedometer data list
    let PedometerRecordLogList_tlb = "PedometerRecordLogListTlb"
    let dbSteps = "Steps"
    
    //Pedometer record Table List
    func PedometerRecordLogListTableCreate() {
        sqlExecute("create table "
            + PedometerRecordLogList_tlb + "("
            + dbSteps + " text, " + dbDateTime + " text, "
            + dbIsServerSync + " text "
            + " )")
    }
    func PedometerRecordLogListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + PedometerRecordLogList_tlb)
        PedometerRecordLogListTableCreate()
    }
    
    //////Calories data list
    let CaloriesRecordLogList_tlb = "CaloriesRecordLogListTlb"
    
    //Calories record Table List
    func CaloriesRecordLogListTableCreate() {
        sqlExecute("create table "
            + CaloriesRecordLogList_tlb + "("
            + dbCalories + " text, " + dbDateTime + " text, "
            + dbIsServerSync + " text "
            + " )")
    }
    func CaloriesRecordLogListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + CaloriesRecordLogList_tlb)
        CaloriesRecordLogListTableCreate()
    }
    
    //////Distance data list
    let DistanceRecordLogList_tlb = "DistanceRecordLogListTlb"
    let dbDistance = "Distance"
    
    //Distance record Table List
    func DistanceRecordLogListTableCreate() {
        sqlExecute("create table "
            + DistanceRecordLogList_tlb + "("
            + dbDistance + " text, " + dbDateTime + " text, "
            + dbIsServerSync + " text "
            + " )")
    }
    func DistanceRecordLogListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + DistanceRecordLogList_tlb)
        DistanceRecordLogListTableCreate()
    }
    
    
    //////Expert communication message
    let ExpertCommMsgList_tlb = "ExpertCommMsgListTlb"
    let dbMessage = "Message"
    let dbExpertName = "ExpertName"
    let dbExpertID = "ExpertID"
    let dbMsgID = "MsgID"
    let dbIsRead = "IsRead" //0 - unread, 1 - read
    let dbIsInbox = "IsInbox" //0 - send, 1 - receive
    let dbAllotedGrpID = "AllotedGrpID"
    let dbMsgType = "MsgType"
    let dbFileName = "FileName"
    
    //ExpertCommMsg record Table List
    func ExpertCommMsgListTableCreate() {
        sqlExecute("create table "
            + ExpertCommMsgList_tlb + "("
            + dbMsgID + " text, " + dbMessage + " text, "
            + dbExpertID + " text, " + dbExpertName + " text, "
            + dbIsRead + " text, " + dbIsServerSync + " text, "
            + dbDateTime + " text, " + dbIsInbox + " text, "
            + dbMsgType + " text, " + dbFileName + " text "
            + " )")
    }
    func ExpertCommMsgListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + ExpertCommMsgList_tlb)
        ExpertCommMsgListTableCreate()
    }
    
    //////Expert user List
    let ExpertUserList_tlb = "ExpertUserListTlb"
    let dbExpertDesig = "ExpertDesig"
    let dbExpertEdu = "ExpertEdu"
    let dbExpertAbout = "ExpertAbout"
    let dbExpertSpecility = "ExpertSpecility"
    let dbExpertCatID = "ExpertCatID"
    
    //ExpertCommMsg record Table List
    func ExpertUserListTableCreate() {
        sqlExecute("create table "
            + ExpertUserList_tlb + "("
            + dbExpertID + " text, " + dbExpertName + " text, "
            + dbExpertEdu + " text, "
            + dbExpertAbout + " text, " + dbExpertSpecility + " text, "
            + dbAllotedGrpID + " text, " + dbExpertCatID + " text, "
            + dbMobileNo + " text "
            + " )")
    }
    func ExpertUserListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + ExpertUserList_tlb)
        ExpertUserListTableCreate()
    }
    
    
    //////////////FAQ list
    //Recent dashboard table
    let recDashboardList_tlb = "recDashboardListTlb"
    
    let dbRecName = "RecName"
    let dbRecDesc = "RecDesc"
    let dbRecType = "RecType"
    
    //recDashboard Table List
    func recDashboardListTableCreate() {
        sqlExecute("create table "
            + recDashboardList_tlb + "("
            + recID + " integer primary key autoincrement, "
            + dbRecName + " text," + dbRecDesc + " text, "
            + dbRecType + " text," + dbDateTime + " text "
            + " )")
    }
    func recDashboardListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + recDashboardList_tlb)
        recDashboardListTableCreate()
    }
    
    //Recent dashboard contain table
    let recDashboardSubList_tlb = "recDashboardSubListTlb"
    
    let dbIsChecked = "IsChecked"
    let dbOptionID = "OptionID"
    let dbOptionName = "OptionName"
    let dbVideoLink = "VideoLink"
    let dbDays = "Days"
    
    //containerID  = Containter Details
    //actID = activity/food names
    //recDashboard Sub Table List
    func recDashboardSubListTableCreate() {
        sqlExecute("create table "
            + recDashboardSubList_tlb + "("
            + recID + " integer, "
            + dbActID + " text,"
            + dbIsChecked + " text, " + dbQuantity + " text,"
            + dbContainID + " text, " + dbRecDesc + " text, "
            + dbOptionID + " text, " + dbOptionName + " text, "
            + dbVideoLink + " text, " + dbMessage + " text, "
            + dbDays + " text "
            + " )")
    }
    func recDashboardSubListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + recDashboardSubList_tlb)
        recDashboardSubListTableCreate()
    }
    
    let RoutingTrackList_tlb = "RoutingTrackListTlb"
    
    //Routing Track Table List
    func RoutingTrackListTableCreate() {
        sqlExecute("create table "
            + RoutingTrackList_tlb + "("
            + dbOptionID + " text, " + dbDateTime + " text, "
            + dbRouteName + " text "
            + " )")
    }
    func RoutingTrackListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + RoutingTrackList_tlb)
        RoutingTrackListTableCreate()
    }
    /////////////End Recent Dashboard
    
    
    //////notification Table
    let NotificationList_tlb = "NotificationListTlb"
        
    //Notification record Table List
    func NotificationListTableCreate() {
        sqlExecute("create table "
            + NotificationList_tlb + "("
            + dbMsgID + " text, " + dbMessage + " text, "
            + dbIsRead + " text, " + dbMsgTitle + " text, "
            + dbDateTime + " text, " + dbImageName + " text "
                + " );")
    }
    func NotificationListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + NotificationList_tlb)
        NotificationListTableCreate()
    }
    ////////End Notification
    
    //////Purchase Plan Table
    let PurchasePlanList_tlb = "PurchasePlanListTlb"
    let dbPriceMonth = "PriceMonth"
    let dbComboPlan = "ComboPlan"
    let dbComboPrice = "ComboPrice"
    let dbFeatures = "Features"
    let dbLongDesc = "LongDesc"
    
    //Purchase Plan record Table List
    func PurchasePlanListTableCreate() {
        sqlExecute("create table "
            + PurchasePlanList_tlb + "("
            + dbMsgID + " text, " + dbMsgTitle + " text, "
            + dbPriceMonth + " text, " + dbComboPlan + " text, "
            + dbComboPrice + " text, " + dbMsgDescri + " text, "
            + dbFeatures + " text, " + dbLongDesc + " text, "
            + dbImageName + " text "
            + " )")
    }
    func PurchasePlanListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + PurchasePlanList_tlb)
        PurchasePlanListTableCreate()
    }
    ////////End Purchase Plan
    
    /////////////////Document wallet file table
    let DocWalletFile_Tlb = "DocWalletFileTable"
    
    let docID = "docID"
    let dbFileSize = "FileSize"
    let dbModifiDate = "ModifiDate"
    let dbOnTime = "OnTime"
    let dbServerID = "ServerID"
    let dbName = "GroupName"
    
    //functions for create doc wallet file
    func DocWalletFileTableCreate() {
        sqlExecute("create table "
            + DocWalletFile_Tlb + " ( "
            + docID + " integer primary key autoincrement, "
            + dbFileName + " text, " + dbFileSize + " text, "
            + dbModifiDate + " text, " + dbOnTime + " text, "
            + dbRecType + " text, "
            + dbServerID + " text, " + dbPosition + " text "
            + " ) ")
    }
    //functions for upgrade doc wallet file table
    func DocWalletFileTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + DocWalletFile_Tlb)
        DocWalletFileTableCreate()
    }
    ///////// end file list Date Table
    
    
    //////////////Timeline
    let TimelineSummeryList_tlb = "TimelineSummeryListTlb"
    let dbDate = "DateStr"
    let dbCounter = "Counter"
    
    //Timeline Single record Table List
    func TimelineSummeryListTableCreate() {
        sqlExecute("create table "
            + TimelineSummeryList_tlb + "("
            + dbRecType + " text, " + dbDateTime + " text, "
            + dbMessage + " text, " + dbCounter + " text , "
            + dbDate + " text "
            + " );")
    }
    func TimelineSummeryListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + TimelineSummeryList_tlb)
        TimelineSummeryListTableCreate()
    }
    
    
    let TimelineSingleList_tlb = "TimelineSingleListTlb"
    
    //Timeline Single record Table List
    func TimelineSingleListTableCreate() {
        sqlExecute("create table "
            + TimelineSingleList_tlb + "("
            + dbRecType + " text, " + dbDateTime + " text, "
            + dbMessage + " text, " + dbDate + " text, "
            + dbMsgTitle + " text "
            + " )")
    }
    func TimelineSingleListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + TimelineSingleList_tlb)
        TimelineSingleListTableCreate()
    }
    ////////End Timeline Single
    
    //////Schedule Date Table
    let ScheduleList_tlb = "ScheduleListTlb"
    
    let dbTime = "DBTime"
    let dbSelectType = "SelectType"
    
    //Schedule record Table List
    func ScheduleListTableCreate() {
        sqlExecute("create table "
            + ScheduleList_tlb + "("
            + dbRecType + " text, " + dbTime + " text, "
            + dbName + " text, " + dbImageName + " text, "
            + dbRouteName + " text, " + dbServerID + " text, "
            + dbSelectType + " text "
            + " )")
    }
    func ScheduleListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + ScheduleList_tlb)
        ScheduleListTableCreate()
    }
    ////////End Schedule
    
    //////Assessment record List
    let AssessmentList_tlb = "AssessmentListTlb";
    let dbParamName = "ParamName";
    let dbParamValue = "ParamValue";
    let dbGroupType = "GroupType";
    
    //Food record Table List
    func AssessmentListTableCreate() {
        sqlExecute("create table "
            + AssessmentList_tlb + "("
            + dbParamName + " text, "
            + dbParamValue + " text, "
            + dbGroupType + " text "
            + " );")
    }
    func AssessmentListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + AssessmentList_tlb)
        AssessmentListTableCreate()
    }
    //////End Assessment record List
    
    
    //Appointment list table
    let AppointmentList_tlb = "AppointmentListTlb";
    let dbAptID = "AptID";
    let dbCompanyID = "CompanyID";
    let dbSpecialID = "SpecialID";
    let dbDoctorID = "DoctorID";
    let dbDoctorName = "DoctorName";
    let dbFromTime = "FromTime";
    let dbToTime = "ToTime";
    let dbAptFor = "AptFor";
    let dbRemindBy = "RemindBy";
    let dbDetails = "Details";
    let dbAptType = "AptType";
    let dbSpeciality = "Speciality";
    
    //appoint Table List
    func AppointmentListTableCreate() {
        sqlExecute("create table "
            + AppointmentList_tlb + "("
            + dbAptID + " text, " + dbCompanyID + " text, "
            + dbSpecialID + " text, " + dbDoctorID + " text, "
            + dbDate + " text, " + dbFromTime + " text, "
            + dbToTime + " text, " + dbAptFor + " text, "
            + dbRemindBy + " text, " + dbDetails + " text, "
            + dbAptType + " text, " + dbName + " text, "
            + dbEmail + " text, " + dbMobileNo + " text, "
            + dbDoctorName + " text, " + dbSpeciality + " text "
            + " );")
    }
    func AppointmentListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + AppointmentList_tlb)
        AppointmentListTableCreate()
    }
    //End Appointment list table
    
    //My subscription List
    let MySubscriptionList_tlb = "MySubscriptionListTlb";
    let dbPlanName = "PlanName";
    let dbPlanDura = "PlanDura";
    let dbKey = "Key";
    let dbFromDate = "FromDate";
    let dbToDate = "ToDate";
    let dbMeasure = "Measure";
    let dbStatus = "LicStatus"; // 1 - Active, 0 - InActive
    
    //appoint Table List
    func MySubscriptionListTableCreate() {
        sqlExecute("create table "
            + MySubscriptionList_tlb + "("
            + dbPlanName + " text, " + dbPlanDura + " text, "
            + dbKey + " text, " + dbFromDate + " text, "
            + dbToDate + " text, " + dbMeasure + " text, "
            + dbCompanyID + " text, " + dbName + " text, "
            + dbFeatures + " text, " + dbStatus + " text "
            + " );")
    }
    func MySubscriptionListTableUpgrade() {
        sqlExecute("DROP TABLE IF EXISTS " + MySubscriptionList_tlb)
        MySubscriptionListTableCreate()
    }
    //End My subscription List
    
    
    
    
    func sqlExecute(_ sqlString: String) {
        if database!.executeStatements(sqlString) {
            
        } else {
            Print.printLog("Databse Error===\(database!.lastErrorMessage())")
        }
    }
    
    func selectRecords(_ queryString: String) -> FMResultSet? {
        do {
            return try database.executeQuery(queryString, values: nil)
        } catch {
            Print.printLog("Databse Error===\(database.lastErrorMessage())")
            return nil
        }
    }
    
    func insertExecuteBind(_ tableName: String, parameter: NSMutableDictionary, value: NSMutableDictionary) {
        
        var query = "INSERT INTO \(tableName)"
        
        var paraStr = ""
        var valueStr = ""
        var arr = [String]()
        
        for i in 0 ..< parameter.count {
            paraStr = paraStr + "\(paraStr.isEmpty ? "\(parameter.value(forKey: "\(i+1)") as! String)":",\(parameter.value(forKey: "\(i+1)") as! String)")"
            valueStr  = valueStr + "\(valueStr.isEmpty ? "?":",?")"
            arr.append(value.value(forKey: "\(i+1)") as! String)
        }
        query = query + "(" + paraStr + ") VALUES (" + valueStr + ");"
        
        
        if database.executeUpdate(query, withArgumentsIn: arr) {
            
        } else {
            Print.printLog("Databse Insert Error===\(database.lastErrorMessage())")
        }
    }
    
    func insertMultipleExecuteBind(_ tableName: String, parameter: NSMutableDictionary, value: NSMutableArray) {
        
        var query = "INSERT INTO \(tableName)"
        
        var paraStr = ""
        var valueStr = ""
        
        
        for i in 0 ..< parameter.count {
            paraStr = paraStr + "\(paraStr.isEmpty ? "\(parameter.value(forKey: "\(i+1)") as! String)":",\(parameter.value(forKey: "\(i+1)") as! String)")"
            valueStr  = valueStr + "\(valueStr.isEmpty ? "?":",?")"
        }
        query = query + "(" + paraStr + ") VALUES (" + valueStr + ");"
        
        database.beginTransaction()
        for i in 0 ..< value.count {
            let dict = value.object(at: i) as! NSMutableDictionary
            var arr = [String]()
            for j in 0 ..< dict.count {
                arr.append(dict.value(forKey: "\(j+1)") as! String)
            }
            
            if database.executeUpdate(query, withArgumentsIn: arr) {
                
            } else {
                Print.printLog("Databse Multiple Insert Error===\(database.lastErrorMessage())")
            }
        }
        database.commit()
    }
    
    func updateExecuteBinds(_ tableName: String, parameter: NSMutableDictionary, value: NSMutableDictionary, condition: String) {
        
        updateExecuteBind(tableName, parameter: parameter, value: value, condition: condition)
    }
    
    func updateExecuteBind(_ tableName: String, parameter: NSMutableDictionary, value: NSMutableDictionary, condition: String) -> Int {
        
        var totalReflected: Int = 0
        
        //find rececord Avail as per condition
        if !condition.isEmpty {
            let sql = "SELECT count(*) FROM " + tableName + " WHERE " + condition
            let cursor = selectRecords(sql)
            if cursor != nil {
                if cursor!.next() {
                    totalReflected = Int(cursor!.int(forColumnIndex: 0))
                }
                cursor!.close()                
            }
        } else {
            totalReflected = 1
        }
        
        var query = "UPDATE \(tableName) SET "
        var arr = [String]()
        
        for i in 0 ..< parameter.count {
            query = query + "\(parameter.value(forKey: "\(i+1)") as! String) = ?" + (((i+1) != parameter.count) ? "," : "")
            arr.append(value.value(forKey: "\(i+1)") as! String)
        }
        if !condition.isEmpty {
            query = query + " WHERE " + condition
        }
        
        if database.executeUpdate(query, withArgumentsIn: arr) {
            
        } else {
            Print.printLog("Databse Error===\(database!.lastErrorMessage())")
        }
        
        return totalReflected
    }
    
    func deleteRecord(_ tableName: String, condition: String) {
        var query = "";
        if condition.isEmpty {
            query = "delete from " + tableName;
        } else {
            query = "delete from " + tableName + " WHERE " + condition;
        }
        
        if database!.executeUpdate(query, withArgumentsIn: nil) {
            
        } else {
            Print.printLog("Databse Error===\(database!.lastErrorMessage())")
        }
    }
}
