//
//  DBOperation.swift
//  Salk
//
//  Created by Chetan  on 28/06/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation
import FMDB

class DBOperation: DBTableInfo {
    
    override init() {
        super.init()
        
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase = documentsDirectory.appending("/natures_five.sqlite")
    }
    
    func openDatabase(_ isReadWrite: Bool) {
        // Call for create database, if not available
        if database == nil {
            database = FMDatabase(path: pathToDatabase)
        }
        
        if database != nil {
            let appPref = MySharedPreferences()
            if !appPref.getISDBOperationCreated() {
                database.open()
                self.createDatabase()
            } else if appPref.getPreviousDBVersion() != SalkProjectConstant().dbVersion {
                database.open()
                self.upgradePreviousTable()
            } else {
                if isReadWrite {
                    database.open()
                } else {
                    database.open(withFlags: SQLITE_OPEN_READONLY)
                }
            }
        }
    }
    
    func closeDatabase() {
        database.close()
    }
    
    
    func createDatabase() {
        // General
        NotificationListTableCreate()
        PurchasePlanListTableCreate()
        DocWalletFileTableCreate()
        AppointmentListTableCreate()
        MySubscriptionListTableCreate()
        
        // Article
        categoryListTableCreate()
        subCategoryListTableCreate()
        AuthorListTableCreate()
        ArticleListTableCreate()
        ArticlePageListTableCreate()
        
        //fitness
        WaterTrackListTableCreate()
        SleepTrackListTableCreate()
        SportActivityListTableCreate()
        SportActivityRecordListTableCreate()
        
        FoodRecordListTableCreate()
        FoodContainerListTableCreate()
        FoodRecordLogListTableCreate()
        PedometerRecordLogListTableCreate()
        DistanceRecordLogListTableCreate()
        CaloriesRecordLogListTableCreate()
        ScheduleListTableCreate()
        
        TimelineSingleListTableCreate()
        TimelineSummeryListTableCreate()
        
        //Expert
        ExpertCommMsgListTableCreate()
        ExpertUserListTableCreate()
        recDashboardListTableCreate()
        recDashboardSubListTableCreate()
        RoutingTrackListTableCreate()
        
        AssessmentListTableCreate()
        
        let appPref = MySharedPreferences()
        appPref.setISDBOperationCreated(true)
        appPref.setPreviousDBVersion(text: SalkProjectConstant().dbVersion)
        appPref.setIsNeedBuildApplication(text: true)
    }
    
    func upgradePreviousTable() {
        // General
        NotificationListTableUpgrade()
        PurchasePlanListTableUpgrade()
        DocWalletFileTableUpgrade()
        AppointmentListTableUpgrade()
        MySubscriptionListTableUpgrade()
        
        // Article
        categoryListTableUpgrade()
        subCategoryListTableUpgrade()
        AuthorListTableUpgrade()
        ArticleListTableUpgrade()
        ArticlePageListTableUpgrade()
        
        //fitness
        WaterTrackListTableUpgrade()
        SleepTrackListTableUpgrade()
        SportActivityListTableUpgrade()
        SportActivityRecordListTableUpgrade()
        
        FoodRecordListTableUpgrade()
        FoodContainerListTableUpgrade()
        FoodRecordLogListTableUpgrade()
        PedometerRecordLogListTableUpgrade()
        DistanceRecordLogListTableUpgrade()
        CaloriesRecordLogListTableUpgrade()
        ScheduleListTableUpgrade()
        
        TimelineSingleListTableUpgrade()
        TimelineSummeryListTableUpgrade()
        
        //Expert
        ExpertCommMsgListTableUpgrade()
        ExpertUserListTableUpgrade()
        recDashboardListTableUpgrade()
        recDashboardSubListTableUpgrade()
        RoutingTrackListTableUpgrade()
        
        AssessmentListTableUpgrade()
        
        let appPref = MySharedPreferences()
        appPref.setPreviousDBVersion(text: SalkProjectConstant().dbVersion)
        appPref.setIsNeedBuildApplication(text: true)
    }
    
    ///////////////Fitnesss
    //Insert one record into List table
    func InsertWaterTrackList(currentValue: String, dateTime: String, isServerSync: String, isDelete: String)
    {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbWaterGlass, forKey: "1")
        parameter.setValue(dbDateTime, forKey: "2")
        parameter.setValue(dbIsServerSync, forKey: "3")
        parameter.setValue(dbIsDeleteFlag, forKey: "4")
        
        let values = NSMutableDictionary()
        values.setValue(currentValue, forKey: "1")
        values.setValue(dateTime, forKey: "2")
        values.setValue(isServerSync, forKey: "3")
        values.setValue(isDelete, forKey: "4")
        
        let result = updateExecuteBind(WaterTrackList_tlb, parameter: parameter, value: values, condition: dbDateTime + " = '" + dateTime + "' ")
        
        if(result <= 0) {
            insertExecuteBind(WaterTrackList_tlb, parameter: parameter, value: values)
        }
    }
    //Update one record into List table
    func UpdateWaterTrackListSyncLog() {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsServerSync, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue("1", forKey: "1")
        
        updateExecuteBinds(WaterTrackList_tlb, parameter: parameter, value: values, condition: "")
    }
    func TruncateWaterTrackList() {
        deleteRecord(WaterTrackList_tlb, condition: "")
    }
    
    
    //Insert one record into List table
    func InsertSleepTrackList(currentValue: String, sleepValue: String, dateTime: String, quality: String, isServerSync: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbSleepMinutes, forKey: "1")
        parameter.setValue(dbSleepValue, forKey: "2")
        parameter.setValue(dbDateTime, forKey: "3")
        parameter.setValue(dbIsServerSync, forKey: "4")
        parameter.setValue(dbSleepQuality, forKey: "5")
        
        let values = NSMutableDictionary()
        values.setValue(currentValue, forKey: "1")
        values.setValue(sleepValue, forKey: "2")
        values.setValue(dateTime, forKey: "3")
        values.setValue(isServerSync, forKey: "4")
        values.setValue(quality, forKey: "5")
        
        let result = updateExecuteBind(SleepTrackList_tlb, parameter: parameter, value: values, condition: dbDateTime + " = '" + dateTime + "' ")
        
        if(result <= 0) {
            insertExecuteBind(SleepTrackList_tlb, parameter: parameter, value: values)
        }
    }
    
    //Update one record into List table
    func UpdateSleepTrackListSyncLog() {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsServerSync, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue("1", forKey: "1")
        
        updateExecuteBinds(SleepTrackList_tlb, parameter: parameter, value: values, condition: "")
    }
    func TruncateSleepTrackList() {
        deleteRecord(SleepTrackList_tlb, condition: "")
    }
    
    
    //Insert one record into List table
    func InsertSportActivityList(actID: String, actName: String, calories: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbActID, forKey: "1")
        parameter.setValue(dbActName, forKey: "2")
        parameter.setValue(dbCalories, forKey: "3")
        
        let values = NSMutableDictionary()
        values.setValue(actID, forKey: "1")
        values.setValue(actName, forKey: "2")
        values.setValue(calories, forKey: "3")
        
        insertExecuteBind(SportActivityList_tlb, parameter: parameter, value: values)
    }
    func TruncateSportActivityList() {
        deleteRecord(SportActivityList_tlb, condition: "")
    }
    
    //Insert one record into List table
    func InsertSportActivityRecordList(actID: String, duration: String,
                                       dateTime: String, actDoType: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbActID, forKey: "1")
        parameter.setValue(dbDuration, forKey: "2")
        parameter.setValue(dbDateTime, forKey: "3")
        parameter.setValue(dbIsServerSync, forKey: "4")
        parameter.setValue(dbActDoType, forKey: "5")
        
        let values = NSMutableDictionary()
        values.setValue(actID, forKey: "1")
        values.setValue(duration, forKey: "2")
        values.setValue(dateTime, forKey: "3")
        values.setValue("0", forKey: "4")
        values.setValue(actDoType, forKey: "5")
       
        insertExecuteBind(SportActivityRecordList_tlb, parameter: parameter, value: values)
    }
    func UpdateSportActivityRecordList(recIDs: String, actID: String, duration: String, dateTime: String, actDoType: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbActID, forKey: "1")
        parameter.setValue(dbDuration, forKey: "2")
        parameter.setValue(dbDateTime, forKey: "3")
        parameter.setValue(dbIsServerSync, forKey: "4")
        parameter.setValue(dbActDoType, forKey: "5")
        
        let values = NSMutableDictionary()
        values.setValue(actID, forKey: "1")
        values.setValue(duration, forKey: "2")
        values.setValue(dateTime, forKey: "3")
        values.setValue("0", forKey: "4")
        values.setValue(actDoType, forKey: "5")
        
        updateExecuteBinds(SportActivityRecordList_tlb, parameter: parameter, value: values, condition: recID + " = " + recIDs)
    }
    
    //Update one record into List table
    func UpdateActivityRecordLogSyncList() {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsServerSync, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue("1", forKey: "1")
        
        updateExecuteBinds(SportActivityRecordList_tlb, parameter: parameter, value: values, condition: "")
    }
    func DeleteActivityRecordLogSyncList(actID: String, dateTime: String) {
        deleteRecord(SportActivityRecordList_tlb, condition: dbActID + " = '" + actID + "' AND " + dbDateTime + " = '" + dateTime + "' ")
    }
    func TruncateSportActivityRecordList(recIDs: String) {
        deleteRecord(SportActivityRecordList_tlb, condition: recID + " = " + recIDs)
    }
    
    
    ///////Food
    //Insert one record into List table
    func InsertFoodContainerList(containID: String, containName: String, value: String, unit: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbContainID, forKey: "1")
        parameter.setValue(dbContainerName, forKey: "2")
        parameter.setValue(dbContainUnit, forKey: "3")
        parameter.setValue(dbContainValue, forKey: "4")
        
        let values = NSMutableDictionary()
        values.setValue(containID, forKey: "1")
        values.setValue(containName, forKey: "2")
        values.setValue(unit, forKey: "3")
        values.setValue(value, forKey: "4")
        
        insertExecuteBind(FoodContainerList_tlb, parameter: parameter, value: values)
    }
    func TruncateFoodContainerList() {
        deleteRecord(FoodContainerList_tlb, condition: "")
    }
    
    //Insert one record into List table
    func InsertFoodRecordList(actID: String, actName: String, calories: String
    , protein: String, fat: String, carbs: String, fiber: String, containID: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbFoodID, forKey: "1")
        parameter.setValue(dbFoodName, forKey: "2")
        parameter.setValue(dbCalories, forKey: "3")
        parameter.setValue(dbProtein, forKey: "4")
        parameter.setValue(dbFat, forKey: "5")
        parameter.setValue(dbCarbs, forKey: "6")
        parameter.setValue(dbFiber, forKey: "7")
        parameter.setValue(dbContainID, forKey: "8")
        
        let values = NSMutableDictionary()
        values.setValue(actID, forKey: "1")
        values.setValue(actName, forKey: "2")
        values.setValue(calories, forKey: "3")
        values.setValue(protein, forKey: "4")
        values.setValue(fat, forKey: "5")
        values.setValue(carbs, forKey: "6")
        values.setValue(fiber, forKey: "7")
        values.setValue(containID, forKey: "8")
        
        insertExecuteBind(FoodRecordList_tlb, parameter: parameter, value: values)
    }
    func TruncateFoodRecordList() {
        deleteRecord(FoodRecordList_tlb, condition: "")
    }
    
    //Insert one record into List table
    func InsertFoodRecordLogList(foodID: String, quanti: String, dateTime: String, containID: String, isRoutineRec: String, schedule: String, isServerSync: String, serverID: String)
    {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbFoodID, forKey: "1")
        parameter.setValue(dbQuantity, forKey: "2")
        parameter.setValue(dbDateTime, forKey: "3")
        parameter.setValue(dbIsServerSync, forKey: "4")
        parameter.setValue(dbContainID, forKey: "5")
        parameter.setValue(dbISRoutineRec, forKey: "6")
        parameter.setValue(dbRouteName, forKey: "7")
        
        let values = NSMutableDictionary()
        values.setValue(foodID, forKey: "1")
        values.setValue(quanti, forKey: "2")
        values.setValue(dateTime, forKey: "3")
        values.setValue(isServerSync, forKey: "4")
        values.setValue(containID, forKey: "5")
        values.setValue(isRoutineRec, forKey: "6")
        values.setValue(schedule, forKey: "7")
    
        let update = updateExecuteBind(FoodRecordLogList_tlb, parameter: parameter, value: values, condition: dbDateTime + " = '" + dateTime + "' AND " + dbFoodID + " = '" + foodID + "' ")
        if(update <= 0) {
            parameter.setValue(dbServerID, forKey: "8")
            values.setValue(serverID, forKey: "8")
            
            insertExecuteBind(FoodRecordLogList_tlb, parameter: parameter, value: values)
        }
    }
    
    func UpdateFoodRecordLogList(recIDs: String, foodID: String, quanti: String, containID: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbFoodID, forKey: "1")
        parameter.setValue(dbQuantity, forKey: "2")
        parameter.setValue(dbIsServerSync, forKey: "3")
        parameter.setValue(dbContainID, forKey: "4")
        
        let values = NSMutableDictionary()
        values.setValue(foodID, forKey: "1")
        values.setValue(quanti, forKey: "2")
        values.setValue("0", forKey: "3")
        values.setValue(containID, forKey: "4")
        
        updateExecuteBinds(FoodRecordLogList_tlb, parameter: parameter, value: values, condition: recID + " = " + recIDs)
    }
    func InsertFoodRecordLogRoutineList(foodName: String, quanti: String,  dateTime: String, contain: String, schedule: String) {
        
        var foodID = ""
        var sql = "SELECT " + dbFoodID + " FROM " + FoodRecordList_tlb +
            " WHERE UPPER(" + dbFoodName + ") = '" + foodName.uppercased() + "' "
        let cur = selectRecords(sql)
        if cur != nil {
            if cur!.next() {
                foodID = cur!.string(forColumnIndex: 0)
            }
            cur!.close()
        }
        
        var containID = ""
        sql = "SELECT " + dbContainID + " FROM " + FoodContainerList_tlb +
            " WHERE UPPER(" + dbContainerName + ") = '" + contain.uppercased() + "' "
        let cursor = selectRecords(sql)
        if cursor != nil {
            if cursor!.next() {
                containID = cursor!.string(forColumnIndex: 0)
            }
            cursor!.close()
        }
        
        if(!foodID.isEmpty && !containID.isEmpty) {
            InsertFoodRecordLogList(foodID: foodID, quanti: quanti, dateTime: dateTime, containID: containID, isRoutineRec: "0", schedule: schedule, isServerSync: "0", serverID: "0")
        }
    }
    
    //Update one record into List table
    func UpdateFoodRecordLogSyncList(uniqueID: String, serverID: String) {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsServerSync, forKey: "1")
        parameter.setValue(dbServerID, forKey: "2")
        
        let values = NSMutableDictionary()
        values.setValue("1", forKey: "1")
        values.setValue(serverID, forKey: "2")
    
        updateExecuteBinds(FoodRecordLogList_tlb, parameter: parameter, value: values, condition: recID + " = " + uniqueID)
    }
    func DeleteFoodRecordLogList(actID: String, dateTime: String) {
        deleteRecord(FoodRecordLogList_tlb, condition: dbFoodID + " = '" + actID + "' AND " + dbDateTime + " = '" + dateTime + "' ")
    }
    func TruncateFoodRecordLogList(recIDs: String) {
        deleteRecord(FoodRecordLogList_tlb, condition: recID + " = " + recIDs)
    }
    func TruncateFoodRecordLogServerIDList(serverID: String) {
        deleteRecord(FoodRecordLogList_tlb, condition: dbServerID + " = '\(serverID)'")
    }
    
    ///////////////End Food
    
    ////////////////////Pedometer
    //Update one record into List table
    func UpdatePedometerRecordLogSyncList() {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsServerSync, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue("1", forKey: "1")
        
        updateExecuteBinds(PedometerRecordLogList_tlb, parameter: parameter, value: values, condition: "")
    }
    func UpdatePedometerRecordLogList(steps: String, dateTime: String, isServerSync: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbSteps, forKey: "1")
        parameter.setValue(dbIsServerSync, forKey: "2")
        
        let values = NSMutableDictionary()
        values.setValue(steps, forKey: "1")
        values.setValue(isServerSync, forKey: "2")
        
        let result = updateExecuteBind(PedometerRecordLogList_tlb, parameter: parameter, value: values, condition: dbDateTime + " = '" + dateTime + "' ")
    
        if (result <= 0) {
            parameter.setValue(dbDateTime, forKey: "3")
            values.setValue(dateTime, forKey: "3")
            
            insertExecuteBind(PedometerRecordLogList_tlb, parameter: parameter, value: values)
        }
    }
    func TruncatePedometerRecordLogList() {
        deleteRecord(PedometerRecordLogList_tlb, condition: "")
    }
    //////////////////////End pedometer
    
    ////////////////////Activity Distance
    //Update one record into List table
    func UpdateDistanceRecordLogSyncList() {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsServerSync, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue("1", forKey: "1")
        
        updateExecuteBinds(DistanceRecordLogList_tlb, parameter: parameter, value: values, condition: "")
    }
    func UpdateDistanceRecordLogList(steps: String, dateTime: String, isServerSync: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbDistance, forKey: "1")
        parameter.setValue(dbIsServerSync, forKey: "2")
        
        let values = NSMutableDictionary()
        values.setValue(steps, forKey: "1")
        values.setValue(isServerSync, forKey: "2")
        
        let result = updateExecuteBind(DistanceRecordLogList_tlb, parameter: parameter, value: values, condition: dbDateTime + " = '" + dateTime + "' ")
    
        if (result <= 0) {
            parameter.setValue(dbDateTime, forKey: "3")
            values.setValue(dateTime, forKey: "3")
            
            insertExecuteBind(DistanceRecordLogList_tlb, parameter: parameter, value: values)
        }
    }
    func TruncateDistanceRecordLogList() {
        deleteRecord(DistanceRecordLogList_tlb, condition: "")
    }
    //////////////////////End Activity Distance
    
    ////////////////////Activity Calories
    //Update one record into List table
    func UpdateCaloriesRecordLogSyncList() {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsServerSync, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue("1", forKey: "1")
        
        updateExecuteBinds(CaloriesRecordLogList_tlb, parameter: parameter, value: values, condition: "")
    }
    func UpdateCaloriesRecordLogList(steps: String, dateTime: String,isServerSync: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbCalories, forKey: "1")
        parameter.setValue(dbIsServerSync, forKey: "2")
        
        let values = NSMutableDictionary()
        values.setValue(steps, forKey: "1")
        values.setValue(isServerSync, forKey: "2")
        
        let result = updateExecuteBind(CaloriesRecordLogList_tlb, parameter: parameter, value: values, condition: dbDateTime + " = '" + dateTime + "' ")
    
        if (result <= 0) {
            parameter.setValue(dbDateTime, forKey: "3")
            values.setValue(dateTime, forKey: "3")
            
            insertExecuteBind(CaloriesRecordLogList_tlb, parameter: parameter, value: values)
        }
    }
    func TruncateCaloriesRecordLogList() {
        deleteRecord(CaloriesRecordLogList_tlb, condition: "")
    }
    //////////////////////End Activity Calories
    
    
    ////////////////////////Expert Communication
    //Insert one record into List table
    func InsertExpertUserList(expID: String, expName: String, edu: String, about: String, specilist: String, expertCatID: String, allGrpID: String, mobile: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbExpertID, forKey: "1")
        parameter.setValue(dbExpertName, forKey: "2")
        parameter.setValue(dbExpertEdu, forKey: "3")
        parameter.setValue(dbExpertSpecility, forKey: "4")
        parameter.setValue(dbExpertAbout, forKey: "5")
        parameter.setValue(dbExpertCatID, forKey: "6")
        parameter.setValue(dbAllotedGrpID, forKey: "7")
        parameter.setValue(dbMobileNo, forKey: "8")
        
        let values = NSMutableDictionary()
        values.setValue(expID, forKey: "1")
        values.setValue(expName, forKey: "2")
        values.setValue(edu, forKey: "3")
        values.setValue(specilist, forKey: "4")
        values.setValue(about, forKey: "5")
        values.setValue(expertCatID, forKey: "6")
        values.setValue(allGrpID, forKey: "7")
        values.setValue(mobile, forKey: "8")
    
        let result = updateExecuteBind(ExpertUserList_tlb, parameter: parameter, value: values, condition: dbExpertID + " = '" + expID + "' ")
        if (result <= 0) {
            insertExecuteBind(ExpertUserList_tlb, parameter: parameter, value: values)
        }
    }
    func TruncateExpertUserList() {
        deleteRecord(ExpertUserList_tlb, condition: "")
    }
    
    //Insert one record into List table
    func InsertExpertCommunicationList(msgID: String, msg: String, expID: String, expName: String, dateTime: String, isInbox: String, isRead: String, fileName: String, msgType: String, IsServerSync: String)
    {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbMsgID, forKey: "1")
        parameter.setValue(dbMessage, forKey: "2")
        parameter.setValue(dbExpertID, forKey: "3")
        parameter.setValue(dbExpertName, forKey: "4")
        parameter.setValue(dbDateTime, forKey: "5")
        parameter.setValue(dbIsRead, forKey: "6")
        parameter.setValue(dbIsInbox, forKey: "7")
        parameter.setValue(dbFileName, forKey: "8")
        parameter.setValue(dbMsgType, forKey: "9")
        parameter.setValue(dbIsServerSync, forKey: "10")
        
        let values = NSMutableDictionary()
        values.setValue(msgID, forKey: "1")
        values.setValue(msg, forKey: "2")
        values.setValue(expID, forKey: "3")
        values.setValue(expName, forKey: "4")
        values.setValue(dateTime, forKey: "5")
        values.setValue(isRead, forKey: "6")
        values.setValue(isInbox, forKey: "7")
        values.setValue(fileName, forKey: "8")
        values.setValue(msgType, forKey: "9")
        values.setValue(IsServerSync, forKey: "10")
        
        let totalRec = updateExecuteBind(ExpertCommMsgList_tlb, parameter: parameter, value: values, condition: dbMsgID + " = '" + msgID + "' ")
        if(totalRec <= 0) {
            insertExecuteBind(ExpertCommMsgList_tlb, parameter: parameter, value: values)
        }
    }
    
    //Update one record into List table
    func UpdateExpertCommunicationListServerSync() {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsServerSync, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue("1", forKey: "1")
        
        updateExecuteBinds(ExpertCommMsgList_tlb, parameter: parameter, value: values, condition: "")
    }
    func UpdateExpertCommunicationListIDStatus(chatID: String, dateTime: String)
    {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbMsgID, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue(chatID, forKey: "1")
        
        updateExecuteBinds(ExpertCommMsgList_tlb, parameter: parameter, value: values, condition: dbDateTime + " = '" + dateTime + "' ")
    }
    //Update one record into List table
    func UpdateExpertCommunicationListReadStatus(expertID: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsRead, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue("1", forKey: "1")
        
        updateExecuteBinds(ExpertCommMsgList_tlb, parameter: parameter, value: values, condition: dbExpertID + " = '" + expertID + "' ")
    }
    func DeleteExpertCommunicationList(msgID: String) {
        deleteRecord(ExpertCommMsgList_tlb, condition: dbMsgID + " IN (" + msgID + ") ")
    }
    func TruncateExpertCommunicationList(expertID: String) {
        deleteRecord(ExpertCommMsgList_tlb, condition: dbExpertID + " = '" + expertID + "' ")
    }
    func TruncateExpertCommunicationList() {
        deleteRecord(ExpertCommMsgList_tlb, condition: "")
    }
    //////////////////End Expert communication
    
    
    ////////////////////////Recent Dashboard
    //Insert one record into List table
    func InsertRecentDashboardList(recName: String, recDesc: String, recType: String, dateTime: String) -> String {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbRecName, forKey: "1")
        parameter.setValue(dbRecDesc, forKey: "2")
        parameter.setValue(dbRecType, forKey: "3")
        parameter.setValue(dbDateTime, forKey: "4")
    
        let values = NSMutableDictionary()
        values.setValue(recName, forKey: "1")
        values.setValue(recDesc, forKey: "2")
        values.setValue(recType, forKey: "3")
        values.setValue(dateTime, forKey: "4")
    
        let result = updateExecuteBind(recDashboardList_tlb, parameter: parameter, value: values, condition: dbRecType + " = '" + recType + "' ")
        if (result <= 0) {
        insertExecuteBind(recDashboardList_tlb, parameter: parameter, value: values)
        }
    
        var recIDStr = "0"
        let sql = "SELECT " + recID + " FROM " + recDashboardList_tlb + " WHERE " + dbRecType + " = '" + recType + "' "
        let cursor = selectRecords(sql)
        if cursor != nil {
            if cursor!.next() {
                recIDStr = cursor!.string(forColumnIndex: 0)
            }
            cursor!.close()
        }
        
        return recIDStr;
    }
    func TruncateRecentDashboardList() {
        deleteRecord(recDashboardList_tlb, condition: "")
    }
    
    //Insert one record into List table
    func InsertRecentDashboardSubList(actID: String, recIDStr: String, qunti: String, contID: String, desc: String, optID: String, optName: String,
    videoLink: String, msg: String, days: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbActID, forKey: "1")
        parameter.setValue(dbQuantity, forKey: "2")
        parameter.setValue(dbContainID, forKey: "3")
        parameter.setValue(dbRecDesc, forKey: "4")
        parameter.setValue(dbOptionID, forKey: "5")
        parameter.setValue(dbOptionName, forKey: "6")
        parameter.setValue(recID, forKey: "7")
        parameter.setValue(dbIsChecked, forKey: "8")
        parameter.setValue(dbVideoLink, forKey: "9")
        parameter.setValue(dbMessage, forKey: "10")
        parameter.setValue(dbDays, forKey: "11")
        
        let values = NSMutableDictionary()
        values.setValue(actID, forKey: "1")
        values.setValue(qunti, forKey: "2")
        values.setValue(contID, forKey: "3")
        values.setValue(desc, forKey: "4")
        values.setValue(optID, forKey: "5")
        values.setValue(optName, forKey: "6")
        values.setValue(recIDStr, forKey: "7")
        values.setValue("0", forKey: "8")
        values.setValue(videoLink, forKey: "9")
        values.setValue(msg, forKey: "10")
        values.setValue(days, forKey: "11")
        
        let result = updateExecuteBind(recDashboardSubList_tlb, parameter: parameter, value: values, condition: dbActID + " = '" + actID + "' AND " + recID + " = '" + recIDStr + "' AND " + dbOptionID + " = '" + optID + "' ")
        if (result <= 0) {
            insertExecuteBind(recDashboardSubList_tlb, parameter: parameter,value: values)
        }
    }
    
    //Update one record into List table
    func UpdateRecentDashboardSubList(isChecked: String, recIDStr: String, actID: String, isChoice: Bool) -> Int {
        
        if (isChoice) {
            let parameter = NSMutableDictionary()
            parameter.setValue(dbIsChecked, forKey: "1")
            
            let values = NSMutableDictionary()
            values.setValue("0", forKey: "1")
            
            updateExecuteBinds(recDashboardSubList_tlb, parameter: parameter, value: values, condition: recID + " = " + recIDStr)
        }
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsChecked, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue(isChecked, forKey: "1")
    
        let value = updateExecuteBind(recDashboardSubList_tlb, parameter: parameter, value: values, condition: dbOptionID + " = '" + actID + "' AND " + recID + " = " + recIDStr)
    
        return value
    }
    func UpdateRecentDashboardSubList(isChecked: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsChecked, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue(isChecked, forKey: "1")
   
        updateExecuteBinds(recDashboardSubList_tlb, parameter: parameter, value: values, condition: "")
    }
    
    func UpdateRecentDashboardSubMultipleList(isChecked: String, optionIDs: String, recIDStr: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsChecked, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue(isChecked, forKey: "1")
    
        updateExecuteBinds(recDashboardSubList_tlb, parameter: parameter, value: values, condition: dbOptionID + " IN (" + optionIDs + ") AND " + recID + " = " + recIDStr)
    }
    func TruncateRecentDashboardSubList(recIDStr: String) {
        deleteRecord(recDashboardSubList_tlb, condition: recID + " = " + recIDStr)
    }
    
    //Insert one record into List table
    func InsertRoutingList(optionID: String, dateTime: String, schedule: String)
    {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbOptionID, forKey: "1")
        parameter.setValue(dbDateTime, forKey: "2")
        parameter.setValue(dbRouteName, forKey: "3")
        
        let values = NSMutableDictionary()
        values.setValue(optionID, forKey: "1")
        values.setValue(dateTime, forKey: "2")
        values.setValue(schedule, forKey: "3")
        
        insertExecuteBind(RoutingTrackList_tlb, parameter: parameter, value: values)
    }
    func TruncateRoutingList() {
        deleteRecord(RoutingTrackList_tlb, condition: "")
    }
    //////////////////End Recent Dashboard
    
    
    //////////Notification List table
    func InsertNotificationList(msgID: String, msg: String, msgTitle: String,
    dateTime: String, imageName: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbMsgID, forKey: "1")
        parameter.setValue(dbMessage, forKey: "2")
        parameter.setValue(dbMsgTitle, forKey: "3")
        parameter.setValue(dbDateTime, forKey: "4")
        parameter.setValue(dbImageName, forKey: "5")
        parameter.setValue(dbIsRead, forKey: "6")
        
        let values = NSMutableDictionary()
        values.setValue(msgID, forKey: "1")
        values.setValue(msg, forKey: "2")
        values.setValue(msgTitle, forKey: "3")
        values.setValue(dateTime, forKey: "4")
        values.setValue(imageName, forKey: "5")
        values.setValue("0", forKey: "6")
        
        let totalRec = updateExecuteBind(NotificationList_tlb, parameter: parameter, value: values, condition: dbMsgID + " = '" + msgID + "' ")
        if(totalRec <= 0) {
            insertExecuteBind(NotificationList_tlb, parameter: parameter, value: values)
        }
    }
    
    //Update one record into List table
    func UpdateNotificationRead(msgID: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbIsRead, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue("1", forKey: "1")
        
        updateExecuteBinds(NotificationList_tlb, parameter: parameter, value: values, condition: dbMsgID + " = '" + msgID + "' ")
    }
    func TruncateNotificationList() {
        deleteRecord(NotificationList_tlb, condition: "")
    }
    //////////////////End Expert communication
    
    ///////Puchase Plan
    //Insert one record into List table
    func InsertPurchasePlanList(id: String, name: String, price: String,    comboPlan: String, comboPrice: String, details: String, feature: String, longDesc: String, imageName: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbMsgID, forKey: "1")
        parameter.setValue(dbMsgTitle, forKey: "2")
        parameter.setValue(dbMsgDescri, forKey: "3")
        parameter.setValue(dbPriceMonth, forKey: "4")
        parameter.setValue(dbComboPlan, forKey: "5")
        parameter.setValue(dbComboPrice, forKey: "6")
        parameter.setValue(dbFeatures, forKey: "7")
        parameter.setValue(dbLongDesc, forKey: "8")
        parameter.setValue(dbImageName, forKey: "9")
        
        let values = NSMutableDictionary()
        values.setValue(id, forKey: "1")
        values.setValue(name, forKey: "2")
        values.setValue(details, forKey: "3")
        values.setValue(price, forKey: "4")
        values.setValue(comboPlan, forKey: "5")
        values.setValue(comboPrice, forKey: "6")
        values.setValue(feature, forKey: "7")
        values.setValue(longDesc, forKey: "8")
        values.setValue(imageName, forKey: "9")
        
        insertExecuteBind(PurchasePlanList_tlb, parameter: parameter, value: values)
    }
    func TruncatePurchasePlanList() {
        deleteRecord(PurchasePlanList_tlb, condition: "")
    }
    //////////End Purchase Plan
    
    
    ///////////Doc wallet File
    //Insert one record into Doc wallet file table
    func InsertDocWalletFile(fileName: String, fileSize: String, modifyDate: String, onTime: String, recType: String, groupName: String, serverID: String, position: String)
    {
        var parameter = NSMutableDictionary()
        parameter.setValue(dbFileName, forKey: "1")
        parameter.setValue(dbFileSize, forKey: "2")
        parameter.setValue(dbModifiDate, forKey: "3")
        parameter.setValue(dbOnTime, forKey: "4")
        parameter.setValue(dbRecType, forKey: "5")
        parameter.setValue(dbName, forKey: "6")
        parameter.setValue(dbServerID, forKey: "7")
        parameter.setValue(dbPosition, forKey: "8")
        
        var values = NSMutableDictionary()
        values.setValue(fileName, forKey: "1")
        values.setValue(fileSize, forKey: "2")
        values.setValue(modifyDate, forKey: "3")
        values.setValue(onTime, forKey: "4")
        values.setValue(recType, forKey: "5")
        values.setValue(groupName, forKey: "6")
        values.setValue(serverID, forKey: "7")
        values.setValue(position, forKey: "8")
        
        let totalAvail = updateExecuteBind(DocWalletFile_Tlb, parameter: parameter, value: values, condition: dbServerID + " = '" + serverID + "' ")
        if (totalAvail <= 0) {
            insertExecuteBind(DocWalletFile_Tlb, parameter: parameter, value: values)
        }
    
        //update all page list
        parameter = NSMutableDictionary()
        parameter.setValue(dbOnTime, forKey: "1")
        parameter.setValue(dbRecType, forKey: "2")
        
        values = NSMutableDictionary()
        values.setValue(onTime, forKey: "1")
        values.setValue(recType, forKey: "2")
        
        updateExecuteBinds(DocWalletFile_Tlb, parameter: parameter, value: values, condition: dbName + " = '" + groupName + "' ")
    }
    //Delete file on filename
    func DeleteDocFile(serverID: String) {
        deleteRecord(DocWalletFile_Tlb, condition: dbServerID + " = '" + serverID + "' ")
    }
    func UpdateDocWalletSeverIDTable(docWalletID: String, serverID: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbServerID, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue(serverID, forKey: "1")
        
        updateExecuteBinds(DocWalletFile_Tlb, parameter: parameter, value: values, condition: docID + " = '" + docWalletID + "' ")
    }
    //Delete all record into Doc Wallet file table
    func TruncateDocWalletFile() {
        deleteRecord(DocWalletFile_Tlb, condition: "")
    }
    /////////// end DocWalletFile
    
    
    //////////Timeline Single List table
    func InsertTimelineSingleList(type: String, title: String, dateTime: String, date: String, message: String)
    {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbRecType, forKey: "1")
        parameter.setValue(dbMessage, forKey: "2")
        parameter.setValue(dbDateTime, forKey: "3")
        parameter.setValue(dbDate, forKey: "4")
        parameter.setValue(dbMsgTitle, forKey: "5")
        
        let values = NSMutableDictionary()
        values.setValue(type, forKey: "1")
        values.setValue(message, forKey: "2")
        values.setValue(dateTime, forKey: "3")
        values.setValue(date, forKey: "4")
        values.setValue(title, forKey: "5")
        
        insertExecuteBind(TimelineSingleList_tlb, parameter: parameter, value: values)
    }
    
    func TruncateTimelineSingleList(date: String) {
        deleteRecord(TimelineSingleList_tlb, condition: dbDate + " = '" + date + "' ")
    }
    func TruncateTimelineSingleList(date: String, type: String) {
        deleteRecord(TimelineSingleList_tlb, condition: dbDate + " = '" + date + "' AND " + dbRecType + " = '" + type + "' ")
    }
    
    func InsertTimelineSummeryList(type: String, dateTime: String, date: String, counter: String, message: String)
    {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbRecType, forKey: "1")
        parameter.setValue(dbMessage, forKey: "2")
        parameter.setValue(dbCounter, forKey: "3")
        parameter.setValue(dbDateTime, forKey: "4")
        parameter.setValue(dbDate, forKey: "5")
        
        let values = NSMutableDictionary()
        values.setValue(type, forKey: "1")
        values.setValue(message, forKey: "2")
        values.setValue(counter, forKey: "3")
        values.setValue(dateTime, forKey: "4")
        values.setValue(date, forKey: "5")
        
        insertExecuteBind(TimelineSummeryList_tlb, parameter: parameter, value: values)
    }
    
    func TruncateTimelineSummeryList(date: String) {
        deleteRecord(TimelineSummeryList_tlb, condition: dbDate + " = '"+date+"' ")
    }
    //////////////////End Timeline
    
    
    //////////Schedule List table
    func InsertScheduleList(type: String, time: String, scheduleName: String,
    routineOf: String, imageName: String, id: String, selectType: String) {
    
        let parameter = NSMutableDictionary()
        parameter.setValue(dbRecType, forKey: "1")
        parameter.setValue(dbTime, forKey: "2")
        parameter.setValue(dbName, forKey: "3")
        parameter.setValue(dbRouteName, forKey: "4")
        parameter.setValue(dbImageName, forKey: "5")
        parameter.setValue(dbServerID, forKey: "6")
        parameter.setValue(dbSelectType, forKey: "7")
        
        let values = NSMutableDictionary()
        values.setValue(type, forKey: "1")
        values.setValue(time, forKey: "2")
        values.setValue(scheduleName, forKey: "3")
        values.setValue(routineOf, forKey: "4")
        values.setValue(imageName, forKey: "5")
        values.setValue(id, forKey: "6")
        values.setValue(selectType, forKey: "7")
        
        insertExecuteBind(ScheduleList_tlb, parameter: parameter, value: values)
    }
    func UpdateScheduleTimeList(time: String, scheduleName: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbTime, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue(time, forKey: "1")
        
        updateExecuteBind(ScheduleList_tlb, parameter: parameter, value: values, condition: dbName + " = '" + scheduleName + "' ")
    }
    func TruncateScheduleList() {
        deleteRecord(ScheduleList_tlb, condition: "")
    }
    //////////////////End Expert communication
    
    //////////Assessment Data List
    private func InsertAssessmentItemTable(paramName: String, paramVal: String, groupType: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbParamName, forKey: "1")
        parameter.setValue(dbParamValue, forKey: "2")
        parameter.setValue(dbGroupType, forKey: "3")
        
        let values = NSMutableDictionary()
        values.setValue(paramName, forKey: "1")
        values.setValue(paramVal, forKey: "2")
        values.setValue(groupType, forKey: "3")
    
        insertExecuteBind(AssessmentList_tlb, parameter: parameter, value: values)
    }
    public func UpdateAssessmentItemTable(paramName: String, paramVal: String, groupType: String) {
        
        let parameter = NSMutableDictionary()
        parameter.setValue(dbParamValue, forKey: "1")
        
        let values = NSMutableDictionary()
        values.setValue(paramVal, forKey: "1")
        
        let result = updateExecuteBind(AssessmentList_tlb, parameter: parameter, value: values, condition: dbParamName + " = '" + paramName + "' " + " AND " + dbGroupType + " = '" + groupType + "' ")
        
        if (result <= 0) {
            InsertAssessmentItemTable(paramName: paramName, paramVal: paramVal, groupType: groupType)
        }
    }
    //////////End Assessment Data List
    
    //////////Appointment  List
    public func InsertAppointmentListTable(aptID: String, companyID: String, specialID: String,
                                           doctorID: String, doctorName: String, date: String, fromTime: String, toTime: String,
                                           aptFor: String, remindBy: String, details: String, aptType: String,
                                           name: String, email: String, mobileNo: String, speciality: String)
    {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbAptID, forKey: "1")
        parameter.setValue(dbCompanyID, forKey: "2")
        parameter.setValue(dbSpecialID, forKey: "3")
        parameter.setValue(dbDoctorID, forKey: "4")
        parameter.setValue(dbDoctorName, forKey: "5")
        parameter.setValue(dbDate, forKey: "6")
        parameter.setValue(dbFromTime, forKey: "7")
        parameter.setValue(dbToTime, forKey: "8")
        parameter.setValue(dbAptFor, forKey: "9")
        parameter.setValue(dbRemindBy, forKey: "10")
        parameter.setValue(dbDetails, forKey: "11")
        parameter.setValue(dbAptType, forKey: "12")
        parameter.setValue(dbName, forKey: "13")
        parameter.setValue(dbEmail, forKey: "14")
        parameter.setValue(dbMobileNo, forKey: "15")
        parameter.setValue(dbSpeciality, forKey: "16")
        
        let values = NSMutableDictionary()
        values.setValue(aptID, forKey: "1")
        values.setValue(companyID, forKey: "2")
        values.setValue(specialID, forKey: "3")
        values.setValue(doctorID, forKey: "4")
        values.setValue(doctorName, forKey: "5")
        values.setValue(date, forKey: "6")
        values.setValue(fromTime, forKey: "7")
        values.setValue(toTime, forKey: "8")
        values.setValue(aptFor, forKey: "9")
        values.setValue(remindBy, forKey: "10")
        values.setValue(details, forKey: "11")
        values.setValue(aptType, forKey: "12")
        values.setValue(name, forKey: "13")
        values.setValue(email, forKey: "14")
        values.setValue(mobileNo, forKey: "15")
        values.setValue(speciality, forKey: "16")
    
        insertExecuteBind(AppointmentList_tlb, parameter: parameter, value: values)
    }
    public func TruncateAppointmentList() {
        deleteRecord(AppointmentList_tlb, condition: "")
    }
    
    //////////My subscription List
    public func InsertMySubscriptionTable(planName: String, planDura: String, key: String,
                                          fromDate: String, toDate: String, measure: String,
                                          features: String, cmpID: String, cmpName: String, status: String)
    {
        let parameter = NSMutableDictionary()
        parameter.setValue(dbPlanName, forKey: "1")
        parameter.setValue(dbPlanDura, forKey: "2")
        parameter.setValue(dbKey, forKey: "3")
        parameter.setValue(dbFromDate, forKey: "4")
        parameter.setValue(dbToDate, forKey: "5")
        parameter.setValue(dbMeasure, forKey: "6")
        parameter.setValue(dbFeatures, forKey: "7")
        parameter.setValue(dbCompanyID, forKey: "8")
        parameter.setValue(dbName, forKey: "9")
        parameter.setValue(dbStatus, forKey: "10")
        
        let values = NSMutableDictionary()
        values.setValue(planName, forKey: "1")
        values.setValue(planDura, forKey: "2")
        values.setValue(key, forKey: "3")
        values.setValue(fromDate, forKey: "4")
        values.setValue(toDate, forKey: "5")
        values.setValue(measure, forKey: "6")
        values.setValue(features, forKey: "7")
        values.setValue(cmpID, forKey: "8")
        values.setValue(cmpName, forKey: "9")
        values.setValue(status, forKey: "10")
        
        insertExecuteBind(MySubscriptionList_tlb, parameter: parameter, value: values)
    }
    public func TruncateMySubscriptionList() {
        deleteRecord(MySubscriptionList_tlb, condition: "")
    }
    
}
