//
//  ArrTimelineData.swift
//  Salk
//
//  Created by Chetan  on 09/08/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class ArrTimelineData {
    var points:TimelinePoint? = TimelinePoint()
    var color:UIColor = UIColor.black, headColor:UIColor = PrimaryColor.colorPrimary(), cardBGColor:UIColor = UIColor.lightGray
    
    var title = "", desc = "", time = "", date = "", imageIcon = ""
    var padding:Int = 0    
    var oriDateTime:Double = 0
    var totalSubRec:Int = 0
}
