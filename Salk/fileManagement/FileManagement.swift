//
//  FileManagement.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class FileManagement {
    
    class func createFolder(_ folderName: String) {
        let homeDir = NSHomeDirectory()
        let filemgr = FileManager.default
    
        let newDir = homeDir + "/Documents/\(folderName)"
    
        do {
            try filemgr.createDirectory(atPath: newDir, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            Print.printLog(error.description)
        }
    }
    
    class func deleteFolder(_ folderName: String) {
        let homeDir = NSHomeDirectory()
        let filemgr = FileManager.default
        
        let newDir = homeDir + "/Documents/\(folderName)"
        
        if filemgr.fileExists(atPath: newDir) {
            do {
                try filemgr.removeItem(atPath: newDir)
            } catch let error as NSError {
                Print.printLog(error.description)
            }
        }
    }
    
    class func deletePlist(_ folderName: String) {
        let plistPath = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first!
        let filemgr = FileManager.default
        
        let newDir = plistPath + "/Preferences/\(folderName)"
        
        if filemgr.fileExists(atPath: newDir) {
            do {
                try filemgr.removeItem(atPath: newDir)
            } catch let error as NSError {
                Print.printLog(error.description)
            }
        }
    }
    
    class func RenameFile(_ getFileName: String, setFileName: String) {
        let filemgr = FileManager.default
        if filemgr.fileExists(atPath: getFileName) {
            do {
                try filemgr.moveItem(atPath: getFileName, toPath: setFileName)
            } catch let error as NSError {
                Print.printLog(error.description)
            }
        }
    }
    
    class func decodeNSaveFile(_ getPath: String, fileData: Data, AESKey: String) {
        let filemgr = FileManager.default
        do {
            try getPath.aesDecrypt(AESKey, iv: "gqLOHUioQ0QjhuvI")
        } catch let error as NSError {
            Print.printLog(error.description)
        }
        
        filemgr.createFile(atPath: getPath, contents: fileData, attributes: nil)
    }
    
    class func encodeNSaveFile(_ getPath: String, fileData: Data, AESKey: String) {
        let filemgr = FileManager.default
        do {
            try getPath.aesEncrypt(AESKey, iv: "gqLOHUioQ0QjhuvI")
        } catch let error as NSError {
            Print.printLog(error.localizedDescription)
        }
        
        filemgr.createFile(atPath: getPath, contents: fileData, attributes: nil)
    }
 }

