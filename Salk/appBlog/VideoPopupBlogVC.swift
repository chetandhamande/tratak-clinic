//
//  VideoPopupBlogVC.swift
//  Salk
//
//  Created by Salk on 20/11/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class VideoPopupBlogVC: UIViewController, YTPlayerViewDelegate {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    @IBOutlet var videoPlayer: YTPlayerView!
    @IBOutlet weak var viewParent: UIView!
    
    var arrBlog = ArrBlog()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = CustomColor.black_semi_transparent()
        
        viewParent.layer.cornerRadius = 5
        viewParent.layer.masksToBounds = true
        
        lblHeading.text = arrBlog.title
                
        // Load video from YouTube URL
        videoPlayer.delegate = self
        
        let playerVars = ["playsinline" : 1, "rel" : 0, "showinfo" : 0, "modestbranding" : 1] // 0: will play video in fullscreen
        videoPlayer.load(withVideoId: arrBlog.videoID, playerVars: playerVars)
        
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        videoPlayer.playVideo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickOkButton(_ sender: UIButton) {
        closePopup()
    }
    
    func closePopup() {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    func showAlert() {
        if let appDelegate = UIApplication.shared.delegate, let window = appDelegate.window, let rootViewController = window?.rootViewController {
            
            var topViewController = rootViewController
            while topViewController.presentedViewController != nil {
                topViewController = topViewController.presentedViewController!
            }
            
            // Add the alert view controller to the top most UIViewController of the application
            topViewController.addChildViewController(self)
            topViewController.view.addSubview(view)
            viewWillAppear(true)
            didMove(toParentViewController: topViewController)
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.frame = topViewController.view.bounds
        }
    }

}
