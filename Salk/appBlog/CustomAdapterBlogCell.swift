//
//  CustomAdapterBlogCell.swift
//  Salk
//
//  Created by Salk on 19/11/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomAdapterBlogCell: UITableViewCell {

    @IBOutlet weak var imgThumbnail: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var viewContain: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundView?.backgroundColor = CustomColor.background()
        self.viewContain.backgroundColor = CustomColor.background()
    }

    func onBindViewHolder(_ featureList: [ArrBlog], position: Int)
    {
        let user: ArrBlog = featureList[position]
        
        lblTitle.text = user.title
        lblSubTitle.text = user.subTitle
        
        let URL = Foundation.URL(string: "http://img.youtube.com/vi/\(user.videoID)/0.jpg")!
        imgThumbnail.kf.setImage(with: URL, placeholder: nil, options: [.transition(.fade(1))], progressBlock: { receivedSize, totalSize in
            Print.printLog("\(receivedSize)/\(totalSize)")
        }, completionHandler: { image, error, cacheType, imageURL in
            if error != nil {
                self.imgThumbnail.image = UIImage().imageWithImage(UIImage(named: "ic_person_white_gray_24dp.png")!, scaledToSize: CGSize(width: 35, height: 35))
            }
        })
    }
}
