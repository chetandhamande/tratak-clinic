//
//  ExpertProfileDetailsVC.swift
//  Salk
//
//  Created by Salk on 26/03/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Kingfisher

class ExpertProfileDetailsVC: UIViewController {

    @IBOutlet weak var imgProfileImage: UIImageView!
    @IBOutlet weak var lblExpertName: UILabel!
    @IBOutlet weak var lblSpeciality: UILabel!
    @IBOutlet weak var lblEducation: UILabel!
    @IBOutlet weak var lblExperience: UILabel!
    @IBOutlet weak var lblAvailable: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    
    var expertID = "", expertName = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationItem()
        
        do {
            try loadProfileDetails()
        } catch {}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }   

    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Expert"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    private func loadProfileDetails() throws {
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        var education = "", about = "", speciality = "", experiance = "--", availability = ""
        let sql = "SELECT * FROM \(datasource.ExpertUserList_tlb) WHERE \(datasource.dbExpertID) = '\(expertID)'"
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                education = cursor!.string(forColumnIndex: 2)
                about = cursor!.string(forColumnIndex: 3)
                speciality = cursor!.string(forColumnIndex: 4)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        
        lblExpertName.text = expertName
        lblAvailable.text = availability
        lblExperience.text = experiance
        lblSpeciality.text = speciality
        lblEducation.text = education
        lblAbout.text = about
        
        let URL = Foundation.URL(string: "\(SalkProjectConstant().ServeraddExpertImg)\(expertID.sha1() + Constant.FolderNames.imageExtensionjpg)")!
        imgProfileImage.kf.setImage(with: URL, placeholder: nil, options: [.transition(.fade(1))], progressBlock: { receivedSize, totalSize in
            //Print.printLog("\(receivedSize)/\(totalSize)")
        }, completionHandler: { image, error, cacheType, imageURL in
            if error != nil {
                self.imgProfileImage.image = UIImage(named: "ic_person_black_48dp.png")
            }
        })
    }
    
}
