//
//  GenderTableCell.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class GenderTableCell: UITableViewCell {
    
    @IBOutlet weak var genderLabel: UILabel!
    
    @IBOutlet weak var genderButton: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        genderLabel.textAlignment = .left
        genderLabel.textColor = CustomColor.dark_gray()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

//        if selected == true {
//            genderButton.imageView!.image = UIImage(named: "radio_on.png")
//        } else {
//            genderButton.imageView!.image = UIImage(named: "radio_off.png")
//        }
    }
    
    
}
