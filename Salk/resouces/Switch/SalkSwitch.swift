//
//  SalkSwitch.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation
import Material

extension Switch {
    func SalkSwitch() {
        self.buttonOnColor = PrimaryColor.colorPrimary()
        self.trackOnColor = CustomColor.switchShade()
    }
}
