//
//  ConvertionClass.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class ConvertionClass {
    func conLongToDate(_ timeInterval: Double) -> String {
        return self.conLongToDate(timeInterval, dateFormat: "yyyy-MM-dd hh:mm:ss a")
    }
    func conLongToDate(_ timeInterval: Double, dateFormat: String) -> String {
        let df = DateFormatter()
        df.dateFormat = dateFormat
        
        let stringFromDate = df.string(from: Date(timeIntervalSince1970: timeInterval))
        return stringFromDate
    }
    func conLongToDate(_ timeInterval: Double, dateFormat: DateFormatter) -> String {
        let stringFromDate = dateFormat.string(from: Date(timeIntervalSince1970: timeInterval))
        return stringFromDate
    }
    
    func conDateToLong(_ date: String) -> TimeInterval {
        return conDateToLong(date, formate: "yyyy-MM-dd hh:mm:ss a")
    }
    
    func conDateToLong(_ date: String, formate: String) -> TimeInterval {
        let df = DateFormatter()
        df.dateFormat = formate
        return conDateToLong(df.date(from: date)!)
    }
    
    func conDateToLong(_ insertDate: Date) -> TimeInterval {
        return insertDate.timeIntervalSince1970
    }
    
    func currentTime() -> Double {
        // get current date/time
        let today = Date()
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss a"
        
        let currentTime = dateFormatter.string(from: today)
        
        return ConvertionClass().conDateToLong(dateFormatter.date(from: currentTime)!)
    }
    
    func currentDate() -> String {
        let today = Date()
        
        let dateFormatter = DateFormatter()        
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        return dateFormatter.string(from: today)
    }
    
    func getDiffFromCurrentDate(_ time: Double) -> String {
        var timeStr = ""
    
        var time = time
        time = abs(currentTime() - time)
        time = Double(time / (1000 * 60)) // in minute
    
        if time <= 59 {
            timeStr = "\(time)" + " min"
        } else {
            time = Double(time / 60) // in hour
            if time <= 23 {
				timeStr = "\(time)" + " hour"
            } else {
				time = Double(time / 24) // in days
				if(time <= 30) {
                    timeStr = "\(time)" + " days"
				} else {
                    time = Double(time / 30) // in month
                    if time <= 11 {
                        timeStr = "\(time)" + " month"
                    } else {
                        time = Double(time / 12) // in year
                        timeStr = "\(time)" + " year"
                    }
				}
            }
        }
        return timeStr
    }
    
    
    func getDiffFromCurrentDate(_ time1: Double, time2: Double, isAbsolute: Bool) -> String {
        var timeStr = ""
        var time1: Int64 = Int64(time1)
        
        if isAbsolute {
            time1 = abs(time1 - Int64(time2))
        } else {
            time1 = time1 - Int64(time2)
        }
        
        if time1 <= 59 {
            timeStr = "\(time1)" + " sec"
        } else {
            time1 = (time1 / 60) // in min
            if time1 <= 59 {
                timeStr = "\(time1)" + " min"
            } else {
				time1 = (time1 / 60) // in hour
				if time1 <= 23 {
                    timeStr = "\(time1)" + " hour"
				} else {
                    time1 = (time1 / 24) // in days
                    if time1 <= 30 {
                        timeStr = "\(time1)" + " days"
                    } else {
                        time1 = (time1 / 30) // in month
                        if time1 <= 11 {
                            timeStr = "\(time1)" + " month"
                        } else {
                            time1 = (time1 / 12) // in year
                            timeStr = "\(time1)" + " year"
                        }
                    }
				}
            }
        }

        return timeStr;
    }

    func getDiffFromCurrentDateInExactly(_ time1: Double, time2: Double, isAbsolute: Bool) -> String {
        var timeStr = ""
        var time1: Int64 = Int64(time1)
        let time2: Int64 = Int64(time2)
        
        if isAbsolute {
            time1 = abs(time1 - time2)
        } else {
            time1 = time1 - time2
        }
    
        if time1 <= 59 {
            timeStr = "\(time1)" + " sec"
        } else {
            time1 = (time1 / 60) // in min
            if time1 <= 59 {
				timeStr = "\(time1)" + " min"
            } else {
				time1 = (time1 / 60) // in hour
                if time1 <= 23 {
                    timeStr = "\(time1)" + " hour"
				} else {
                    var time: Int64 = (time1 / 24) // in days
                    if time == 1 {
                        timeStr = "\(time1)" + " day  "
    
                        if time1 % 24 > 0 {
                            timeStr += "\((time1 % 24))" + " hour"
                        }
                    } else if time <= 30 {
                        timeStr = "\(time)" + " days  "

                        if time1 % 24 > 0 {
                            timeStr += "\((time1 % 24))" + " hour"
                        }
                    } else {
                        time1 = time
                        time = (time / 30) // in month
                        if time == 1 {
                            timeStr = "\(time)" + " month  "
    
                            if time1 % 30 > 0 {
                                timeStr += "\((time1 % 30))" + " days"
                            }
                        } else if time <= 11 {
                            timeStr = "\(time)" + " months  "
    
                            if time1 % 30 > 0 {
                                timeStr += "\((time1 % 30))" + " days"
                            }
                        } else {
                            time1 = time
                            time = (time1 / 12) // in year
                            if time == 1 {
                                timeStr = "\(time)" + " year  "
    
                                if time1 % 12 > 0 {
                                    timeStr += "\((time1 % 12))" + " months"
                                }
    
                            } else {
                                timeStr = "\(time)" + " years  "
    
                                if time1 % 12 > 0 {
                                    timeStr += "\((time1 % 12))" + " months"
                                }
                            }
                        }
                    }
				}
            }
        }
        return timeStr
    }

    
    func getDiffFromLicenseExpire(_ time: Double, serverTime: Double) -> String {
        var timeStr = ""
        var time: Int64 = Int64(time)
        let serverTime: Int64 = Int64(serverTime)
    
        time = time - serverTime
        time = (time / 60) // in minute
    
        if time <= 59 {
            timeStr = "\(time)" + " min"
        } else {
            time = (time / 60) // in hour
            if time <= 23 {
				timeStr = "\(time)" + " hour"
            } else {
				time = (time / 24) // in days
				time += 1 //for 1 day extra
    
                if time == 1 {
                    timeStr = "\(time)" + " day"
                } else if time > 1 {
                    timeStr = "\(time)" + " days"
                }
            }
        }
        return timeStr
    }

    func getDateParsingFrmMinute(_ duration: Int, isFullString: Bool) -> String{
        var dur = ""
        var duration = duration
        
        if duration > 60 {
            var rem = duration % 60
            duration = duration / 60
    
            if duration > 24 {
				rem = duration % 24
				duration = duration / 24
    
                if duration == 1 {
                    dur = "\(duration)" + " day " //day
                } else if duration > 1 {
                    dur = "\(duration)" + " days " //day
                }
    
				if rem > 0 {
                    dur += "\(rem)" + " hour" //hour
				}
            } else {
                if duration > 0 {
                    dur = "\(duration)" + " hour " //hour
                }
				if(rem > 0) {
                    dur += "\(rem)" + (isFullString ? " minute" : " min") //minute
				}
            }
        } else {
            dur = "\(duration)" + (isFullString ? " minute" : " min") //minute
        }
    
        return dur
    }
    
    func getDateParsingFrmSecond(_ duration: Int, isFullString: Bool) -> String{
        var dur = ""
        var duration = duration
        
        if duration > 60 {
            var rem = duration % 60
            duration = duration / 60
            
            if duration > 60 {
                rem = duration % 60
                duration = duration / 60
                
                if duration > 24 {
                    rem = duration % 24
                    duration = duration / 24
                    
                    if duration == 1 {
                        dur = "\(duration)" + " day " //day
                    } else if duration > 1 {
                        dur = "\(duration)" + " days " //day
                    }
                    
                    if rem > 0 {
                        dur += "\(rem)" + " hour" //hour
                    }
                } else {
                    if duration > 0 {
                        dur = "\(duration)" + " hour " //hour
                    }
                    if(rem > 0) {
                        dur += "\(rem)" + (isFullString ? " minute" : " min") //minute
                    }
                }
            } else {
                if (duration > 0) {
                    if duration == 1 {
                        dur = "\(duration)" + (isFullString ? " minute " : " min ") //minute
                    } else {
                        dur = "\(duration)" + (isFullString ? " minutes " : " min ") //minute
                    }
                }
                
                if (rem > 0) {
                    dur += "\(rem)" + (isFullString ? " seconds" : " sec"); //second
                }
            }
            
        } else {
            dur = "\(duration)" + (isFullString ? " seconds" : " sec") //second
        }
        
        return dur
    }
    
    func getTimezoneDateTime(_ gmtTime: Double) -> Double {
        return gmtTime + TimeInterval(NSTimeZone.system.secondsFromGMT())
    }
}

extension Date {
    var age: Int {
        return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
    }
}
