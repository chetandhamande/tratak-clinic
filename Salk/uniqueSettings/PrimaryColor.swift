//
//  PrimaryColor.swift
//  Salk
//
//  Created by Chetan on 10/01/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import Foundation

class PrimaryColor: NSObject {
    // Primary app color
    class func colorPrimary() -> UIColor {
        return UIColor(rgba: "#282828FF")
    }
    class func colorPrimary1() -> UIColor {
        return UIColor(rgba: "#282828FF")
    }
    class func colorPrimary2() -> UIColor {
        return UIColor(rgba: "#282828FF")
    }
    class func colorPrimaryDark() -> UIColor {
        return UIColor(rgba: "#323131ff")
    }
    class func colorAccent() -> UIColor {
        return UIColor(rgba: "#00E5FFFF")
    }
    class func colorWhiteBackgroud() -> UIColor {
        return UIColor(rgba: "#ffffffFF")
    }
    class func colorPrimaryBackgroud() -> UIColor {
        return UIColor(rgba: "#ffffffFF")
    }
    class func colorControlNormal() -> UIColor {
        return UIColor(rgba: "#78909CFF")
    }
    class func colorControlActivated() -> UIColor {
        return UIColor(rgba: "#002366FF")
    }
    class func colorSwitchThumbNormal() -> UIColor {
        return UIColor(rgba: "#78909CFF")
    }
    class func colorPrimaryTrans() -> UIColor {
        return UIColor(rgba: "#002366DD")
    }
}
