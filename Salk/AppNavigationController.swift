//
//  AppNavigationController.swift
//

import Foundation
import Material

class AppNavigationController: NavigationController {
    
    open override func prepare() {
        super.prepare()
        guard let v = navigationBar as? NavigationBar else {
            return
        }
        
        v.depthPreset = .none
        v.dividerColor = Color.grey.lighten3
        
        v.tintColor = UIColor.black
        v.backgroundColor = UIColor.white
        
        v.backIndicatorImage = Icon.arrowBack
        v.hideBottomHairline()
                
        //Change status bar color
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = PrimaryColor.colorPrimaryDark()
        }
    }
}

extension UIViewController {
    func showPopUpViewController(selfViewController: UIViewController, presentViewController: UIViewController) {
        presentViewController.modalPresentationStyle = .overFullScreen
        
        presentViewController.modalTransitionStyle = .crossDissolve
        
        self.present(presentViewController, animated: true, completion: nil)
    }
}
