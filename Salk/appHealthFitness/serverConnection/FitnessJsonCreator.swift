class FitnessJsonCreator {
    
    var err: AutoreleasingUnsafeMutablePointer<NSError?>? = nil
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeFoodDetailsLogJson(_ json: Data) throws {
        
        let appPrefs = MySharedPreferences()
        
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            let foodDetails = JsonParser.getJsonValueMutableArray(dict, valueForKey: "foodDetails")
    
            let len = foodDetails.count
            if (len > 0) {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                datasource.TruncateFoodRecordList()
               
                let dbValue = NSMutableArray()
                for i in 0 ..< len {
                    guard let jsonObj = foodDetails[i] as? NSMutableDictionary else {
                        continue
                    }
                    
                    let foodID = JsonParser.getJsonValueString(jsonObj, valueForKey: "foodID")
                    let name = JsonParser.getJsonValueString(jsonObj, valueForKey: "name")
                    var protein = JsonParser.getJsonValueString(jsonObj, valueForKey: "protein")
                    var fiber = JsonParser.getJsonValueString(jsonObj, valueForKey: "fiber")
                    var fat = JsonParser.getJsonValueString(jsonObj, valueForKey: "fat")
                    var carbohydrate = JsonParser.getJsonValueString(jsonObj, valueForKey: "carbohydrate")
                    var total_gram = JsonParser.getJsonValueString(jsonObj, valueForKey: "total_gram")
                    let containerID = JsonParser.getJsonValueString(jsonObj, valueForKey: "contain_map")
                    //String image_name = jsonObj.getString("image_name")
                    
                    if (protein == "<null>" || protein.isEmpty) { protein = "0" }
                    if (fiber == "<null>" || fiber.isEmpty) { fiber = "0"}
                    if (fat == "<null>" || fat.isEmpty) { fat = "0" }
                    if (carbohydrate == "<null>" || carbohydrate.isEmpty) { carbohydrate = "0" }
                    if (total_gram == "<null>" || total_gram.isEmpty) { total_gram = "0" }
                    
                    let dbDict = NSMutableDictionary()
                    dbDict.setValue(foodID, forKey: "1")
                    dbDict.setValue(name, forKey: "2")
                    dbDict.setValue(protein, forKey: "3")
                    dbDict.setValue(fiber, forKey: "4")
                    dbDict.setValue(fat, forKey: "5")
                    dbDict.setValue(carbohydrate, forKey: "6")
                    dbDict.setValue(total_gram, forKey: "7")
                    dbDict.setValue(containerID, forKey: "8")
                    
                    dbValue.add(dbDict)
                }
                
                let parameter = NSMutableDictionary()
                parameter.setValue(datasource.dbFoodID, forKey: "1")
                parameter.setValue(datasource.dbFoodName, forKey: "2")
                parameter.setValue(datasource.dbProtein, forKey: "3")
                parameter.setValue(datasource.dbFiber, forKey: "4")
                parameter.setValue(datasource.dbFat, forKey: "5")
                parameter.setValue(datasource.dbCarbs, forKey: "6")
                parameter.setValue(datasource.dbCalories, forKey: "7")
                parameter.setValue(datasource.dbContainID, forKey: "8")
                
                datasource.insertMultipleExecuteBind(datasource.FoodRecordList_tlb, parameter: parameter, value: dbValue)
                
                datasource.closeDatabase()
          
        
                appPrefs.setLastFoodLogListSycnTime(text: ConvertionClass().currentTime())
            }
        }
    }
    
    func storeFoodContainerLogJson( _ json: Data) throws {
    
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            let containerDetails = JsonParser.getJsonValueMutableArray(dict, valueForKey: "containDetails")
            
            let len = containerDetails.count
            if (len > 0) {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                datasource.TruncateFoodContainerList()
    
                for i in 0 ..< len {
                    guard let jsonObj = containerDetails[i] as? NSMutableDictionary else {
                        continue
                    }
                    
                    var containerID = JsonParser.getJsonValueString(jsonObj, valueForKey: "containID")
                    var name = JsonParser.getJsonValueString(jsonObj, valueForKey: "name")
                    var value = JsonParser.getJsonValueString(jsonObj, valueForKey: "value")
                    var unit = JsonParser.getJsonValueString(jsonObj, valueForKey: "unit")
                    
                    if (containerID == "<null>" || containerID.isEmpty) {
                        containerID = "0" }
                    if (value == "<null>" || value.isEmpty) { value = "" }
                    if (unit == "<null>" || unit.isEmpty) { unit = "" }
                    if (name == "<null>" || name.isEmpty) { name = "" }
                    if (containerID == "0") { continue }
                    
                    datasource.InsertFoodContainerList(containID: containerID.trimmed, containName: name.trimmed, value: value, unit: unit)
                }
                
                datasource.closeDatabase()
            }
        }
    }
    
    
    func storeActivityDetailsLogJson( _ json: Data) throws {
    
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            let foodDetails = JsonParser.getJsonValueMutableArray(dict, valueForKey: "activityDetails")
            
            let len = foodDetails.count
            if (len > 0) {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                datasource.TruncateSportActivityList()
                
                let dbValue = NSMutableArray()
                for i in 0 ..< len {
                    guard let jsonObj = foodDetails[i] as? NSMutableDictionary else {
                        continue
                    }
                    
                    let id = JsonParser.getJsonValueString(jsonObj, valueForKey: "id")
                    let activity = JsonParser.getJsonValueString(jsonObj, valueForKey: "activity")
                    var perUnit = JsonParser.getJsonValueString(jsonObj, valueForKey: "perUnit")
                    //let moderateCal = JsonParser.getJsonValueString(jsonObj, valueForKey: "moderateCal")
                    //let casualCal = JsonParser.getJsonValueString(jsonObj, valueForKey: "casualCal")
                    //let intenseCal = JsonParser.getJsonValueString(jsonObj, valueForKey: "intenseCal")
                    var calaries = "100" //jsonObj.getString("calaries")
                    
                    if (calaries == "<null>" || calaries.isEmpty) { calaries = "0" }
                    if (perUnit == "<null>" || perUnit.isEmpty) { perUnit = "0" }
                    
                    let dbDict = NSMutableDictionary()
                    dbDict.setValue(id, forKey: "1")
                    dbDict.setValue(activity, forKey: "2")
                    dbDict.setValue(calaries, forKey: "3")
                    
                    dbValue.add(dbDict)
                }
                
                let parameter = NSMutableDictionary()
                parameter.setValue(datasource.dbActID, forKey: "1")
                parameter.setValue(datasource.dbActName, forKey: "2")
                parameter.setValue(datasource.dbCalories, forKey: "3")
                
                datasource.insertMultipleExecuteBind(datasource.SportActivityList_tlb, parameter: parameter, value: dbValue)
                
                datasource.closeDatabase()
                
                appPrefs.setLastActivityLogListSycnTime(text: ConvertionClass().currentTime())
            }
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonFoodSyncLog() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID()
        ]
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let jsonArray = NSMutableArray()
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        //food
        let sql = "SELECT * FROM " + datasource.FoodRecordLogList_tlb + " WHERE " + datasource.dbIsServerSync + " = '0' "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let jsonObj = NSMutableDictionary()
                jsonObj.setValue(cursor!.string(forColumnIndex: 0), forKey: "foodID")
                jsonObj.setValue(cursor!.string(forColumnIndex: 1), forKey: "qty")
                
                let dateTime = "\(ConvertionClass().conLongToDate(cursor!.double(forColumnIndex: 2), dateFormat: df))"
                jsonObj.setValue(dateTime, forKey: "date")
                jsonObj.setValue(cursor!.string(forColumnIndex: 4), forKey: "containID")
                jsonObj.setValue(cursor!.string(forColumnIndex: 7), forKey: "schedule")
                jsonObj.setValue(cursor!.string(forColumnIndex: 5), forKey: "uniqueID")
                jsonObj.setValue(cursor!.string(forColumnIndex: 8), forKey: "eatID")
                
                jsonArray.add(jsonObj)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        data.setValue(jsonArray, forKey: "data")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
    
        return json
    }
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsonFoodSyncLog( _ json: Data) throws {
        let appPrefs = MySharedPreferences()
    
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            appPrefs.setLastLogDataSycnType(text: "")
            appPrefs.setLastLogDataSycnTime(text: ConvertionClass().currentTime())
    
            let dataArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
            let len = dataArray.count
            if (len > 0) {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                
                for i in 0 ..< len {
                    guard let subJson = dataArray[i] as? NSMutableDictionary else {
                        continue
                    }
                    
                    let serverID = JsonParser.getJsonValueString(subJson, valueForKey: "serverID")
                    let uniqueID = JsonParser.getJsonValueString(subJson, valueForKey: "uniqueID")
                    
                    datasource.UpdateFoodRecordLogSyncList(uniqueID: uniqueID, serverID: serverID)
                }
                datasource.closeDatabase()
            }
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonActivitySyncLog() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID()
        ]
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let jsonArray = NSMutableArray()
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        //food
        let sql = "SELECT * FROM " + datasource.SportActivityRecordList_tlb + " WHERE " + datasource.dbIsServerSync + " = '0' "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let jsonObj = NSMutableDictionary()
                jsonObj.setValue(cursor!.string(forColumnIndex: 0), forKey: "activityID")
                jsonObj.setValue(cursor!.string(forColumnIndex: 1), forKey: "duration")
                
                let dateTime = "\(ConvertionClass().conLongToDate(cursor!.double(forColumnIndex: 0), dateFormat: df))"
                jsonObj.setValue(dateTime, forKey: "date")
                jsonObj.setValue(cursor!.string(forColumnIndex: 4), forKey: "type")
                
                jsonArray.add(jsonObj)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        data.setValue(jsonArray, forKey: "data")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsonActivitySyncLog( _ json: Data) throws {
        let appPrefs = MySharedPreferences()
        
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            appPrefs.setLastLogDataSycnType(text: "")
            appPrefs.setLastLogDataSycnTime(text: ConvertionClass().currentTime())
    
            let datasource = DBOperation()
            datasource.openDatabase(true)
            datasource.UpdateActivityRecordLogSyncList()
            datasource.closeDatabase()
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonWaterSyncLog() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID()
        ]
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let jsonArray = NSMutableArray()
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        //food
        let sql = "SELECT * FROM " + datasource.WaterTrackList_tlb + " WHERE " + datasource.dbIsServerSync + " = '0' "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let jsonObj = NSMutableDictionary()
                jsonObj.setValue(cursor!.string(forColumnIndex: 0), forKey: "qty")
                let dateTime = "\(ConvertionClass().conLongToDate(cursor!.double(forColumnIndex: 1), dateFormat: df))"
                jsonObj.setValue(dateTime, forKey: "date")
                jsonObj.setValue(cursor!.string(forColumnIndex: 3), forKey: "flag")
                                
                jsonArray.add(jsonObj)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        data.setValue(jsonArray, forKey: "data")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsonWaterSyncLog( _ json: Data) throws  {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            appPrefs.setLastLogDataSycnType(text: "")
            appPrefs.setLastLogDataSycnTime(text: ConvertionClass().currentTime())
    
            let datasource = DBOperation()
            datasource.openDatabase(true)
            datasource.UpdateWaterTrackListSyncLog()
            datasource.closeDatabase()
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonPedometerSyncLog() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID()
        ]
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let jsonArray = NSMutableArray()
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        //food
        let sql = "SELECT p." + datasource.dbSteps + ", p." + datasource.dbDateTime
            + ", c." + datasource.dbCalories + ", d." + datasource.dbDistance
            + "  FROM " + datasource.PedometerRecordLogList_tlb + " p, " + datasource.CaloriesRecordLogList_tlb + " c, "
            + datasource.DistanceRecordLogList_tlb + " d "
            + " WHERE (p." + datasource.dbIsServerSync + " = '0' OR c." + datasource.dbIsServerSync + " = '0'"
            + " OR d." + datasource.dbIsServerSync + " = '0') "
            + " AND (p." + datasource.dbDateTime + " = c." + datasource.dbDateTime
            + " AND c." + datasource.dbDateTime + " = d." + datasource.dbDateTime + ") "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let jsonObj = NSMutableDictionary()
                jsonObj.setValue(cursor!.string(forColumnIndex: 0), forKey: "steps")
                jsonObj.setValue(cursor!.string(forColumnIndex: 1) + " 12:00:01 AM", forKey: "date")
                jsonObj.setValue(cursor!.string(forColumnIndex: 2), forKey: "cal")
                jsonObj.setValue(cursor!.string(forColumnIndex: 3), forKey: "dist")
                
                jsonArray.add(jsonObj)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        data.setValue(jsonArray, forKey: "data")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsonPedometerSyncLog( _ json: Data) throws  {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            appPrefs.setLastLogDataSycnType(text: "")
            appPrefs.setLastLogDataSycnTime(text: ConvertionClass().currentTime())
    
            let datasource = DBOperation()
            datasource.openDatabase(true)
            datasource.UpdatePedometerRecordLogSyncList()
            datasource.UpdateCaloriesRecordLogSyncList()
            datasource.UpdateDistanceRecordLogSyncList()
            datasource.closeDatabase()
        }
    }
    
    // create JSON values to send it to server
    func putJsonSleepSyncLog() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID()
        ]
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let jsonArray = NSMutableArray()
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        //food
        let sql = "SELECT * FROM " + datasource.SleepTrackList_tlb + " WHERE " + datasource.dbIsServerSync + " = '0' "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let jsonObj = NSMutableDictionary()
                jsonObj.setValue(cursor!.string(forColumnIndex: 0), forKey: "qty")
                
                let dateTime = "\(ConvertionClass().conLongToDate(cursor!.double(forColumnIndex: 2), dateFormat: df))"
                jsonObj.setValue(dateTime, forKey: "date")
                jsonObj.setValue(cursor!.string(forColumnIndex: 4), forKey: "quality")
                
                let timeVal = cursor!.string(forColumnIndex: 1)!
                let values = timeVal.trimmed.components(separatedBy: ",")
                if values.count < 2 {
                    continue
                }
                jsonObj.setValue(values[0], forKey: "sleepTime")
                jsonObj.setValue(values[1], forKey: "wakeUpTime")
                
                jsonArray.add(jsonObj)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        data.setValue(jsonArray, forKey: "data")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsonSleepSyncLog( _ json: Data) throws  {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            appPrefs.setLastLogDataSycnType(text: "")
            appPrefs.setLastLogDataSycnTime(text: ConvertionClass().currentTime())
    
            let datasource = DBOperation()
            datasource.openDatabase(true)
            datasource.UpdateSleepTrackListSyncLog()
            datasource.closeDatabase()
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonExpertChatSync() throws -> Data {
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "patientID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID()
        ]
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        var expertID: [String]! = [String]()
        var expertCatID: [String]! = [String]()
        var sql = "SELECT " + datasource.dbExpertID + ", " + datasource.dbExpertCatID + " FROM " + datasource.ExpertUserList_tlb
        var cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                expertID.append(cursor!.string(forColumnIndex: 0))
                expertCatID.append(cursor!.string(forColumnIndex: 1))
            }
            cursor!.close()
        }
        
        let jsonArray = NSMutableArray()
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        //expert Chat Communication
        sql = "SELECT *  FROM " + datasource.ExpertCommMsgList_tlb
            + " WHERE " + datasource.dbIsInbox + " = '0' "
            + " AND " + datasource.dbMsgID + " = '0' "
        cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let jsonObj = NSMutableDictionary()
                jsonObj.setValue(cursor!.string(forColumnIndex: 1), forKey: "msg")
                jsonObj.setValue(cursor!.string(forColumnIndex: 8), forKey: "msgType")
                jsonObj.setValue(cursor!.string(forColumnIndex: 9), forKey: "fileName")
                
                let dateTime = "\(ConvertionClass().conLongToDate(cursor!.double(forColumnIndex: 6), dateFormat: df))"
                jsonObj.setValue(dateTime, forKey: "date")
                jsonObj.setValue(cursor!.string(forColumnIndex: 6), forKey: "phoneMsgID")
                jsonObj.setValue(cursor!.string(forColumnIndex: 2), forKey: "doctorID")
                
                let index = expertID.index(of: cursor!.string(forColumnIndex: 2))
                if (index != nil) {
                    jsonObj.setValue(expertCatID[index!], forKey: "expertCatID")
                } else {
                    jsonObj.setValue("0", forKey: "expertCatID")
                }
                
                jsonArray.add(jsonObj)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        data.setValue(jsonArray, forKey: "data")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsonExpertChatSync( _ json: Data) throws  {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            appPrefs.setIsPendingChatRec(text: false)
    
            let jsonObject = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
            let chatIDArray = JsonParser.getJsonValueMutableArray(jsonObject, valueForKey: "chatIDs")
            
            let len = chatIDArray.count
            if (len > 0) {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                datasource.UpdateExpertCommunicationListServerSync()
                
                for i in 0 ..< len {
                    guard let jsonObj = chatIDArray[i] as? NSMutableDictionary else {
                        continue
                    }
        
                    let msgID = JsonParser.getJsonValueString(jsonObj, valueForKey: "chatID")
                    let phoneMsgID = JsonParser.getJsonValueString(jsonObj, valueForKey: "phoneMsgID")
                    
                    datasource.UpdateExpertCommunicationListIDStatus(chatID: msgID, dateTime: phoneMsgID)
                }
    
                datasource.closeDatabase()
            }
        }
    }
    
    // create JSON values to send it to server
    func putJsonExpertUserSync() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "patientID": appUserInfo.getUserID(),
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    // Store Server data in shared preferences depending upon the type of
    // current request
    func storeJsonExpertUserSync( _ json: Data) throws  {
        
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            let userList = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
            
            if (userList.count > 0) {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                datasource.TruncateExpertUserList()
    
                for i in 0 ..< userList.count {
                    guard let jsonObj = userList[i] as? NSMutableDictionary else {
                        continue
                    }
                    
                    var id = JsonParser.getJsonValueString(jsonObj, valueForKey: "doctorID")
                    var name = JsonParser.getJsonValueString(jsonObj, valueForKey: "fname") + " " + JsonParser.getJsonValueString(jsonObj, valueForKey: "lname")
                    var education = JsonParser.getJsonValueString(jsonObj, valueForKey: "education")
                    var about = JsonParser.getJsonValueString(jsonObj, valueForKey: "about_author")
                    var specility = JsonParser.getJsonValueString(jsonObj, valueForKey: "specialist")
                    var mobile = JsonParser.getJsonValueString(jsonObj, valueForKey: "mobile")
                    var allotedGroupID = JsonParser.getJsonValueString(jsonObj, valueForKey: "allot_group_id")
                    
                    if (id == "<null>" || id.isEmpty) { id = "0"  }
                    if (name == "<null>" || name.isEmpty) { name = "" }
                    if (education == "<null>" || education.isEmpty) { education = "" }
                    if (about == "<null>" || about.isEmpty) { about = "" }
                    if (specility == "<null>" || specility.isEmpty) { specility = "" }
                    if (allotedGroupID == "<null>" || allotedGroupID.isEmpty) { allotedGroupID = "" }
                    if (mobile == "<null>" || mobile.isEmpty) { mobile = "" }
                    
                    datasource.InsertExpertUserList(expID: id, expName: name, edu: education, about: about, specilist: specility, expertCatID: "0", allGrpID: allotedGroupID, mobile: mobile)
                }
                datasource.closeDatabase()
                
                appPrefs.setLastExpertListSycnTime(text: ConvertionClass().currentTime())
            }
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonSyncRoutingTrack() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID()
        ]
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let jsonArray = NSMutableArray()
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        //food
        let sql = "SELECT * FROM " + datasource.RoutingTrackList_tlb
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let jsonObj = NSMutableDictionary()
                jsonObj.setValue(cursor!.string(forColumnIndex: 0), forKey: "optionID")
                let dateTime = "\(ConvertionClass().conLongToDate(cursor!.double(forColumnIndex: 1), dateFormat: df))"
                jsonObj.setValue(dateTime, forKey: "date")
                jsonObj.setValue(cursor!.string(forColumnIndex: 2), forKey: "schedule")
                
                jsonArray.add(jsonObj)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        data.setValue(jsonArray, forKey: "data")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    
    }
    
    // current request
    func storeJsonSyncRoutingTrack( _ json: Data) throws {
        
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            appPrefs.setLastLogDataSycnTime(text: ConvertionClass().currentTime())
    
            let datasource = DBOperation()
            datasource.openDatabase(true)
            datasource.TruncateRoutingList()
            datasource.closeDatabase()
        }
    }
    
    // create JSON values to send it to server
    func putJsonSyncMyDiary() throws -> Data {
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "patientID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID(),
                "prescriptionType": appUserInfo.getTempRecordID()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeJsonSyncMyDiary( _ json: Data) throws  {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200 && appPrefs.getIsPremiumPlanActive()) {
            let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
            
            let datasource = DBOperation()
            datasource.openDatabase(true)
    
            var arrSchedule: [String]! = [String]()
            var arrScheduleType: [String]! = [String]()
            let sql = "SELECT * FROM " + datasource.ScheduleList_tlb
                + " ORDER BY CAST(" + datasource.dbTime + " as INTEGER) "
            let cursor = datasource.selectRecords(sql)
            if cursor != nil {
                while cursor!.next() {
                    arrScheduleType.append(cursor!.string(forColumnIndex: 0))
                    arrSchedule.append(cursor!.string(forColumnIndex: 2))
                }
                cursor!.close()
            }
            
            for i in 0 ..< arrSchedule.count {
                do {
                    let breakfastJson = JsonParser.getJsonValueMutableDictionary(dataJson, valueForKey: arrSchedule[i])
                    try performStoreDiaryData(recType: arrScheduleType[i], recName: arrSchedule[i], jsonRec: breakfastJson, datasource: datasource, appPref: appPrefs)
                } catch {}
            }
            datasource.closeDatabase()
        
            appPrefs.setLastMyDiaryDataSycnTime(text: ConvertionClass().currentTime())
        }
    }
    
    fileprivate func performStoreDiaryData(recType: String, recName: String, jsonRec: NSMutableDictionary, datasource: DBOperation, appPref: MySharedPreferences) throws
    {
        //daily goal
        Print.printLog("MyDiary : \(jsonRec)")
        
        let body = JsonParser.getJsonValueString(jsonRec, valueForKey: "descr")
        var dateTime = JsonParser.getJsonValueString(jsonRec, valueForKey: "date")
        if dateTime.isEmpty { return }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        dateTime =  "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime)!))"
        
        let jsonArrList = JsonParser.getJsonValueMutableArray(jsonRec, valueForKey: "options")
        let totalList = jsonArrList.count
        if (totalList > 0) {
            let recID = datasource.InsertRecentDashboardList(recName: recName, recDesc: body, recType: recName, dateTime: dateTime)
    
            if (!recID.isEmpty && recID != "0") {
                if (appPref.getTempRecordID().equalsIgnoreCase("diet")) {
                    var time = JsonParser.getJsonValueString(jsonRec, valueForKey: "time") //reminder time
                    
                    if !time.equalsIgnoreCase("12:00 am") {
                        let foodName = appPref.getFoodRemName().components(separatedBy: ",")
                        var foodVal = appPref.getFoodRemValue().components(separatedBy: ",")
                        for i in 0 ..< foodName.count {
                            if(foodName[i] == recName) {
                                foodVal[i] = time
                                break
                            }
                        }
                        
                        var val = "", nameVal = ""
                        for i in 0 ..< foodName.count {
                            val += val.isEmpty ? foodVal[i] : "," + foodVal[i]
                            nameVal += nameVal.isEmpty ? foodName[i] : "," + foodName[i]
                        }
                        appPref.setFoodRemValue(text: val)
                        appPref.setFoodRemName(text: nameVal)
                        
                        //update Schedule Tables
                        dateFormatter.dateFormat = "hh:mm a"
                        time =  "\(ConvertionClass().conDateToLong(dateFormatter.date(from: time)!))"
                        datasource.UpdateScheduleTimeList(time: time, scheduleName: recName)
                    }
                }
                
                //get previously selected optionID, before truncate
                var preSelOptionIDs = ""
                let sql = "SELECT " + datasource.dbOptionID + " FROM " + datasource.recDashboardSubList_tlb + " WHERE " + datasource.recID + " = " + recID + " AND " + datasource.dbIsChecked + " = '1' "
                let cursor = datasource.selectRecords(sql)
                if cursor != nil {
                    while cursor!.next() {
                        preSelOptionIDs += preSelOptionIDs.isEmpty ? "'" + cursor!.string(forColumnIndex: 0) + "'" : ",'" + cursor!.string(forColumnIndex: 0) + "'"
                    }
                    cursor!.close()
                }
                datasource.TruncateRecentDashboardSubList(recIDStr: recID)
    
                for i in 0 ..< totalList {
                    guard let jsonObj = jsonArrList[i] as? NSMutableDictionary else {
                        continue
                    }
                    
                    let optionID = JsonParser.getJsonValueString(jsonObj, valueForKey: "optionID")
                    let optionName = JsonParser.getJsonValueString(jsonObj, valueForKey: "optionName")
                    
                    var days = ""
                    let daysJson = JsonParser.getJsonValueMutableArray(jsonObj, valueForKey: "days")
                    for j in 0 ..< daysJson.count {
                        guard let daysJson = daysJson[j] as? NSMutableDictionary else {
                            continue
                        }
                        let dayVal = JsonParser.getJsonValueString(daysJson, valueForKey: "day")
                        days += days.isEmpty ? "\(dayVal)" : ", \(dayVal)"
                    }
                    
                    let optionJson = JsonParser.getJsonValueMutableArray(jsonObj, valueForKey: "subOption")
                    for j in 0 ..< optionJson.count {
                        guard let optSubJson = optionJson[j] as? NSMutableDictionary else {
                            continue
                        }
                        
                        var id = "", quanti = "", containID = "", desc = "", videoLink = ""
                        if(recType.equalsIgnoreCase("Food")) {
                            id = JsonParser.getJsonValueString(optSubJson, valueForKey: "foodName")
                            quanti = JsonParser.getJsonValueString(optSubJson, valueForKey: "qty")
                            containID = JsonParser.getJsonValueString(optSubJson, valueForKey: "containName")
                            
                        } else if(recType.equalsIgnoreCase("Activity")) {
                            id = JsonParser.getJsonValueString(optSubJson, valueForKey: "title")
                            quanti = JsonParser.getJsonValueString(optSubJson, valueForKey: "qty")
                            desc = JsonParser.getJsonValueString(optSubJson, valueForKey: "description")
                            videoLink = JsonParser.getJsonValueString(optSubJson, valueForKey: "videoURL")
                            if (videoLink == "<null>" || videoLink == "NA") { videoLink = "" }
                            
                        } else if(recType.equalsIgnoreCase("Therapy")) {
                            id = JsonParser.getJsonValueString(optSubJson, valueForKey: "therapyName")
                            quanti = JsonParser.getJsonValueString(optSubJson, valueForKey: "qty")
                            desc = JsonParser.getJsonValueString(optSubJson, valueForKey: "description")
                            videoLink = JsonParser.getJsonValueString(optSubJson, valueForKey: "videoURL")
                            if (videoLink == "<null>" || videoLink == "NA") { videoLink = "" }
                            
                        } else if(recType.equalsIgnoreCase("Medicine")) {
                            id = JsonParser.getJsonValueString(optSubJson, valueForKey: "medicine")
                            quanti = JsonParser.getJsonValueString(optSubJson, valueForKey: "qty")
                            containID = JsonParser.getJsonValueString(optSubJson, valueForKey: "containName")
                        }
                        
                        datasource.InsertRecentDashboardSubList(actID: id, recIDStr: recID, qunti: quanti, contID: containID, desc: desc, optID: optionID, optName: optionName, videoLink: videoLink, msg: "", days: days)
                    }
                }
                
                //update previously selected option ids
                if(!preSelOptionIDs.isEmpty) {
                    datasource.UpdateRecentDashboardSubMultipleList(isChecked: "1", optionIDs: preSelOptionIDs, recIDStr: recID)
                }
            }
        }
    }
    
    // create JSON values to send it to server
    func putJsonSyncChatMessage() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "patientID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID(),
                "doctorID": appUserInfo.getExpertChatID(),
                "position": "0"
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeJsonSyncChatMessage( _ json: Data) throws {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            let dataJson = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
    
            let len = dataJson.count
            if (len > 0) {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                
                for i in 0 ..< len {
                    guard let jsonRec = dataJson[i] as? NSMutableDictionary else {
                        continue
                    }
                    
                    let msg = JsonParser.getJsonValueString(jsonRec, valueForKey: "msg")
                    let expertID = appPrefs.getExpertChatID()
                    let msgID = JsonParser.getJsonValueString(jsonRec, valueForKey: "chatID")
                    let read = JsonParser.getJsonValueString(jsonRec, valueForKey: "readFlag")
                    let type = (JsonParser.getJsonValueString(jsonRec, valueForKey: "type").uppercased() == "USER") ? "0" : "1"
                    var dateTime = JsonParser.getJsonValueString(jsonRec, valueForKey: "date")
                    dateTime =  "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime)!))"
                    
                    let msgType = JsonParser.getJsonValueString(jsonRec, valueForKey: "msgType")
                    let fileName = JsonParser.getJsonValueString(jsonRec, valueForKey: "fileName")
                    
                    datasource.InsertExpertCommunicationList(msgID: msgID, msg: msg, expID: expertID, expName: "", dateTime: dateTime, isInbox: type, isRead: read, fileName: fileName, msgType: msgType, IsServerSync: "1")
                }
            
                datasource.closeDatabase()
            }
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonSyncChatStatus() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "patientID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID(),
                "deleteIDs": appUserInfo.getIsPendingDeleteChatRec(),
                "readIDs": appUserInfo.getIsPendingReadChatRec(),
                "deliveryIDs": appUserInfo.getIsPendingDeliveredChatRec()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeJsonSyncChatStatus( _ json: Data) throws  {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            appPrefs.setIsPendingDeleteChatRec(text: "")
            appPrefs.setIsPendingDeliveredChatRec(text: "")
            appPrefs.setIsPendingReadChatRec(text: "")
        }
    }
    
    // create JSON values to send it to server
    func putJsonSyncTimeLine() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID(),
                "position": "0"
        ]
     
        let calendar = Calendar.current
        let toDate = calendar.date(byAdding: .day, value: 0, to: Date())
        let fromDate = calendar.date(byAdding: .day, value: -90, to: Date())
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        var dateTime = dateFormatter.string(from: toDate!) + " 11:59:50 pm"
        data.setValue(dateTime, forKey: "toDate")
        
        dateTime = dateFormatter.string(from: fromDate!) + " 12:00:01 am"
        data.setValue(dateTime, forKey: "fromDate")
        
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeJsonSyncTimeLine( _ json: Data, method: String)
    throws
    {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            let dataArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
    
            let len = dataArray.count
            if (len > 0) {
                for i in 0 ..< len {
                    guard let subJson = dataArray[i] as? NSMutableDictionary else {
                        continue
                    }
                    
                    let datasource = DBOperation()
                    datasource.openDatabase(true)
                    
                    if (method == Constant.Fitness.MethodSyncTLSleep) {
                        
                        let qty = JsonParser.getJsonValueString(subJson, valueForKey: "qty")
                        var date = JsonParser.getJsonValueString(subJson, valueForKey: "date")
                        var sleepTime = JsonParser.getJsonValueString(subJson, valueForKey: "sleepTime")
                        let wakeUpTime = JsonParser.getJsonValueString(subJson, valueForKey: "wakeUpTime")
                        let quality = JsonParser.getJsonValueString(subJson, valueForKey: "quality")
                    
                        sleepTime = sleepTime + "," + wakeUpTime
                    
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                        let dateDouble: Double = ConvertionClass().conDateToLong(dateFormatter.date(from: date)!)
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        date = ConvertionClass().conLongToDate(dateDouble, dateFormat: dateFormatter) + " 00:00:01 am"
                        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                        date = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: date)!))"
                    
                        datasource.InsertSleepTrackList(currentValue: qty, sleepValue: sleepTime, dateTime: date, quality: quality, isServerSync: "1")
                    
                    } else if (method == Constant.Fitness.MethodSyncTLWater) {
                        let quantity = JsonParser.getJsonValueString(subJson, valueForKey: "quantity")
                        var date = JsonParser.getJsonValueString(subJson, valueForKey: "date")
                        let flag = JsonParser.getJsonValueString(subJson, valueForKey: "flag")
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                        date = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: date)!))"
                        
                        datasource.InsertWaterTrackList(currentValue: quantity, dateTime: date, isServerSync: "1", isDelete: flag)
                    
                    } else if (method == Constant.Fitness.MethodSyncTLPedometer) {
                        let steps = JsonParser.getJsonValueString(subJson, valueForKey: "steps")
                        let cal = JsonParser.getJsonValueString(subJson, valueForKey: "cal")
                        let dist = JsonParser.getJsonValueString(subJson, valueForKey: "dist")
                    
                        var date = JsonParser.getJsonValueString(subJson, valueForKey: "date")
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                        let dateDouble: Double = ConvertionClass().conDateToLong(dateFormatter.date(from: date)!)
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        date = ConvertionClass().conLongToDate(dateDouble, dateFormat: dateFormatter)
                    
                        datasource.UpdatePedometerRecordLogList(steps: steps, dateTime: date, isServerSync: "1")
                        datasource.UpdateDistanceRecordLogList(steps: dist, dateTime: date, isServerSync: "1")
                        datasource.UpdateCaloriesRecordLogList(steps: cal, dateTime: date, isServerSync: "1")
                        
                    } else if (method == Constant.Fitness.MethodSyncTLFood) {
                        
                        let foodID = JsonParser.getJsonValueString(subJson, valueForKey: "foodID")
                        let qty = JsonParser.getJsonValueString(subJson, valueForKey: "qty")
                        let containID = JsonParser.getJsonValueString(subJson, valueForKey: "containID")
                        let scheduleName = JsonParser.getJsonValueString(subJson, valueForKey: "schedule")
                        let serverID = JsonParser.getJsonValueString(subJson, valueForKey: "eatID")
                    
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                        
                        var date = JsonParser.getJsonValueString(subJson, valueForKey: "date")
                        date = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: date)!))"
                    
                        datasource.InsertFoodRecordLogList(foodID: foodID, quanti: qty, dateTime: date, containID: containID, isRoutineRec: "1", schedule: scheduleName, isServerSync: "1", serverID: serverID)
                    }
                    
                    datasource.closeDatabase()
                    
                }
            }
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonSendReminder() throws -> Data {
        
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID()
        ]
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let jsonArray = NSMutableArray()
        
        var jsonObj = NSMutableDictionary()
        jsonObj.setValue("water", forKey: "pname")
        jsonObj.setValue(appUserInfo.getWaterRemValue(), forKey: "pvalue")
        jsonObj.setValue(appUserInfo.getIsWaterRemOn() ? "1" : "0", forKey: "isActive")
        jsonArray.add(jsonObj)
        
        jsonObj = NSMutableDictionary()
        jsonObj.setValue("shortWalk", forKey: "pname")
        jsonObj.setValue(appUserInfo.getWalkRemValue(), forKey: "pvalue")
        jsonObj.setValue(appUserInfo.getIsWalkRemOn() ? "1" : "0", forKey: "isActive")
        jsonArray.add(jsonObj)
        
        jsonObj = NSMutableDictionary()
        jsonObj.setValue("sleep", forKey: "pname")
        jsonObj.setValue(appUserInfo.getSleepRemValue(), forKey: "pvalue")
        jsonObj.setValue(appUserInfo.getIsSleepRemOn() ? "1" : "0", forKey: "isActive")
        jsonArray.add(jsonObj)
        
        jsonObj = NSMutableDictionary()
        jsonObj.setValue("food", forKey: "pname")
        jsonObj.setValue(appUserInfo.getFoodRemValue(), forKey: "pvalue")
        jsonObj.setValue(appUserInfo.getIsFoodRemOn() ? "1" : "0", forKey: "isActive")
        jsonArray.add(jsonObj)
        
        jsonObj = NSMutableDictionary()
        jsonObj.setValue("fitness", forKey: "pname")
        jsonObj.setValue(appUserInfo.getFitnessRemValue(), forKey: "pvalue")
        jsonObj.setValue(appUserInfo.getIsFitnessRemOn() ? "1" : "0", forKey: "isActive")
        jsonArray.add(jsonObj)
                
        data.setValue(jsonArray, forKey: "data")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeJsonSendReminder( _ json: Data) throws  {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            appPrefs.setIsPendingReminder(text: false)
        }
    }
    
    func storeJsonSyncReminder( _ json: Data) throws  {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            let jsonArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
    
            let len = jsonArray.count
            for i in 0 ..< len {
                guard let jsonObj = jsonArray[i] as? NSMutableDictionary else {
                    continue
                }
                
                let pname = JsonParser.getJsonValueString(jsonObj, valueForKey: "pname")
                let pvalue = JsonParser.getJsonValueString(jsonObj, valueForKey: "pvalue")
                let isActive = JsonParser.getJsonValueString(jsonObj, valueForKey: "isActive")
                
                if (pvalue == "<null>" || pvalue == "na" || pvalue.isEmpty) {               continue }
                if (pname == "<null>" || pname == "na" || pname.isEmpty) {
                    continue }
                
                if (pname == "water") {
                    appPrefs.setWaterRemValue(text: pvalue)
                    appPrefs.setIsWaterRemOn(text: false)
                } else if (pname == "shortWalk") {
                    appPrefs.setWalkRemValue(text: pvalue)
                    appPrefs.setIsWalkRemOn(text: false)
                } else if (pname == "sleep") {
                    appPrefs.setSleepRemValue(text: pvalue)
                    appPrefs.setIsSleepRemOn(text: false)
                } else if (pname == "food") {
                    appPrefs.setFoodRemValue(text: pvalue)
                    appPrefs.setIsFoodRemOn(text: false)
                } else if (pname == "fitness") {
                    appPrefs.setFitnessRemValue(text: pvalue)
                    appPrefs.setIsFitnessRemOn(text: false)
                } else if (pname == "Nutraceuticals") {
                    appPrefs.setNutraceuticalsRemValue(text: pvalue)
                    appPrefs.setIsNutraceuticalsRemOn(text: false)
                }
            }
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonSyncTimeLineSingle() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID(),
                "date": appUserInfo.getTimelineFromDate(),
                "type": appUserInfo.getTimelineType()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeJsonSyncTimeLine(_ json: Data) throws
    {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
        
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
        
        if (appPrefs.getErrorCode() == 200) {
            let jsonArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
         
            if(jsonArray.count > 0) {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                
                datasource.TruncateTimelineSingleList(date: appPrefs.getTimelineFromDate(), type: appPrefs.getTimelineType())
                
                for i in 0 ..< jsonArray.count {
                    guard let subJson = jsonArray[i] as? NSMutableDictionary else {
                        continue
                    }
                    
                    let title = JsonParser.getJsonValueString(subJson, valueForKey: "title")
                    let jsonVal = JsonParser.getJsonValueString(subJson, valueForKey: "json")
                    let type = JsonParser.getJsonValueString(subJson, valueForKey: "type")
                    var date = JsonParser.getJsonValueString(subJson, valueForKey: "date")
                    
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd kk:mm:ss"
                    date = "\(ConvertionClass().conDateToLong(df.date(from: date)!))"
                    
                    datasource.InsertTimelineSingleList(type: type, title: title, dateTime: date, date: appPrefs.getTimelineFromDate(), message: jsonVal)
                }
                
                datasource.closeDatabase();
            }
        }
    }
    
    
    // create JSON values to send it to server
    func putJsonSyncSummeryTimeLineSingle() throws -> Data {
    
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID(),
                "fromDate": appUserInfo.getTimelineFromDate() + " 12:00:10 AM",
                "toDate": appUserInfo.getTimelineToDate() + " 11:59:50 PM",
                "position": "0"
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    func storeJsonSyncTimeLineSummery(_ json: Data) throws
    {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
        
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
        
        if (appPrefs.getErrorCode() == 200) {
            let jsonData = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
            
            let fromArr = appPrefs.getTimelineFromDate().components(separatedBy: "-")
            let toArr = appPrefs.getTimelineToDate().components(separatedBy: "-")
            
            let fromMonth:Int = Int(fromArr[1])!
            let toMonth:Int = Int(toArr[1])! + 1
            let fromDate:Int = Int(fromArr[2])!
            let toDate:Int = Int(toArr[2])!
            
            let parseDate = fromArr[0]
            
            let datasource = DBOperation()
            datasource.openDatabase(true)
            
            for k in fromMonth ..< toMonth {
                for i in fromDate ..< toDate {
                    let recDate = parseDate + "-" + (k < 10 ? "0\(k)" : "\(k)") + "-" + (i < 10 ? "0\(i)" : "\(i)")
                    
                    guard let subArrayJson = jsonData.value(forKey: recDate) as? NSMutableArray else {
                        continue
                    }
                    
                    datasource.TruncateTimelineSummeryList(date: recDate)
                    datasource.TruncateTimelineSingleList(date: recDate)
                    
                    for j in 0 ..< subArrayJson.count {
                        guard let subJson = subArrayJson[j] as? NSMutableDictionary else
                        {
                            continue
                        }
                        
                        var serverTime = JsonParser.getJsonValueString(subJson, valueForKey: "date") + " 10:30:00"
                        let title = JsonParser.getJsonValueString(subJson, valueForKey: "title")
                        let type = JsonParser.getJsonValueString(subJson, valueForKey: "type")
                        let ctr = JsonParser.getJsonValueString(subJson, valueForKey: "ctr")
                        
                        let df = DateFormatter()
                        df.dateFormat = "yyyy-MM-dd kk:mm:ss"
                        serverTime = "\(ConvertionClass().conDateToLong(df.date(from: serverTime)!))"
                        
                        datasource.InsertTimelineSummeryList(type: type, dateTime: serverTime, date: recDate, counter: ctr, message: title)
                    }
                }
            }
            
            datasource.closeDatabase()
        }
    }
    
    
    
    func storeJsonScheduleList( _ json: Data) throws {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
    
        if (appPrefs.getErrorCode() == 200) {
            let dataJson = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
    
            if (dataJson.count > 0) {
                let datasource = DBOperation()
                datasource.openDatabase(true)
                datasource.TruncateScheduleList()
    
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm a"
                
                for i in 0 ..< dataJson.count {
                    guard let subJson = dataJson[i] as? NSMutableDictionary else {
                        continue
                    }
                    
                    let schedule = JsonParser.getJsonValueString(subJson, valueForKey: "schedule")
                    let id = JsonParser.getJsonValueString(subJson, valueForKey: "id")
                    let type = JsonParser.getJsonValueString(subJson, valueForKey: "type")
                    let routineOf = JsonParser.getJsonValueString(subJson, valueForKey: "routineOf")
                    let imageName = JsonParser.getJsonValueString(subJson, valueForKey: "imageName")
                    let selectType = JsonParser.getJsonValueString(subJson, valueForKey: "selectionType")
                    var time = JsonParser.getJsonValueString(subJson, valueForKey: "time")
                    time =  "\(ConvertionClass().conDateToLong(dateFormatter.date(from: time)!))"
                    
                    datasource.InsertScheduleList(type: type, time: time, scheduleName: schedule, routineOf: routineOf, imageName: imageName, id: id, selectType: selectType)
                }
                datasource.closeDatabase()
            }
        }
    }
    
    // create JSON values to send it to server
//    public JSONObject putJsonSendDataVault() throws {
//    
//    let appUserInfo = MySharedPreferences()
//    JSONObject mJson = new JSONObject()
//    
//    mJson.put("userID", appUserInfo.getUserID())
//    mJson.put("phonebookID", appUserInfo.getPhonebookID())
//    mJson.put("companyID", appUserInfo.getCompanyID())
//    
//    String data[] = appUserInfo.getDocUploadData().split(",")
//    for (int i = 0 i < data.length i++) {
//    if (i == 0) {
//    mJson.put("groupName", data[i])
//    } else if (i == 1) {
//    mJson.put("fileName", data[i])
//    } else if (i == 2) {
//    mJson.put("fileSize", data[i])
//    } else if (i == 3) {
//    mJson.put("createOnDate", data[i])
//    } else if (i == 4) {
//    mJson.put("recordType", data[i])
//    } else if (i == 5) {
//    mJson.put("position", data[i])
//    } else if (i == 6) {
//    mJson.put("modifiDate", data[i])
//    } else if (i == 7) {
//    mJson.put("serverID", data[i])
//    }
//    }
//    
//    mJson.put("docID", String.valueOf(text: ConvertionClass().currentTime()))
//    
//    return mJson
//    }
    
    // create JSON values to send it to server
//    public JSONObject putJsonSendDataVaultDelete() throws {
//    
//    let appUserInfo = MySharedPreferences()
//    JSONObject mJson = new JSONObject()
//    
//    mJson.put("userID", appUserInfo.getUserID())
//    mJson.put("phonebookID", appUserInfo.getPhonebookID())
//    mJson.put("companyID", appUserInfo.getCompanyID())
//    
//    mJson.put("serverID", appUserInfo.getDocUploadData())
//    
//    return mJson
//    }
//    
//    func storeJsonDefaultDelete( _ json: Data) throws  {
//    let appPrefs = MySharedPreferences()
//    
//    appPrefs.setErrorCode(text: 0)
//    appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
//    appPrefs.setErrorMessage(json.getString("message"))
//    
//    if(appPrefs.getErrorCode() == 200) {
//    String serverID = appPrefs.getDocUploadData()
//    
//    let datasource = DBOperation()
//    datasource.openDatabase(true)
//    datasource.DeleteDocFile(serverID)
//    datasource.closeDatabase()
//    }
//    }
    
    
    func storeJsonDefault( _ json: Data) throws  {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
    
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
        appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
    }
    
//    func storeJsonSendDataVault( _ json: Data)
//    throws 
//    {
//    let appPrefs = MySharedPreferences()
//    
//    appPrefs.setErrorCode(text: 0)
//    appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
//    appPrefs.setErrorMessage(json.getString("message"))
//    
//    if(appPrefs.getErrorCode() == 200) {
//    JSONArray jsonArray = json.getJSONArray("data")
//    
//    if(jsonArray.length() > 0) {
//    let datasource = DBOperation()
//    datasource.openDatabase(true)
//    datasource.TruncateDocWalletFile()
//    
//    for (int i = 0 i < jsonArray.length() i++) {
//    _ json: DataSub = jsonArray.getJSONObject(i)
//    
//    String groupName = jsonSub.getString("groupName")
//    String fileName = jsonSub.getString("fileName")
//    String fileSize = jsonSub.getString("fileSize")
//    String createOnDate = jsonSub.getString("createOnDate")
//    String recordType = jsonSub.getString("recordType")
//    String position = jsonSub.getString("position")
//    String modifiDate = jsonSub.getString("modifiDate")
//    String serverID = jsonSub.getString("serverID")
//    
//    modifiDate = DateTimeCasting.getDateLongFrm(modifiDate)
//    createOnDate = DateTimeCasting.getDateLongFrm(createOnDate)
//    
//    datasource.InsertDocWalletFile(fileName, fileSize, modifiDate, createOnDate,
//    recordType, groupName, serverID, position)
//    }
//    
//    datasource.closeDatabase()
//    }
//    }
//    }
    
    
    func storeJsonSyncTargetVal( _ json: Data) throws  {
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let appPrefs = MySharedPreferences()
        
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
        
        if (appPrefs.getErrorCode() == 200) {
            let jsonObject = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
            
            var calEat = JsonParser.getJsonValueString(jsonObject, valueForKey: "cal_eat")
            var calOut = JsonParser.getJsonValueString(jsonObject, valueForKey: "cal_out")
            var sleep = JsonParser.getJsonValueString(jsonObject, valueForKey: "sleep")
            var water = JsonParser.getJsonValueString(jsonObject, valueForKey: "water")
            var padometer = JsonParser.getJsonValueString(jsonObject, valueForKey: "padometer")
            var distance = JsonParser.getJsonValueString(jsonObject, valueForKey: "distance")
            
            if calEat.uppercased() == "NULL" || calEat.uppercased() == "NA" || calEat.isEmpty
            {
                calEat = "2500"
            }
            if calOut.uppercased() == "NULL" || calOut.uppercased() == "NA" || calOut.isEmpty
            {
                calOut = "2500"
            }
            if sleep.uppercased() == "NULL" || sleep.uppercased() == "NA" || sleep.isEmpty
            {
                sleep = "8"
            }
            if water.uppercased() == "NULL" || water.uppercased() == "NA" || water.isEmpty
            {
                water = "5000"
            }
            if padometer.uppercased() == "NULL" || padometer.uppercased() == "NA" || padometer.isEmpty
            {
                water = "10000"
            }
            if distance.uppercased() == "NULL" || distance.uppercased() == "NA" || distance.isEmpty
            {
                distance = "8"
            }
            
            appPrefs.setDailySleep(text: sleep)
            appPrefs.setDailyCalBurn(text: calOut)
            appPrefs.setDailyCalTake(text: calEat)
            appPrefs.setDailySteps(text: padometer)
            appPrefs.setDailyWater(text: water)
            appPrefs.setDailyDistance(text: distance)
        }
    }
    
    // create JSON values to send it to server
    func putJsonDeleteFoodRec() throws -> Data {
        
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        data = ["osType":"iOS",
                "userID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "timeZone": appUserInfo.getTimeZoneID(),
                "GMT": appUserInfo.getTimeZone(),
                "companyID": appUserInfo.getCompanyID(),
                "serverID": appUserInfo.getTempRecordID()
        ]
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
}
