//
//  ActivityDonePopupVC.swift
//
//  Copyright © 2017. All rights reserved.
//

import UIKit
import SnapKit
import Async

class ActivityDonePopupVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    
    @IBOutlet weak var btnOk: UIButton!
    
    @IBOutlet weak var viewParent: UIView!
    @IBOutlet weak var mainCollectionView: UITableView!
    
    var bundleJson: NSMutableDictionary! = nil
    
    fileprivate var heading = "", recID = ""
    fileprivate var mainType = "", logTime = ""
    fileprivate var isChoice:Bool = false, isOptSelVisible:Bool = true
    
    fileprivate var arrMainList = [ArrActivity]()
    let CustomActivityIdentifier = "CustomActivityCell"
    
    let thread1 = DispatchQueue(label: "ActivityDonePopupVC.Thread1", attributes: DispatchQueue.Attributes.concurrent)
    
    var classRefs: AnyObject! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = CustomColor.black_semi_transparent()
        
        viewParent.layer.cornerRadius = 5
        viewParent.layer.masksToBounds = true        
                
        recID = bundleJson.value(forKey: "recID") as! String
        heading = bundleJson.value(forKey: "heading") as! String
        
        lblHeading.text = heading
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        mainCollectionView.estimatedRowHeight = 44
        mainCollectionView.rowHeight = UITableViewAutomaticDimension
                        
        loadList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showAlert() {
        if let appDelegate = UIApplication.shared.delegate, let window = appDelegate.window, let rootViewController = window?.rootViewController {
            
            var topViewController = rootViewController
            while topViewController.presentedViewController != nil {
                topViewController = topViewController.presentedViewController!
            }
            
            // Add the alert view controller to the top most UIViewController of the application
            topViewController.addChildViewController(self)
            topViewController.view.addSubview(view)
            viewWillAppear(true)
            didMove(toParentViewController: topViewController)
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.frame = topViewController.view.bounds
        }
    }
    
    
    @IBAction func onClickOkButton(_ sender: UIButton) {
        closePopup()
    }
    
    func closePopup() {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        bundleJson = nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var settingCell: CustomActivityCell! = mainCollectionView.dequeueReusableCell(withIdentifier: CustomActivityIdentifier) as? CustomActivityCell
        
        if settingCell == nil {
            mainCollectionView.register(UINib(nibName: CustomActivityIdentifier, bundle: nil), forCellReuseIdentifier: CustomActivityIdentifier)
            settingCell = tableView.dequeueReusableCell(withIdentifier: CustomActivityIdentifier) as? CustomActivityCell
        }
        
        settingCell!.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        return settingCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        performClickOpr(indexPath.row, tableCell: tableView.cellForRow(at: indexPath)!)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 72
    }
}

extension ActivityDonePopupVC {
    
    fileprivate func loadList() {

        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        
        let currDaysVal = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df).uppercased())"
        
        df.dateFormat = "dd MMM, yyyy"
        let currDaysValStr = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df).uppercased())"
        
        lblHeading.text = heading + " - " + currDaysValStr
    
        //getting schedule list and time
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        var sql = "SELECT * FROM " + datasource.ScheduleList_tlb + " WHERE " +  datasource.dbName + " = '" + heading + "' "
        var cursor = datasource.selectRecords(sql)
        if cursor != nil {
            if cursor!.next() {
                mainType = cursor!.string(forColumnIndex: 0)
                isChoice = cursor!.string(forColumnIndex: 6) == "Single"
                isOptSelVisible = cursor!.string(forColumnIndex: 6) != "none"
                
                df.dateFormat = "hh:mm:ss a"
                logTime = "\(ConvertionClass().conLongToDate(cursor!.double(forColumnIndex: 1), dateFormat: df).uppercased())"
                
                df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
                logTime = "\(ConvertionClass().conDateToLong(df.date(from: currDaysVal + " " + logTime)!))"
            }
            cursor!.close()
        }
        
        
        //load contain list
        arrMainList = [ArrActivity]()
        
        if(mainType.uppercased() == "FOOD") {
            sql = "SELECT " + datasource.dbOptionID + ", " + datasource.dbOptionName
                + ", " + datasource.dbIsChecked
                + ", (Select group_concat(" + datasource.dbActID + ", ';') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID+") as optionVal "
                + ", (Select group_concat("+datasource.dbQuantity + ", ';') "
                + " FROM "+datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS."+datasource.dbOptionID + ") as quaVal "
                + ", (Select group_concat(" + datasource.dbContainID + ", ';') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as containVal "
                + ", rD." + datasource.dbRecDesc + ", rDS." + datasource.dbVideoLink
                + ", rDS." + datasource.dbDays
                + " FROM " + datasource.recDashboardList_tlb + " rD, " + datasource.recDashboardSubList_tlb + " rDS "
                + " WHERE rD." + datasource.recID + " = rDS." + datasource.recID
                + " AND UPPER(" + datasource.dbRecType + ") = '" + heading.uppercased() + "' "
                + " AND UPPER(" + datasource.dbDays + ") like '%" + currDaysVal + "%' "
                + " GROUP BY " + datasource.dbOptionID
                + " ORDER BY " + datasource.dbRecName
        } else {
            sql = "SELECT " + datasource.dbOptionID + ", " + datasource.dbOptionName
                + ", " + datasource.dbIsChecked
                + ", (Select group_concat(" + datasource.dbActID + ", ';') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as optionVal "
                + ", (Select group_concat(" + datasource.dbQuantity + ", ';') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as quaVal "
                + ", rD." + datasource.dbRecDesc
                + ", (Select group_concat(" + datasource.dbVideoLink + ", ';') "
                + " FROM " + datasource.recDashboardSubList_tlb + " rDSL "
                + " WHERE " + datasource.dbOptionID+" = rDS." + datasource.dbOptionID + ") as video "
                + ", rDS." + datasource.dbDays
                + " FROM " + datasource.recDashboardList_tlb + " rD, " + datasource.recDashboardSubList_tlb + " rDS "
                + " WHERE rD." + datasource.recID + " = rDS." +  datasource.recID
                + " AND UPPER(" + datasource.dbRecType + ") = '" + heading.uppercased() + "' "
                + " AND UPPER(" + datasource.dbDays + ") like '%" + currDaysVal + "%' "
                + " GROUP BY " + datasource.dbOptionID
                + " ORDER BY " + datasource.dbRecName
        }
        
        var details = ""
        cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let temp = ArrActivity()
                temp.recID = cursor!.string(forColumnIndex: 0)
                temp.recName = cursor!.string(forColumnIndex: 1)
                temp.isChecked = cursor!.string(forColumnIndex: 2) == "1"
                temp.recOriID = recID
                temp.type = heading
                temp.mainType = mainType
                temp.logTime = logTime
                
                var tempVal = ""
                tempVal = cursor!.string(forColumnIndex: 3)
                let actName = tempVal.components(separatedBy: ";")
                
                tempVal = ""
                tempVal = cursor!.string(forColumnIndex: 4)
                let quantity = tempVal.components(separatedBy: ";")
                
                var val = "", videoLink = "", days = ""
                if(mainType.uppercased() == "FOOD") {
                    let contain = cursor!.string(forColumnIndex: 5).components(separatedBy: ";")
                    details = cursor!.string(forColumnIndex: 6)
                    videoLink = cursor!.string(forColumnIndex: 7)
                    days = cursor!.string(forColumnIndex: 8)
                    
                    for i in 0 ..< actName.count {
                        var get = ""
                        if i < actName.count {
                            get = actName[i]
                            
                        }
                        if i < quantity.count {
                            get = actName[i] + " - " + quantity[i]
                        }
                        
                        if i < contain.count {
                            get = actName[i] + " - " + quantity[i] + " - " + contain[i]
                        }
                        
                        val += val.isEmpty ? get : "<br> " + get
                    }
                } else {
                    details = cursor!.string(forColumnIndex: 5)
                    videoLink = cursor!.string(forColumnIndex: 6)
                    days = cursor!.string(forColumnIndex: 7)
                    
                    for i in 0 ..< actName.count {
                        var get = ""
                        if i < actName.count {
                            get = actName[i] + " min"
                        }
                        if i < quantity.count {
                            get = actName[i] + " - " + quantity[i] + " min"
                        }
                        val += val.isEmpty ? get : "<br> " + get
                    }
                }
                temp.recSubName = val
                temp.days = days
                temp.videoLink = videoLink
                
                if (temp.isChecked) { temp.isSubmited = true }
                temp.isChoice = isChoice
                temp.isSelOptVisible = isOptSelVisible
                
                arrMainList.append(temp)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        if(arrMainList.count > 0) {
            lblNote.attributedText = ("Note : " + details).html2AttributedString
            
            mainCollectionView.reloadData()
        } else {
            lblNote.attributedText = "Today's schedule record not available. <br>Goto My Diary option and check your complete schedule plan.".html2AttributedString
        }
    }    
    
    fileprivate func performClickOpr(_ index: Int, tableCell: UITableViewCell) {
        let user = arrMainList[index]
        
        user.isChecked = !user.isChecked;
        
        let settingCell: CustomActivityCell! = tableCell as! CustomActivityCell
        if(user.isChecked) {
            if(user.isChoice) {
                settingCell.imgOption.image = UIImage(named: "radio_on.png")
            } else {
                settingCell.imgOption.image = UIImage(named: "check_select.png")
            }
        } else {
            if(user.isChoice) {
                settingCell.imgOption.image = UIImage(named: "radio_off.png")
            } else {
                settingCell.imgOption.image = UIImage(named: "check_unselect.png")
            }
        }
        
        let datasource = DBOperation()
        datasource.openDatabase(true)
        let value = user.isChecked ? "1" : "0"
        let result = datasource.UpdateRecentDashboardSubList(isChecked: value, recIDStr: user.recOriID, actID: user.recID, isChoice: user.isChoice)
        
        //decide proper schedule as per server demands
        let type = user.type
        
        //if modified, then add into act Table
        if(result > 0) {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd"
            let dateTime = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df).uppercased())"
            
            df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            let currentDayDate = "\(ConvertionClass().conDateToLong(df.date(from: dateTime + " 00:00:01 am")!))"
            
            var optionID = user.recID
            if(!user.isChoice) {
                //check for all selected
                optionID = ""
                
                for i in 0 ..< arrMainList.count {
                    let userTemp = arrMainList[i]
                    if(userTemp.isChecked) {
                        optionID += optionID.isEmpty ? userTemp.recID : "," + userTemp.recID
                    }
                }
            }
            
            datasource.InsertRoutingList(optionID: optionID, dateTime: currentDayDate, schedule: type)
        }
        
        if(user.mainType.uppercased() == "FOOD") {
            //maintain record in food log
            let totalFood = user.recSubName.components(separatedBy: "<br>")
            for i in 0 ..< totalFood.count {
                let tempFoodDetails = totalFood[i].trimmed.components(separatedBy: "-")
                let foodName = tempFoodDetails[0].trimmed
                let tempDetails = tempFoodDetails[1].trimmed.components(separatedBy: " ")
                let qut = tempDetails[0].trimmed
                var container = ""
                for j in 1 ..< tempDetails.count {
                    container += container.isEmpty ? tempDetails[j] : " " +   tempDetails[j];
                }
                
                datasource.InsertFoodRecordLogRoutineList(foodName: foodName, quanti: qut, dateTime: user.logTime, contain: container.trimmed, schedule: type)
            }
        }
        datasource.closeDatabase()
        
        //fire Broadcaster, to send to user
        var dict = ["resultCode":1,
                    "udpateType":"sendPendingData"
            ] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
       

        if(user.mainType.uppercased() == "FOOD") {
            
            dict = ["resultCode":1,
                    "udpateType":"foodRef"
                ] as [String : Any]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
            
            Async.custom(queue: thread1, after: 0.01, {
                dict = ["resultCode":1,
                        "udpateType":"DietRef"
                    ] as [String : Any]
                NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
            }).main({
                dict = ["resultCode":1,
                        "udpateType":"ActivityRef"
                    ] as [String : Any]
                NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
            })
            
        } else {
            dict = ["resultCode":1,
                    "udpateType":"ActivityRef"
                ] as [String : Any]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
        }
        
        if(user.isChoice) {
            refreshList(position: index)
        }
    }
    
    fileprivate func refreshList(position: Int) {
    
        for i in 0 ..< arrMainList.count {
            if(i == position) {
                arrMainList[i].isChecked = true
            } else {
                arrMainList[i].isChecked = false
            }
        }
    
        mainCollectionView.reloadData()
    }
    
    func moveToVideoController(_ optionID: String, mainType: String) {
        closePopup()
        
        (classRefs as! ActivityTabVC).moveToVideoController(optionID, mainType: mainType)
    }
    
}


