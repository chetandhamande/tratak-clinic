//
//  DietTabVC.swift
//  Salk
//
//  Created by Chetan  on 16/06/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Async

class DietTabVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    fileprivate var arrMainList = [ArrRecentAct]()
    let CustomAdapterRecentIdentifier = "CustomAdapterRecentCell"
    var offscreenCells = Dictionary<String, UICollectionViewCell>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        let nib = UINib(nibName: CustomAdapterRecentIdentifier, bundle: nil)
        mainCollectionView.register(nib, forCellWithReuseIdentifier: CustomAdapterRecentIdentifier)
       
        viewConfiguration()
        self.onCreateView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewConfiguration() {
        self.mainCollectionView.backgroundColor = CustomColor.background()
        self.view.backgroundColor = UIColor.white
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CustomAdapterRecentCell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomAdapterRecentIdentifier, for: indexPath) as! CustomAdapterRecentCell
        
        cell.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        // Make sure layout subviews
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //open vehical details list
        
        self.moveToDetailController(indexPath.row)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let orientation = UIApplication.shared.statusBarOrientation
        var cellWidth = mainCollectionView.frame.width
        
        // Use fake cell to calculate height
        let reuseIdentifier = CustomAdapterRecentIdentifier
        var cell: CustomAdapterRecentCell? = self.offscreenCells[reuseIdentifier] as? CustomAdapterRecentCell
        if cell == nil {
            cell = Bundle.main.loadNibNamed(CustomAdapterRecentIdentifier, owner: self, options: nil)![0] as? CustomAdapterRecentCell
            self.offscreenCells[reuseIdentifier] = cell
        }
        
        cell!.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        if traitCollection.horizontalSizeClass == .regular || traitCollection.verticalSizeClass == .regular
        {
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                if(orientation == .portrait) {
                    cellWidth = (cellWidth - 1) / 2
                } else {
                    cellWidth = (cellWidth - 5) / 3
                }
            } else {
                if(orientation == .portrait) {
                } else {
                    cellWidth = (cellWidth - 1) / 2
                }
            }
        } else {
            if(orientation == .portrait) {
            } else {
                cellWidth = (cellWidth - 1) / 2
            }
        }
        
        cell!.bounds = CGRect(x: 0, y: 0, width: cellWidth, height: cell!.bounds.height)
        cell!.contentView.bounds = cell!.bounds
        
        // Layout subviews, this will let labels on this cell to set preferredMaxLayoutWidth
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        
        var size = cell!.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        // Still need to force the width, since width can be smalled due to break mode of labels
        size.width = cellWidth
        return size
    }
    
    func moveToDetailController(_ index: Int) {
        let user = arrMainList[index]
        
        if (user.layoutType == 1) {
            //get is Record available in db
            let datasource = DBOperation()
            datasource.openDatabase(false)
            
            let sql = "SELECT " + datasource.recID + " FROM " + datasource.recDashboardList_tlb + " rD " + " WHERE " + datasource.dbRecType +  " = '" + user.heading + "' "
            
            var recAvail:Bool = false
            let cursor = datasource.selectRecords(sql)
            if cursor != nil {
                if cursor!.next() {
                    recAvail = true
                }
                cursor!.close()
            }
            datasource.closeDatabase();
            
            if(recAvail) {
                let userJson = NSMutableDictionary()
                userJson.setValue(user.heading, forKey: "heading")
                userJson.setValue(user.recID, forKey: "recID")
                
                let desktopPopUp = ActivityDonePopupVC(nibName: "ActivityDonePopupVC", bundle: nil)
                desktopPopUp.bundleJson = userJson
                desktopPopUp.showAlert()
            } else {
                SweetAlert().showOnlyAlert("", subTitle: "Our experts are preparing your schedule. We will notify you soon!", style: AlertStyle.none, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary(), action: nil)
            }
        }
    }
}

extension DietTabVC {

    fileprivate func onCreateView() {
        do {
            try self.onLoad()
        } catch _ {}
        
//        let appPref = MySharedPreferences()
//        if(appPref.getIsPremiumPlanActive()) {
//            //rootView.findViewById(R.id.relPremium).setVisibility(View.GONE);
//        } else {
//            //rootView.findViewById(R.id.relPremium).setVisibility(View.VISIBLE);
//        }
    }
    
    func onLoad() throws {
        
        arrMainList = [ArrRecentAct]()
        
        let df = DateFormatter()
        let temp = ArrRecentAct()
        temp.layoutType = 0
        temp.heading = "Today"
        df.dateFormat = "dd"
        temp.details = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df)
        df.dateFormat = "MMM"
        temp.time = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df)
        arrMainList.append(temp)
        
        //find list of diet
        var index = 0
        df.dateFormat = "hh:mm a"
        let currTimeLong = ConvertionClass().conDateToLong(df.date(from: ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df))!)
        
        let datasource = DBOperation()
        datasource.openDatabase(true)
        
        var sql = "SELECT * FROM " + datasource.ScheduleList_tlb + " ORDER BY CAST(" + datasource.dbTime + " as INTEGER) "
        
        var cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let cdd = ArrRecentAct()
                cdd.layoutType = 1
                df.dateFormat = "hh:mm a"
                cdd.time = ConvertionClass().conLongToDate(Double(cursor!.string(forColumnIndex: 1))!, dateFormat: df)
                cdd.heading = cursor!.string(forColumnIndex: 2)
                cdd.imgResouceID = FoodScheduleType.getScheduleDefaultImage(type: cdd.heading)
                cdd.imgBackgourID = "profile_back"
                cdd.recID = "0"
                arrMainList.append(cdd)
                if cursor!.double(forColumnIndex: 1) < currTimeLong {
                    index += 1
                }
            }
            cursor!.close()
        }
        
        
        sql = "SELECT " + datasource.recID + ", " + datasource.dbRecName + ", " +  datasource.dbRecType + ", " + datasource.dbRecDesc + " FROM " + datasource.recDashboardList_tlb + " rD "
        
        cursor = datasource.selectRecords(sql)
        var totalRecAvail = 0
        if cursor != nil {
            while cursor!.next() {
                for i in 0 ..< arrMainList.count {
                    if(arrMainList[i].heading == cursor!.string(forColumnIndex: 2)) {
                        arrMainList[i].recID = cursor!.string(forColumnIndex: 0)
                        arrMainList[i].recAvail = true
                        totalRecAvail += 1
                        break
                    }
                }
            }
            cursor!.close()
        }
        
        //if record available of some schedule, then show only them
        if(totalRecAvail > 0) {
            var i = 0
            while i < arrMainList.count {
                if (!arrMainList[i].recAvail && arrMainList[i].layoutType == 1) {
                    arrMainList.remove(at: i)
                    i -= 1
                }
                i += 1
            }
        }
        
        //reset selected text box on next date
        let appPref = MySharedPreferences()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let currDate = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter)
        if(appPref.getLastRecentRecDate() != currDate) {
            appPref.setLastRecentRecDate(text: currDate)
            
            datasource.UpdateRecentDashboardSubList(isChecked: "0")
        }
        
        //check for current data filled for not
        sql = "SELECT " + datasource.dbRecType + ", " + datasource.dbIsChecked
            + ", (Select group_concat(" + datasource.dbActID + ", ';') "
            + " FROM " + datasource.recDashboardSubList_tlb + " rDSL WHERE " + datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as optionVal , (Select group_concat(" + datasource.dbQuantity + ", ';') " + " FROM " + datasource.recDashboardSubList_tlb + " rDSL " + " WHERE "+datasource.dbOptionID + " = rDS." + datasource.dbOptionID + ") as quaVal, (Select group_concat("+datasource.dbContainID + ", ';') FROM " + datasource.recDashboardSubList_tlb + " rDSL WHERE " + datasource.dbOptionID+" = rDS." + datasource.dbOptionID + ") as containVal, " + datasource.dbOptionName + " FROM " + datasource.recDashboardList_tlb + " rD, " + datasource.recDashboardSubList_tlb + " rDS WHERE rD." + datasource.recID + " = rDS." + datasource.recID + " AND UPPER(" + datasource.dbDays + ") like '%" + currDate + "%'  AND " + datasource.dbIsChecked + " = '1' GROUP BY " + datasource.dbOptionID + " ORDER BY " + datasource.dbRecName
        
        cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                for i in 0 ..< arrMainList.count {
                    if(arrMainList[i].heading == cursor!.string(forColumnIndex: 0)) {
                        arrMainList[i].isTodaysDone = cursor!.string(forColumnIndex: 1) == "1"
                        
                        var tempVal = cursor!.string(forColumnIndex: 2)!
                        let actName = tempVal.components(separatedBy: ";")
                        
                        tempVal = cursor!.string(forColumnIndex: 3)!
                        let quantity = tempVal.components(separatedBy: ";")
                        
                        var val = ""
                        let contain = cursor!.string(forColumnIndex: 4).components(separatedBy: ";")
                        
                        if(arrMainList[i].heading == "Workout") {
                            for j in 0 ..< actName.count {
                                var temp = ""
                                if j < actName.count {
                                    temp = actName[j]
                                }
                                if j < quantity.count {
                                    temp = actName[j] + " - " + quantity[j] + " min"
                                }
                                
                                val += val.isEmpty ? temp : ", " + temp
                            }
                            
                        } else {
                            for j in 0 ..< actName.count {
                                var temp = ""
                                if j < actName.count {
                                    temp = actName[j]
                                }
                                if j < quantity.count {
                                    temp = actName[j] + " - " + quantity[j]
                                }
                                if j < contain.count {
                                    temp = actName[j] + " - " + quantity[j] + " " + contain[j]
                                }
                                
                                val += val.isEmpty ? temp : ", " + temp
                            }
                        }
                        
                        val = "<b>" + cursor!.string(forColumnIndex: 5) + "</b> : " +  val
                        arrMainList[i].details += arrMainList[i].details.isEmpty ? val : ", " + val
                        break
                    }
                }
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        if index == 0 {
            index = 1
        }
        if arrMainList.count >= index {
            arrMainList[index].layoutType = 2
        }
        
        mainCollectionView.reloadData()
        mainCollectionView.isHidden = false
        
        //Scroll routing to current time
        if index > 0 {
            index -= 1
        }
    }
    
}

