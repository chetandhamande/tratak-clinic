//
//  DashboardTabVC.swift
//  Salk
//
//  Created by Chetan  on 16/06/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import SnapKit
import MaterialControls
import Material
import Async

class DashboardTabVC: UIViewController {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var customWater: CustomTrackerFeaturesView!
    @IBOutlet weak var customFood: CustomTrackerFeaturesView!
    @IBOutlet weak var customSleep: CustomTrackerFeaturesView!
    @IBOutlet weak var customCalBurn: CustomTrackerFeaturesView!
    @IBOutlet weak var customPadometer: CustomTrackerFeaturesView!
    @IBOutlet weak var customRunKm: CustomTrackerFeaturesView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Async.main(after: 0.1, {
            do {
                try self.loadWaterInformation()
                try self.loadSleepInformation()
                try self.loadFoodInformation()
                try self.showFitnessBandData()
            } catch {}
            
            self.manageFitnessBand()
            self.checkNewRoutineServer()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func viewConfiguration() {
        
        self.view.backgroundColor = UIColor.white
        self.mainScrollView.backgroundColor = UIColor.white
        
        let appPref = MySharedPreferences()
        
        customPadometer.lblCounter.text = "0"
        customPadometer.lblTitle.text = "Steps"
        customPadometer.lblMaxLimit.text = appPref.getDailySteps()
        customPadometer.imgIcon.image = UIImage(named: "padometer.png")
        customPadometer.prBar.progress = 0
        let stepsTapGesture = UITapGestureRecognizer(target: self, action: #selector(showStepsTimeline))
        customPadometer.mainView.addGestureRecognizer(stepsTapGesture)
        customPadometer.btnAdd.addTarget(self, action: #selector(showAppsAndDevices), for: UIControlEvents.touchUpInside)
        
        customCalBurn.lblCounter.text = "0"
        customCalBurn.lblTitle.text = "Calories out"
        customCalBurn.lblMaxLimit.text = appPref.getDailyCalBurn()
        customCalBurn.imgIcon.image = UIImage(named: "calories_color.png")
        customCalBurn.btnAdd.isHidden = true
        customCalBurn.prBar.progress = 0
        let calTapGesture = UITapGestureRecognizer(target: self, action: #selector(showCaloriesTimeline))
        customCalBurn.mainView.addGestureRecognizer(calTapGesture)
        
        customRunKm.lblCounter.text = "0"
        customRunKm.lblTitle.text = "Km"
        customRunKm.lblMaxLimit.text = appPref.getDailyDistance()
        customRunKm.imgIcon.image = UIImage(named: "marker.png")
        customRunKm.btnAdd.isHidden = true
        customRunKm.prBar.progress = 0
        let runTapGesture = UITapGestureRecognizer(target: self, action: #selector(showDistanceTimeline))
        customRunKm.mainView.addGestureRecognizer(runTapGesture)
        
        customFood.lblCounter.text = "0"
        customFood.lblTitle.text = "Calories in"
        customFood.lblMaxLimit.text = appPref.getDailyCalTake()
        customFood.imgIcon.image = UIImage(named: "food_color.png")
        customFood.btnAdd.isHidden = true
        customFood.prBar.progress = 0
        let foodTapGesture = UITapGestureRecognizer(target: self, action: #selector(showFoodTimeline))
        customFood.mainView.addGestureRecognizer(foodTapGesture)
        
        customWater.lblCounter.text = "0"
        customWater.lblTitle.text = "ml"
        customWater.lblMaxLimit.text = appPref.getDailyWater()
        customWater.imgIcon.image = UIImage(named: "water_color.png")
        customWater.btnAdd.isHidden = true
        customWater.prBar.progress = 0
        let waterTapGesture = UITapGestureRecognizer(target: self, action: #selector(showWaterTimeline))
        customWater.mainView.addGestureRecognizer(waterTapGesture)
        
        customSleep.lblCounter.text = "0"
        customSleep.lblTitle.text = "hour"
        customSleep.lblMaxLimit.text = appPref.getDailySleep()
        customSleep.imgIcon.image = UIImage(named: "sleep_color.png")
        customSleep.btnAdd.isHidden = true
        customSleep.prBar.progress = 0
        let sleepTapGesture = UITapGestureRecognizer(target: self, action: #selector(showSleepTimeline))
        customSleep.mainView.addGestureRecognizer(sleepTapGesture)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let orientation = UIApplication.shared.statusBarOrientation
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
                
                
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.topMargin.equalTo(0)
                    make.leadingMargin.equalTo(0)
                    make.trailingMargin.equalTo(0)
                    make.bottomMargin.equalTo(0)
                    make.width.equalTo(mainStackView)
                })
            }
        }
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
                
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.width.equalTo(mainStackView)
                })
            }
        }
    }
}

extension DashboardTabVC {
    
    fileprivate func checkNewRoutineServer() {
        let internet = CheckInternetConnection().isCheckInternetConnection()
        if(internet) {
            let appPref = MySharedPreferences()
            var time:Double = ConvertionClass().currentTime() - appPref.getLastMyDiaryDataSycnTime()
            time = (time / (60)) //in minute
    
            if(time > 60) {
                appPref.setTempRecordID(text: "diet")
                
                FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncMyDiary, classRef: self)
            }
        }
    }
    
    func loadWaterInformation() throws {
        var currentValue: Int = 0
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateTime = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter)
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        let fromTime = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime + " 12:00:01 am")!))"
        let toTime = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime + " 11:59:59 pm")!))"
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
    
        let sql = "SELECT * FROM " + datasource.WaterTrackList_tlb + " WHERE CAST(" + datasource.dbDateTime + " as INTEGER) >= " + fromTime + " AND CAST(" + datasource.dbDateTime + " as INTEGER) <= " + toTime
        
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                if Int(cursor!.int(forColumnIndex: 3)) == 0 {
                    currentValue += Int(cursor!.int(forColumnIndex: 0))
                } else {
                    currentValue -= Int(cursor!.int(forColumnIndex: 0))
                }
            }
            cursor!.close()
        }
        datasource.closeDatabase()
    
        let appPref = MySharedPreferences()
        let maxVal:Int = Int(appPref.getDailyWater())!
        let val:Float = Float(currentValue) / Float(maxVal)
        customWater.prBar.setProgress(val, animated: false)
    
        if(currentValue > 0) {
            customWater.lblCounter.text = "\(currentValue)"
        } else {
            customWater.lblCounter.text = "0"
        }
    }
    
    func loadSleepInformation() throws {
        var currentValue: Int = 0
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateTime = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter)
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        let currentDayDate = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime + " 00:00:01 am")!))"
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
    
        let sql = "SELECT * FROM " + datasource.SleepTrackList_tlb + " WHERE " + datasource.dbDateTime + " = '" + currentDayDate + "' "
        
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            if cursor!.next() {
                currentValue = Int(cursor!.int(forColumnIndex: 0))
            } else {
                currentValue = 0
            }
            cursor!.close()
        }
        datasource.closeDatabase()
    
        let appPref = MySharedPreferences()
        let maxVal:Int = Int(appPref.getDailySleep())! * 60
        let val:Float = Float(currentValue) / Float(maxVal)
        customSleep.prBar.setProgress(val, animated: false)
        
        if(currentValue > 0) {
            customSleep.lblCounter.text = ConvertionClass().getDateParsingFrmMinute(currentValue, isFullString: false)
        }
        
        if customSleep.lblCounter.text != "0" {
            customSleep.lblTitle.text = ""
        }
        customSleep.lblCounter.sizeToFit()
    }
    
    func loadFoodInformation() throws {
    
        var currentValue: Double = 0
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateTime = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter)
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        let fromTime = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime + " 12:00:10 am")!))"
        let toTime = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime + " 11:59:50 pm")!))"
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
    
        let sql = "SELECT " + datasource.dbCalories + ", " + datasource.dbQuantity     + " from " + datasource.FoodRecordList_tlb + " as F, " + datasource.FoodRecordLogList_tlb + " as FL " + " WHERE F." + datasource.dbFoodID + " = FL." + datasource.dbFoodID + " AND CAST(" + datasource.dbDateTime + " as INTEGER) >= " + fromTime + " AND CAST(" +  datasource.dbDateTime + " as INTEGER) <= " + toTime
        
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                currentValue += Double(cursor!.string(forColumnIndex: 0))! * Double(cursor!.string(forColumnIndex: 1))!
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        let appPref = MySharedPreferences()
        let maxVal:Int = Int(appPref.getDailyCalTake())!
        let val:Float = Float(currentValue) / Float(maxVal)
        customFood.prBar.setProgress(val, animated: false)
        
        if(currentValue > 0) {
            customFood.lblCounter.text = String(format: "%.1f", currentValue)
        }
    }
    
    
    fileprivate func manageFitnessBand() {
        gettingFitnessPartyData(isInitial: true)
    }
    
    fileprivate func gettingFitnessPartyData(isInitial: Bool) {
    
        let appPref = MySharedPreferences()
        if(appPref.getIsFitnessBandConnect()) {
            if (appPref.getFitnessBandName() == "Health") {
                ReadAppleFitData().readFitnessData()
            }            
        } else if(!isInitial) {
            let a = FitnessBandConnectVC(nibName: "FitnessBandConnectVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        }
    }
    
    func showFitnessBandData() throws {
    
        let appPref = MySharedPreferences()
        if(appPref.getIsFitnessBandConnect()) {
            customPadometer.btnAdd.setImage(UIImage().imageWithImage(UIImage(named: "ic_refresh_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30)), for: .normal)
        } else {
            customPadometer.btnAdd.setImage(UIImage(named: "ic_add_48.png"), for: .normal)
        }
        
        if(appPref.getIsFitnessBandConnect()) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateTimeToday = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter)
            
            let datasource = DBOperation()
            datasource.openDatabase(false)
    
            //Steps
            var sql = "SELECT " + datasource.dbSteps + " FROM " + datasource.PedometerRecordLogList_tlb + " WHERE " + datasource.dbDateTime + " = '" + dateTimeToday + "' "
    
            var pgbValue:Int = 0
            var cursor = datasource.selectRecords(sql)
            if cursor != nil {
                if cursor!.next() {
                    pgbValue = Int(cursor!.int(forColumnIndex: 0))
                }
                cursor!.close()
            }
            
            if(pgbValue > 0) {
                customPadometer.lblCounter.text = "\(pgbValue)"
                
                let maxVal:Int = Int(appPref.getDailySteps())!
                let val:Float = Float(pgbValue) / Float(maxVal)
                customPadometer.prBar.setProgress(val, animated: false)
            }
    
            //calories
            sql = "SELECT " + datasource.dbCalories + " FROM " + datasource.CaloriesRecordLogList_tlb + " WHERE " + datasource.dbDateTime + " = '" + dateTimeToday + "' "
    
            pgbValue = 0;
            cursor = datasource.selectRecords(sql)
            if cursor != nil {
                if cursor!.next() {
                    pgbValue = Int(cursor!.int(forColumnIndex: 0))
                }
                cursor!.close()
            }
            
            if(pgbValue > 0) {
                customCalBurn.lblCounter.text = "\(pgbValue)"
                
                let maxVal:Int = Int(appPref.getDailyCalBurn())!
                let val:Float = Float(pgbValue) / Float(maxVal)
                customCalBurn.prBar.setProgress(val, animated: false)
            }
    
            //distance
            sql = "SELECT " + datasource.dbDistance + " FROM " + datasource.DistanceRecordLogList_tlb + " WHERE " + datasource.dbDateTime + " = '" + dateTimeToday + "' "
    
            var pgbValueDouble:Double = 0;
            cursor = datasource.selectRecords(sql)
            if cursor != nil {
                if cursor!.next() {
                    pgbValueDouble = cursor!.double(forColumnIndex: 0)
                }
                cursor!.close()
            }
            
            if(pgbValueDouble > 0) {
                customRunKm.lblCounter.text = "\(pgbValueDouble)"
                
                let maxVal:Int = Int(appPref.getDailyDistance())!
                let val:Float = Float(pgbValueDouble) / Float(maxVal)
                customRunKm.prBar.setProgress(val, animated: false)
            }
            
            datasource.closeDatabase()
        }
    }
    
    
}

extension DashboardTabVC {
    func showFoodTimeline() {
        let a = FoodTimelineTVC(nibName: "FoodTimelineTVC", bundle: nil)
        self.navigationController?.pushViewController(a, animated: true)
    }
    func showCaloriesTimeline() {
        let a = CaloriesTimelineTVC(nibName: "FoodTimelineTVC", bundle: nil)
        self.navigationController?.pushViewController(a, animated: true)
    }
    func showDistanceTimeline() {
        let a = DistanceTimelineTVC(nibName: "FoodTimelineTVC", bundle: nil)
        self.navigationController?.pushViewController(a, animated: true)
    }
    func showStepsTimeline() {
        let a = StepsTimelineTVC(nibName: "FoodTimelineTVC", bundle: nil)
        self.navigationController?.pushViewController(a, animated: true)
    }
    func showWaterTimeline() {
        let a = WaterTimelineTVC(nibName: "FoodTimelineTVC", bundle: nil)
        self.navigationController?.pushViewController(a, animated: true)
    }
    func showSleepTimeline() {
        let a = SleepTimelineTVC(nibName: "FoodTimelineTVC", bundle: nil)
        self.navigationController?.pushViewController(a, animated: true)
    }
    func showAppsAndDevices() {
        gettingFitnessPartyData(isInitial: false)
    }
}
