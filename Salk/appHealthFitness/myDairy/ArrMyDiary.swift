//
//  ArrMyDiary.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

public class ArrMyDiary {
    public var day: String = "", dayStr = "", type = "", dateTime = ""
    public var listLayoutIndex: Int = 0;
    public var typeDataList = [ArrFitnessTypeData]()
}
