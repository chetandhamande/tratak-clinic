//
//  DashboardHTTPConnection.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class DashboardHTTPConnection {
    
    var classRefs: AnyObject! = nil
    
    func sendServerRequest(_ url: String, mMethod: String, classRef: AnyObject!, dict: NSMutableDictionary?) {
        
        classRefs = classRef
        var json = Data()
        
        if dict != nil {
            json = try! JSONSerialization.data(withJSONObject: dict!, options: JSONSerialization.WritingOptions.prettyPrinted)
        }
        
        do {
            if mMethod == Constant.BookAppointment.methodLoadSpecialList
                || mMethod == Constant.BookAppointment.methodLoadDoctorList
                || mMethod == Constant.Purchase.MethodSyncPlan
                || mMethod == Constant.Purchase.MethodSyncOffers
                || mMethod == Constant.Purchase.MethodSyncCorpList
                || mMethod == Constant.Purchase.methodLoadPatientLicList
                || mMethod == Constant.BookAppointment.methodLoadAppointList
            {
                try json = DashboardJsonCreator().putJsonDefualt() as Data
            }
        } catch {}
        
        
        //execute Http Connection
        let customObj = CustomNetworkConnection()
        customObj.sendGetRequestToRest(url, json: json, methodName: mMethod, classRef: self)
    }
    
    func getServerResponce(_ respCode: Int?, json: Data?, mMethod: String) {
        
        let appPrefs = MySharedPreferences()
        
        do {
            if respCode == 200 {
                let respJson = NSString(data: json!, encoding: String.Encoding.utf8.rawValue)!
                
                Print.printLog("Rec Code= \(String(describing: respCode)), Resp Data=\(respJson)")
                
                if mMethod == Constant.Purchase.MethodSyncPlan {
                    try DashboardJsonCreator().storeSyncPlanJson(json!)
                    
                    if(classRefs.isKind(of: PlanSelectionAct.self)) {
                        if(appPrefs.getErrorCode() == 200) {
                            (classRefs as! PlanSelectionAct).refreshPlanList()
                        }
                    }
                } else if mMethod == Constant.BookAppointment.methodLoadSpecialList {
                    
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if(classRefs.isKind(of: DocSpecialityListAct.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            (classRefs as! DocSpecialityListAct).successResponse(respJson: json!)
                        } else {
                            (classRefs as! DocSpecialityListAct).failGettingInfo()
                        }
                    }
                } else if mMethod == Constant.BookAppointment.methodLoadDoctorList {
                    
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if(classRefs.isKind(of: DoctorsListAct.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            try (classRefs as! DoctorsListAct).successResponse(respJson: json!)
                        } else {
                            (classRefs as! DoctorsListAct).failGettingInfo()
                        }
                    }
                } else if mMethod == Constant.BookAppointment.methodLoadAppointSlot {
                    
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if(classRefs.isKind(of: BookAppointTimeAct.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            try (classRefs as! BookAppointTimeAct).successResponse(json!)
                        } else {
                            (classRefs as! BookAppointTimeAct).failGettingInfo()
                        }
                    }
                } else if mMethod == Constant.BookAppointment.methodLoadAppointList {
                    
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if(classRefs.isKind(of: BookAppointListAct.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            try (classRefs as! BookAppointListAct).successResponse(json!)
                        } else {
                            (classRefs as! BookAppointListAct).mErrDialogue(appPrefs.getErrorMessage())
                        }
                    }
                } else if mMethod == Constant.BookAppointment.methodAddAppoint
                    || mMethod == Constant.BookAppointment.methodAppointResched
                {
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if (appPrefs.getErrorCode() == 200) {
                        try (classRefs as! BookAppointFinalAct).successResponse(respJson: json!)
                    } else {
                        (classRefs as! BookAppointFinalAct).failGettingInfo()
                    }
                } else if mMethod == Constant.BookAppointment.methodAppointCancel {
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if (appPrefs.getErrorCode() == 200) {
                        try (classRefs as! BookAppointCancelAct).successResponse(respJson: json!)
                    } else {
                        (classRefs as! BookAppointCancelAct).failGettingInfo()
                    }
                } else if mMethod == Constant.Purchase.MethodRazorPlaceOrder {
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if (appPrefs.getErrorCode() == 200) {
                        try (classRefs as! RazorPayGatewayVC).startPayment(json!)
                    } else {
                        (classRefs as! RazorPayGatewayVC).errorPlaceOrder()
                    }
                } else if mMethod == Constant.Purchase.MethodRazorPaymentDone {
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if (appPrefs.getErrorCode() == 200) {
                        (classRefs as! RazorPayGatewayVC).successPlanPurchase()
                    } else {
                        (classRefs as! RazorPayGatewayVC).errorPlaceOrder()
                    }
                } else if mMethod == Constant.Purchase.methodLoadPatientLicList {
                    try DashboardJsonCreator().storeJsonLoadLicence(json!)
                    
                    if(classRefs.isKind(of: RazorPayGatewayVC.self)) {
                        try (classRefs as! RazorPayGatewayVC).restartAppOnSuccess()
                    } else if(classRefs.isKind(of: MySubscriptionVC.self)) {
                        (classRefs as! MySubscriptionVC).successRefresh()
                    }
                } else if mMethod == Constant.Purchase.MethodAddNonCorpPlanEnquiry {
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if appPrefs.getErrorCode() == 200 {
                        (classRefs as! PlanCheckoutVC).successResponse()
                    } else {
                        (classRefs as! PlanCheckoutVC).errorResponse()
                    }
                } else if mMethod == Constant.Purchase.MethodSyncCorpList {
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if appPrefs.getErrorCode() == 200 {
                        try (classRefs as! CorporateSelectionList).successResponse(json!)
                    } else {
                        (classRefs as! CorporateSelectionList).errorResponse()
                    }
                } else if mMethod == Constant.Purchase.MethodAddPlanEnquiry {
                    try DashboardJsonCreator().storeDefaultJson(json!)
                    
                    if appPrefs.getErrorCode() == 200 {
                        try (classRefs as! CorporateSelectionList).restartAppOnSuccess()
                    } else {
                        (classRefs as! CorporateSelectionList).errorResponse()
                    }
                }
            } else {
                if mMethod == Constant.BookAppointment.methodLoadSpecialList {
                    if(classRefs.isKind(of: DocSpecialityListAct.self)) {
                        (classRefs as! DocSpecialityListAct).failGettingInfo()
                    }
                } else if mMethod == Constant.BookAppointment.methodLoadDoctorList {
                    if(classRefs.isKind(of: DoctorsListAct.self)) {
                        (classRefs as! DoctorsListAct).failGettingInfo()
                    }
                } else if mMethod == Constant.BookAppointment.methodLoadAppointSlot {
                    if(classRefs.isKind(of: BookAppointTimeAct.self)) {
                        (classRefs as! BookAppointTimeAct).failGettingInfo()
                    }
                } else if mMethod == Constant.BookAppointment.methodLoadAppointList {
                    if(classRefs.isKind(of: BookAppointListAct.self)) {
                        (classRefs as! BookAppointListAct).mErrDialogue(appPrefs.getErrorMessage())
                    }
                } else if mMethod == Constant.BookAppointment.methodAddAppoint
                        || mMethod == Constant.BookAppointment.methodAppointResched
                {
                    (classRefs as! BookAppointFinalAct).failGettingInfo()
                } else if mMethod == Constant.Purchase.MethodRazorPlaceOrder {
                    (classRefs as! RazorPayGatewayVC).errorPlaceOrder()
                } else if mMethod == Constant.Purchase.MethodRazorPaymentDone {
                    (classRefs as! RazorPayGatewayVC).errorPlaceOrder()
                } else if mMethod == Constant.Purchase.methodLoadPatientLicList {
                    if(classRefs.isKind(of: RazorPayGatewayVC.self)) {
                        try (classRefs as! RazorPayGatewayVC).restartAppOnSuccess()
                    } else if(classRefs.isKind(of: MySubscriptionVC.self)) {
                        (classRefs as! MySubscriptionVC).mErrDialogue(appPrefs.getErrorMessage())
                    }
                } else if mMethod == Constant.Purchase.MethodAddNonCorpPlanEnquiry {
                    (classRefs as! PlanCheckoutVC).errorResponse()
                } else if mMethod == Constant.BookAppointment.methodAppointCancel {
                    (classRefs as! BookAppointCancelAct).failGettingInfo()
                }
            }
        } catch {}
    }
    
    // Alert Box to Show error Messages
    fileprivate func mErrDialogue(_ message: String) {
        SweetAlert().showAlert("fail".localized, subTitle: message, style: .error)
    }
}
