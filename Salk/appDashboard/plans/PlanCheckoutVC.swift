//
//  PlanCheckoutVC.swift
//  Salk
//
//  Created by Salk on 16/03/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import SnapKit
import RAMAnimatedTabBarController

class PlanCheckoutVC: UIViewController {

    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var imgBackground: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle0: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    
    @IBOutlet weak var durListView1: UIView!
    @IBOutlet weak var durListView2: UIView!
    @IBOutlet weak var btnGetPlan: UIButton!
    
    @IBOutlet weak var durPlan1: CustomPlanDurationView!
    @IBOutlet weak var durPlan2: CustomPlanDurationView!
    @IBOutlet weak var durPlan3: CustomPlanDurationView!
    @IBOutlet weak var durPlan4: CustomPlanDurationView!
    @IBOutlet weak var durPlan5: CustomPlanDurationView!
    @IBOutlet weak var durPlan6: CustomPlanDurationView!
    
    fileprivate let loading = CustomLoading()
    
    var planID: String = "", planName = "", planDetail = "", price = "",
    comboPlan = "", comboPrice = "", longDesc = "",
    features = "", imageName = "";
    var currSelPlanID: Int = -1
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationItem()
        viewConfiguration()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let orientation = UIApplication.shared.statusBarOrientation
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.topMargin.equalTo(0)
                    make.leadingMargin.equalTo(0)
                    make.trailingMargin.equalTo(0)
                    make.bottomMargin.equalTo(0)
                    make.width.equalTo(mainStackView)
                })
            }
        }
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
                
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.width.equalTo(mainStackView)
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Checkout"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }

    fileprivate func viewConfiguration() {
        lblTitle.text = planName
        lblSubTitle0.text = planDetail
        lblDetails.text = longDesc
        
        if(!imageName.isEmpty) {
            let appPref = MySharedPreferences()
            let temp = "\(SalkProjectConstant().ServerFitnessChatAttach + appPref.getCompanyID())/plan/\(imageName)"
            let URL = Foundation.URL(string: temp)!
            self.imgBackground.kf.setImage(with: URL, placeholder: nil, options: [.transition(.fade(1))], progressBlock: { receivedSize, totalSize in
                Print.printLog("\(receivedSize)/\(totalSize)")
            }, completionHandler: { image, error, cacheType, imageURL in
                if error != nil {
                    self.imgBackground.image = UIImage().imageWithImage(UIImage(named: "back.jpg")!, scaledToSize: CGSize(width: 50, height: 50))
                }
            })
        } else {
            self.imgBackground.isHidden = false
        }
        
        
        //showing plan duration list
        do {
            try showPlanDuration()
        } catch {}
    }
    
    fileprivate func resetAllDuration() {
        durPlan1.parentView.backgroundColor = CustomColor.colorSalk()
        durPlan2.parentView.backgroundColor = CustomColor.colorSalk()
        durPlan3.parentView.backgroundColor = CustomColor.colorSalk()
        durPlan4.parentView.backgroundColor = CustomColor.colorSalk()
        durPlan5.parentView.backgroundColor = CustomColor.colorSalk()
        durPlan6.parentView.backgroundColor = CustomColor.colorSalk()
    }
    
    fileprivate func showPlanDuration() throws {
        resetAllDuration()
        
        durPlan1.parentView.isHidden = true
        durPlan2.parentView.isHidden = true
        durPlan3.parentView.isHidden = true
        durPlan4.parentView.isHidden = true
        durPlan5.parentView.isHidden = true
        durPlan6.parentView.isHidden = true
        durListView1.isHidden = true
        durListView2.isHidden = true
        btnGetPlan.backgroundColor = CustomColor.colorSalk()
        
        // convert String to NSData
        do {
            let data: Data = comboPlan.data(using: String.Encoding.utf8)! as Data
            let dataArray = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray

            for i in 0 ..< dataArray.count {
                guard let subJson = dataArray[i] as? NSDictionary else {
                    continue
                }
                let duration = JsonParser.getJsonValueString(subJson, valueForKey: "duration")
                let price = JsonParser.getJsonValueString(subJson, valueForKey: "price")

                if i == 0 {
                    durListView1.isHidden = false
                    durPlan1.parentView.isHidden = false
                    durPlan1.lblPrice.text = "\(price) INR"
                    durPlan1.lblDuration.text = duration

                    let stepsTapGesture = UITapGestureRecognizer(target: self, action: #selector(planDurSelect1))
                    durPlan1.mainView.addGestureRecognizer(stepsTapGesture)
                } else if i == 1 {
                    durPlan2.parentView.isHidden = false
                    durPlan2.lblPrice.text = "\(price) INR"
                    durPlan2.lblDuration.text = duration

                    let stepsTapGesture = UITapGestureRecognizer(target: self, action: #selector(planDurSelect2))
                    durPlan2.mainView.addGestureRecognizer(stepsTapGesture)
                } else if i == 2 {
                    durPlan3.parentView.isHidden = false
                    durPlan3.lblPrice.text = "\(price) INR"
                    durPlan3.lblDuration.text = duration

                    let stepsTapGesture = UITapGestureRecognizer(target: self, action: #selector(planDurSelect3))
                    durPlan3.mainView.addGestureRecognizer(stepsTapGesture)
                } else if i == 3 {
                    durListView2.isHidden = false
                    durPlan4.parentView.isHidden = false
                    durPlan4.lblPrice.text = "\(price) INR"
                    durPlan4.lblDuration.text = duration

                    let stepsTapGesture = UITapGestureRecognizer(target: self, action: #selector(planDurSelect4))
                    durPlan4.mainView.addGestureRecognizer(stepsTapGesture)
                } else if i == 4 {
                    durPlan5.parentView.isHidden = false
                    durPlan5.lblPrice.text = "\(price) INR"
                    durPlan5.lblDuration.text = duration

                    let stepsTapGesture = UITapGestureRecognizer(target: self, action: #selector(planDurSelect5))
                    durPlan5.mainView.addGestureRecognizer(stepsTapGesture)
                } else if i == 5 {
                    durPlan6.parentView.isHidden = false
                    durPlan6.lblPrice.text = "\(price) INR"
                    durPlan6.lblDuration.text = duration

                    let stepsTapGesture = UITapGestureRecognizer(target: self, action: #selector(planDurSelect6))
                    durPlan6.mainView.addGestureRecognizer(stepsTapGesture)
                }
            }
        } catch {}
    }
    
    @IBAction func performCheckout(button: UIButton) {
        if currSelPlanID > -1 {
            
            do {
                let data: Data = comboPlan.data(using: String.Encoding.utf8)! as Data
                let dataArray = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                
                guard let subJson = dataArray[currSelPlanID] as? NSDictionary else {
                    return
                }
                
                let appPref = MySharedPreferences()
                if(appPref.getRegUserType().equalsIgnoreCase("Corporate")) {
                    let a = CorporateSelectionList(nibName: "CorporateSelectionList", bundle: nil)
                    a.planID = planID
                    a.planName = planName
                    self.navigationController?.pushViewController(a, animated: true)
                    
                } else if("razorPayKey".applocalized.isEmpty) {
                    let checkConn = CheckInternetConnection()
                    if (checkConn.isCheckInternetConnection()) {
                        let appPref = MySharedPreferences()
                        var data = NSMutableDictionary()
                        data = ["osType":"iOS",
                                "by":"PATIENT",
                                "patientID": appPref.getUserID(),
                                "phonebookID": appPref.getPhonebookID(),
                                "companyID": appPref.getCompanyID(),
                                "plan_id": planID,
                                "duration": JsonParser.getJsonValueString(subJson, valueForKey: "duration"),
                                "price": JsonParser.getJsonValueString(subJson, valueForKey: "price")
                        ]
                        self.view.addSubview(loading.loading())

                        DashboardRequestManager().getRequest(Constant.Purchase.MethodAddNonCorpPlanEnquiry, dict: data, classRef: self)
                    } else {
                        SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
                    }
                } else {
                    let a = RazorPayGatewayVC(nibName: "RazorPayGatewayVC", bundle: nil)
                    a.planName = planName
                    a.planID = planID
                    a.duration = JsonParser.getJsonValueString(subJson, valueForKey: "duration")
                    a.durationID = JsonParser.getJsonValueString(subJson, valueForKey: "id")
                    a.priceFinal = JsonParser.getJsonValueString(subJson, valueForKey: "price")
                    self.navigationController?.pushViewController(a, animated: true)
                }
            } catch {}
            
        } else {
            _ = SweetAlert().showAlert("Oops...", subTitle: "Select any plan", style: .warning)
        }        
    }
    
    func planDurSelect1() {
        currSelPlanID = 0
        resetAllDuration()
        durPlan1.parentView.backgroundColor = CustomColor.colorSalkContrast()
        
        mainScrollView.contentOffset = CGPoint.init(x: 0, y: 10);
    }
    func planDurSelect2() {
        currSelPlanID = 1
        resetAllDuration()
        durPlan2.parentView.backgroundColor = CustomColor.colorSalkContrast()
        
        mainScrollView.contentOffset = CGPoint.init(x: 0, y: 10);
    }
    func planDurSelect3() {
        currSelPlanID = 2
        resetAllDuration()
        durPlan3.parentView.backgroundColor = CustomColor.colorSalkContrast()
        
        mainScrollView.contentOffset = CGPoint.init(x: 0, y: 10);
    }
    func planDurSelect4() {
        currSelPlanID = 3
        resetAllDuration()
        durPlan4.parentView.backgroundColor = CustomColor.colorSalkContrast()
        
        mainScrollView.contentOffset = CGPoint.init(x: 0, y: 10);
    }
    func planDurSelect5() {
        currSelPlanID = 4
        resetAllDuration()
        durPlan5.parentView.backgroundColor = CustomColor.colorSalkContrast()
        
        mainScrollView.contentOffset = CGPoint.init(x: 0, y: 10);
    }
    func planDurSelect6() {
        currSelPlanID = 5
        resetAllDuration()
        durPlan6.parentView.backgroundColor = CustomColor.colorSalkContrast()
        
        mainScrollView.contentOffset = CGPoint.init(x: 0, y: 10);
    }
    
    func successResponse() {
        _ = SweetAlert().showAlert("Success", subTitle: "Your schedule call successfully submitted to our expert.", style: .success, buttonTitle:"OK", buttonColor: PrimaryColor.colorPrimary()) { (isOtherButton) -> Void in
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func errorResponse() {
        loading.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        _ = SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
}
