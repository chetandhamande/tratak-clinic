//
//  RazorPayGatewayVC.swift
//  Salk
//
//  Created by Salk on 17/03/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material

class RazorPayGatewayVC: UIViewController, RazorpayPaymentCompletionProtocol {
    
    fileprivate var razorpay : Razorpay!
    
    fileprivate let loading = CustomLoading()
    let alertObj = SweetAlert()
    
    var planID = "", planName = "", duration = "", durationID = "", priceFinal = "", priceInPaisa = ""
    var orderID = "", receipt = "", amount = "", currency = "", notes = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    } 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationItem()
        
        //convert rupee to paisa
        if !priceFinal.isEmpty {
            priceInPaisa = "\((Int)(Double(priceFinal)! * 100))"
            if (priceInPaisa.isEmpty) {
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
        razorpay = Razorpay.initWithKey("razorPayKey".applocalized, andDelegate: self)
        
        self.performPlaceOrderRequest()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Checkout"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    fileprivate func initOrderId() -> String {
        let appPref = MySharedPreferences()
        return "\(appPref.getUserID())\((Int)(ConvertionClass().currentTime()))";
    }
    
    fileprivate func performPlaceOrderRequest() {
        let checkConn = CheckInternetConnection()
        if (checkConn.isCheckInternetConnection()) {
            let appPref = MySharedPreferences()
            var data = NSMutableDictionary()
            data = ["osType":"iOS",
                    "by":"PATIENT",
                    "patientID": appPref.getUserID(),
                    "phonebookID": appPref.getPhonebookID(),
                    "companyID": appPref.getCompanyID(),
                    "amount": priceInPaisa,
                    "currency": "INR",
                    "receipt": "\(initOrderId())",
                    "payment_capture": "1",
                    "notes": "\(planName) for \(duration)"
            ]
            
            self.view.addSubview(loading.loading())
            
            DashboardRequestManager().getRequest(Constant.Purchase.MethodRazorPlaceOrder, dict: data, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func startPayment(_ json: Data) throws {
        loading.removeFromSuperview()
        
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        let subJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
        
        orderID = JsonParser.getJsonValueString(subJson, valueForKey: "orderID")
        receipt = JsonParser.getJsonValueString(subJson, valueForKey: "receipt")
        amount = JsonParser.getJsonValueString(subJson, valueForKey: "amount")
        currency = JsonParser.getJsonValueString(subJson, valueForKey: "currency")
        notes = JsonParser.getJsonValueString(subJson, valueForKey: "notes")
        
        if(orderID.equalsIgnoreCase("null") || orderID.equalsIgnoreCase("na")) {
            orderID = ""
        }
        if(amount.equalsIgnoreCase("null") || amount.equalsIgnoreCase("na")) {
            amount = ""
        }
        
        let appPref = MySharedPreferences()
        if(!orderID.isEmpty && !amount.isEmpty && appPref.getUserID() > 0) {
            let options: [String:Any] = [
                "name" : "app_name".applocalized,
                "description" : notes,
                "image" : UIImage(named: "app_icon_black") as Any,
                "currency" : currency,
                "amount" : amount,
                "order_id" : orderID,
                "prefill": [
                    "contact": appPref.getMobile(),
                    "email": appPref.getEmailID()
                ],
                "theme": [
                    "color": "#00ccd3"
                ]
            ]
            razorpay.open(options)
        }        
    }
    
    func errorPlaceOrder() {
        loading.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        alertObj.showAlert("Oops", subTitle: appPref.getErrorMessage(), style: .error, buttonTitle:"OK", buttonColor: PrimaryColor.colorPrimary()) { (isOtherButton) -> Void in
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func onPaymentError(_ code: Int32, description str: String) {
        alertObj.showAlert("Oops", subTitle: "Fail to initiate payment.", style: .error, buttonTitle:"OK", buttonColor: PrimaryColor.colorPrimary()) { (isOtherButton) -> Void in
            self.navigationController?.popViewController(animated: true)
        }
    }
    func onPaymentSuccess(_ payment_id: String) {
        
        let appPref = MySharedPreferences()
        var data = NSMutableDictionary()
        data = ["osType":"iOS",
                "by":"PATIENT",
                "patientID": appPref.getUserID(),
                "phonebookID": appPref.getPhonebookID(),
                "companyID": appPref.getCompanyID(),
                "planID": planID,
                "durationID": durationID,
                "paymentID": payment_id,
                "orderID": orderID,
                "receipt": receipt,
                "amount": amount,
                "currency": currency,
                "payGateway": "razorpay"
        ]
        
        self.view.addSubview(loading.loading())
        
        DashboardRequestManager().getRequest(Constant.Purchase.MethodRazorPaymentDone, dict: data, classRef: self)
    }
    
    func successPlanPurchase() {
        DashboardRequestManager().getRequest(Constant.Purchase.methodLoadPatientLicList, classRef: self)
    }
    
    func restartAppOnSuccess() throws {
        loading.removeFromSuperview()
        
        let mainObj = MainViewController()
        mainObj.moveToDecideAppCondition()
    }
    
}
