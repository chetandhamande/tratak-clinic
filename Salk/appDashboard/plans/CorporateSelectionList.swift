//
//  CorporateSelectionList.swift
//  Salk
//
//  Created by Salk on 23/03/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import SnapKit
import RAMAnimatedTabBarController
import DropDown
import Async

class CorporateSelectionList: UIViewController {

    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var mainStackView: UIScrollView!
    
    @IBOutlet weak var btnOrganization: UIButton!
    @IBOutlet weak var btnLevel: UIButton!
    @IBOutlet weak var btnDepartment: UIButton!
    @IBOutlet weak var btnDesignation: UIButton!
    @IBOutlet weak var btnBranch: UIButton!
    
    private let loading = CustomLoading()
    
    private var dataArray: NSMutableArray = NSMutableArray()
    
    private var listOrganization: Array<String>! = nil, listOrgID: Array<String>! = nil,
        listLevel: Array<String>! = nil, listLevelID: Array<String>! = nil,
        listDepartment: Array<String>! = nil, listDepartmentID: Array<String>! = nil,
        listDesignation: Array<String>! = nil, listDesignationID: Array<String>! = nil,
        listBranch: Array<String>! = nil, listBranchID: Array<String>! = nil
    let chooseDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDropDown,
            ]
    }()
    
    var planID = "", planName = "", selOrgID = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationItem()
        setupDefaultDropDown()
        resetOptions()
        
        Async.main(after: 0.1, {
            self.getRecordServer()
        })
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let orientation = UIApplication.shared.statusBarOrientation
        if UI_USER_INTERFACE_IDIOM() == .pad {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.topMargin.equalTo(0)
                    make.leadingMargin.equalTo(0)
                    make.trailingMargin.equalTo(0)
                    make.bottomMargin.equalTo(0)
                    make.width.equalTo(mainStackView)
                })
            }
        }
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
                
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.width.equalTo(mainStackView)
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Premium Plans"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    @IBAction func selectOrganization(button: UIButton) {
        durationPopup(btnOrganization, mListOption: listOrganization,isOrgnation: true)
    }
    @IBAction func selectDepartment(button: UIButton) {
        durationPopup(btnDepartment, mListOption: listDepartment, isOrgnation: false)
    }
    @IBAction func selectLevel(button: UIButton) {
        durationPopup(btnLevel, mListOption: listLevel, isOrgnation: false)
    }
    @IBAction func selectBranch(button: UIButton) {
        durationPopup(btnBranch, mListOption: listBranch, isOrgnation: false)
    }
    @IBAction func selectDesignation(button: UIButton) {
        durationPopup(btnDesignation, mListOption: listDesignation, isOrgnation: false)
    }
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    private func durationPopup(_ button: UIButton, mListOption: [String], isOrgnation: Bool) {
        chooseDropDown.anchorView = button
        //chooseDropDown.bottomOffset = CGPoint(x: 0, y: linNightMainPanel.btnTime.bounds.height)
        chooseDropDown.dataSource = mListOption
        chooseDropDown.selectionAction = { [weak self] (index, item) in
            button.setTitle(item, for: .normal)
            button.setTitleColor(Color.black, for: .normal)
            
            if(isOrgnation) {
                do {
                    try self?.setOrganisationInfo()
                } catch {}
            }
        }
        
        chooseDropDown.show()
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loading.loading())
            
            DashboardRequestManager().getRequest(Constant.Purchase.MethodSyncCorpList, classRef: self)
        } else {
            _ = SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func successResponse(_ json: Data) throws {
        
        loading.removeFromSuperview()
        let appPrefs = MySharedPreferences()
        
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
        appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
        
        if JsonParser.getJsonValueInt(dict, valueForKey: "code") == 200 {
            dataArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
            
            listOrganization = [String]()
            listOrgID = [String]()
            for i in 0 ..< dataArray.count {
                guard let subJson = dataArray[i] as? NSDictionary else {
                    continue
                }
                
                let org_id = JsonParser.getJsonValueString(subJson, valueForKey: "org_id")
                let organization = JsonParser.getJsonValueString(subJson, valueForKey: "organization")
                
                listOrgID.append(org_id)
                listOrganization.append(organization)
            }
        }
    }
    
    func errorResponse() {
        loading.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        _ = SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    private func resetOptions() {
        listDepartment = [String] ()
        listBranch = [String] ()
        listLevel = [String] ()
        listDesignation = [String] ()
        listDepartmentID = [String] ()
        listBranchID = [String] ()
        listLevelID = [String] ()
        listDesignationID = [String] ()
        btnDepartment.setTitle("Select Department", for: .normal)
        btnBranch.setTitle("Select Branch", for: .normal)
        btnLevel.setTitle("Select Level", for: .normal)
        btnDesignation.setTitle("Select Department", for: .normal)
        btnDepartment.setTitleColor(UIColor.gray, for: .normal)
        btnBranch.setTitleColor(UIColor.gray, for: .normal)
        btnLevel.setTitleColor(UIColor.gray, for: .normal)
        btnDesignation.setTitleColor(UIColor.gray, for: .normal)
    }
    
    private func setOrganisationInfo() throws {
        
        let organization = btnOrganization.titleLabel?.text
        for i in 0 ..< listOrganization.count {
            if listOrganization[i].equalsIgnoreCase(organization!) {
                selOrgID = listOrgID[i]
                break
            }
        }
        
        resetOptions()
        
        for i in 0 ..< dataArray.count {
            guard let subJson = dataArray[i] as? NSDictionary else {
                continue
            }
            
            let org_id = JsonParser.getJsonValueString(subJson, valueForKey: "org_id")
            
            if(selOrgID.equalsIgnoreCase(org_id)) {
                let deptArray = JsonParser.getJsonValueMutableArray(subJson, valueForKey: "dept")
                let branchArray = JsonParser.getJsonValueMutableArray(subJson, valueForKey: "branch")
                let levelArray = JsonParser.getJsonValueMutableArray(subJson, valueForKey: "level")
                let desgArray = JsonParser.getJsonValueMutableArray(subJson, valueForKey: "desg")
                
                for j in 0 ..< deptArray.count {
                    guard let subSubJson = deptArray[j] as? NSDictionary else {
                        continue
                    }
                    let id = JsonParser.getJsonValueString(subSubJson, valueForKey: "dept_id")
                    let name = JsonParser.getJsonValueString(subSubJson, valueForKey: "department")
                    listDepartmentID.append(id)
                    listDepartment.append(name)
                }
                for j in 0 ..< branchArray.count {
                    guard let subSubJson = branchArray[j] as? NSDictionary else {
                        continue
                    }
                    let id = JsonParser.getJsonValueString(subSubJson, valueForKey: "branch_id")
                    let name = JsonParser.getJsonValueString(subSubJson, valueForKey: "branch")
                    listBranchID.append(id)
                    listBranch.append(name)
                }
                for j in 0 ..< levelArray.count {
                    guard let subSubJson = levelArray[j] as? NSDictionary else {
                        continue
                    }
                    let id = JsonParser.getJsonValueString(subSubJson, valueForKey: "level_id")
                    let name = JsonParser.getJsonValueString(subSubJson, valueForKey: "level")
                    listLevelID.append(id)
                    listLevel.append(name)
                }
                for j in 0 ..< desgArray.count {
                    guard let subSubJson = desgArray[j] as? NSDictionary else {
                        continue
                    }
                    let id = JsonParser.getJsonValueString(subSubJson, valueForKey: "desig_id")
                    let name = JsonParser.getJsonValueString(subSubJson, valueForKey: "designation")
                    listDesignationID.append(id)
                    listDesignation.append(name)
                }
                
                if(deptArray.count > 0) {
                    btnDepartment.setTitle(listDepartment[0], for: .normal)
                    btnDepartment.setTitleColor(Color.black, for: .normal)
                }
                if(branchArray.count > 0) {
                    btnBranch.setTitle(listBranch[0], for: .normal)
                    btnBranch.setTitleColor(Color.black, for: .normal)
                }
                if(levelArray.count > 0) {
                    btnLevel.setTitle(listLevel[0], for: .normal)
                    btnLevel.setTitleColor(Color.black, for: .normal)
                }
                if(desgArray.count > 0) {
                    btnDesignation.setTitle(listDesignation[0], for: .normal)
                    btnDesignation.setTitleColor(Color.black, for: .normal)
                }
            }
        }
    }
    
    @IBAction func performPurchansePlan(button: UIButton) {
        if selOrgID.isEmpty || selOrgID.equalsIgnoreCase("0") {
            _ = SweetAlert().showAlert("Oops...", subTitle: "Select at least one organisation!", style: .warning)
        } else {
            let checkConn = CheckInternetConnection()
            if checkConn.isCheckInternetConnection() {
                self.view.addSubview(loading.loading())
                
                var branchID = "0"
                for i in 0 ..< listBranch.count {
                    if listBranch[i].equalsIgnoreCase((btnBranch.titleLabel?.text!)!) {
                        branchID = listBranchID[i]
                        break
                    }
                }
                var levelID = "0"
                for i in 0 ..< listLevel.count {
                    if listLevel[i].equalsIgnoreCase((btnLevel.titleLabel?.text!)!) {
                        levelID = listLevelID[i]
                        break
                    }
                }
                var deptID = "0"
                for i in 0 ..< listDepartment.count {
                    if listDepartment[i].equalsIgnoreCase((btnDepartment.titleLabel?.text!)!) {
                        deptID = listDepartmentID[i]
                        break
                    }
                }
                var desgID = "0"
                for i in 0 ..< listDesignation.count {
                    if listDesignation[i].equalsIgnoreCase((btnDesignation.titleLabel?.text!)!) {
                        desgID = listDesignationID[i]
                        break
                    }
                }
                
                let appPref = MySharedPreferences()
                var data = NSMutableDictionary()
                data = ["osType":"iOS",
                        "by":"PATIENT",
                        "patientID": appPref.getUserID(),
                        "phonebookID": appPref.getPhonebookID(),
                        "companyID": appPref.getCompanyID(),
                        "dept_id": deptID,
                        "desg_id": desgID,
                        "level_id": levelID,
                        "branch_id": branchID,
                        "plan_id": planID,
                        "org_id": selOrgID
                ]
                
                DashboardRequestManager().getRequest(Constant.Purchase.MethodAddPlanEnquiry, dict: data, classRef: self)
            } else {
                _ = SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
            }
        }
    }
    
    func restartAppOnSuccess() throws {
        loading.removeFromSuperview()
        
        SweetAlert().showAlert("popupSuccess".localized, subTitle: "Your schedule call successfully submitted to our expert.", style: .success, buttonTitle: "OK") { click in
            let mainObj = MainViewController()
            mainObj.moveToDecideAppCondition()
        }
    }
    
}
