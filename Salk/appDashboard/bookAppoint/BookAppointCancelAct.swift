//
//  BookAppointCancelAct.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async
import SnapKit
import DropDown

protocol BookAppointCancelViewControllerDelegate {
    func onViewControllerReturn(data: AnyObject)
}

class BookAppointCancelAct: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var txtDocName: UILabel!
    @IBOutlet weak var txtSpeciality: UILabel!
    @IBOutlet weak var imgDoctor: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var txtDetails: MKTextField!
    @IBOutlet weak var btnCancelReson: UIButton!
    @IBOutlet weak var btnNotifyBy: UIButton!
    
    private let loading = CustomLoading()
    
    var speciality = "", aptID = "", name = "", date = "", timeDiff = "", fromTime = "", toTime = "", doc_id = ""
    
    var delegate: BookAppointCancelViewControllerDelegate?
   
    private var listCancelReason: Array<String>! = nil, listNotifyBy: Array<String>! = nil
    let chooseDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDropDown,
            ]
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        let orientation = UIApplication.shared.statusBarOrientation

        if UI_USER_INTERFACE_IDIOM() == .pad {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.topMargin.equalTo(0)
                    make.leadingMargin.equalTo(0)
                    make.trailingMargin.equalTo(0)
                    make.bottomMargin.equalTo(0)
                    make.width.equalTo(mainStackView)
                })
            }
        }

        if UI_USER_INTERFACE_IDIOM() == .phone {
            if orientation == .landscapeLeft || orientation == .landscapeRight {

                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })

            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.width.equalTo(mainStackView)
                })
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationItem()
        init1()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Appointment cancellation"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    private func init1() {
        txtDocName.text = name
        txtSpeciality.text = speciality
        
        let URL = Foundation.URL(string: "\(SalkProjectConstant().ServeraddExpertImg)\(doc_id.sha1() + Constant.FolderNames.imageExtensionjpg)")!
        imgDoctor.kf.setImage(with: URL, placeholder: nil, options: [.transition(.fade(1))], progressBlock: { receivedSize, totalSize in
            
        }, completionHandler: { image, error, cacheType, imageURL in
            if error != nil {
                self.imgDoctor.image = UIImage().imageWithImage(UIImage(named: "ic_person_white_gray_24dp.png")!, scaledToSize: CGSize(width: 45, height: 45))
            }
        })
        
        let currDate = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: "dd MMM, yyyy")
        if currDate.equalsIgnoreCase(date) {
            lblTitle.text = "Today  \(fromTime)"
        } else {
            lblTitle.text = "\(date)  \(fromTime)"
        }
        
        let touch = UITapGestureRecognizer(target: self, action:#selector(dismissKeyboard))
        self.view.addGestureRecognizer(touch)
        
        configureTextField(txtDetails, placeHolder: "Details")
        
        listCancelReason = [String]()
        listCancelReason.append("Feeling Better")
        listCancelReason.append("Condition Worse")
        listCancelReason.append("Away")
        listCancelReason.append("Sick")
        listCancelReason.append("Work")
        listCancelReason.append("Other")
        
        listNotifyBy = [String] ()
        listNotifyBy.append("Email")
        listNotifyBy.append("SMS")
        listNotifyBy.append("Notification")
    }
    
    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func configureTextField(_ textField : UITextField, placeHolder: String) {
        textField.delegate = self
        
        if textField == txtDetails {
            textField.returnKeyType = UIReturnKeyType.done
        } else {
            textField.returnKeyType = UIReturnKeyType.next
        }
        
        //textField.layer.borderColor = UIColor.clear.cgColor
        textField.placeholder = placeHolder
        textField.backgroundColor = UIColor.white
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.autocapitalizationType = .words
    }

    @IBAction func selectCancelReason(button: UIButton) {
        durationPopup(btnCancelReson, mListOption: listCancelReason)
    }
    @IBAction func selectNotifyBy(button: UIButton) {
        durationPopup(btnNotifyBy, mListOption: listNotifyBy)
    }
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    private func durationPopup(_ button: UIButton, mListOption: [String]) {
        chooseDropDown.anchorView = button
        //chooseDropDown.bottomOffset = CGPoint(x: 0, y: linNightMainPanel.btnTime.bounds.height)
        chooseDropDown.dataSource = mListOption
        chooseDropDown.selectionAction = { [weak self] (index, item) in
            button.setTitle(item, for: .normal)
            button.setTitleColor(Color.black, for: .normal)
        }
        
        chooseDropDown.show()
    }
    
    @IBAction func performCancelAppointment(button: UIButton) {
        dismissKeyboard()
        
        if (btnCancelReson.titleLabel?.text?.isEmpty)! || (btnCancelReson.titleLabel?.text?.equalsIgnoreCase("Reason for cancel"))! {
            errorDialogue("Missing cancel reason")
        } else if (btnNotifyBy.titleLabel?.text?.isEmpty)! || (btnNotifyBy.titleLabel?.text?.equalsIgnoreCase("Notify by"))! {
            errorDialogue("Missing notify by")
        } else {
            let checkConn = CheckInternetConnection()
            if checkConn.isCheckInternetConnection() {
                self.view.addSubview(loading.loading())
                
                do {
                    try DashboardRequestManager().getRequest(Constant.BookAppointment.methodAppointCancel, dict: createJsonAppointSlot(), classRef: self)
                } catch {}
            } else {
                errorDialogue("noInternet".localized)
            }
        }
    }
    
    public func successResponse(respJson: Data) throws {
        loading.removeFromSuperview()
        
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: respJson, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            if Int(JsonParser.getJsonValueString(dict, valueForKey: "code")) == 200 {
                mSuccDialogue("cancelAppointSuccess".localized)
            }
        } catch _ { }
    }
    
    // Alert Box to Show Success Messages
    private func mSuccDialogue(_ message: String) {
        SweetAlert().showAlert("popupSuccess".localized, subTitle: message, style: .success, buttonTitle: "OK") { click in
            self.navigationController?.popViewController(animated: false)
            self.delegate?.onViewControllerReturn(data: "Success" as AnyObject)
        }
    }
    
    // Alert Box to Show Success Messages
    private func errorDialogue(_ message: String) {
        SweetAlert().showAlert("Oops...".localized, subTitle: message, style: .warning, buttonTitle: "OK")
    }
    
    public func failGettingInfo() {
        loading.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        errorDialogue(appPref.getErrorMessage())
    }
    
    // create JSON values to send it to server
    func createJsonAppointSlot() throws -> NSMutableDictionary {
        
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        data = ["osType":"iOS",
                "by":"PATIENT",
                "patientID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "companyID": appUserInfo.getCompanyID(),
                "id": aptID,
                "cancelReason": btnCancelReson.titleLabel?.text as Any,
                "cancelNotify": btnNotifyBy.titleLabel?.text as Any,
                "details": txtDetails.text as Any
        ]
        
        return data
    }
}
