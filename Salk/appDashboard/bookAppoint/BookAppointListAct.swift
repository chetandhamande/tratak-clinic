//
//  BookAppointListAct.swift
//  Salk
//
//  Created by Chetan  on 19/03/18
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Async
import Material
import RAMAnimatedTabBarController

class BookAppointListAct: UIViewController, UITableViewDelegate, UITableViewDataSource, DocSpecialityViewControllerDelegate, BookTimeViewControllerDelegate, BookAppointCancelViewControllerDelegate
{
        
    @IBOutlet weak var listview: UITableView!
    @IBOutlet weak var stackNotAvail: UIStackView!
    @IBOutlet weak var bookView: UIView!

    private var arrMainList = [ArrAppointDetail]()
    private let tableCellIdentifier = "CustomAdapterBookAppointList"
    private let loading = CustomLoading()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationItem()
        init1()
        
        let internet = CheckInternetConnection().isCheckInternetConnection()
        if internet {
            let appPref = MySharedPreferences()
            var time = ConvertionClass().currentTime() - appPref.getLastAppointmentSycnTime()
            time = (time / 60) //in minute
            
            if time > 10 {
                Async.main(after: 0.1, {
                    self.getRecordServer()
                })
            } else {
                loadAppointList()
            }
        } else {
            loadAppointList()
        }
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Appointments History".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let refreshImg: UIImage = UIImage().imageWithImage(UIImage(named: "ic_refresh_black_36dp")!, scaledToSize: CGSize(width: 30, height: 30))
        let refreshButton = UIBarButtonItem(image: refreshImg, style: .plain, target: self, action: #selector(self.refreshButtonClick))
        navigationItem.rightBarButtonItems = [refreshButton]
    }
    
    private func init1() {
        listview.delegate = self
        listview.dataSource = self
        self.listview.separatorStyle = .none
        
        self.view.backgroundColor = CustomColor.white()
        self.listview.backgroundColor = CustomColor.white()
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(BookAppointListAct.moveToDocSpecialityListAct))
        self.bookView.addGestureRecognizer(tap)
    }
    
    //Method for load booked appointment in list
    private func loadAppointList() {
    
        arrMainList = [ArrAppointDetail]()
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
    
        let sql = "Select * " + " from " + datasource.AppointmentList_tlb
    
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                let temp = ArrAppointDetail()
                temp.aptID = cursor!.string(forColumnIndex: 0)
                temp.specialityID = cursor!.string(forColumnIndex: 2)
                temp.doctorID = cursor!.string(forColumnIndex: 3)
    
                let df = DateFormatter()
                df.dateFormat = "yyyy-MM-dd hh:mm a"
                
                guard let val = df.date(from: cursor!.string(forColumnIndex: 4) + " " + "00:00 AM") else {
                    continue
                }
                let tempDate = ConvertionClass().conDateToLong(val)
                temp.date = ConvertionClass().conLongToDate(tempDate, dateFormat: "dd MMM, yyyy")
                
                temp.appointTimeFrom = cursor!.string(forColumnIndex: 5)
                
                let a = ConvertionClass().conLongToDate(tempDate, dateFormat: "yyyy-MM-dd")
                df.dateFormat = "yyyy-MM-dd hh:mm a"
                guard let tempdate = df.date(from: "\(a) \(temp.appointTimeFrom)") else {
                    continue
                }
                let time = ConvertionClass().conDateToLong(tempdate)
                temp.timeDiff = ConvertionClass().getDiffFromCurrentDate(time, time2: ConvertionClass().currentTime(), isAbsolute: false)
                
                temp.appointTimeTo = cursor!.string(forColumnIndex: 6)
                temp.aptFor = cursor!.string(forColumnIndex: 7)
                temp.reminderBy = cursor!.string(forColumnIndex: 8)
                temp.details = cursor!.string(forColumnIndex: 9)
                temp.aptType = cursor!.string(forColumnIndex: 10)
                temp.name = cursor!.string(forColumnIndex: 11)
                temp.email = cursor!.string(forColumnIndex: 12)
                temp.mobileNo = cursor!.string(forColumnIndex: 13)
                temp.doctorName = cursor!.string(forColumnIndex: 14)
                temp.speciality = cursor!.string(forColumnIndex: 15)
    
                arrMainList.append(temp)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
        
        if arrMainList.count == 0 {
            stackNotAvail.isHidden = false
            listview.isHidden = true
        } else {
            stackNotAvail.isHidden = true
            listview.isHidden = false
            listview.reloadData()
        }
    }
    
    @objc func refreshButtonClick() {
        getRecordServer()
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loading.loading())
            
            DashboardRequestManager().getRequest(Constant.BookAppointment.methodLoadAppointList, classRef: self)
        } else {
            _ = SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    func successResponse(_ json: Data) throws {
        
        loading.removeFromSuperview()
        let appPrefs = MySharedPreferences()
        
        let dict = (try! JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSMutableDictionary
        
        appPrefs.setErrorCode(text: 0)
        appPrefs.setErrorCode(text: JsonParser.getJsonValueInt(dict, valueForKey: "code"))
        appPrefs.setErrorMessage(text: JsonParser.getJsonValueString(dict, valueForKey: "message"))
        
        if JsonParser.getJsonValueInt(dict, valueForKey: "code") == 200 {
            
            let datasource = DBOperation()
            datasource.openDatabase(true)
            datasource.TruncateAppointmentList()
    
            let dataArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
            
            for i in 0 ..< dataArray.count {
                guard let subJson = dataArray[i] as? NSDictionary else {
                    continue
                }
                let aptID = JsonParser.getJsonValueString(subJson, valueForKey: "id")
                let doctorID = JsonParser.getJsonValueString(subJson, valueForKey: "doctorID")
                let doctorName = JsonParser.getJsonValueString(subJson, valueForKey: "doctorName")
                let specialityID = JsonParser.getJsonValueString(subJson, valueForKey: "specialityID")
                let aptFor = JsonParser.getJsonValueString(subJson, valueForKey: "aptFor")
                let reminderBy = JsonParser.getJsonValueString(subJson, valueForKey: "reminderBy")
                let date = JsonParser.getJsonValueString(subJson, valueForKey: "date")
                let appointTimeFrom = JsonParser.getJsonValueString(subJson, valueForKey: "appointTimeFrom")
                let appointTimeTo = JsonParser.getJsonValueString(subJson, valueForKey: "appointTimeTo")
                let details = JsonParser.getJsonValueString(subJson, valueForKey: "details")
                let appointmentType = JsonParser.getJsonValueString(subJson, valueForKey: "appointmentType")
                let name = JsonParser.getJsonValueString(subJson, valueForKey: "name")
                let email = JsonParser.getJsonValueString(subJson, valueForKey: "email")
                let mobileNo = JsonParser.getJsonValueString(subJson, valueForKey: "mobileNo")
                let speciality = JsonParser.getJsonValueString(subJson, valueForKey: "speciality")
                
    
                datasource.InsertAppointmentListTable(aptID: aptID, companyID: appPrefs.getCompanyID(), specialID: specialityID, doctorID: doctorID, doctorName: doctorName, date: date, fromTime: appointTimeFrom, toTime: appointTimeTo, aptFor: aptFor, remindBy: reminderBy, details: details, aptType: appointmentType, name: name, email: email, mobileNo: mobileNo, speciality: speciality)
            }
            datasource.closeDatabase()
        }
        appPrefs.setLastAppointmentSycnTime(text: ConvertionClass().currentTime())
        
        //load list
        loadAppointList()
    }
    
    func mErrDialogue(_ message: String) {
        loading.removeFromSuperview()
        _ = SweetAlert().showAlert("popupAlert".localized, subTitle: message, style: .warning)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var currCell: CustomAdapterBookAppointList!  = listview.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? CustomAdapterBookAppointList
        
        if currCell == nil {
            listview.register(UINib(nibName: tableCellIdentifier, bundle: nil), forCellReuseIdentifier: tableCellIdentifier)
            currCell = listview.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? CustomAdapterBookAppointList
        }
        
        currCell.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        return currCell
    }
    
    func moveToBookAppointTimeAct(_ index: Int) throws {
        let temp: ArrAppointDetail = arrMainList[index]
        
        let act = BookAppointTimeAct(nibName: "BookAppointTimeAct", bundle: nil)
        act.doc_id = temp.doctorID
        act.sp_id = temp.specialityID
        act.name = temp.doctorName
        act.speciality = temp.speciality
        act.datePre = temp.date
        act.fromTimePre = temp.appointTimeFrom
        act.toTimePre = temp.appointTimeTo
        act.aptID = temp.aptID
        act.delegate = self
        self.navigationController?.pushViewController(act, animated: true)
    }
    
    func moveToBookAppointCancelAct(_ index: Int) throws {
        let temp: ArrAppointDetail = arrMainList[index]
        
        let act = BookAppointCancelAct(nibName: "BookAppointCancelAct", bundle: nil)
        act.name = temp.doctorName
        act.doc_id = temp.doctorID
        act.speciality = temp.speciality
        act.timeDiff = temp.timeDiff
        act.date = temp.date
        act.fromTime = temp.appointTimeFrom
        act.toTime = temp.appointTimeTo
        act.aptID = temp.aptID
        act.delegate = self
        self.navigationController?.pushViewController(act, animated: true)
    }
    
    @objc func moveToDocSpecialityListAct() {
        let act = DocSpecialityListAct(nibName: "DocSpecialityListAct", bundle: nil)
        act.delegate = self
        self.navigationController?.pushViewController(act, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //back call function on return view controller
    func onViewControllerReturn(data: AnyObject) {
        if (data as! String).equalsIgnoreCase("success") {
            getRecordServer()
        }
    }
    
}
