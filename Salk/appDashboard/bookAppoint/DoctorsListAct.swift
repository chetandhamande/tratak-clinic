//
//  DoctorsListAct.swift
//  Salk
//
//  Created by Chetan  on 20/03/18
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async

protocol DoctorsViewControllerDelegate {
    func onViewControllerReturn(data: AnyObject)
}

class DoctorsListAct: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, BookTimeViewControllerDelegate {
   
    @IBOutlet var listview: UITableView!
    @IBOutlet var edit_search: UISearchBar!
    @IBOutlet weak var txtNotAvail: UILabel!
    
    private var arrDoctors = [ArrDoctor]()
    private var arrTempDoctors = [ArrDoctor]()
    
    var delegate: DoctorsViewControllerDelegate?
    var sp_id: String = ""
    
    let tableCellIdentifier = "CustomAdapterDoctorList"
    private let loading = CustomLoading()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        prepareNavigationItem()
        self.view.backgroundColor = CustomColor.white()
        
        edit_search.placeholder = "Search Doctors"
        edit_search.delegate = self
        
        listview.delegate = self
        listview.dataSource = self
        listview.isHidden = true
        listview.separatorStyle = .none
        
        // Setup the Search Controller
        Async.main(after: 0.1) {
            self.getRecordServer()
        }
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Find & Book".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loading.loading())
            
            DashboardRequestManager().getRequest(Constant.BookAppointment.methodLoadDoctorList, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    public func failGettingInfo() {
        loading.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("popupAlert".localized, subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    public func successResponse(respJson: Data) throws {
        loading.removeFromSuperview()
    
        arrDoctors = [ArrDoctor]()
        arrTempDoctors = [ArrDoctor]()
        
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: respJson, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            if Int(JsonParser.getJsonValueString(dict, valueForKey: "code")) == 200 {
                let dataArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
                
                for i in 0 ..< dataArray.count {
                    guard let subJson = dataArray.object(at: i) as? NSMutableDictionary else {
                        continue
                    }
                    
                    let temp: ArrSpeciality = ArrSpeciality()
                    
                    temp.id = JsonParser.getJsonValueString(subJson, valueForKey: "id")
                    temp.speciality = JsonParser.getJsonValueString(subJson, valueForKey: "speciality")
                    temp.description = JsonParser.getJsonValueString(subJson, valueForKey: "description")
                    temp.reg_date = JsonParser.getJsonValueString(subJson, valueForKey: "reg_date")
                    temp.reg_id = JsonParser.getJsonValueString(subJson, valueForKey: "reg_id")
                    temp.is_delete = JsonParser.getJsonValueString(subJson, valueForKey: "is_delete")
                    temp.status = JsonParser.getJsonValueString(subJson, valueForKey: "status")
                    
                    let docListArray = JsonParser.getJsonValueMutableArray(subJson, valueForKey: "doctorList")

                    for j in 0 ..< docListArray.count {
                        guard let subJson = docListArray.object(at: j) as? NSMutableDictionary else {
                            continue
                        }
                        
                        if temp.id.equals(sp_id) {
                            let doctor = ArrDoctor()
                            doctor.doctorID = JsonParser.getJsonValueString(subJson, valueForKey: "doctorID")
                            doctor.fname = JsonParser.getJsonValueString(subJson, valueForKey: "fname")
                            doctor.lname = JsonParser.getJsonValueString(subJson, valueForKey: "lname")
                            doctor.speciality = temp.speciality;
                            doctor.speciality_id = temp.id;
                            arrTempDoctors.append(doctor);
                        }
                        
                    }
                }
            }
        } catch _ { }
        showResult("")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDoctors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var currCell: CustomAdapterDoctorList!  = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? CustomAdapterDoctorList
        
        if currCell == nil {
            tableView.register(UINib(nibName: tableCellIdentifier, bundle: nil), forCellReuseIdentifier: tableCellIdentifier)
            currCell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? CustomAdapterDoctorList
        }
        
        currCell.onBindViewHolder(arrDoctors, position: indexPath.row, classRef: self)
        
        return currCell
    }
    
    private func showResult(_ search: String) {
        
        arrDoctors = [ArrDoctor]()
        for i in 0 ..< arrTempDoctors.count {
            if (arrTempDoctors[i].speciality.uppercased().contains(search)
                || arrTempDoctors[i].lname.uppercased().contains(search)
                || arrTempDoctors[i].fname.uppercased().contains(search))
            {
                arrDoctors.append(arrTempDoctors[i]);
            }
        }
        
        if search.isEmpty {
            arrDoctors = arrTempDoctors
        }
    
        if arrDoctors.count > 0 {
            listview.isHidden = false
            txtNotAvail.isHidden = true
            listview.reloadData()
        } else {
            listview.isHidden = true
            txtNotAvail.isHidden = false
        }
    }
    
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        showResult(searchText.uppercased())
    }
    
    func moveToBookAppointTimeAct(_ index: Int) throws {
        let temp: ArrDoctor = arrDoctors[index]
        
        let act = BookAppointTimeAct(nibName: "BookAppointTimeAct", bundle: nil)
        act.doc_id = temp.doctorID
        act.sp_id = temp.speciality_id
        act.name = "\(temp.fname) \(temp.lname)"
        act.speciality = temp.speciality
        act.delegate = self
        self.navigationController?.pushViewController(act, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //back call function on return view controller
    func onViewControllerReturn(data: AnyObject) {
        if (data as! String).equalsIgnoreCase("success") {
            self.navigationController?.popViewController(animated: false)
            self.delegate?.onViewControllerReturn(data: "Success" as AnyObject)
        }
    }
    
}
