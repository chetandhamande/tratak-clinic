//
//  CustomAdapterBookAppointList.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomAdapterBookAppointList: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var txt_aptStatus: UILabel!
    @IBOutlet weak var txt_timeRemain: UILabel!
    @IBOutlet weak var txt_special: UILabel!
    @IBOutlet weak var textHead: UILabel!
    @IBOutlet weak var txtAptTime: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnReSched: UIButton!
    
    private var mValues: Array<ArrAppointDetail>! = nil
    private var classRef: AnyObject! = nil
   
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundView?.backgroundColor = CustomColor.white()
        
    }
   
    func onBindViewHolder(_ arrMainList: Array<ArrAppointDetail>, position: Int, classRef: AnyObject) {
    
        mValues = arrMainList
        self.classRef = classRef
        btnReSched.tag = position
        btnCancel.tag = position
        
        let user = mValues[position]
        if user.listLayoutIndex == 0 {
            //Add view to each list

            textHead.text = user.doctorName
            txt_special.text = user.speciality
            txtAptTime.text = user.date + ", " + user.appointTimeFrom

            if !user.timeDiff.contains("-") {
                txt_timeRemain.text = "in " + user.timeDiff
                txt_timeRemain.textColor = CustomColor.google4()
            } else {
                txt_timeRemain.text = "Closed"
                txt_timeRemain.textColor = CustomColor.google1()
            }
        }
    }
    
    //Code for reschedule
    @IBAction func bntReSchedClick(sender: UIButton) {
        do {
            try (classRef as! BookAppointListAct).moveToBookAppointTimeAct(sender.tag)
        } catch {}
    }
    
    //Code for cancel
    @IBAction func bntCancelClick(sender: UIButton) {
        do {
            try (classRef as! BookAppointListAct).moveToBookAppointCancelAct(sender.tag)
        } catch {}
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
