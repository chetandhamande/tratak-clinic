//
//  DocSpecialityListAct.swift
//  Salk
//
//  Created by Chetan  on 20/03/18
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async

protocol DocSpecialityViewControllerDelegate {
    func onViewControllerReturn(data: AnyObject)
}

class DocSpecialityListAct: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, DoctorsViewControllerDelegate  {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var edit_search: UISearchBar!
    @IBOutlet weak var txtNotAvail: UILabel!
    
    var delegate: DocSpecialityViewControllerDelegate?
    
    var arrSpecialities = [ArrSpeciality]()
    var arrTempSpecialities = [ArrSpeciality]()
    
    let searchController = UISearchController(searchResultsController: nil)
    private let loading = CustomLoading()
    
    let tableCellIdentifier = "CustomAdapterSpecialityList"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        prepareNavigationItem()
        
        edit_search.placeholder = "Search Speciality"
        edit_search.delegate = self
        
        Async.main(after: 0.1) {
            self.getRecordServer()
        }
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Find & Book".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loading.loading())
            
            DashboardRequestManager().getRequest(Constant.BookAppointment.methodLoadSpecialList, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    public func failGettingInfo() {
        loading.removeFromSuperview()
        
        let appPrefs = MySharedPreferences()
        SweetAlert().showAlert("popupAlert".localized, subTitle: appPrefs.getErrorMessage(), style: .warning)
    }
    
    public func successResponse(respJson: Data) {
        loading.removeFromSuperview()
        
        arrSpecialities = [ArrSpeciality]()
        arrTempSpecialities = [ArrSpeciality]()
        
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: respJson, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            if Int(JsonParser.getJsonValueString(dict, valueForKey: "code")) == 200 {
                let dataArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
                
                for i in 0 ..< dataArray.count {
                    guard let subJson = dataArray.object(at: i) as? NSMutableDictionary else {
                        continue
                    }
                    
                    let temp: ArrSpeciality = ArrSpeciality()
                    
                    temp.id = JsonParser.getJsonValueString(subJson, valueForKey: "id")
                    temp.speciality = JsonParser.getJsonValueString(subJson, valueForKey: "speciality")
                    temp.description = JsonParser.getJsonValueString(subJson, valueForKey: "description")
                    temp.reg_date = JsonParser.getJsonValueString(subJson, valueForKey: "reg_date")
                    temp.reg_id = JsonParser.getJsonValueString(subJson, valueForKey: "reg_id")
                    temp.is_delete = JsonParser.getJsonValueString(subJson, valueForKey: "is_delete")
                    temp.status = JsonParser.getJsonValueString(subJson, valueForKey: "status")
                
                    arrTempSpecialities.append(temp)
                }
                showResult("")
            }
        } catch _ { }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSpecialities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var currCell: CustomAdapterSpecialityList!  = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? CustomAdapterSpecialityList
        
        if currCell == nil {
            tableView.register(UINib(nibName: tableCellIdentifier, bundle: nil), forCellReuseIdentifier: tableCellIdentifier)
            currCell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? CustomAdapterSpecialityList
        }
        
        let candy: ArrSpeciality = arrSpecialities[indexPath.row]
        currCell.title.text = candy.speciality
    
        return currCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        do {
            try openDoctorSpecility(index: indexPath.row)
        } catch {}
    }
    
    fileprivate func openDoctorSpecility(index: Int) throws {
        let temp: ArrSpeciality = arrSpecialities[index]
        
        let a = DoctorsListAct(nibName: "DoctorsListAct", bundle: nil)
        a.sp_id = temp.id
        a.delegate = self
        self.navigationController?.pushViewController(a, animated: true)
    }
    
    private func showResult(_ search: String) {
        arrSpecialities = [ArrSpeciality]()
        
        for i in 0 ..< arrTempSpecialities.count {
            if (arrTempSpecialities[i].speciality.uppercased().contains(search)
                || arrTempSpecialities[i].description.uppercased().contains(search))
            {
                arrSpecialities.append(arrTempSpecialities[i]);
            }
        }
        
        if search.isEmpty {
            arrSpecialities = arrTempSpecialities
        }
        
        if arrSpecialities.count > 0 {
            tableView.isHidden = false
            txtNotAvail.isHidden = true
            tableView.reloadData()
        } else {
            tableView.isHidden = true
            txtNotAvail.isHidden = false
        }
    }
    
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        showResult(searchText.uppercased())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //back call function on return view controller
    func onViewControllerReturn(data: AnyObject) {
        if (data as! String).equalsIgnoreCase("success") {
            self.navigationController?.popViewController(animated: false)
            self.delegate?.onViewControllerReturn(data: "Success" as AnyObject)
        }
    }
    
}

