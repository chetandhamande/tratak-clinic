//
//  CustomAdapterDoctorList.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomAdapterDoctorList: UITableViewCell {

    @IBOutlet weak var textHead: UILabel!
    @IBOutlet weak var txtDetails: UILabel!
    @IBOutlet weak var btnBook: UIButton!
    
    private var classRef: AnyObject! = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundView?.backgroundColor = CustomColor.white()
    }
    
    public func onBindViewHolder(_ mValues: [ArrDoctor], position: Int, classRef: AnyObject) {
        
        self.classRef = classRef
        btnBook.tag = position
        let user: ArrDoctor = mValues[position]
        
        if user.listLayoutIndex == 0 {
            //Add view to each list
            
            textHead.text = user.fname + " " + user.lname
            
            if user.speciality.isEmpty {
                txtDetails.isHidden = true
            } else {
                txtDetails.isHidden = false
                txtDetails.text = user.speciality
            }
            
        }
    }
    
    @IBAction func bookBtnClick(sender: UIButton) {
        
        if classRef.isKind(of: DoctorsListAct.self) {
            do {
                try (classRef as! DoctorsListAct).moveToBookAppointTimeAct(sender.tag)
            } catch {}
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

