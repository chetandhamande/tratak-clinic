//
//  BookAppointTimeAct.swift
//  Salk
//
//  Created by Chetan  on 20/03/18.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async
import DatePickerDialog
import DropDown

private class ArrTime {
    var fromTime: String = "", toTime = "";
    var fromTimeLong: Double = 0, toTimeLong: Double = 0
}

protocol BookTimeViewControllerDelegate {
    func onViewControllerReturn(data: AnyObject)
}

class BookAppointTimeAct: UIViewController, BookAppointViewControllerDelegate {

    @IBOutlet weak var txtDocName: UILabel!
    @IBOutlet weak var txtSpeciality: UILabel!
    @IBOutlet weak var imgDoctor: UIImageView!
    
    @IBOutlet weak var txtSelectDate: UIButton!
    
    @IBOutlet weak var linMainPanel: UIScrollView!
    @IBOutlet weak var txtNotAvail: UILabel!
    
    @IBOutlet weak var morningPanel: CustomTimeSelection!
    @IBOutlet weak var afterPanel: CustomTimeSelection!
    @IBOutlet weak var eveningPanel: CustomTimeSelection!
    @IBOutlet weak var nightPanel: CustomTimeSelection!
    
    var delegate: BookTimeViewControllerDelegate?
    
    let chooseDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDropDown,
        ]
    }()
    
    private let loading = CustomLoading()
    
    private var arrTimes: Array<ArrTime>! = nil
    private var listMornTimes: Array<String>! = nil, listAfterTimes: Array<String>! = nil, listEvnTimes: Array<String>! = nil, listNightTimes: Array<String>! = nil
    
    var sp_id: String = "", doc_id = "", speciality = "", name = "", aptID = "", datePre = "", fromTimePre = "", toTimePre = ""
    private var dateLong: Double = 0;
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationItem()
        init1()
        
        linMainPanel.isHidden = true
                
        setupDefaultDropDown()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Select time".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    private func init1() {
        txtDocName.text = name
        txtSpeciality.text = speciality
        
        let URL = Foundation.URL(string: "\(SalkProjectConstant().ServeraddExpertImg)\(doc_id.sha1() + Constant.FolderNames.imageExtensionjpg)")!
        imgDoctor.kf.setImage(with: URL, placeholder: nil, options: [.transition(.fade(1))], progressBlock: { receivedSize, totalSize in
            
        }, completionHandler: { image, error, cacheType, imageURL in
            if error != nil {
                self.imgDoctor.image = UIImage().imageWithImage(UIImage(named: "ic_person_white_gray_24dp.png")!, scaledToSize: CGSize(width: 45, height: 45))
            }
        })
        
        //call default of current date
        let df = DateFormatter()
        
        if aptID.isEmpty {
            dateLong = ConvertionClass().currentTime()
        } else {
            df.dateFormat = "dd MMM, yyyy"
            guard let date = df.date(from: datePre) else {
                return
            }
            dateLong = ConvertionClass().conDateToLong(date)
        }
        df.dateFormat = "dd MMM, yyyy"
        let dateStr = ConvertionClass().conLongToDate(dateLong, dateFormat: df)
        
        configureSelectButton(txtSelectDate, setString: dateStr)
        
        getRecordServer();
    }
    
    @IBAction func txtSelectDateClick(_ sender: UIButton) {
        var currLong = ConvertionClass().currentTime()
        
        if dateLong != 0 {
            currLong = dateLong
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, yyyy"
        
        let date = ConvertionClass().conLongToDate(currLong, dateFormat: formatter)
        DatePickerDialog().show(title: date, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd MMM, yyyy"
                
                self.dateLong = ConvertionClass().conDateToLong(dt)
                self.configureSelectButton(self.txtSelectDate, setString: formatter.string(from: dt))
                
                self.getRecordServer()
            }
        }
    }
    
    private func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        //        label.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(CustomColor.textBlackColor(), for: .normal)
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loading.loading())
            
            DashboardRequestManager().getRequest(Constant.BookAppointment.methodLoadAppointSlot, dict: createJsonAppointSlot(), classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    // create JSON values to send it to server
    func createJsonAppointSlot() -> NSMutableDictionary {
        
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let date = ConvertionClass().conLongToDate(dateLong, dateFormat: df)
        
        data = ["osType":"iOS",
                "by":"PATIENT",
                "patientID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "companyID": appUserInfo.getCompanyID(),
                "specialityID": sp_id,
                "doctorID": doc_id,
                "date": date,
                "appointTimeFrom": "00:00:00",
                "appointTimeTo": "23:59:59"
        ]
        
        return data
    }
    
    public func successResponse(_ respJson: Data) throws {
        loading.removeFromSuperview()
        
        arrTimes = [ArrTime]()
        
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: respJson, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            if Int(JsonParser.getJsonValueString(dict, valueForKey: "code")) == 200 {
                let dataJson = JsonParser.getJsonValueMutableDictionary(dict, valueForKey: "data")
                let dataArray = JsonParser.getJsonValueMutableArray(dataJson, valueForKey: "str")
                
                for i in 0 ..< dataArray.count {
                    guard let subJson = dataArray.object(at: i) as? NSMutableDictionary else {
                        continue
                    }
                    
                    let temp = ArrTime();
                    
                    temp.fromTime = JsonParser.getJsonValueString(subJson, valueForKey: "from")
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd"
                    let fromDate = ConvertionClass().conLongToDate(dateLong, dateFormat: df) + " " + temp.fromTime
                    df.dateFormat = "yyyy-MM-dd hh:mm a"
                    if df.date(from: fromDate) != nil {
                        temp.fromTimeLong = ConvertionClass().conDateToLong(df.date(from: fromDate)!)
                    }
            
                    temp.toTime = JsonParser.getJsonValueString(subJson, valueForKey: "to")
                    df.dateFormat = "yyyy-MM-dd"
                    let toDate = ConvertionClass().conLongToDate(dateLong, dateFormat: df) + " " + temp.toTime
                    df.dateFormat = "yyyy-MM-dd hh:mm a"
                    if df.date(from: toDate) != nil {
                        temp.toTimeLong = ConvertionClass().conDateToLong(df.date(from: toDate)!)
                    }

                    arrTimes.append(temp);
                }
                
//                if dataArray.count <= 0 {
//                    let message = JsonParser.getJsonValueString(dataJson, valueForKey: "msg")
//                    txtNotAvail.attributedText = message.html2AttributedString
//                }
            }
        } catch _ { }
        
        if arrTimes.count > 0 {
            linMainPanel.isHidden = false
            txtNotAvail.isHidden = true
            designLayout()
        } else {
            linMainPanel.isHidden = true
            txtNotAvail.isHidden = false
        }
    }
    
    public func failGettingInfo() {
        loading.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("popupAlert".localized, subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    //Method for design layout
    private func designLayout() {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let tempDate12 = ConvertionClass().conLongToDate(dateLong, dateFormat: df) + " " + "12:00 PM"
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        let long12 = ConvertionClass().conDateToLong(df.date(from: tempDate12)!)
        
        df.dateFormat = "yyyy-MM-dd"
        let tempDate4 = ConvertionClass().conLongToDate(dateLong, dateFormat: df) + " " + "04:00 PM"
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        let long4 = ConvertionClass().conDateToLong(df.date(from: tempDate4)!)
        
        df.dateFormat = "yyyy-MM-dd"
        let tempDate8 = ConvertionClass().conLongToDate(dateLong, dateFormat: df) + " " + "08:00 PM"
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        let long8 = ConvertionClass().conDateToLong(df.date(from: tempDate8)!)
        
        df.dateFormat = "yyyy-MM-dd hh:mm a"
        let tempcurrTime = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df)
        let currTime = ConvertionClass().conDateToLong(df.date(from: tempcurrTime)!)

        listAfterTimes = [String]()
        listEvnTimes = [String]()
        listMornTimes = [String]()
        listNightTimes = [String]()
    
        for i in 0 ..< arrTimes.count {
            if arrTimes[i].fromTimeLong < currTime {
                continue
            }
            if arrTimes[i].fromTimeLong < long12 {
                listMornTimes.append(arrTimes[i].fromTime + "  -  " + arrTimes[i].toTime)
                
            } else if arrTimes[i].fromTimeLong >= long12 && arrTimes[i].fromTimeLong < long4 {
                listAfterTimes.append(arrTimes[i].fromTime + "  -  " + arrTimes[i].toTime)
                
            } else if arrTimes[i].fromTimeLong >= long4 && arrTimes[i].fromTimeLong < long8 {
                listEvnTimes.append(arrTimes[i].fromTime + "  -  " + arrTimes[i].toTime)
            
            } else if arrTimes[i].fromTimeLong >= long8 {
                listEvnTimes.append(arrTimes[i].fromTime + "  -  " + arrTimes[i].toTime)
                
            }
        }
        
        morningPanel.isHidden = true
        afterPanel.isHidden = true
        eveningPanel.footerView.isHidden = true
        nightPanel.footerView.isHidden = true
        morningPanel.isHidden = true
        afterPanel.isHidden = true
        eveningPanel.isHidden = true
        nightPanel.isHidden = true
        
        if(listMornTimes.count > 0) {
            morningPanel.isHidden = false
            morningPanel.lblTitle.text = "Morning"
            morningPanel.lblTime.text = "before 12 pm"
            
            morningPanel.imgTimeSlot.image = #imageLiteral(resourceName: "morning.png")
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openMorningFooter))
            morningPanel.headerView.addGestureRecognizer(tapGesture)
            
            morningPanel.dropDownTimeSlot.addTarget(self, action: #selector(openMorningTimeSlot), for: UIControlEvents.touchUpInside)
            morningPanel.btnNext.addTarget(self, action: #selector(openMorningNextOpr), for: UIControlEvents.touchUpInside)
        }
        if(listAfterTimes.count > 0) {
            afterPanel.isHidden = false
            afterPanel.lblTitle.text = "Afternoon"
            afterPanel.lblTime.text = "12 - 4 pm"
            
            afterPanel.imgTimeSlot.image = #imageLiteral(resourceName: "afternoon.png")
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openAfterNoonFooter))
            afterPanel.headerView.addGestureRecognizer(tapGesture)
            
            afterPanel.dropDownTimeSlot.addTarget(self, action: #selector(openAfterNoonTimeSlot), for: UIControlEvents.touchUpInside)
            afterPanel.btnNext.addTarget(self, action: #selector(openAfterNoonNextOpr), for: UIControlEvents.touchUpInside)
        }
        if(listEvnTimes.count > 0) {
            eveningPanel.isHidden = false
            eveningPanel.lblTitle.text = "Evening"
            eveningPanel.lblTime.text = "4 - 8 pm"
            
            eveningPanel.imgTimeSlot.image = #imageLiteral(resourceName: "evening.png")
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openEveningFooter))
            eveningPanel.headerView.addGestureRecognizer(tapGesture)
            
            eveningPanel.dropDownTimeSlot.addTarget(self, action: #selector(openEveningTimeSlot), for: UIControlEvents.touchUpInside)
            eveningPanel.btnNext.addTarget(self, action: #selector(openEveningNextOpr), for: UIControlEvents.touchUpInside)
        }
        if(listNightTimes.count > 0) {
            nightPanel.isHidden = false
            nightPanel.lblTitle.text = "Night"
            nightPanel.lblTime.text = "after 8 pm"
            
            nightPanel.imgTimeSlot.image = #imageLiteral(resourceName: "night.png")
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openNightFooter))
            nightPanel.headerView.addGestureRecognizer(tapGesture)
            
            nightPanel.dropDownTimeSlot.addTarget(self, action: #selector(openNightTimeSlot), for: UIControlEvents.touchUpInside)
            nightPanel.btnNext.addTarget(self, action: #selector(openNightNextOpr), for: UIControlEvents.touchUpInside)
        }
    
        if(listMornTimes.count > 0) {
            showDurationTimeSlot("Morn");
        } else if(listAfterTimes.count > 0) {
            showDurationTimeSlot("AN");
        } else if(listEvnTimes.count > 0) {
            showDurationTimeSlot("Eve");
        } else if(listNightTimes.count > 0) {
            showDurationTimeSlot("Night");
        }
    }
    
    @objc private func openMorningFooter() {
        showDurationTimeSlot("Morn")
    }
    @objc private func openAfterNoonFooter() {
        showDurationTimeSlot("AN")
    }
    @objc private func openEveningFooter() {
        showDurationTimeSlot("Eve")
    }
    @objc private func openNightFooter() {
        showDurationTimeSlot("Night")
    }
    
    private func showDurationTimeSlot(_ clickType: String) {
        morningPanel.footerView.isHidden = true
        afterPanel.footerView.isHidden = true
        eveningPanel.footerView.isHidden = true
        nightPanel.footerView.isHidden = true

        if(clickType.equalsIgnoreCase("Morn")) {
            morningPanel.footerView.isHidden = false
        } else if(clickType.equalsIgnoreCase("AN")) {
            afterPanel.footerView.isHidden = false
        } else if(clickType.equalsIgnoreCase("Eve")) {
            eveningPanel.footerView.isHidden = false
        } else if(clickType.equalsIgnoreCase("Night")) {
            nightPanel.footerView.isHidden = false
        }
    }
    
    @objc private func openMorningTimeSlot() {
        durationPopup(morningPanel.dropDownTimeSlot, mListOption: listMornTimes)
    }
    @objc private func openAfterNoonTimeSlot() {
        durationPopup(afterPanel.dropDownTimeSlot, mListOption: listAfterTimes)
    }
    @objc private func openEveningTimeSlot() {
        durationPopup(eveningPanel.dropDownTimeSlot, mListOption: listEvnTimes)
    }
    @objc private func openNightTimeSlot() {
        durationPopup(nightPanel.dropDownTimeSlot, mListOption: listNightTimes)
    }
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    private func durationPopup(_ button: UIButton, mListOption: [String]) {
        chooseDropDown.anchorView = button
        //chooseDropDown.bottomOffset = CGPoint(x: 0, y: linNightMainPanel.btnTime.bounds.height)
        chooseDropDown.dataSource = mListOption
        chooseDropDown.selectionAction = { [weak self] (index, item) in
            button.setTitle(item, for: .normal)
        }
        
        chooseDropDown.show()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    @objc private func openMorningNextOpr() {
        performNextOperation((morningPanel.dropDownTimeSlot.titleLabel?.text)!, timeType: "Morn")
    }
    @objc private func openAfterNoonNextOpr() {
        performNextOperation((afterPanel.dropDownTimeSlot.titleLabel?.text)!, timeType: "AN")
    }
    @objc private func openEveningNextOpr() {
        performNextOperation((eveningPanel.dropDownTimeSlot.titleLabel?.text)!, timeType: "Eve")
    }
    @objc private func openNightNextOpr() {
        performNextOperation((nightPanel.dropDownTimeSlot.titleLabel?.text)!, timeType: "Night")
    }
    
    private func performNextOperation(_ timeSlotStr: String, timeType: String) {
        if timeSlotStr.isEmpty || timeSlotStr.equalsIgnoreCase("Select Time") {
            SweetAlert().showAlert("popupAlert".localized, subTitle: "Appointment time is missing !", style: .warning)
        } else {
            let act = BookAppointFinalAct(nibName: "BookAppointFinalAct", bundle: nil)
            act.name = name
            act.speciality = speciality
            act.sp_id = sp_id
            act.doc_id = doc_id
            act.date = (txtSelectDate.titleLabel?.text)!
            act.time = timeSlotStr
            act.aptID = aptID
            act.timeSlot = timeType
            act.delegate = self
            self.navigationController?.pushViewController(act, animated: true)
        }
    }
    
    //back call function on return view controller
    func onViewControllerReturn(data: AnyObject) {
        if (data as! String).equalsIgnoreCase("success") {
            self.navigationController?.popViewController(animated: false)
            self.delegate?.onViewControllerReturn(data: "Success" as AnyObject)
        }
    }
    
}
