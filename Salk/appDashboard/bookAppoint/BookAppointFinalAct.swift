//
//  BookAppointFinalAct.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async
import SnapKit
import DropDown

protocol BookAppointViewControllerDelegate {
    func onViewControllerReturn(data: AnyObject)
}

class BookAppointFinalAct: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var txtDocName: UILabel!
    @IBOutlet weak var txtSpeciality: UILabel!
    @IBOutlet weak var imgDoctor: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTimeSlot: UIImageView!
    
    @IBOutlet weak var txtEmail: MKTextField!
    @IBOutlet weak var txtPhoneNumber: MKTextField!
    @IBOutlet weak var txtName: MKTextField!
    @IBOutlet weak var txtDetails: MKTextField!
    
    @IBOutlet weak var btnAppintFor: UIButton!
    @IBOutlet weak var btnReminderBy: UIButton!
    @IBOutlet weak var btnBookApt: UIButton!
    
    private let loading = CustomLoading()
    
    var delegate: BookAppointViewControllerDelegate?
    var sp_id: String = "", speciality = "", doc_id = "", name = "", date = "", time = "", aptID = "", timeSlot = "";
    
    private var listAppointFor: Array<String>! = nil, listReminder: Array<String>! = nil    
    let chooseDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDropDown,
            ]
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let orientation = UIApplication.shared.statusBarOrientation
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.topMargin.equalTo(0)
                    make.leadingMargin.equalTo(0)
                    make.trailingMargin.equalTo(0)
                    make.bottomMargin.equalTo(0)
                    make.width.equalTo(mainStackView)
                })
            }
        }
        
        if UI_USER_INTERFACE_IDIOM() == .phone {
            if orientation == .landscapeLeft || orientation == .landscapeRight {
                
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.height.equalTo(mainStackView)
                })
                
            } else {
                mainScrollView.snp_makeConstraints({ (make: ConstraintMaker) -> Void in
                    make.width.equalTo(mainStackView)
                })
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationItem()
        init1()
        
        setupDefaultDropDown()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Enter contact details".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    private func init1() {
        txtDocName.text = name
        txtSpeciality.text = speciality
        
        let URL = Foundation.URL(string: "\(SalkProjectConstant().ServeraddExpertImg)\(doc_id.sha1() + Constant.FolderNames.imageExtensionjpg)")!
        imgDoctor.kf.setImage(with: URL, placeholder: nil, options: [.transition(.fade(1))], progressBlock: { receivedSize, totalSize in
            
        }, completionHandler: { image, error, cacheType, imageURL in
            if error != nil {
                self.imgDoctor.image = UIImage().imageWithImage(UIImage(named: "ic_person_white_gray_24dp.png")!, scaledToSize: CGSize(width: 45, height: 45))
            }
        })
        
        
        let currDate = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: "dd MMM, yyyy")
        if currDate.equalsIgnoreCase(date) {
            lblTitle.text = "Today  \(time)"
        } else {
            lblTitle.text = "\(date)  \(time)"
        }
        if timeSlot.equalsIgnoreCase("Morn") {
            imgTimeSlot.image = #imageLiteral(resourceName: "morning.png")
        } else if timeSlot.equalsIgnoreCase("AN") {
            imgTimeSlot.image = #imageLiteral(resourceName: "afternoon.png")
        } else if timeSlot.equalsIgnoreCase("Eve") {
            imgTimeSlot.image = #imageLiteral(resourceName: "evening.png")
        } else if timeSlot.equalsIgnoreCase("Night") {
            imgTimeSlot.image = #imageLiteral(resourceName: "night.png")
        }
        
        let appPref = MySharedPreferences()
        txtName.text = "\(appPref.getFirstName()) \(appPref.getLastName())"
        txtEmail.text = appPref.getEmailID()
        txtPhoneNumber.text = appPref.getMobile()
        
        configureTextField(txtName, placeHolder: "Name")
        configureTextField(txtEmail, placeHolder: "Email Address")
        configureTextField(txtPhoneNumber, placeHolder: "Mobile Number")
        configureTextField(txtDetails, placeHolder: "Details")
        
        let touch = UITapGestureRecognizer(target: self, action:#selector(dismissKeyboard))
        self.view.addGestureRecognizer(touch)
        
        listAppointFor = [String]()
        listAppointFor.append("First Consultation")
        listAppointFor.append("Followup")
        listAppointFor.append("None")
        
        listReminder = [String] ()
        listReminder.append("Email")
        listReminder.append("SMS")
        listReminder.append("Notification")
        
        if !aptID.isEmpty {
            btnBookApt.setTitle("DONE", for: .normal)
            
            do {
                try loadPrevRec()
            } catch {}
        }
    }
    
    private func loadPrevRec() throws {
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let sql = "Select * " + " from " + datasource.AppointmentList_tlb + " where " + datasource.dbAptID + " = '" + aptID + "'"
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            if cursor!.next() {
                btnAppintFor.setTitle(cursor!.string(forColumnIndex: 7), for: .normal)
                btnReminderBy.setTitle(cursor!.string(forColumnIndex: 8), for: .normal)
                txtDetails.text = cursor!.string(forColumnIndex: 9)
                
                btnAppintFor.setTitleColor(Color.black, for: .normal)
                btnReminderBy.setTitleColor(Color.black, for: .normal)
            }
            cursor!.close()
        }
        datasource.closeDatabase()
    }
    
    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func configureTextField(_ textField : UITextField, placeHolder: String) {
        textField.delegate = self
        
        if textField == txtDetails {
            textField.returnKeyType = UIReturnKeyType.done
        } else {
            textField.returnKeyType = UIReturnKeyType.next
        }
        
        //textField.layer.borderColor = UIColor.clear.cgColor
        textField.placeholder = placeHolder
        textField.backgroundColor = UIColor.white
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.autocapitalizationType = .words
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func selectAppointmentFor(button: UIButton) {
        durationPopup(btnAppintFor, mListOption: listAppointFor)
    }
    @IBAction func selectReminderBy(button: UIButton) {
        durationPopup(btnReminderBy, mListOption: listReminder)
    }
    
    func setupDefaultDropDown() {
        DropDown.setupDefaultAppearance()
        
        dropDowns.forEach {
            $0.cellNib = UINib(nibName: "DropDownCell", bundle: Bundle(for: DropDownCell.self))
            $0.customCellConfiguration = nil
        }
    }
    
    private func durationPopup(_ button: UIButton, mListOption: [String]) {
        chooseDropDown.anchorView = button
        //chooseDropDown.bottomOffset = CGPoint(x: 0, y: linNightMainPanel.btnTime.bounds.height)
        chooseDropDown.dataSource = mListOption
        chooseDropDown.selectionAction = { [weak self] (index, item) in
            button.setTitle(item, for: .normal)
            button.setTitleColor(Color.black, for: .normal)
        }
        
        chooseDropDown.show()
    }
    
    @IBAction func performBookAppointment(button: UIButton) {
        dismissKeyboard()
        
        if (txtName.text?.isEmpty)! {
            errorDialogue("Missing name")
        } else if (txtEmail.text?.isEmpty)! {
            errorDialogue("Missing email")
        } else if (txtPhoneNumber.text?.isEmpty)! {
            errorDialogue("Missing mobile number")
        } else if (btnAppintFor.titleLabel?.text?.isEmpty)! || (btnAppintFor.titleLabel?.text?.equalsIgnoreCase("Appointment for"))! {
            errorDialogue("Missing appointment for")
        } else if (btnReminderBy.titleLabel?.text?.isEmpty)! || (btnReminderBy.titleLabel?.text?.equalsIgnoreCase("Reminder by"))! {
            errorDialogue("Missing remider by")
        } else {
            let checkConn = CheckInternetConnection()
            if checkConn.isCheckInternetConnection() {
                self.view.addSubview(loading.loading())
                
                do {
                    if(aptID.isEmpty) {
                        try DashboardRequestManager().getRequest(Constant.BookAppointment.methodAddAppoint, dict: createJsonAppointSlot(), classRef: self)
                    } else {
                        try DashboardRequestManager().getRequest(Constant.BookAppointment.methodAppointResched, dict: createJsonAppointSlot(), classRef: self)
                    }
                } catch {}
            } else {
                errorDialogue("noInternet".localized)
            }
        }
    }
    
    // create JSON values to send it to server
    func createJsonAppointSlot() throws -> NSMutableDictionary {
        
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        let df = DateFormatter()
        df.dateFormat = "dd MMM, yyyy"
        let dateLong = ConvertionClass().conDateToLong(df.date(from: date)!)
        
        let timeArr = time.split("-")
        
        data = ["osType":"iOS",
                "by":"PATIENT",
                "patientID": appUserInfo.getUserID(),
                "phonebookID": appUserInfo.getPhonebookID(),
                "companyID": appUserInfo.getCompanyID(),
                "specialityID": sp_id,
                "doctorID": doc_id,
                "date": ConvertionClass().conLongToDate(dateLong, dateFormat: "yyyy-MM-dd"),
                "fromTime": timeArr[0].trim(), //e.g. 8:30 AM
                "toTime": timeArr[1].trim(), //e.g. 8.45 AM
                "aptFor": btnAppintFor.titleLabel?.text as Any, //e.g. First Consultation,Followup,None
                "reminderBy": btnReminderBy.titleLabel?.text as Any, //e.g. SMS,Email,Notification
                "details": txtDetails.text as Any,
                "aptType": "ONLINE", //e.g. ONLINE
                "name": txtName.text as Any,
                "email": txtEmail.text as Any,
                "mobileNo": txtPhoneNumber.text as Any
        ]
        
        if !aptID.isEmpty {
            data.setValue(aptID, forKey: "id")
        }
                
        return data
    }
    
    // Alert Box to Show Success Messages
    private func errorDialogue(_ message: String) {
        SweetAlert().showAlert("Oops...".localized, subTitle: message, style: .warning, buttonTitle: "OK")
    }
    
    public func successResponse(respJson: Data) throws {
        loading.removeFromSuperview()
        
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: respJson, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            if Int(JsonParser.getJsonValueString(dict, valueForKey: "code")) == 200 {
                
                if aptID.isEmpty {
                    mSuccDialogue("bookAppointSuccess".localized)
                } else {
                    mSuccDialogue("editAppointSuccess".localized)
                }
            }
        } catch _ { }
    }
    
    // Alert Box to Show Success Messages
    private func mSuccDialogue(_ message: String) {
        SweetAlert().showAlert("popupSuccess".localized, subTitle: message, style: .success, buttonTitle: "OK") { click in
            self.navigationController?.popViewController(animated: false)
            self.delegate?.onViewControllerReturn(data: "Success" as AnyObject)
        }
    }
    
    public func failGettingInfo() {
        loading.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        errorDialogue(appPref.getErrorMessage())
    }
    
}
