//
//  JsonParser.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

open class JsonParser {
    
    class func getJsonValueString(_ subJson: NSMutableDictionary, valueForKey: String) -> String {
        let output = subJson.value(forKey: valueForKey)
        if(output == nil) {
            return ""
        } else {
            return "\(subJson.value(forKey: valueForKey)!)"
        }
    }
    class func getJsonValueInt(_ subJson: NSMutableDictionary, valueForKey: String) -> Int {
        guard let output = subJson.value(forKey: valueForKey) as? Int else {
            return 0
        }
        
        return output
    }
    class func getJsonValueDouble(_ subJson: NSMutableDictionary, valueForKey: String) -> Double {
        guard let output = subJson.value(forKey: valueForKey) as? Double else {
            return 0.0
        }
        
        return output
    }
    class func getJsonValueMutableDictionary(_ subJson: NSMutableDictionary, valueForKey: String) -> NSMutableDictionary {
        guard let output = subJson.value(forKey: valueForKey) as? NSMutableDictionary else {
            return NSMutableDictionary()
        }
        
        return output
    }
    class func getJsonValueMutableArray(_ subJson: NSMutableDictionary, valueForKey: String) -> NSMutableArray {
        guard let output = subJson.value(forKey: valueForKey) as? NSMutableArray else {
            return NSMutableArray()
        }
        
        return output
    }
    
    
    
    class func getJsonValueString(_ subJson: NSDictionary, valueForKey: String) -> String {
        guard let output = subJson.value(forKey: valueForKey) as? String else {
            return ""
        }
        
        return output
    }
    class func getJsonValueInt(_ subJson: NSDictionary, valueForKey: String) -> Int {
        guard let output = subJson.value(forKey: valueForKey) as? Int else {
            return 0
        }
        
        return output
    }
    class func getJsonValueDouble(_ subJson: NSDictionary, valueForKey: String) -> Double {
        guard let output = subJson.value(forKey: valueForKey) as? Double else {
            return 0.0
        }
        
        return output
    }
    class func getJsonValueMutableDictionary(_ subJson: NSDictionary, valueForKey: String) -> NSDictionary {
        guard let output = subJson.value(forKey: valueForKey) as? NSDictionary else {
            return NSDictionary()
        }
        
        return output
    }
    class func getJsonValueMutableArray(_ subJson: NSDictionary, valueForKey: String) -> NSMutableArray {
        guard let output = subJson.value(forKey: valueForKey) as? NSMutableArray else {
            return NSMutableArray()
        }
        
        return output
    }
    
}
