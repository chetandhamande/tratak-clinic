//
//  SalkProjectConstant.swift
//  Salk
//
//  Created by Chetan  on 14/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class SalkProjectConstant: NSObject {
    
    public let isDebugMode: Bool = false
    
    public let userName: String = "sapi"
    public let password: String = "Serving@world/-"
    
    public let XMPPLink = "im.salk.healthcare"
    public let XMPPPort: UInt16 = 5222
    public let XMPPKeyWord = "salk"
    
    //databse Version Number
    public let dbVersion: Int = 3
    
    
    /*-----Server Informations---------*/
    
    public let Serveradd: String = "https://api.salk.healthcare/api/aaa/api_v1/"; //new1_1
    
    public let ServeraddFTP: String = "https://api.salk.healthcare/api/aaa/"; // new2
    
    public let ServeraddCI: String = "https://api.salk.healthcare/api/bba/index.php/patient/api/v2/";  //new3
    
    public let ServeraddFTPCI: String = "https://file.salk.healthcare/"; //old4
    
    public let ServeraddExpertImg: String = "https://file.salk.healthcare/assets/savefiles/doctor/profile/"
    
    public let ServerFitnessProfileImg: String = "https://file.salk.healthcare/assets/savefiles/patient/profile/"
    
    public let ServerFitnessChatAttach = "https://file.salk.healthcare/assets/savefiles/"
    
}
