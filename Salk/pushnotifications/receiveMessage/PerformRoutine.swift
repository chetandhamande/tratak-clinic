//
//  PerformRoutine.swift
//  Salk
//
//  Created by Salk on 04/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class PerformRoutine {
    
    func performRoutineOpr(_ data: NSMutableDictionary) throws {
        
        let appPref = MySharedPreferences()
        if !appPref.getIsPremiumPlanActive() {
            return
        }
        
        var expertName = "", dateTime = ""
        
        let datasource = DBOperation()
        datasource.openDatabase(true)
        
        var scheduleList = [String]()
        var arrScheduleTypeList = [String]()
        var sql = "Select * from " + datasource.ScheduleList_tlb
        var cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                arrScheduleTypeList.append(cursor!.string(forColumnIndex: 0))
                scheduleList.append(cursor!.string(forColumnIndex: 2))
            }
            cursor?.close()
        }
        
        //extract Data from Json
        for k in 0 ..< scheduleList.count {
            let recName = scheduleList[k]
            let recType = arrScheduleTypeList[k]
            
            let scheduleJson = JsonParser.getJsonValueMutableDictionary(data, valueForKey: recName)
            if scheduleJson.count <= 0 {
                continue
            }
            
            //daily goal
            let type = JsonParser.getJsonValueString(scheduleJson, valueForKey: "type")
            let title = JsonParser.getJsonValueString(scheduleJson, valueForKey: "title")
            let body = JsonParser.getJsonValueString(scheduleJson, valueForKey: "note")
            expertName = JsonParser.getJsonValueString(scheduleJson, valueForKey: "expertName")
            dateTime = JsonParser.getJsonValueString(scheduleJson, valueForKey:  "dateTime")
            if dateTime.isEmpty { continue }
            
            dateTime =  "\(ConvertionClass().conDateToLong(dateTime))"
            
            let jsonArrList = JsonParser.getJsonValueMutableArray(scheduleJson, valueForKey: "list")
            if jsonArrList.count > 0 {
                let recID = datasource.InsertRecentDashboardList(recName: title, recDesc: body, recType: type, dateTime: dateTime)
                
                if (!recID.isEmpty && recID != "0") {
                    if(recType.equalsIgnoreCase("food")) {
                        var time = JsonParser.getJsonValueString(scheduleJson, valueForKey: "time") //reminder time
                        
                        if !time.equalsIgnoreCase("12:00 am") {
                            let foodName = appPref.getFoodRemName().components(separatedBy: ",")
                            var foodVal = appPref.getFoodRemValue().components(separatedBy: ",")
                            for i in 0 ..< foodName.count {
                                if(foodName[i] == recName) {
                                    foodVal[i] = time
                                    break
                                }
                            }
                            
                            var val = "", nameVal = ""
                            for i in 0 ..< foodName.count {
                                val += val.isEmpty ? foodVal[i] : "," + foodVal[i]
                                nameVal += nameVal.isEmpty ? foodName[i] : "," + foodName[i]
                            }
                            appPref.setFoodRemValue(text: val)
                            appPref.setFoodRemName(text: nameVal)
                            
                            //update Schedule Tables
                            time =  "\(ConvertionClass().conDateToLong(time, formate: "hh:mm a"))"
                            datasource.UpdateScheduleTimeList(time: time, scheduleName: recName)
                        }
                    }
                    
                    
                    //get previously selected optionID, before truncate
                    var preSelOptionIDs = ""
                    sql = "SELECT " + datasource.dbOptionID + " FROM " + datasource.recDashboardSubList_tlb + " WHERE " + datasource.recID + " = " + recID + " AND " + datasource.dbIsChecked + " = '1' "
                    cursor = datasource.selectRecords(sql)
                    if cursor != nil {
                        while cursor!.next() {
                            preSelOptionIDs += preSelOptionIDs.isEmpty ? "'" + cursor!.string(forColumnIndex: 0) + "'" : ",'" + cursor!.string(forColumnIndex: 0) + "'"
                        }
                        cursor?.close()
                    }
                    
                    datasource.TruncateRecentDashboardSubList(recIDStr: recID)
                    
                    for i in 0 ..< jsonArrList.count {
                        guard let jsonObj = jsonArrList[i] as? NSMutableDictionary else {
                            continue
                        }
                        
                        let optionID = JsonParser.getJsonValueString(jsonObj, valueForKey: "optionID")
                        let optionName = JsonParser.getJsonValueString(jsonObj, valueForKey: "optionName")
                        
                        var days = ""
                        let daysJson = JsonParser.getJsonValueMutableArray(jsonObj, valueForKey: "days")
                        for j in 0 ..< daysJson.count {
                            guard let tempVal = daysJson[j] as? String else {
                                continue
                            }
                            
                            days += days.isEmpty ? tempVal : ", \(tempVal)"
                        }
                        
                        let optionJson = JsonParser.getJsonValueMutableArray(jsonObj, valueForKey: "combo")
                        for j in 0 ..< optionJson.count {
                            guard let optSubJson = optionJson[j] as? NSMutableDictionary else {
                                continue
                            }
                         
                            let id = JsonParser.getJsonValueString(optSubJson, valueForKey: "foodName")
                            let quanti = JsonParser.getJsonValueString(optSubJson, valueForKey: "quanti")
                            let desc = JsonParser.getJsonValueString(optSubJson, valueForKey: "desc") // on Activity
                            
                            var videoLink = ""
                            videoLink = JsonParser.getJsonValueString(optSubJson, valueForKey: "videoLink")
                            if (videoLink == "NA") {
                                videoLink = ""
                            }
                            
                            var containID = ""
                            containID = JsonParser.getJsonValueString(optSubJson, valueForKey: "contain") // on Food
                            
                            datasource.InsertRecentDashboardSubList(actID: id, recIDStr: recID, qunti: quanti, contID: containID, desc: desc,
                                                                    optID: optionID, optName: optionName, videoLink: videoLink, msg: "", days: days)
                        }
                    }
                    
                    //update previously selected option ids
                    if (!preSelOptionIDs.isEmpty) {
                        datasource.UpdateRecentDashboardSubMultipleList(isChecked: "1", optionIDs: preSelOptionIDs, recIDStr: recID)
                    }
                }
            }
        }
        
        let msgTitle = "New Routine allotted"
        datasource.InsertNotificationList(msgID: "\(ConvertionClass().currentTime())", msg: "By " + expertName, msgTitle: msgTitle, dateTime: dateTime, imageName: "")
        
        datasource.closeDatabase();
        
    }
}
