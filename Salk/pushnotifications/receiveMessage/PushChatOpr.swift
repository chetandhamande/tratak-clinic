//
//  PushChatOpr.swift
//  Salk
//
//  Created by Salk on 04/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class PushChatOpr {
    
    func PerformPushChatOpration(_ data: NSMutableDictionary) throws {
        
        let msg = JsonParser.getJsonValueString(data, valueForKey: "msg")
        let expertID = JsonParser.getJsonValueString(data, valueForKey: "expertID")
        let expertName = JsonParser.getJsonValueString(data, valueForKey: "expertName")
        let msgID = JsonParser.getJsonValueString(data, valueForKey: "msgID")
        var dateTime = JsonParser.getJsonValueString(data, valueForKey: "dateTime")
        let msgType = JsonParser.getJsonValueString(data, valueForKey: "msgType")
        let fileName = JsonParser.getJsonValueString(data, valueForKey: "fileName")
        
        if !msgID.isEmpty {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            dateTime =  "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime)!))"
            
            let datasource = DBOperation()
            datasource.openDatabase(true)
            datasource.InsertExpertCommunicationList(msgID: msgID, msg: msg, expID: expertID, expName: expertName, dateTime: dateTime, isInbox: "1", isRead: "0", fileName: fileName, msgType: msgType, IsServerSync: "1")
            datasource.closeDatabase()
        }
    }    
}

