//
//  ReminderTimeCell.swift
//  Salk
//
//  Created by Chetan  on 20/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class ReminderTimeCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func onBindViewHolder(_ featureList: [ArrReminder], position: Int, classRef: AnyObject)
    {
        let user: ArrReminder = featureList[position]
        
        self.lblTitle.text = user.title
        self.lblDetails.text = user.details
    }
}
