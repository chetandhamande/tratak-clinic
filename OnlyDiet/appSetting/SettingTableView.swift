//
//  SettingTableView.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class SettingTableView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var backNavButton: UIButton!
    @IBOutlet weak var settingTableView: UITableView!
    
    var settingArray : [String?] = []
    var settingImageArray : [String?] = []
    
    let alertObj = SweetAlert()
    let mysharedObj = MySharedPreferences()
    
    let optionTableCellIdentifier = "OptionsTableViewCell"
    let socialTableCellIdentifier = "SocialTableViewCell"
    let profileTableCellIdentifier = "ProfileTableViewCell"
    
    var moreButton: IconButton? = nil
    
    fileprivate var loadingObj = CustomLoading()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        settingArray = ["pro", "naviSyncAppGoal".localized, "naviReminders".localized, "naviUpdateDB".localized, "naviShareApp".localized + " " + "app_name".applocalized, "naviFeedback".localized, "naviLogout".localized, ""]
        
        settingImageArray = ["pro", "sync_apps_devices.png", "reminder.png", "update_db.png","ic_share_black.png", "feedback.png", "logout.png",""]
        
        settingTableView.delegate = self
        settingTableView.dataSource = self
        
        settingTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.viewConfiguration()
        
        self.prepareNavigationItem()
    }
    
    func viewConfiguration() {

    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviMenuSetting".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.titleLabel.drawText(in: UIEdgeInsetsInsetRect(navigationItem.titleLabel.frame, UIEdgeInsetsMake(0, 20, 0, 20)))
    }
    
    func navBackSettingClick() {
        let mainObj = MainViewController()
        mainObj.moveToDecideAppCondition()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if settingArray[indexPath.row] == "" {
            var settingCell: SocialTableViewCell!  = settingTableView.dequeueReusableCell(withIdentifier: socialTableCellIdentifier) as? SocialTableViewCell
            
            if settingCell == nil {
                settingTableView.register(UINib(nibName: socialTableCellIdentifier, bundle: nil), forCellReuseIdentifier: socialTableCellIdentifier)
                settingCell = settingTableView.dequeueReusableCell(withIdentifier: socialTableCellIdentifier) as? SocialTableViewCell
            }
            
            return settingCell
        } else if settingArray[indexPath.row] == "pro" {
            if #available(iOS 10.0, *) {
                var settingCell: ProfileTableViewCell!  = settingTableView.dequeueReusableCell(withIdentifier: profileTableCellIdentifier) as? ProfileTableViewCell
                
                if settingCell == nil {
                    settingTableView.register(UINib(nibName: profileTableCellIdentifier, bundle: nil), forCellReuseIdentifier: profileTableCellIdentifier)
                    settingCell = settingTableView.dequeueReusableCell(withIdentifier: profileTableCellIdentifier) as? ProfileTableViewCell
                }
                settingCell.getRefs(self)
                
                return settingCell
            } else {
                // Fallback on earlier versions
                
                return UITableViewCell()
                
            }
        } else {
            var settingCell: OptionsTableViewCell! = settingTableView.dequeueReusableCell(withIdentifier: optionTableCellIdentifier) as? OptionsTableViewCell
            
            if settingCell == nil {
                settingTableView.register(UINib(nibName: optionTableCellIdentifier, bundle: nil), forCellReuseIdentifier: optionTableCellIdentifier)
                settingCell = tableView.dequeueReusableCell(withIdentifier: optionTableCellIdentifier) as? OptionsTableViewCell
            }
            
            if settingArray[indexPath.row] == "naviMenuHelp".localized {
                settingCell.accessoryType = .disclosureIndicator
            }
            
            settingCell.optionLabel.text = settingArray[indexPath.row]
            settingCell.optionImage.image = UIImage(named: settingImageArray[indexPath.row]!)
            
            return settingCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if settingArray[indexPath.row] == "pro" {
            let helpViewController = ProfileSettingHealthInfoVC(nibName: "SettingTableView", bundle: nil)
            self.navigationController?.pushViewController(helpViewController, animated: true)
            
        } else if settingArray[indexPath.row] == "naviMenuHelp".localized {
//            let helpViewController = HelpTableView(nibName: "HelpTableView", bundle: nil)
//            self.navigationController?.pushViewController(helpViewController, animated: true)
            
        } else if settingArray[indexPath.row] == "naviSyncAppGoal".localized {
            let feedbackView = FitnessBandConnectVC(nibName: "FitnessBandConnectVC", bundle: nil)
            self.navigationController?.pushViewController(feedbackView, animated: true)
        } else if settingArray[indexPath.row] == "naviReminders".localized {
            let feedbackView = ReminderVC(nibName: "ReminderVC", bundle: nil)
            self.navigationController?.pushViewController(feedbackView, animated: true)
        } else if settingArray[indexPath.row] == "helpMenuFeedback".localized {
            let feedbackView = FeedbackViewController(nibName: "FeedbackViewController", bundle: nil)
            self.navigationController?.pushViewController(feedbackView, animated: true)
        } else if settingArray[indexPath.row] == "naviShareApp".localized + " " + "app_name".applocalized
        {
            let url = URL(string: "itms-apps://itunes.apple.com/app/id" + "app_id".applocalized)
            let objectToShare = [url as Any]
            
            let controller = UIActivityViewController(activityItems: objectToShare, applicationActivities: nil)
            self.present(controller, animated: true, completion: nil)
        } else if settingArray[indexPath.row] == "helpMenuRate".localized {
            UIApplication.shared.openURL(URL(string : "itms-apps://itunes.apple.com/app/id" + "app_id".applocalized)!)
        } else if settingArray[indexPath.row] == "naviUpdateDB".localized {
            self.performUpdateDB()
            
        } else if settingArray[indexPath.row] == "setMenuLogout".localized {
            alertObj.showAlert("logout".localized, subTitle: "logoutMsg".localized , style: AlertStyle.none, buttonTitle:"confirm".localized, buttonColor: PrimaryColor.colorPrimary(), otherButtonTitle:  "btnCancel".localized, otherButtonColor: UIColor.colorFromRGB(0xD0D0D0) ) { (isOtherButton) -> Void in
            
                if isOtherButton == true {
                    self.resetAllData()
                    self.cancelAllReminders()
                    
                    let viewController = MainViewController(nibName: "MainViewController", bundle: nil)
//                    let navController = UINavigationController(rootViewController: viewController)
                    
                    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window!.rootViewController = viewController
                    appDelegate.window!.makeKeyAndVisible()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if settingArray[indexPath.row] == "" {
            return 160
        } else if settingArray[indexPath.row] == "pro" {
            return 170
        } else {
            return 50
        }
    }
    
    func moveToProfileViewController() {
        let profileViewController = ProfileSettingHealthInfoVC(nibName: "SettingTableView", bundle: nil)
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    func resetAllData() {
        var appPref = MySharedPreferences()
        let apnToken = appPref.getAPNToken()
        
        for key in UserDefaults.standard.dictionaryRepresentation().keys {
            UserDefaults.standard.removeObject(forKey: key)
        }
        FileManagement.deleteFolder("/natures_five.sqlite")
        
        //set apn token key in PList again
        appPref = MySharedPreferences()
        appPref.setAPNToken(apnToken)
    }
    
    fileprivate func cancelAllReminders() {
        UIApplication.shared.cancelAllLocalNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension SettingTableView {
    
    fileprivate func performUpdateDB() {
        alertObj.showAlert("naviUpdateDB".localized, subTitle: "Are you sure?  You want to update food database", style: AlertStyle.none, buttonTitle:"OK", buttonColor: PrimaryColor.colorPrimary(), otherButtonTitle:  "btnCancel".localized, otherButtonColor: UIColor.colorFromRGB(0xD0D0D0) ) { (isOtherButton) -> Void in
            
            if isOtherButton == true {
                let isInternet = CheckInternetConnection().isCheckInternetConnection()
                if (isInternet) {
                    self.view.addSubview(self.loadingObj.loading())
                    
                    FitnessRequestManager().getRequest(Constant.Fitness.MethodDownloadFoodLog, classRef: self)
                } else {
                    SweetAlert().showOnlyAlert("Oops...", subTitle: "noInternet", style: AlertStyle.none, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
                }
            }
        }
    }
    
    func refreshFoodList() {
        FitnessRequestManager().getRequest(Constant.Fitness.MethodDownloadFoodContainer, classRef: self)
    }
    
    func refreshFoodContinerList() {
        FitnessRequestManager().getRequest(Constant.Fitness.MethodDownloadActivityLog, classRef: self)
    }
    
    func refreshActivityListDone() {
        self.stopProgessList()
        
        SweetAlert().showOnlyAlert("Success", subTitle: "Your Food database updated successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    func stopProgessList() {
        loadingObj.removeFromSuperview()
    }
}
