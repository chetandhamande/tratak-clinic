//
//  TimelineTableVC.swift
//  Salk
//
//  Created by Chetan  on 16/06/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import Async

class BlogDashboardVC: UITableViewController {
        
    fileprivate var arrMainList = [ArrBlog]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appPref = MySharedPreferences()
        appPref.setLastDashboardTypeOpen(text: "Blog")
        
        let CustomAdapterBlogCellNib = UINib(nibName: "CustomAdapterBlogCell", bundle: Bundle(for: CustomAdapterBlogCell.self))
        self.tableView.register(CustomAdapterBlogCellNib, forCellReuseIdentifier: "CustomAdapterBlogCell")
        
        self.tableView.estimatedRowHeight = 300
        self.tableView.rowHeight = UITableViewAutomaticDimension 
        
        self.prepareNavigationItem()
        
        self.loadValues()
        
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Health Feed"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.titleLabel.drawText(in: UIEdgeInsetsInsetRect(navigationItem.titleLabel.frame, UIEdgeInsetsMake(0, 20, 0, 20)))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomAdapterBlogCell", for: indexPath) as! CustomAdapterBlogCell
        
        cell.onBindViewHolder(arrMainList, position: indexPath.row)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.playVideoPopup(arrBlog: arrMainList[indexPath.row])
    }    
}

extension BlogDashboardVC {
    
    func playVideoPopup(arrBlog: ArrBlog) {
        let desktopPopUp = VideoPopupBlogVC(nibName: "VideoPopupBlogVC", bundle: nil)
        desktopPopUp.arrBlog = arrBlog
        desktopPopUp.showAlert()
    }
    
    func loadValues() {
        
        arrMainList = [ArrBlog]()
        
        var temp = ArrBlog()
        temp.title = "Exercise options for Busy Executives"
        temp.videoID = "PuRi7bIVENo"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Exercise for toned Thighs"
        temp.videoID = "95FAHuVc19E"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Tips For Newbie Runners"
        temp.videoID = "B4vaaPVvapU"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Workout During Lunch Hours"
        temp.videoID = "2rb7Xfz2p10"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Yoga poses During Pregnancy"
        temp.videoID = "f8iaL2c8cUo"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Easy-To-Do Yoga Poses For Stress Relief"
        temp.videoID = "5dGqdncoanc"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "How to Do a Calf Raise"
        temp.videoID = "WhPG3jbFEG0"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Kick out the Stress"
        temp.videoID = "0aPRcNOTliU"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "The Blissful Sleep"
        temp.videoID = "m-j8ntTltfk"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "How To Deal with Stress"
        temp.videoID = "Us9ZZmzqa1U"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "5 winter tips for healthy living"
        temp.videoID = "h7fH6BYP5Uc"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Combating winter tiredness"
        temp.videoID = "w51O47gvmlw"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "BREAST CANCER SYMPTOMS"
        temp.videoID = "T8LxGA3Khsk"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Diabetes symptoms in men"
        temp.videoID = "CCDgl3mYOro"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "The ultimate Pre-Pregnancy to-do list."
        temp.videoID = "d2gMFT_wfp4"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Defeat Depression"
        temp.videoID = "Pfer6obaOaY"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Make your Teeth and Gums Stronger"
        temp.videoID = "e1lVS6FI3RU"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Drinking Water at the Right Time"
        temp.videoID = "urcqO0uscTQ"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies For Winter Skin Hazards"
        temp.videoID = "xxP4q-5d8tA"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "How to Get Rid of Skin Pigmentation"
        temp.videoID = "BnHLWIEbQ1U"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "How To Get Rid Of Blackheads"
        temp.videoID = "BAFwstdJXiY"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies For Dandruff"
        temp.videoID = "sE51MCW8TM8"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies for Dry Hair"
        temp.videoID = "HCq0IfwgscA"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Benefits of Boiled Water"
        temp.videoID = "g7oYspQqzjk"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Health benefits of Mushrooms"
        temp.videoID = "es0T2EdzKqA"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Benefits Of Waking Up Early"
        temp.videoID = "lpU4DYd4jzo"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Benefits of Egg"
        temp.videoID = "wVE26iGm7p0"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Benefits of Beetroot"
        temp.videoID = "M98PC-WNaRE"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Benefits of Carrot"
        temp.videoID = "-V2V98303r0"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Benefits of Good Food"
        temp.videoID = "D3n9r26TxbM"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Benefits of Sound Sleep"
        temp.videoID = "B9GsP8QryE4"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Benefits of Apple Cider Vinegar"
        temp.videoID = "Zerzm-CWGc8"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Benefits of lemon detox diet"
        temp.videoID = "T1Q1yI6HgJk"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Pets makes you healthier and happier"
        temp.videoID = "vCdp1Capc1o"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "The Benefits of Outdoor Play"
        temp.videoID = "K1QFJSn1odk"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "PAPAYA"
        temp.videoID = "6ezmPCkondE"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Amazing Health Benefits of Cucumbers"
        temp.videoID = "5F2MBqs9b60"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Powerful Health Benefits of Asparagus"
        temp.videoID = "1DLBaxiUwU4"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Benefits of Almonds"
        temp.videoID = "UG7Ckgh5yy4"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Peanuts - The World's Healthiest Food"
        temp.videoID = "TXI51UhG3EM"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Amazing Benefits and Uses Of Cumin (Jeera)"
        temp.videoID = "SLnxzxd1t2c"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Power Food You should eat this Winter"
        temp.videoID = "CErDLYAwGWs"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Healthy diet during pregnancy"
        temp.videoID = "CqEv2QKEJIk"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Why I use Aloe Vera Daily"
        temp.videoID = "voNm-oPmgtg"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Best foods for Cold and Cough"
        temp.videoID = "IMn_eh67OnE"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Food options for changing weather"
        temp.videoID = "hAMtd11r-OU"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Why Protein Is Important"
        temp.videoID = "9Okk25gAs1k"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Super Foods For Babies"
        temp.videoID = "T8JtsrucHrU"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "10 No Cook Dinner Ideas"
        temp.videoID = "Hqiap39ZMII"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Healthy Fats: A guide to Oils, Butter and More"
        temp.videoID = "2XRVHl6bdsU"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Healthy Food Options For Ladies"
        temp.videoID = "-6Y_c_bl0y0"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Best Food For the Belly"
        temp.videoID = "uWNCgoP14uk"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Herbal Drinks to Detox your Kidney"
        temp.videoID = "1KcDcWk2YDc"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Cancer Causing Foods You Should Avoid!"
        temp.videoID = "AxtMC5loxO4"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Drinking Water at the Right Time"
        temp.videoID = "urcqO0uscTQ"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies For Winter Skin Hazards"
        temp.videoID = "xxP4q-5d8tA"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies For Dandruff"
        temp.videoID = "sE51MCW8TM8"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies For Dry Cough"
        temp.videoID = "DeXHvvUudDo"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies For Bad Breath"
        temp.videoID = "mewYv_Lxn9c"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies How to Get Rid of a Hangover"
        temp.videoID = "V8-3C4jUw04"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies For Dry Feet"
        temp.videoID = "pXuZyjVDsK0"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies for Dry Hair"
        temp.videoID = "HCq0IfwgscA"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies For Tooth Ache"
        temp.videoID = "T9c9g7ANRq0"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "5 Teeth Whitening Remedies"
        temp.videoID = "d3idUWUughM"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Ways to use lemon for super white skin"
        temp.videoID = "JC8eU2Q3oQU"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Lighten up dark private skin naturally"
        temp.videoID = "GHsxbqcIw5Y"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Herbal solution to Treat Ovarian Cysts"
        temp.videoID = "HE4FlcypylE"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Home Remedies for Indigestion"
        temp.videoID = "YxB_ZFRHk48"
        arrMainList.append(temp)
        
        temp = ArrBlog()
        temp.title = "Quick Tricks to Stop Hiccups"
        temp.videoID = "-KujL1_f9wA"
        arrMainList.append(temp)
        
        
        //refresh list elements
        self.tableView.reloadData()
    }
}
