//
//  SalkProjectConstant.swift
//  Salk
//
//  Created by Chetan  on 14/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class SalkProjectConstant: NSObject {
    
    public let isDebugMode: Bool = false
    
    public let userName: String = "sapi"
    public let password: String = "Serving@world/-"
    
    public let XMPPLink = "im.salk.healthcare"
    public let XMPPPort: UInt16 = 5222
    public let XMPPKeyWord = "salk"
    
    //databse Version Number
    public let dbVersion: Int = 3
    
    
    /*-----Server Informations---------*/
    // public let Serveradd: String = "http://www.salk.healthcare/doctor/api_v1/"; //old1
    
    public let Serveradd: String = " http://api.salk.healthcare/api/bba/index.php/doctor/api_v1/"; //new1
    
    //  public let ServeraddFTP: String = "http://www.salk.healthcare/doctor/"; old2
    
    public let ServeraddFTP: String = " http://api.salk.healthcare/api/bba/index.php/doctor/"; // new2
    
    
    // public let ServeraddCI: String = "http://api.salk.healthcare/index.php/patient/api/v2/"; //old3
    
    public let ServeraddCI: String = "http://api.salk.healthcare/api/bba/index.php/patient/api/v2/";  //new3
    
    
    //    public let ServeraddCI: String = "http://salk.tech/salkAPI/index.php/patient/api/v2/"; /*-----old-Server Add CI-------*/
    
    public let ServeraddFTPCI: String = "http://file.salk.healthcare/"; //old4
    
    //public let ServeraddFTPCI: String = "http://salk.healthcare/SalkFilesMaster/Version-2.2.1/"; //new4
    
    public let ServeraddExpertImg: String = "http://file.salk.healthcare/assets/savefiles/doctor/profile/"
    
    
    public let ServerFitnessProfileImg: String = "http://file.salk.healthcare/assets/savefiles/patient/profile/"
    
    
    public let ServerFitnessChatAttach = "http://file.salk.healthcare/assets/savefiles/"
    
}
