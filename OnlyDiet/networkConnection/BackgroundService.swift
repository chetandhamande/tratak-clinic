//
//  BackgroundService.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation
import UIKit

class BackgroundService: NSObject {
    var backgroundTask: UIBackgroundTaskIdentifier!
    var frequency = Double()
    var updateTimer: Timer!
    
    func initWithFrequency(_ seconds: Double) {
        self.frequency = seconds
    }
    
    func startService() {
        self.startBackgroundTask()
    }
    
    func doInBackground() {
        
    }
    
    func stopService() {
        self.updateTimer.invalidate()
        self.updateTimer = nil
        UIApplication.shared.endBackgroundTask(self.backgroundTask)
        self.backgroundTask = UIBackgroundTaskInvalid
    }
    
    func startBackgroundTask() {
        self.updateTimer = Timer.scheduledTimer(timeInterval: frequency, target: self, selector: #selector(self.doInBackground), userInfo: nil, repeats: true)
        self.backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            self.endBackgroundTask()
        })
    }
    
    func endBackgroundTask() {
        self.updateTimer.invalidate()
        self.updateTimer = nil
        UIApplication.shared.endBackgroundTask(self.backgroundTask)
        self.backgroundTask = UIBackgroundTaskInvalid
        self.startBackgroundTask()
    }
}
