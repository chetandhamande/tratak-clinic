
import UIKit

class CustomNetworkConnection {
    func sendGetRequestToRest(_ urlEndString: String, json: Data, methodName: String, classRef: AnyObject!) {
        let userName = SalkProjectConstant().userName
        let password = SalkProjectConstant().password
        
        Print.printLog("url:==\(urlEndString)")
                
        let request = NSMutableURLRequest()
        request.url = URL(string: urlEndString)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = json
        
        let loginString = NSString(format: "%@:%@", userName, password)
        let loginData: Data = loginString.data(using: String.Encoding.utf8.rawValue)!
        let base64LoginString = loginData.base64EncodedString(options: [])
        
        request.addValue("Basic \(base64LoginString) ", forHTTPHeaderField: "Authorization")
        
        let urlConfig = URLSessionConfiguration.default
        urlConfig.timeoutIntervalForRequest = 30.0
        urlConfig.timeoutIntervalForResource = 60.0
        
        let session = URLSession(configuration: urlConfig, delegate: nil, delegateQueue: nil )
        
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data,response,error) in
            Print.printLog("error in dataTask====\(String(describing: error))")
            if error == nil {
                let respCode = (response as! HTTPURLResponse).statusCode
                
                Print.printLog("responce in dataTask====\(String(describing: response))")
                Print.printLog("error in dataTask====\(String(describing: error?._code))")
                
                //Jump On HTTP Connection Class After Getting Responce
                DispatchQueue.main.async(execute: {
                    if classRef.isKind(of: UserRegistationHTTPConnection.self) {
                        (classRef as! UserRegistationHTTPConnection).getServerResponce(respCode, json: data!, methodName: methodName)
                    } else if classRef.isKind(of: FitnessHTTPConnection.self) {
                        (classRef as! FitnessHTTPConnection).getServerResponce(respCode, json: data!, mMethod: methodName)
                    } else if classRef.isKind(of: DashboardHTTPConnection.self) {
                        (classRef as! DashboardHTTPConnection).getServerResponce(respCode, json: data!, mMethod: methodName)
                    }
                });
            } else {
                //Jump On HTTP Connection Class After Getting Responce
                DispatchQueue.main.async(execute: {
                    if classRef.isKind(of: UserRegistationHTTPConnection.self) {
                        (classRef as! UserRegistationHTTPConnection).getServerResponce(0, json: nil, methodName: methodName)
                    } else if classRef.isKind(of: FitnessHTTPConnection.self) {
                        (classRef as! FitnessHTTPConnection).getServerResponce(0, json: nil, mMethod: methodName)
                    } else if classRef.isKind(of: DashboardHTTPConnection.self) {
                        (classRef as! DashboardHTTPConnection).getServerResponce(0, json: nil, mMethod: methodName)
                    }
                });
            }
        });
        dataTask.resume()
    }
}



