//
//  ArrSpeciality.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

public class ArrSpeciality {
    public var id = "",speciality = "",description = "",reg_date = "",reg_id = "",is_delete = "",
    status = "";
    public var listLayoutIndex: Int = 0
    public var arrDoctors = [ArrDoctor]()
}
