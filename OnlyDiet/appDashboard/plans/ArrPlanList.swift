//
//  ArrPlanList.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

public class ArrPlanList {
    var planID: String = "", planName = "", planDetail = "", price = "",
    comboPlan = "", comboPrice = "", longDesc = "",
    features = "", imageName = "";
    var listLayoutIndex: Int = 0
}
