//
//  MainDashboardActivity.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material

class MainDashboardActivity: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var mainTableView: UITableView!
    
    let tableCellIdentifier = "DashboardCell"
    
//    let imgArray = ["dash_book_appoint", "plans", "dash_expert_chat"]
//    let subTitleArray = ["Book your appointment with listed doctors",
//                         "Details about plan which is listed by doctors",
//                         "Communicate with your listed experts"]
//    let titleArray = ["Book an appointment", "Plans", "Chat with experts"]

    let imgArray = ["plans", "dash_expert_chat"]
    let subTitleArray = ["Details about plan which is listed by doctors",
                         "Communicate with your listed experts"]
    let titleArray = ["Plans", "Chat with experts"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.tabBarItem.isEnabled = false
        
        mainTableView.delegate = self
        mainTableView.dataSource = self
        
        prepareNavigationItem()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "app_name".applocalized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var currCell: DashboardCell!  = mainTableView.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? DashboardCell
        
        if currCell == nil {
            mainTableView.register(UINib(nibName: tableCellIdentifier, bundle: nil), forCellReuseIdentifier: tableCellIdentifier)
            currCell = mainTableView.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? DashboardCell
        }
        
        currCell.img.image = UIImage(named: imgArray[indexPath.row])
        currCell.title.text = titleArray[indexPath.row]
        currCell.subTitle.text = subTitleArray[indexPath.row]
        
        return currCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 0 {
//            let a = BookAppointListAct(nibName: "BookAppointListAct", bundle: nil)
//            self.navigationController?.pushViewController(a, animated: true)
//        } else if indexPath.row == 1 {
//            let a = PlanSelectionAct(nibName: "PlanSelectionAct", bundle: nil)
//            self.navigationController?.pushViewController(a, animated: true)
//        } else if indexPath.row == 2 {
//            let a = MainExpertVC(nibName: "MainExpertVC", bundle: nil)
//            self.navigationController?.pushViewController(a, animated: true)
//        }
        
        if indexPath.row == 0 {
            let a = PlanSelectionAct(nibName: "PlanSelectionAct", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if indexPath.row == 1 {
            let a = MainExpertVC(nibName: "MainExpertVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
