//
//  SleepMngVC.swift
//  Salk
//
//  Created by Chetan  on 10/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material
import HGCircularSlider

class SleepMngVC: UIViewController, GenderViewDelegate {
    
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var bedtimeLabel: UILabel!
    @IBOutlet weak var wakeLabel: UILabel!
    @IBOutlet weak var rangeCircularSlider: RangeCircularSlider!
    @IBOutlet weak var clockFormatSegmentedControl: UISegmentedControl!
    @IBOutlet weak var btnSleepQty: UIButton!

    fileprivate var currentDayDate = "0"
    fileprivate var currentValue:Int = 0
    
    
    fileprivate var backDateButton: UIBarButtonItem? = nil, nextDateButton: UIBarButtonItem? = nil
    
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "hh:mm a"
        return dateFormatter
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = CustomColor.background()
        prepareNavigationItem()
        confClock()
        
        findDateNLoadRecord(isNextDay: false)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Today"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let backDateImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_chevron_left_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        backDateButton = UIBarButtonItem(image: backDateImage, style: .plain, target: self, action: #selector(self.findBackDate))
        
        let nextDateImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_chevron_right_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        nextDateButton = UIBarButtonItem(image: nextDateImage, style: .plain, target: self, action: #selector(self.findNextDate))
        
        navigationItem.rightBarButtonItems = [nextDateButton!, backDateButton!]
    }
    
    fileprivate func confClock() {
        // setup O'clock
        rangeCircularSlider.startThumbImage = UIImage().imageWithImage(UIImage(named: "ic_sleep_at_black.png")!, scaledToSize: CGSize(width: 35, height: 35))
        rangeCircularSlider.endThumbImage = UIImage().imageWithImage(UIImage(named: "ic_sleep_wakeup_black.png")!, scaledToSize: CGSize(width: 35, height: 35))
        
        let dayInSeconds = 24 * 60 * 60
        rangeCircularSlider.maximumValue = CGFloat(dayInSeconds)
        
        rangeCircularSlider.startPointValue = 1 * 60 * 60
        rangeCircularSlider.endPointValue = 8 * 60 * 60
        
        updateTexts(rangeCircularSlider)
    }
    
    @IBAction func updateTexts(_ sender: AnyObject) {
        
        adjustValue(value: &rangeCircularSlider.startPointValue)
        adjustValue(value: &rangeCircularSlider.endPointValue)
        
        
        let bedtime = TimeInterval(rangeCircularSlider.startPointValue)
        let bedtimeDate = Date(timeIntervalSinceReferenceDate: bedtime)
        bedtimeLabel.text = dateFormatter.string(from: bedtimeDate)
        
        let wake = TimeInterval(rangeCircularSlider.endPointValue)
        let wakeDate = Date(timeIntervalSinceReferenceDate: wake)
        wakeLabel.text = dateFormatter.string(from: wakeDate)
        
        let duration = wake - bedtime
        let durationDate = Date(timeIntervalSinceReferenceDate: duration)
        
        dateFormatter.dateFormat = "HH"
        let hour:Int = Int(dateFormatter.string(from: durationDate))!
        dateFormatter.dateFormat = "mm"
        let minute:Int = Int(dateFormatter.string(from: durationDate))!
        
        currentValue = (hour * 60) + minute
        
        
        //set minute in center
        durationLabel.text = ConvertionClass().getDateParsingFrmMinute(currentValue, isFullString: false)
        
        dateFormatter.dateFormat = "hh:mm a"
    }
    
    func adjustValue(value: inout CGFloat) {
        let minutes = value / 60
        let adjustedMinutes =  ceil(minutes / 5.0) * 5
        value = adjustedMinutes * 60
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        performSaveSleep()
    }
}

extension SleepMngVC {
    @objc fileprivate func findBackDate() {
        findDateNLoadRecord(isNextDay: false)
    }
    @objc fileprivate func findNextDate() {
        findDateNLoadRecord(isNextDay: true)
    }
    
    fileprivate func findDateNLoadRecord(isNextDay: Bool)
    {
        performSaveSleep()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if(currentDayDate == "0") {
            let dateTime = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter))" + " 00:00:01 am";
            
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            currentDayDate = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime)!))"
        } else {
            
            var value:Double = Double(currentDayDate)!
            if(isNextDay) {
                value += (60 * 60 * 24); // day
            } else {
                value -= (60 * 60 * 24); // day
            }
            
            let dateTime = "\(ConvertionClass().conLongToDate(value, dateFormat: dateFormatter))" + " 00:00:01 am";
            
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            currentDayDate = "\(ConvertionClass().conDateToLong(dateFormatter.date(from: dateTime)!))"
        }
        
        dateFormatter.dateFormat = "dd MMMM"
        var selDate = "\(ConvertionClass().conLongToDate(Double(currentDayDate)!, dateFormat: dateFormatter))"
        
        let currDate = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter))"
        
        if(selDate == currDate) {
            selDate = "Today"
            
            nextDateButton?.isEnabled = false
        } else {
            nextDateButton?.isEnabled = true
        }
        navigationItem.titleLabel.text = selDate
        
        loadCurrentRecord();
    }
    
    fileprivate func loadCurrentRecord() {
        //load previous values
        var quality = "3"
        var startTime = "", endTime = "";
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
    
        let sql = "SELECT * from " + datasource.SleepTrackList_tlb
            + " WHERE " + datasource.dbDateTime + " = '" + currentDayDate + "' "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            if cursor!.next() {
                currentValue = Int(cursor!.int(forColumnIndex: 0))
                let value = cursor!.string(forColumnIndex: 1)!
                quality = cursor!.string(forColumnIndex: 4)
                if (value.contains(",")) {
                    let temp = value.components(separatedBy: ",")
                    startTime = temp[0]
                    endTime = temp[1]
                }
            } else {
                currentValue = 0
            }
            cursor!.close()
        } else {
            currentValue = 0
        }
        datasource.closeDatabase();
    
        //set minute in center
        durationLabel.text = ConvertionClass().getDateParsingFrmMinute(currentValue, isFullString: false)
        
        wakeLabel.text = endTime
        bedtimeLabel.text = startTime
        
        if(!endTime.isEmpty) {
            var endHour = Int(endTime.substringWithRange(0, end: endTime.indexOf(":")))
            let endMinute = Int(endTime.substringWithRange(endTime.indexOf(":") + 1, end: endTime.indexOf(" ")))
            
            let extTime = endTime.substringFromIndex(endTime.indexOf(" ") + 1)
            if extTime.uppercased() == "PM" {
                endHour = endHour! + 12
            }
            
            let endSecondVal = ((endHour! * 60) + endMinute!) * 60
            rangeCircularSlider.endPointValue = CGFloat(endSecondVal)
        }
        
        if(!startTime.isEmpty) {
            
            var endHour = Int(startTime.substringWithRange(0, end: startTime.indexOf(":")))
            let endMinute = Int(startTime.substringWithRange(startTime.indexOf(":") + 1, end: startTime.indexOf(" ")))
            
            let extTime = startTime.substringFromIndex(startTime.indexOf(" ") + 1)
            if extTime.uppercased() == "PM" {
                endHour = endHour! + 12
            }
            
            let endSecondVal = ((endHour! * 60) + endMinute!) * 60
            rangeCircularSlider.startPointValue = CGFloat(endSecondVal)
        }
    
        if (quality == "1") {
            btnSleepQty.setTitle("Poor", for: UIControlState())
        } else if (quality == "2") {
            btnSleepQty.setTitle("Moderate", for: UIControlState())
        } else if (quality == "3") {
            btnSleepQty.setTitle("Good", for: UIControlState())
        }
    }
    
        
    fileprivate func performSaveSleep() {
        //apply into database
        let startTime = self.bedtimeLabel.text
        let endTime = self.wakeLabel.text
    
        if(!((startTime?.isEmpty)! || (endTime?.isEmpty)!)) {
            var quality = "3"
            if (btnSleepQty.title(for: UIControlState()) == "Poor") {
                quality = "1"
            } else if (btnSleepQty.title(for: UIControlState()) == "Moderate") {
                quality = "2"
            } else if (btnSleepQty.title(for: UIControlState()) == "Good") {
                quality = "3"
            }
    
            let datasource = DBOperation()
            datasource.openDatabase(true)
            datasource.InsertSleepTrackList(currentValue: "\(currentValue)", sleepValue: startTime! + "," + endTime!, dateTime: currentDayDate, quality: quality, isServerSync: "0");
            datasource.closeDatabase()
        }
        
        // Fire the broadcast with intent packaged
        var dict = ["resultCode":1,
                    "udpateType":"sendPendingData"
            ] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
        
        dict = ["resultCode":1,
                "udpateType":"sleepRef"
            ] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
    }
    
    @IBAction func performChangeSleepQaulity(button: UIButton) {
        
        let genderViewObj = GenderViewController(nibName: "GenderViewController", bundle: nil)
        genderViewObj.genderArray = ["Poor", "Moderate", "Good"]
        genderViewObj.headingStr = "Select Quality of Sleep"
        genderViewObj.delegate = self
        
        self.showPopUpViewController(selfViewController: self, presentViewController: genderViewObj)
    }
    
    func didSelectGender(_ controller: GenderViewController, gender: String)
    {
        btnSleepQty.setTitle(gender, for: UIControlState())
    }
}
