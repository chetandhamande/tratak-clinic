//
//  CustomeAdpaterMyDiary.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Async

class CustomeAdpaterMyDiary: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var txtHead: UILabel!
    @IBOutlet weak var subTableView: UITableView!
    @IBOutlet weak var hideView: UIView!
    
    let CustomeSubAdpaterMyDiaryIdentifer = "CustomeSubAdpaterMyDiary"
    
    var classRef: AnyObject!
    private var mValues = [ArrMyDiary]()
    private var subValues = [ArrFitnessTypeData]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        hideView.isHidden = false
        subTableView.isHidden = true
        
        subTableView.delegate = self
        subTableView.dataSource = self
        
        self.subTableView.isScrollEnabled = false
        
        subTableView.estimatedRowHeight = 100
        subTableView.rowHeight = UITableViewAutomaticDimension
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateView() {
        
        // assign height equal to the tableView's Frame
        self.setNeedsLayout()
        self.layoutIfNeeded()
        // redraws views and subviews with updated constraint
    }
    
    func CustomeAdpaterMyDiary(_ users: [ArrMyDiary], position: Int, classRef: AnyObject) {
    
        self.mValues = users
    
        let user: ArrMyDiary = users[position]
        
        self.subValues = user.typeDataList
        
        txtHead.text = user.dayStr
        self.classRef = classRef
        
        if subValues.count == 0 {
            self.subTableView.isHidden = true
            hideView.isHidden = false
        } else {
            hideView.isHidden = true
            self.subTableView.isHidden = false
            
            self.containerViewHeightConstraint.constant = self.subTableView.contentSize.height
            // Make sure layout subviews
            self.setNeedsLayout()
            self.layoutIfNeeded()
            
            subTableView.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var currCell: CustomeSubAdpaterMyDiary!  = subTableView.dequeueReusableCell(withIdentifier: CustomeSubAdpaterMyDiaryIdentifer) as? CustomeSubAdpaterMyDiary
        
        if currCell == nil {
            subTableView.register(UINib(nibName: CustomeSubAdpaterMyDiaryIdentifer, bundle: nil), forCellReuseIdentifier: CustomeSubAdpaterMyDiaryIdentifer)
            currCell = subTableView.dequeueReusableCell(withIdentifier: CustomeSubAdpaterMyDiaryIdentifer) as? CustomeSubAdpaterMyDiary
        }
        
        currCell.title.text = subValues[indexPath.row].type
        currCell.subTitle.attributedText = subValues[indexPath.row].typeData.html2AttributedString
        currCell.img.image = UIImage(named: subValues[indexPath.row].typeIcon)
        
        return currCell
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerCell = subTableView.dequeueReusableHeaderFooterView(withIdentifier: "MyDiaryHeader") as! MyDiaryHeader
//
//        headerCell.
//        headerCell.title.text = subValues[section].type
//
//        return headerCell
//    }
    
}
