
class UserRegistationHTTPConnection {
    
    var classRefs: AnyObject! = nil
    
    func sendServerRequest(_ url: String, methodName: String, classRef: AnyObject!, dict: NSMutableDictionary?) {
        var json = Data()
        
        if dict != nil {
            json = try! JSONSerialization.data(withJSONObject: dict!, options: JSONSerialization.WritingOptions.prettyPrinted)
        }
        
        classRefs = classRef
        if methodName == Constant.AccountMng.MethodLogin {
            json = UserRegistationJsonCreator().putJsonLogin() as Data
        } else if methodName == Constant.AccountMng.MethodUpdateProfile {
            json = UserRegistationJsonCreator().putJsonUpdateProfile() as Data
        } else if methodName == Constant.AccountMng.MethodRegister
            || methodName == Constant.AccountMng.MethodRegisterInCompany
        {
            json = UserRegistationJsonCreator().putJsonRegister() as Data
        } else if methodName == Constant.AccountMng.MethodVerifyCorpEmail {
            json = UserRegistationJsonCreator().putJsonVerifyCorpEmail() as Data
        } else if methodName == Constant.AccountMng.MethodVerifyMobile {
            json = UserRegistationJsonCreator().putJsonVerifyMobileEmail() as Data
        } else if methodName == Constant.AccountMng.MethodSyncProfile {
            json = UserRegistationJsonCreator().putJsonSyncProfile() as Data
        } else if methodName.equals(Constant.General.MethodFeedback) {
            json = UserRegistationJsonCreator().putJsonFeedback() as Data
        }
        
        //execute Http Connection
        let customObj = CustomNetworkConnection()
        customObj.sendGetRequestToRest(url, json: json, methodName: methodName, classRef: self)
    }
    
    func getServerResponce(_ respCode: Int?, json: Data?, methodName: String) {
        if respCode == 200 {
            let respJson = NSString(data: json!, encoding: String.Encoding.utf8.rawValue)!
            
            Print.printLog("Rec Code= \(String(describing: respCode)), Resp Data=\(respJson)")
            
            let mySharedObj = MySharedPreferences()
            
            if methodName == Constant.AccountMng.MethodLogin {
                do {
                    try UserRegistationJsonCreator().storeLoginJson(json!)
                } catch {}
                
                if mySharedObj.getErrorCode() == 200
                    || mySharedObj.getErrorCode() == 202
                    || mySharedObj.getErrorCode() == 203
                {
                    (classRefs as! LoginVC).moveToMainViewController()
                } else {
                    (classRefs as! LoginVC).errorInConnection()
                }
            } else if methodName == Constant.AccountMng.MethodSyncProfile {
                do {
                    try UserRegistationJsonCreator().storeJsonSyncProfile(json!)
                } catch {}
                
                if mySharedObj.getErrorCode() == 200 {
                    if classRefs.isKind(of: PrimaryInformationVC.self) {
                        (classRefs as! PrimaryInformationVC).refreshParameters()
                    } else if classRefs.isKind(of: BasicInformationVC.self) {
                        (classRefs as! BasicInformationVC).refreshParameters()
                    } else if classRefs.isKind(of: CurrentHistoryVC.self) {
                        (classRefs as! CurrentHistoryVC).refreshParameters()
                    } else if classRefs.isKind(of: PersonalityHistoryVC.self) {
                        (classRefs as! PersonalityHistoryVC).refreshParameters()
                    } else if classRefs.isKind(of: PainHistoryVC.self) {
                        (classRefs as! PainHistoryVC).refreshParameters()
                    } else if classRefs.isKind(of: YogaHistoryVC.self) {
                        (classRefs as! YogaHistoryVC).refreshParameters()
                    } else if classRefs.isKind(of: DailyLifestyleVC.self) {
                        (classRefs as! DailyLifestyleVC).refreshParameters()
                    } else if classRefs.isKind(of: MedicalHistoryVC.self) {
                        (classRefs as! MedicalHistoryVC).refreshParameters()
                    } else if classRefs.isKind(of: PersonalHistoryVC.self) {
                        (classRefs as! PersonalHistoryVC).refreshParameters()
                    } else if classRefs.isKind(of: ReikiHistoryVC.self) {
                        (classRefs as! ReikiHistoryVC).refreshParameters()
                    } else if classRefs.isKind(of: StressHistoryVC.self) {
                        (classRefs as! StressHistoryVC).refreshParameters()
                    } else if classRefs.isKind(of: FamilyHistoryVC.self) {
                        (classRefs as! FamilyHistoryVC).refreshParameters()
                    }
                }
            }
            else if methodName == Constant.AccountMng.MethodVerifyMobile
                || methodName == Constant.AccountMng.MethodVerifyCorpEmail
            {
                do {
                    if methodName == Constant.AccountMng.MethodVerifyMobile {
                        try UserRegistationJsonCreator().storeVerifyMobileEmail(json!)
                    } else if methodName == Constant.AccountMng.MethodVerifyCorpEmail {
                        try UserRegistationJsonCreator().storeVerifyCorpEmail(json!)
                    }
                } catch {}
                
                if mySharedObj.getErrorCode() == 200 {
                    if classRefs.isKind(of: RegistrationVC.self) {
                        (classRefs as! RegistrationVC).stopProgressBar()
                    } else if classRefs.isKind(of: VerificationViewController.self) {
                        (classRefs as! VerificationViewController).stopProgressBar()
                    }
                    
                    let main = MainViewController()
                    main.startVerification(method: methodName)
                    
                } else {
                    if classRefs.isKind(of: RegistrationVC.self) {
                        (classRefs as! RegistrationVC).errorInConnection()
                    } else if classRefs.isKind(of: VerificationViewController.self) {
                        (classRefs as! VerificationViewController).errorInConnection()
                    }
                }
            } else if methodName == Constant.AccountMng.MethodRegister
                || methodName == Constant.AccountMng.MethodRegisterInCompany
            {
                do {
                    try UserRegistationJsonCreator().storeRegisterJson(json!)
                } catch {}
                
                if mySharedObj.getErrorCode() == 200 {
                    if classRefs.isKind(of: RegistrationVC.self) {
                        (classRefs as! RegistrationVC).stopProgressBar()
                    } else if classRefs.isKind(of: VerificationViewController.self) {
                        (classRefs as! VerificationViewController).stopProgressBar()
                    }
                    
                    let main = MainViewController()
                    main.moveToDecideAppCondition()
                    
                } else {
                    if classRefs.isKind(of: RegistrationVC.self) {
                        (classRefs as! RegistrationVC).errorInConnection()
                    } else if classRefs.isKind(of: VerificationViewController.self) {
                        (classRefs as! VerificationViewController).errorInConnection()
                    }
                }
            } else if methodName.equals(Constant.General.MethodFeedback) {
                do {
                    try UserRegistationJsonCreator().storeFeedbackJson(json!)
                } catch {}
                
                // if resp code is 200 then open password activity
                if mySharedObj.getErrorCode() == 200 {
                    (classRefs as! FeedbackViewController).testSuccess()
                } else {
                    (classRefs as! FeedbackViewController).testFaliuer()
                }
            } else if methodName == Constant.AccountMng.MethodUpdateProfile {
                do {
                    try UserRegistationJsonCreator().storeJsonUpdateProfile(json!)
                } catch {}
                
                if mySharedObj.getErrorCode() == 200 {
                    if classRefs.isKind(of: VerificationViewController.self) {
                        (classRefs as! VerificationViewController).stopProgressBar()
                        
                        mySharedObj.setVerificationSMSComeCode(text: "")
                        mySharedObj.setUserID(text: mySharedObj.getTempUserID())
                        
                        let main = MainViewController()
                        main.moveToDecideAppCondition()
                        
                    } else if classRefs.isKind(of: MedicalConnModificationVC.self) {
                        (classRefs as! MedicalConnModificationVC).success()
                    } else if classRefs.isKind(of: FoodPreferenceModificationVC.self) {
                        (classRefs as! FoodPreferenceModificationVC).success()
                    } else if classRefs.isKind(of: BasicInformationVC.self) {
                        (classRefs as! BasicInformationVC).success()
                    } else if classRefs.isKind(of: FamilyHistoryVC.self) {
                        (classRefs as! FamilyHistoryVC).success()
                    } else if classRefs.isKind(of: DailyLifestyleAct.self) {
                        (classRefs as! DailyLifestyleAct).success()
                    } else if classRefs.isKind(of: PrimaryInformationVC.self) {
                        (classRefs as! PrimaryInformationVC).success()
                    } else if classRefs.isKind(of: MedicalHistoryVC.self) {
                        (classRefs as! MedicalHistoryVC).success()
                    } else if classRefs.isKind(of: PersonalHistoryVC.self) {
                        (classRefs as! PersonalHistoryVC).success()
                    } else if classRefs.isKind(of: YogaHistoryVC.self) {
                        (classRefs as! YogaHistoryVC).success()
                    } else if classRefs.isKind(of: PainHistoryVC.self) {
                        (classRefs as! PainHistoryVC).success()
                    } else if classRefs.isKind(of: StressHistoryVC.self) {
                        (classRefs as! StressHistoryVC).success()
                    } else if classRefs.isKind(of: PersonalityHistoryVC.self) {
                        (classRefs as! PersonalityHistoryVC).success()
                    } else if classRefs.isKind(of: ReikiHistoryVC.self) {
                        (classRefs as! ReikiHistoryVC).success()
                    } else if classRefs.isKind(of: DailyLifestyleVC.self) {
                        (classRefs as! DailyLifestyleVC).success()
                    } else if classRefs.isKind(of: CurrentHistoryVC.self) {
                        (classRefs as! CurrentHistoryVC).success()
                    }
                    
                } else {
                    if classRefs.isKind(of: VerificationViewController.self) {
                        (classRefs as! VerificationViewController).errorInConnection()
                    } else if classRefs.isKind(of: MedicalConnModificationVC.self) {
                        (classRefs as! MedicalConnModificationVC).errorInConnection()
                    } else if classRefs.isKind(of: FoodPreferenceModificationVC.self) {
                        (classRefs as! FoodPreferenceModificationVC).errorInConnection()
                    } else if classRefs.isKind(of: BasicInformationVC.self) {
                        (classRefs as! BasicInformationVC).errorInConnection()
                    } else if classRefs.isKind(of: FamilyHistoryVC.self) {
                        (classRefs as! FamilyHistoryVC).errorInConnection()
                    } else if classRefs.isKind(of: DailyLifestyleAct.self) {
                        (classRefs as! DailyLifestyleAct).errorInConnection()
                    } else if classRefs.isKind(of: PrimaryInformationVC.self) {
                        (classRefs as! PrimaryInformationVC).errorInConnection()
                    } else if classRefs.isKind(of: MedicalHistoryVC.self) {
                        (classRefs as! MedicalHistoryVC).errorInConnection()
                    } else if classRefs.isKind(of: PersonalHistoryVC.self) {
                        (classRefs as! PersonalHistoryVC).errorInConnection()
                    } else if classRefs.isKind(of: YogaHistoryVC.self) {
                        (classRefs as! YogaHistoryVC).errorInConnection()
                    } else if classRefs.isKind(of: PainHistoryVC.self) {
                        (classRefs as! PainHistoryVC).errorInConnection()
                    } else if classRefs.isKind(of: StressHistoryVC.self) {
                        (classRefs as! StressHistoryVC).errorInConnection()
                    } else if classRefs.isKind(of: PersonalityHistoryVC.self) {
                        (classRefs as! PersonalityHistoryVC).errorInConnection()
                    } else if classRefs.isKind(of: ReikiHistoryVC.self) {
                        (classRefs as! ReikiHistoryVC).errorInConnection()
                    } else if classRefs.isKind(of: DailyLifestyleVC.self) {
                        (classRefs as! DailyLifestyleVC).errorInConnection()
                    } else if classRefs.isKind(of: CurrentHistoryVC.self) {
                        (classRefs as! CurrentHistoryVC).errorInConnection()
                    }
                }
            }
        } else {
            if methodName == Constant.AccountMng.MethodLogin {
                (classRefs as! LoginVC).errorInConnection()
            } else if methodName == Constant.AccountMng.MethodVerifyMobile
                || methodName == Constant.AccountMng.MethodVerifyCorpEmail
                || methodName == Constant.AccountMng.MethodRegister
                || methodName == Constant.AccountMng.MethodRegisterInCompany
            {
                if classRefs.isKind(of: RegistrationVC.self) {
                    (classRefs as! RegistrationVC).errorInConnection()
                } else if classRefs.isKind(of: VerificationViewController.self) {
                    (classRefs as! VerificationViewController).errorInConnection()
                }
            } else if methodName == Constant.General.MethodFeedback {
                (classRefs as! FeedbackViewController).testFaliuer()
            } else if methodName == Constant.AccountMng.MethodUpdateProfile {
                if classRefs.isKind(of: VerificationViewController.self) {
                    (classRefs as! VerificationViewController).errorInConnection()
                } else if classRefs.isKind(of: MedicalConnModificationVC.self) {
                    (classRefs as! MedicalConnModificationVC).errorInConnection()
                } else if classRefs.isKind(of: FoodPreferenceModificationVC.self) {
                    (classRefs as! FoodPreferenceModificationVC).errorInConnection()
                } else if classRefs.isKind(of: BasicInformationVC.self) {
                    (classRefs as! BasicInformationVC).errorInConnection()
                } else if classRefs.isKind(of: FamilyHistoryVC.self) {
                    (classRefs as! FamilyHistoryVC).errorInConnection()
                } else if classRefs.isKind(of: DailyLifestyleAct.self) {
                    (classRefs as! DailyLifestyleAct).errorInConnection()
                }  else if classRefs.isKind(of: MedicalHistoryVC.self) {
                    (classRefs as! MedicalHistoryVC).errorInConnection()
                } else if classRefs.isKind(of: PersonalHistoryVC.self) {
                    (classRefs as! PersonalHistoryVC).errorInConnection()
                } else if classRefs.isKind(of: YogaHistoryVC.self) {
                    (classRefs as! YogaHistoryVC).errorInConnection()
                } else if classRefs.isKind(of: PainHistoryVC.self) {
                    (classRefs as! PainHistoryVC).errorInConnection()
                } else if classRefs.isKind(of: StressHistoryVC.self) {
                    (classRefs as! StressHistoryVC).errorInConnection()
                } else if classRefs.isKind(of: PersonalityHistoryVC.self) {
                    (classRefs as! PersonalityHistoryVC).errorInConnection()
                } else if classRefs.isKind(of: ReikiHistoryVC.self) {
                    (classRefs as! ReikiHistoryVC).errorInConnection()
                } else if classRefs.isKind(of: DailyLifestyleVC.self) {
                    (classRefs as! DailyLifestyleVC).errorInConnection()
                } else if classRefs.isKind(of: CurrentHistoryVC.self) {
                    (classRefs as! CurrentHistoryVC).errorInConnection()
                }
            }
        }
    }
}
