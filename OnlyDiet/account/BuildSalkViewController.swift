//
//  BuildSalkViewController.swift
//
//

import UIKit
import Material

class BuildSalkViewController: UIViewController, UINavigationBarDelegate {

    @IBOutlet weak var upperView: UIView!
    
    @IBOutlet weak var cloudImage: UIImageView!
    @IBOutlet weak var progressImages: UIImageView!
    @IBOutlet weak var mobileImage: UIImageView!
    
    @IBOutlet weak var cloudLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var waitLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var downloadLabel: UILabel!
    
    @IBOutlet weak var lowerView: UIView!
    @IBOutlet weak var horizontalView: UIView!
    
    @IBOutlet weak var buildingLabel: UILabel!
    @IBOutlet weak var thankLabel: UILabel!
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    fileprivate var downloadMethodsDesc = [String]()
    fileprivate var currentIndex = 0
    fileprivate var curIndexFitnessInfo = 0, maxIndexFitnessInfo = 6
    
    var checkNav: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.prepareNavigationItem()
        
        self.viewConfiguration()
        
        self.loadAllDownloadMethod()
        self.setCurrentIndex()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Build " + "app_name".applocalized
        navigationItem.titleLabel.textColor = Color.black
        
        if checkNav == true {
            let backImage: UIImage = Icon.arrowBack!
            let backButton = IconButton()
            backButton.setImage(backImage, for: UIControlState.normal)
            backButton.setImage(backImage, for: UIControlState.highlighted)
            backButton.addTarget(self, action: #selector(self.back(_:)), for: .touchUpInside)
            
            navigationItem.leftViews = [backButton]
        }
    }
    
    func back(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        let alert = SweetAlert()
        alert.showAlert("", subTitle: "Please wait, " + "app_name".applocalized + " configuration is running...", style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    func viewConfiguration() {
        
//        upperView.backgroundColor = UIColor.clear
//        lowerView.backgroundColor = PrimaryColor.colorPrimaryDark()
        
//        cloudLabel.text = "app_name".applocalized + " Cloud"
//        cloudLabel.textColor = CustomColor.textGrayColor()
        
//        mobileLabel.text = "My Mobile"
//        mobileLabel.textColor = CustomColor.textGrayColor()
        
        downloadLabel.font = UIFont.boldSystemFont(ofSize: 18)
        
        waitLabel.text = "Please wait.."
        
//        buildingLabel.text = "strBuilding".localized
//        buildingLabel.font = UIFont.boldSystemFont(ofSize: 18)
        
//        thankLabel.text = "strWaitMsg".localized
//        buildingLabel.font = UIFont.boldSystemFont(ofSize: 14)
        
        counterLabel.textColor = CustomColor.textGrayColor()
        
//        horizontalView.backgroundColor = PrimaryColor.colorPrimary()
        
//        loading.startAnimating()
        
        progressImages.animationImages =
            [UIImage(named: "loading1.png")!,
             UIImage(named: "loading2.png")!,
             UIImage(named: "loading3.png")!,
             UIImage(named: "loading4.png")!,
             UIImage(named: "loading5.png")!,
             UIImage(named: "loading6.png")!]
        
        // How many seconds it should take to go through all images one time.
        progressImages.animationDuration = 10.0
        
        // How many times to repeat the animation (0 for indefinitely).
        progressImages.animationRepeatCount = 0
        
        progressImages.startAnimating()
    }
    
    func loadAllDownloadMethod() {
        downloadMethodsDesc = ["strFitnessFoodList".localized,
                               "strFitnessFoodList".localized,
                               "strFitnessActList".localized,
                               "strFitnessSchedule".localized,
                               "strFitnessInfoList".localized]
    }
    
    func setCurrentIndex() {
        var index = currentIndex + 1
        
        if index > downloadMethodsDesc.count {
            index = downloadMethodsDesc.count
        }
        
        if currentIndex < downloadMethodsDesc.count {
            counterLabel.text = "(\(index)/" + "\(downloadMethodsDesc.count))"
            downloadLabel.text = downloadMethodsDesc[currentIndex]
        }
     
        if currentIndex == 0 {
            //getting Food information list
            FitnessRequestManager().getRequest(Constant.Fitness.MethodDownloadFoodLog, classRef: self)
            
        } else if currentIndex == 1 {
            //getting Food contain information list
            FitnessRequestManager().getRequest(Constant.Fitness.MethodDownloadFoodContainer, classRef: self)
            
        } else if currentIndex == 2 {
            //getting Activity information list
            FitnessRequestManager().getRequest(Constant.Fitness.MethodDownloadActivityLog, classRef: self)
            
        } else if currentIndex == 3 {
            //getting Activity information list
            FitnessRequestManager().getRequest(Constant.Fitness.MethodScheduleList, classRef: self)
            
        } else if currentIndex == 4 {
            //getting Fitness information list
            if(curIndexFitnessInfo == 0) {
                FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncTLPedometer, classRef: self)
            } else if(curIndexFitnessInfo == 1) {
                FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncTLWater, classRef: self)
            } else if(curIndexFitnessInfo == 2) {
                FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncTLSleep, classRef: self)
            } else if(curIndexFitnessInfo == 3) {
                FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncReminder, classRef: self)
            } else if(curIndexFitnessInfo == 4) {                FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncTLFood, classRef: self)
            } else if(curIndexFitnessInfo == 5) {                FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncTargetVal, classRef: self)
            }
            
        } else {
            //close current building form and restart app
            let appPref = MySharedPreferences()
            appPref.setIsNeedBuildApplication(text: false)
            
            let mainObj = MainViewController()
            mainObj.moveToDecideAppCondition()
        }
        
        currentIndex += 1
    }
        
    //function for load new data
    func refreshLoadNextData() {
        //manage sync global record
        if(currentIndex == 5) {
            curIndexFitnessInfo += 1
            if(curIndexFitnessInfo < maxIndexFitnessInfo) {
                currentIndex = 4
            }
        }
        
        self.setCurrentIndex() //call for load next record
    }
    
    /**
     * Show Fail to link, on Sync own address list
     */
    func failGettingInfo() {
        let alert = SweetAlert()
        alert.showAlert("Oops...", subTitle: "ServerNotWorkRetry".localized, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
