//
//  FamilyHistoryVC.swift
//  Salk
//
//  Created by Chetan on 07/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class FamilyHistoryVC: UIViewController {
    
    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!

    @IBOutlet weak var objFHView: FamilyHistoryView!
    
    fileprivate var loadingObj = CustomLoading()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareNavigationItem()

        mainScrollView.backgroundColor = CustomColor.background()
        
        //getting Latest record
        initi()
        getRecordServer()
    }
    
    private func initi() {
        let tap = UITapGestureRecognizer(target: self, action:#selector(FamilyHistoryVC.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        refreshParameters()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviFamilyHistory".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
    }

    public func refreshParameters() {
        let datasource = DBOperation()
        datasource.openDatabase(false)
        let sql = "SELECT * FROM " + datasource.AssessmentList_tlb
            + " WHERE " + datasource.dbGroupType + " = '" + "assGrpFamilyHistory".localized + "' "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                if cursor!.string(forColumnIndex: 0).caseInsensitiveCompare("disease1") == .orderedSame {
                    objFHView.txtFather.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).caseInsensitiveCompare("disease2") == .orderedSame {
                    objFHView.txtMother.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).caseInsensitiveCompare("disease3") == .orderedSame  {
                    self.objFHView.txtSibling.text = cursor!.string(forColumnIndex: 1)
                }
            }
            cursor!.close()
        }
        datasource.closeDatabase()
    }
    
    @objc private func performNextOpr() {
        dismissKeyboard()
        
        if objFHView.txtFather.text!.isEmpty {
            showSweetDialog(title: "popupAlert".localized, message: "Missing father disease")
        } else if objFHView.txtMother.text!.isEmpty{
            showSweetDialog(title: "popupAlert".localized, message: "Missing mother disease")
        } else {
            let datasource = DBOperation()
            datasource.openDatabase(true)
            datasource.UpdateAssessmentItemTable(paramName: "disease1", paramVal: objFHView.txtFather.text!, groupType: "assGrpFamilyHistory".localized)
            datasource.UpdateAssessmentItemTable(paramName: "disease2", paramVal: objFHView.txtMother.text!, groupType: "assGrpFamilyHistory".localized)
            datasource.UpdateAssessmentItemTable(paramName: "disease3", paramVal: objFHView.txtSibling.text!, groupType: "assGrpFamilyHistory".localized)
            datasource.closeDatabase()
            
            // call for getting Register user
            let checkConn = CheckInternetConnection()
            if checkConn.isCheckInternetConnection() {
                self.view.addSubview(loadingObj.loading())
                let appPref = MySharedPreferences()
                appPref.setUpdateReqType(text: "assGrpFamilyHistory".localized)
                UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
            } else {
                showSweetDialog(title: "Oops...".localized, message: "noInternet".localized)
            }
        }
    }
    
    // method for the open swwwt alert dialog
    private func showSweetDialog(title: String, message: String) {
        SweetAlert().showAlert(title, subTitle: message, style: .warning)
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpFamilyHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        showSweetDialog(title: "Oops...", message: appPref.getErrorMessage())
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
