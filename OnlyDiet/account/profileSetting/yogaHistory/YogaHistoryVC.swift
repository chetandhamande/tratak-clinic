//
//  YogaHistoryVC.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async

class YogaHistoryVC: UIViewController {

    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var editPurpose: MKTextField!
    @IBOutlet weak var editPracticied: MKTextField!
    @IBOutlet weak var editCurinj: MKTextField!
    
    fileprivate var loadingObj = CustomLoading()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewConfiguration()

        // Do any additional setup after loading the view.
        init1()
        
        //getting Latest record
        
        Async.main(after: 0.1) {
            self.getRecordServer()
        }
    }
    
    fileprivate func viewConfiguration() {
        mainScrollView.backgroundColor = CustomColor.background()
        
        prepareNavigationItem()
        configureTextField(editPurpose, placeholder: "")
        configureTextField(editPracticied, placeholder: "")
        configureTextField(editCurinj, placeholder: "")
    }
    
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviYogaHist".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
    }
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.returnKeyType = .next
    }
    
    private func init1() {
        let tap = UITapGestureRecognizer(target: self, action:#selector(YogaHistoryVC.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        refreshParameters()
    }
    
    func refreshParameters() {
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        let cursor = datasource.selectRecords("SELECT * FROM " + datasource.AssessmentList_tlb
            + " WHERE " + datasource.dbGroupType + " = '" + "assGrpYogaHistory".localized + "' ")
        if cursor != nil {
            while cursor!.next() {
                
                if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("learning_yoga") {
                    editPurpose.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("practiced_before") {
                    editPracticied.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("current_injuries") {
                    editCurinj.text = cursor!.string(forColumnIndex: 1)
                }
            }
        }
        
        cursor?.close();
        datasource.closeDatabase();
    }
    
    @objc func performNextOpr() {
        dismissKeyboard()
        
        let datasource = DBOperation()
        datasource.openDatabase(true)
        datasource.UpdateAssessmentItemTable(paramName: "learning_yoga", paramVal: editPurpose.text!, groupType: "assGrpYogaHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "practiced_before", paramVal: editPracticied.text!, groupType: "assGrpYogaHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "current_injuries", paramVal: editCurinj.text!, groupType: "assGrpYogaHistory".localized)
        
        datasource.closeDatabase()
        
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loadingObj.loading())
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpYogaHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
        
    }
    
    // call for getting Register user
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpYogaHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
