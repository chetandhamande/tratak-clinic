//
//  PrimaryInformationVC.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import MaterialControls
import Material
import RAMAnimatedTabBarController
import Kingfisher

class PrimaryInformationVC: UIViewController, UITextFieldDelegate, UIScrollViewDelegate, MDDatePickerDialogDelegate, ProfileImageViewDelegate, GenderViewDelegate {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIButton!
    @IBOutlet weak var mobileNoView: UIView!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var mobileNoLabel: UILabel!
    @IBOutlet weak var firstNameTextField: MKTextField!
    @IBOutlet weak var lastNameTextField: MKTextField!
    @IBOutlet weak var dobButton: UIButton!
    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var backNavButton: UIButton!
    
    var loadingObj = CustomLoading()
    var mySharedObj = MySharedPreferences()
    
    var imageName : UIImage!
    var path : String!
    
    private var dobLongStr: Double = 0.0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstNameTextField.delegate=self
        lastNameTextField.delegate=self
        
        scrollView.delegate=self
        
        self.viewConfiguration()
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(PrimaryInformationVC.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        self.prepareNavigationItem()
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func viewConfiguration() {
        
        mobileNoLabel.text = MakeProperNumber.showPhoneNumber(mySharedObj.getMobile())
        
        let tempImage = self.loadImage(String(mySharedObj.getUserID()).sha1())
        if tempImage == nil {
            //download image from server
            let URL = Foundation.URL(string: "\(SalkProjectConstant().ServerFitnessProfileImg)\( String(mySharedObj.getUserID()).sha1() + Constant.FolderNames.imageExtensionjpg)")!
            KingfisherManager.shared.retrieveImage(with: URL, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                if error == nil {
                    self.imageName = image
                    self.profileImageView.setImage(image, for: UIControlState())
                    self.backgroundBlurImage(image!)
                } else {
                    self.imageName = UIImage(named: "ic_person_white_gray_24dp.png")!
                    self.profileImageView.setImage(self.imageName, for: UIControlState())
                }
            })
        } else {
            imageName = tempImage
            profileImageView.setImage(imageName, for: UIControlState())
            self.backgroundBlurImage(imageName)
        }
        
        self.configureTextField(firstNameTextField, placeholder: "firstName".localized)
        self.configureTextField(lastNameTextField, placeholder: "lastName".localized)
        
        
        if mySharedObj.getFirstName() == "" {
            firstNameTextField.text = ""
        } else {
            firstNameTextField.text = mySharedObj.getFirstName()
        }
        
        if mySharedObj.getLastName() == "" {
            lastNameTextField.text = ""
        } else {
            lastNameTextField.text = mySharedObj.getLastName()
        }
        
        if mySharedObj.getGender() == "" || mySharedObj.getGender() == "gender".localized {
            genderButton.setTitle("gender".localized, for: UIControlState.normal)
            genderButton.setTitleColor(CustomColor.textGrayColor(), for: UIControlState.normal)
        } else {
            genderButton.setTitle(mySharedObj.getGender(), for: UIControlState.normal)
            genderButton.setTitleColor(CustomColor.textBlackColor(), for: UIControlState.normal)
        }
        
        do {
            try getDOB()
        } catch _ {}
        
    }
    
    private func getDOB() throws {
        let dobStr = mySharedObj.getDOB()
        if !dobStr.isEmpty {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            
            if df.date(from: dobStr) != nil {
                dobLongStr = ConvertionClass().conDateToLong(df.date(from: dobStr)!)
            } else {
                df.dateFormat = "yyyy-MM-dd hh:mm:ss"
                if df.date(from: dobStr) != nil {
                    dobLongStr = ConvertionClass().conDateToLong(df.date(from: dobStr)!)
                } else {
                    df.dateFormat = "yyyy-MM-dd"
                    dobLongStr = ConvertionClass().conDateToLong(df.date(from: dobStr)!)
                }
            }
            
            df.dateFormat = "dd MMM, yyyy"
            let date = ConvertionClass().conLongToDate(dobLongStr, dateFormat: df)
            dobButton.setTitle(date, for: UIControlState.normal)
            dobButton.setTitleColor(CustomColor.textBlackColor(), for: UIControlState.normal)
        } else {
            dobButton.setTitle("dob".localized, for: UIControlState.normal)
            dobButton.setTitleColor(CustomColor.textGrayColor(), for: UIControlState.normal)
        }
    }
    
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviPrimaryInfo".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(submitButtonClick))
        self.navigationItem.rightBarButtonItem = submitButton
    }
    
    func backgroundBlurImage(_ imageName: UIImage) {
        // show image
        backgroundImageView.image = imageName
        backgroundImageView.contentMode = UIViewContentMode.scaleToFill
        
        // create blur effect
        let blur = UIBlurEffect(style: UIBlurEffectStyle.light)
        
        // create vibrancy effect
        let vibrancy = UIVibrancyEffect(blurEffect: blur)
        
        // add blur to an effect view
        let effectView = UIVisualEffectView(effect: blur)
        effectView.frame = backgroundImageView.frame
        
        // add vibrancy to yet another effect view
        let vibrantView = UIVisualEffectView(effect: vibrancy)
        effectView.frame = backgroundImageView.frame
        
        // add both effect views to the image viewP
        backgroundImageView.addSubview(effectView)
        backgroundImageView.addSubview(vibrantView)
    }
    
    func configureTextField(_ textField: MKTextField, placeholder: String) -> MKTextField {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
        return textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if lastNameTextField == textField {
            scrollView.contentOffset = CGPoint.zero;
            return textField.resignFirstResponder()
        }
        return scrollView.focusNextTextField()
    }
    
    @IBAction func profileButtonClick(_ sender: UIButton) {
        self.dismissKeyboard()
        
        let profilePopUp = ProfileImagePopUpView(nibName: "ProfileImagePopUpView", bundle: nil)
        profilePopUp.delegate = self
        
        profilePopUp.image = imageName
        
        self.showPopUpViewController(selfViewController: self, presentViewController: profilePopUp)
    }
    
    func didSelectProfileImage(_ controller :ProfileImagePopUpView, image : UIImage) {
        self.imageName = image
        self.backgroundImageView.image = self.imageName
        self.profileImageView.setImage(self.imageName, for: UIControlState())
    }
    
    func didSelectGender(_ controller: GenderViewController, gender: String) {
        genderButton.setTitle(gender, for: UIControlState())
        genderButton.setTitleColor(CustomColor.textBlackColor(), for: UIControlState())
    }
    
    func submitButtonClick() {
        dismissKeyboard()
        
        if firstNameTextField.text!.trimmed == "" {
            SweetAlert().showAlert("Oops...", subTitle: "blankfName".localized, style: AlertStyle.warning, buttonTitle: "first", action: nil)
        } else if lastNameTextField.text!.trimmed == "" {
            SweetAlert().showAlert("Oops...", subTitle: "blanklName".localized, style: AlertStyle.warning, buttonTitle: "first", action: nil)
        } else {
            self.setUserInformation()
            self.updateOnServer()
        }
        
        self.storedProfileImage()
    }
    
    func storedProfileImage() {
        let mainPath: NSArray = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true) as NSArray
        
        path = (mainPath[0] as AnyObject).appendingPathComponent("\(Constant.FolderNames.mainFolderName)/\(Constant.FolderNames.folderUserProfile)/\(String(mySharedObj.getUserID()).sha1())\(Constant.FolderNames.imageExtension)")
        
        FileManagement.createFolder("\(Constant.FolderNames.mainFolderName)")
        FileManagement.createFolder("\(Constant.FolderNames.mainFolderName)/\(Constant.FolderNames.folderUserProfile)")
        let imageData = UIImageJPEGRepresentation(self.imageName, 0.1)
        try? imageData!.write(to: URL(fileURLWithPath: path), options: [.atomic])
    }
    
    func loadImage(_ getImage: String) -> UIImage? {
        let mainPath: NSArray = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true) as NSArray
        
        path = (mainPath[0] as AnyObject).appendingPathComponent("\(Constant.FolderNames.mainFolderName)/\(Constant.FolderNames.folderUserProfile)/\(getImage)\(Constant.FolderNames.imageExtension)")
        
        let image = UIImage(contentsOfFile: path)
        return image
    }
    
    @IBAction func genderButtonClick(_ sender: UIButton) {
        dismissKeyboard()
        
        let genderViewObj = GenderViewController(nibName: "GenderViewController", bundle: nil)
        genderViewObj.delegate = self
        genderViewObj.genderArray = ["Male", "Female", "Other"]
        self.showPopUpViewController(selfViewController: self, presentViewController: genderViewObj)
    }
    
    @IBAction func dobButtonClick(_ sender: UIButton) {
        dismissKeyboard()
        
        let datePicker = MDDatePickerDialog()
        if mySharedObj.getDOB() != "" {
            let df = DateFormatter()
            df.dateFormat = "dd MMM, yyyy"
            datePicker.selectedDate = df.date(from: (sender.titleLabel?.text)!)
        }
        
        datePicker.delegate = self
        datePicker.show()
    }
    
    func datePickerDialogDidSelect(_ date: Date) {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd MMM, yyyy"
        
        dobLongStr = ConvertionClass().conDateToLong(date)
        
        dobButton.setTitle(dateFormat.string(from: date), for: UIControlState.normal)
        dobButton.setTitleColor(CustomColor.textBlackColor(), for: UIControlState.normal)
    }
    
    func addBottomLayer(_ left: CGFloat, top: CGFloat, width: CGFloat) {
        let bottomLayer = CALayer()
        bottomLayer.backgroundColor = CustomColor.textBlackColor().cgColor
        bottomLayer.frame = CGRect(x: left, y: top, width: width, height: 1)
        self.view.layer.addSublayer(bottomLayer)
    }
    
    func setUserInformation() {
        mySharedObj.setFirstName(text: firstNameTextField.text!)
        mySharedObj.setLastName(text: lastNameTextField.text!)
                
        if mySharedObj.getGender() != "gender".localized {
            mySharedObj.setGender(text: genderButton.titleLabel!.text!)
        }
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        mySharedObj.setDOB(text: ConvertionClass().conLongToDate(dobLongStr, dateFormat: df))
    }
    
    // call for getting Register user
    func updateOnServer() {
        let appPref = MySharedPreferences()
        
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            self.view.addSubview(loadingObj.loading())
            
            appPref.setUpdateReqType(text: "assGrpPrimaryInfo".localized)
            
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
            
            //call for update old UI on server, only for RightLiving
            if appPref.getDrRefCode().caseInsensitiveCompare("RLIVING") == .orderedSame {
                //                    new ConnRequestManager().getRequestManager(mContext,
                //                            mContext.getString(R.string.MethodRegisterFitness),
                //                            PrimaryformationAct.this);
            }
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: AlertStyle.warning)
        }
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "editProfSuccess".localized, style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        SweetAlert().showAlert("Oops...", subTitle: "ServerNotWork".localized, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpPrimaryInfo".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        }
    }
    
    public func refreshParameters() {
        let appPref = MySharedPreferences()
        
        
        genderButton.setTitle(appPref.getGender(), for: .normal)
        firstNameTextField.text = appPref.getFirstName()
        lastNameTextField.text = appPref.getLastName()
    
        let dobStr = mySharedObj.getDOB()
        if !dobStr.isEmpty {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            
            if df.date(from: dobStr) != nil {
                dobLongStr = ConvertionClass().conDateToLong(df.date(from: dobStr)!)
            } else {
                df.dateFormat = "yyyy-MM-dd hh:mm:ss"
                dobLongStr = ConvertionClass().conDateToLong(df.date(from: dobStr)!)
            }
            
            df.dateFormat = "dd MMM, yyyy"
            let date = ConvertionClass().conLongToDate(dobLongStr, dateFormat: df)
            dobButton.setTitle(date, for: UIControlState.normal)
            dobButton.setTitleColor(CustomColor.textBlackColor(), for: UIControlState.normal)
        } else {
            dobButton.setTitle("dob".localized, for: UIControlState.normal)
            dobButton.setTitleColor(CustomColor.textGrayColor(), for: UIControlState.normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}

