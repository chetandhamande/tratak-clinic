//
//  DailyLifestyleAct.swift
//  Salk
//
//  Created by Chetan  on 16/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material
import DatePickerDialog

class DailyLifestyleAct: UIViewController {

    @IBOutlet weak var btnWakeup: UIButton!
    @IBOutlet weak var btnBreakfast: UIButton!
    @IBOutlet weak var btnLunch: UIButton!
    @IBOutlet weak var btnEvenTea: UIButton!
    @IBOutlet weak var btnDinner: UIButton!
    
    @IBOutlet weak var txtHobbies: UITextField!
    
    @IBOutlet weak var swichFruits: UISwitch!
    @IBOutlet weak var swichDryFruits: UISwitch!
    @IBOutlet weak var swichVeggi: UISwitch!
    @IBOutlet weak var swichAddGadgets: UISwitch!
    @IBOutlet weak var swichSmoke: UISwitch!
    @IBOutlet weak var swichAlcohol: UISwitch!
    
    fileprivate var loadingObj = CustomLoading()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareNavigationItem()
        self.loadPreviousData()
    
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviDailyLifestyle".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let submitImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_agree_colored.png")!, scaledToSize: CGSize(width: 20, height: 20))
        let submitButton = UIBarButtonItem(image: submitImage, style: .plain, target: self, action: #selector(self.performSubmit))
        
        navigationItem.rightBarButtonItems = [submitButton]
        
        btnWakeup.tag = 1
        btnBreakfast.tag = 2
        btnLunch.tag = 3
        btnEvenTea.tag = 4
        btnDinner.tag = 5
    }
    
    @IBAction func datePickerTapped(sender: UIButton) {
        
        //get current Date
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        
        var currDateStr = ""
        if (sender.tag == 1) {
            currDateStr = (btnWakeup.titleLabel?.text)!
        } else if (sender.tag == 2) {
            currDateStr = (btnBreakfast.titleLabel?.text)!
        } else if (sender.tag == 3) {
            currDateStr = (btnLunch.titleLabel?.text)!
        } else if (sender.tag == 4) {
            currDateStr = (btnEvenTea.titleLabel?.text)!
        } else if (sender.tag == 5) {
            currDateStr = (btnDinner.titleLabel?.text)!
        }
        
        var currDate = Date()
        if(!currDateStr.isEmpty) {
            currDate = df.date(from: currDateStr)!
        }
        
        DatePickerDialog().show(title: "DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: currDate,  datePickerMode: .time) {
            (date) -> Void in
            
            if date == nil { return } //skip on cancel button
            
            if sender.tag == 1 {
                self.btnWakeup.setTitle(df.string(from: date!), for: UIControlState.normal)
            } else if sender.tag == 2 {
                self.btnBreakfast.setTitle(df.string(from: date!), for: UIControlState.normal)
            } else if sender.tag == 3 {
                self.btnLunch.setTitle(df.string(from: date!), for: UIControlState.normal)
            } else if sender.tag == 4 {
                self.btnEvenTea.setTitle(df.string(from: date!), for: UIControlState.normal)
            } else if sender.tag == 5 {
                self.btnDinner.setTitle(df.string(from: date!), for: UIControlState.normal)
            }
        }
    }
}

extension DailyLifestyleAct {
    
    fileprivate func loadPreviousData() {
        
        let appPrefs = MySharedPreferences()
        
        var time = appPrefs.getWakeUpTime()
        if time.isEmpty {
            time = "06:00 AM"
        }
        btnWakeup.setTitle(time, for: UIControlState.normal)
        
        time = appPrefs.getBreakfastTime()
        if time.isEmpty {
            time = "07:00 AM"
        }
        btnBreakfast.setTitle(time, for: UIControlState.normal)
        
        time = appPrefs.getLunchTime()
        if time.isEmpty {
            time = "01:00 PM"
        }
        btnLunch.setTitle(time, for: UIControlState.normal)
        
        time = appPrefs.getEveningTeaTime()
        if time.isEmpty {
            time = "05:00 PM"
        }
        btnEvenTea.setTitle(time, for: UIControlState.normal)
        
        time = appPrefs.getDinnerTime()
        if time.isEmpty {
            time = "09:00 PM"
        }
        btnDinner.setTitle(time, for: UIControlState.normal)
        
        
        txtHobbies.text = appPrefs.getUserHobbies()
        
        if appPrefs.getFruitDaily().uppercased() == "YES" {
            swichFruits.isOn = true
        } else {
            swichFruits.isOn = false
        }
        
        if appPrefs.getFruitDietDry().uppercased() == "YES" {
            swichDryFruits.isOn = true
        } else {
            swichDryFruits.isOn = false
        }
        
        if appPrefs.getVegDaily().uppercased() == "YES" {
            swichVeggi.isOn = true
        } else {
            swichVeggi.isOn = false
        }
        
        if appPrefs.getAddictedGadget().uppercased() == "YES" {
            swichAddGadgets.isOn = true
        } else {
            swichAddGadgets.isOn = false
        }
        
        if appPrefs.getSmokeIntake().uppercased() != "NO" {
            swichSmoke.isOn = true
        } else {
            swichSmoke.isOn = false
        }
        
        if appPrefs.getAlcoholIntake().uppercased() != "NO" {
            swichAlcohol.isOn = true
        } else {
            swichAlcohol.isOn = false
        }
        
    }
    
    @objc fileprivate func performSubmit() {
        
        self.dismissKeyboard()
        
        let isInternet = CheckInternetConnection().isCheckInternetConnection()
        if isInternet {
            let appPrefs = MySharedPreferences()
            
            appPrefs.setWakeUpTime(text: (btnWakeup.titleLabel?.text)!)
            appPrefs.setBreakfastTime(text: (btnBreakfast.titleLabel?.text)!)
            appPrefs.setLunchTime(text: (btnLunch.titleLabel?.text)!)
            appPrefs.setEveningTeaTime(text: (btnEvenTea.titleLabel?.text)!)
            appPrefs.setDinnerTime(text: (btnDinner.titleLabel?.text)!)
            
            appPrefs.setUserHobbies(text: txtHobbies.text!)
            
            if swichFruits.isOn {
                appPrefs.setFruitDaily(text: "YES")
            } else {
                appPrefs.setFruitDaily(text: "NO")
            }
            
            if swichDryFruits.isOn {
                appPrefs.setFruitDietDry(text: "YES")
            } else {
                appPrefs.setFruitDietDry(text: "NO")
            }
            
            if swichVeggi.isOn {
                appPrefs.setVegDaily(text: "YES")
            } else {
                appPrefs.setVegDaily(text: "NO")
            }
            
            if swichAddGadgets.isOn {
                appPrefs.setAddictedGadget(text: "YES")
            } else {
                appPrefs.setAddictedGadget(text: "NO")
            }
            
            if swichSmoke.isOn {
                appPrefs.setSmokeIntake(text: "YES")
            } else {
                appPrefs.setSmokeIntake(text: "NO")
            }
            
            if swichAlcohol.isOn {
                appPrefs.setAlcoholIntake(text: "YES")
            } else {
                appPrefs.setAlcoholIntake(text: "NO")
            }
            
            self.view.addSubview(loadingObj.loading())
            
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            showSweetDialog(title: "Oops...", message: "noInternet".localized)
        }
        
    }
    
    fileprivate func showSweetDialog(title: String, message: String) {
        SweetAlert().showOnlyAlert(title, subTitle: message, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        showSweetDialog(title: "Oops...", message: appPref.getErrorMessage())
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showOnlyAlert("Success", subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
}
