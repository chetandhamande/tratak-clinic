//
//  PainHistoryView.swift
//  Salk
//
//  Created by Chetan on 22/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class PainHistoryView: UIView {

    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var editAche: MKTextField!
    @IBOutlet weak var editHead: MKTextField!
    @IBOutlet weak var editUpperLimb: MKTextField!
    @IBOutlet weak var editLowerLimb: MKTextField!
    @IBOutlet weak var editChest: MKTextField!
    @IBOutlet weak var editBack: MKTextField!
    @IBOutlet weak var editAbdomen: MKTextField!
    
    @IBOutlet weak var btnAche: UIButton!
    @IBOutlet weak var btnAcheDay: UIButton!
    @IBOutlet weak var btnHead: UIButton!
    @IBOutlet weak var btnHeadDay: UIButton!
    @IBOutlet weak var btnUpperLimb: UIButton!
    @IBOutlet weak var btnUpperLimbDay: UIButton!
    @IBOutlet weak var btnLowerLimb: UIButton!
    @IBOutlet weak var btnLowerLimbDay: UIButton!
    @IBOutlet weak var btnChest: UIButton!
    @IBOutlet weak var btnChestDay: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnBackDay: UIButton!
    @IBOutlet weak var btnAbdomen: UIButton!
    @IBOutlet weak var btnAbdomenDay: UIButton!
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        self.viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PainHistoryView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        //        headerView.backgroundColor = PrimaryColor.blackTransparent1()
        //
        //        lblHeading.text = "Major Medical Complaints"
        
        configureTextField(editAche, placeholder: "")
        configureTextField(editHead, placeholder: "")
        configureTextField(editUpperLimb, placeholder: "")
        configureTextField(editLowerLimb, placeholder: "")
        configureTextField(editChest, placeholder: "")
        configureTextField(editBack, placeholder: "")
        configureTextField(editAbdomen, placeholder: "")

        configureButton(btnAche, setString: "None")
        configureButton(btnAcheDay, setString: "Day")
        configureButton(btnHead, setString: "None")
        configureButton(btnHeadDay, setString: "Day")
        configureButton(btnUpperLimb, setString: "None")
        configureButton(btnUpperLimbDay, setString: "Day")
        configureButton(btnLowerLimb, setString: "None")
        configureButton(btnLowerLimbDay, setString: "Day")
        configureButton(btnChest, setString: "None")
        configureButton(btnChestDay, setString: "Day")
        configureButton(btnBack, setString: "None")
        configureButton(btnBackDay, setString: "Day")
        configureButton(btnAbdomen, setString: "None")
        configureButton(btnAbdomenDay, setString: "Day")
    }
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.keyboardType = .decimalPad
    }
    
    func configureLabel(_ label: UILabel, setString: String) {
        label.text = setString
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = CustomColor.darker_gray()
    }
    
    func configureButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textGrayColor(), for: UIControlState.normal)
    }
    
    func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textBlackColor(), for: UIControlState.normal)
        
        refreshView()
    }
    
    func refreshView() {
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
}
