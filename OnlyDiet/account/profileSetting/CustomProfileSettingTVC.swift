//
//  CustomProfileSettingTVC.swift
//  Salk
//
//  Created by Chetan  on 16/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomProfileSettingTVC: UITableViewCell {

    @IBOutlet weak var imgOption: UIImageView!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var txtOther: UITextField!
    @IBOutlet weak var stackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func onBindViewHolder(_ featureList: [ArrSetting], position: Int, classRef: AnyObject)
    {
        let user: ArrSetting = featureList[position]
        
        lblDetails.text = user.heading
        
        if user.isRadio {
            if user.isCheck {
                imgOption.image = UIImage(named: "radio_on.png")
            } else {
                imgOption.image = UIImage(named: "radio_off.png")
            }
        } else {
            if user.isCheck {
                imgOption.image = UIImage(named: "check_select.png")
            } else {
                imgOption.image = UIImage(named: "check_unselect.png")
            }
        }
        
        if user.heading.isEmpty {
            stackView.isHidden = true
            txtOther.isHidden = false
            
            txtOther.text = ""
        } else {
            stackView.isHidden = false
            txtOther.isHidden = true
            
            txtOther.text = user.otherData
        }
        
    }
    
}
