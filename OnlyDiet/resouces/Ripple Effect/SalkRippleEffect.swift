//
//  SalkRippleEffect.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation
import MaterialControls

class SalkRippleEffect: MDRippleLayer {
    class func SalkRippleEffect(_ rippleView: UIView)  {
        let r = MDRippleLayer(superView: rippleView)
        r.effectColor = CustomColor.light_gray()
        r.enableElevation = false
        r.effectSpeed = 300
    }
}
