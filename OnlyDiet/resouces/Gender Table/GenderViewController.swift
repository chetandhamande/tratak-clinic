//
//  GenderViewController.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

protocol GenderViewDelegate {
    func didSelectGender(_ controller :GenderViewController, gender : String)
}

class GenderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var delegate: GenderViewDelegate! = nil
    
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var genderTableView: UITableView!
    @IBOutlet weak var selectGenderLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    let genderCellIdentifier = "GenderTableCell"
    
    var genderArray = [String]()
    var headingStr = "Select Any One"
    var gender = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.genderTableView.delegate = self
        self.genderTableView.dataSource = self
    
        self.viewConfiguration()
    }

    func viewConfiguration() {
        self.view.backgroundColor = CustomColor.black_semi_transparent()
        
        genderView.layer.cornerRadius = 3
        selectGenderLabel.font = UIFont.boldSystemFont(ofSize: 16)
        cancelButton.tintColor = PrimaryColor.colorPrimary()
        okButton.tintColor = PrimaryColor.colorPrimary()
        
        selectGenderLabel.text = headingStr
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return genderArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var genderCell: GenderTableCell!  = genderTableView.dequeueReusableCell(withIdentifier: genderCellIdentifier) as? GenderTableCell
        
        if genderCell == nil {
            genderTableView.register(UINib(nibName: genderCellIdentifier, bundle: nil), forCellReuseIdentifier: genderCellIdentifier)
            genderCell = genderTableView.dequeueReusableCell(withIdentifier: genderCellIdentifier) as? GenderTableCell
        }
        
        genderCell.genderLabel.text = genderArray[indexPath.row]
        
        return genderCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let genderCell = genderTableView.cellForRow(at: indexPath) as! GenderTableCell
        
        genderCell.genderButton.imageView!.image = UIImage(named: "radio_on.png")
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let genderCell = genderTableView.cellForRow(at: indexPath) as! GenderTableCell
        
        genderCell.genderButton.imageView!.image = UIImage(named: "radio_off.png")
        genderTableView.reloadData()
    }
    
    @IBAction func cancelButtonClick(_ sender: UIButton) {
        self.dismissGenderPopUpView()
    }
    
    @IBAction func okButtonClick(_ sender: UIButton) {
        if genderTableView.indexPathForSelectedRow == nil {
            Print.printLog("Not Select")
        } else {
            delegate?.didSelectGender(self, gender: genderArray[genderTableView.indexPathForSelectedRow!.row])
        }
        self.dismissGenderPopUpView()
    }
    
    func dismissGenderPopUpView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
