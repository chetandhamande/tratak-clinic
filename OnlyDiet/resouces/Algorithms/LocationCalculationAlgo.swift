//
//  LocationCalculationAlgo.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation
import CoreLocation

extension Double {
    func DegreesToRadians (_ value:Double) -> Double {
        return value * M_PI / 180.0
    }
    
    func RadiansToDegrees (_ value:Double) -> Double {
        return value * 180.0 / M_PI
    }
}


@objc class LocationCalculationAlgo: NSObject {
    
    /**
     * get the distance in meter by providing two lat long parameters
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return
     */
    
    class func HaversineInMeter(_ lat1:Double, lon1:Double, lat2:Double, lon2:Double) -> Double {
        let lat1rad = lat1 * M_PI/180
        let lon1rad = lon1 * M_PI/180
        let lat2rad = lat2 * M_PI/180
        let lon2rad = lon2 * M_PI/180
        
        let dLat = lat2rad - lat1rad
        let dLon = lon2rad - lon1rad
        let a = sin(dLat/2) * sin(dLat/2) + sin(dLon/2) * sin(dLon/2) * cos(lat1rad) * cos(lat2rad)
        let c = 2 * asin(sqrt(a))
        let R = 6372.8
        
        return R * c * 1000
    }
    
    /**
    * Function for getting Degree between two lat longs
    * @param lat1
    * @param lng1
    * @param lat2
    * @param lng2
    * @return
    */
    class func DegreeBeeringInLatLong(_ lat1: Double, lon1: Double, lat2: Double, lon2: Double) -> CLLocationDirection {
        let lat1 = Double().DegreesToRadians(lat1)
        let lat2 = Double().DegreesToRadians(lat2)
        let lon1 = Double().DegreesToRadians(lon1)
        let lon2 = Double().DegreesToRadians(lon2)
        let dLon = lon2-lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        var brng = Double().RadiansToDegrees(atan2(y, x))
        
        brng = brng + 360
        brng = brng > 360 ? brng - 360 : brng
        
        return brng
    }
}
