//
//  ArrDoctor.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

public class ArrDoctor {
    public var doctorID = "",fname = "", lname = "", email = "", mobile = "", speciality = "", speciality_id = ""
    public var listLayoutIndex: Int = 0
}
