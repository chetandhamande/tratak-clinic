//
//  PlanSelectionAct.swift
//  Salk

import UIKit
import Material
import RAMAnimatedTabBarController
import Async

class PlanSelectionAct: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    fileprivate var mRefreshControl: UIRefreshControl!

    private var arrMainList: Array<ArrPlanList>! = nil
    let CustomAdapterPlanListIdentifire = "CustomAdapterPlanList"
    var offscreenCells = Dictionary<String, UICollectionViewCell>()
    
    private let loading = CustomLoading()
    private let appPref = MySharedPreferences()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        let nib = UINib(nibName: CustomAdapterPlanListIdentifire, bundle: nil)
        mainCollectionView.register(nib, forCellWithReuseIdentifier: CustomAdapterPlanListIdentifire)
        
        // Do any additional setup after loading the view.
        prepareNavigationItem()
        viewConfiguration()
        onCreateView()
        syncPlanList(isForce: false)
        
        //pull to refresh
        self.mainCollectionView.alwaysBounceVertical = true
        mRefreshControl = UIRefreshControl()
        mRefreshControl.addTarget(self, action: #selector(self.handleRefresh), for: UIControlEvents.valueChanged)
        mainCollectionView.addSubview(mRefreshControl)
    }
    
    @objc fileprivate func handleRefresh() {
        syncPlanList(isForce: true)
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Premium Plans".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    private func onCreateView() {
    
        arrMainList = [ArrPlanList]()
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
    
        let sql = "Select * from " + datasource.PurchasePlanList_tlb
            + " ORDER BY " + datasource.dbMsgTitle + " ;";
    
        let cursor = datasource.selectRecords(sql);
        if cursor != nil {
            while cursor!.next() {
                let temp = ArrPlanList()
                temp.planID = cursor!.string(forColumnIndex: 0);
                temp.planName = cursor!.string(forColumnIndex: 1);
                temp.price = cursor!.string(forColumnIndex: 2);
                temp.comboPlan = cursor!.string(forColumnIndex: 3);
                temp.comboPrice = cursor!.string(forColumnIndex: 4);
                temp.planDetail = cursor!.string(forColumnIndex: 5);
                temp.features = cursor!.string(forColumnIndex: 6);
                temp.longDesc = cursor!.string(forColumnIndex: 7);
                temp.imageName = cursor!.string(forColumnIndex: 8);
    
                arrMainList.append(temp);
            }
            cursor!.close();
        }
        datasource.closeDatabase()
        
        mainCollectionView.reloadData()
        mainCollectionView.isHidden = false
    }
    
    fileprivate func syncPlanList(isForce: Bool) {
        //Sync plan from server
        let appPre = MySharedPreferences();
        let time = (ConvertionClass().currentTime() - appPre.getLastPlanLogListSycnTime()) / 60; //in minute
        if (time > 10 || arrMainList.count <= 0 || isForce) {
            let checkConn = CheckInternetConnection()
            if (checkConn.isCheckInternetConnection()) {
                self.view.addSubview(loading.loading())
                
                DashboardRequestManager().getRequest(Constant.Purchase.MethodSyncPlan, classRef: self)
            } else {
                SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
            }
        }
    }
    
    public func refreshPlanList() {
        loading.removeFromSuperview()
        mRefreshControl.endRefreshing()
        onCreateView()
    }
    
    func viewConfiguration() {
        self.mainCollectionView.backgroundColor = CustomColor.background()
        self.view.backgroundColor = CustomColor.background()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMainList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CustomAdapterPlanList = collectionView.dequeueReusableCell(withReuseIdentifier: CustomAdapterPlanListIdentifire, for: indexPath) as! CustomAdapterPlanList
        
        cell.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        // Make sure layout subviews
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        if(!arrMainList[indexPath.row].imageName.isEmpty) {
            let temp = "\(SalkProjectConstant().ServerFitnessChatAttach + appPref.getCompanyID())/plan/\(arrMainList[indexPath.row].imageName)"
            let URL = Foundation.URL(string: temp)!
            cell.imgBackground.kf.setImage(with: URL, placeholder: nil, options: [.transition(.fade(1))], progressBlock: { receivedSize, totalSize in
                Print.printLog("\(receivedSize)/\(totalSize)")
            }, completionHandler: { image, error, cacheType, imageURL in
                if error != nil {
                    cell.imgBackground.image = UIImage().imageWithImage(UIImage(named: "back.jpg")!, scaledToSize: CGSize(width: 50, height: 50))
                }
            })
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //open vehical details list
        
        let a = PlanCheckoutVC()
        a.planID = arrMainList[indexPath.row].planID
        a.planName = arrMainList[indexPath.row].planName
        a.planDetail = arrMainList[indexPath.row].planDetail
        a.comboPlan = arrMainList[indexPath.row].comboPlan
        a.comboPrice = arrMainList[indexPath.row].comboPrice
        a.imageName = arrMainList[indexPath.row].imageName
        a.longDesc = arrMainList[indexPath.row].longDesc
        a.price = arrMainList[indexPath.row].price
        a.features = arrMainList[indexPath.row].features
        self.navigationController?.pushViewController(a, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let orientation = UIApplication.shared.statusBarOrientation
        var cellWidth = mainCollectionView.frame.width
        
        // Use fake cell to calculate height
        let reuseIdentifier = CustomAdapterPlanListIdentifire
        var cell: CustomAdapterPlanList? = self.offscreenCells[reuseIdentifier] as? CustomAdapterPlanList
        if cell == nil {
            cell = Bundle.main.loadNibNamed(CustomAdapterPlanListIdentifire, owner: self, options: nil)![0] as? CustomAdapterPlanList
            self.offscreenCells[reuseIdentifier] = cell
        }
        
        cell!.onBindViewHolder(arrMainList, position: indexPath.row, classRef: self)
        
        if traitCollection.horizontalSizeClass == .regular || traitCollection.verticalSizeClass == .regular
        {
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                if(orientation == .portrait) {
                    cellWidth = (cellWidth - 1) / 2
                } else {
                    cellWidth = (cellWidth - 5) / 3
                }
            } else {
                if(orientation == .portrait) {
                } else {
                    cellWidth = (cellWidth - 1) / 2
                }
            }
        } else {
            if(orientation == .portrait) {
            } else {
                cellWidth = (cellWidth - 1) / 2
            }
        }
        
        cell!.bounds = CGRect(x: 0, y: 0, width: cellWidth, height: cell!.bounds.height)
        cell!.contentView.bounds = cell!.bounds
        
        // Layout subviews, this will let labels on this cell to set preferredMaxLayoutWidth
        cell!.setNeedsLayout()
        cell!.layoutIfNeeded()
        
        var size = cell!.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        // Still need to force the width, since width can be smalled due to break mode of labels
        size.width = cellWidth
        return size
    }

}
