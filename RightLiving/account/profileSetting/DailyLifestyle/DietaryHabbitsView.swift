//
//  DietaryHabbitsView.swift
//  Salk
//
//  Created by Chetan on 13/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import DLRadioButton

class DietaryHabbitsView: UIView {

    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var fruitYes: DLRadioButton!
    @IBOutlet weak var fruitNo: DLRadioButton!
    @IBOutlet weak var dietDryYes: DLRadioButton!
    @IBOutlet weak var dietDryNo: DLRadioButton!
    @IBOutlet weak var vegDailyYes: DLRadioButton!
    @IBOutlet weak var vegDailyNo: DLRadioButton!
    
    @IBOutlet weak var eatDaily: DLRadioButton!
    @IBOutlet weak var eatWeekly: DLRadioButton!
    @IBOutlet weak var eatMonth: DLRadioButton!
    @IBOutlet weak var eatRear: DLRadioButton!
    
    var eatout_preferences = "",
    eat_fruits_daily = "", diet_dry_fruits = "", eat_vegetables = "";
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "DietaryHabbitsView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        //        headerView.backgroundColor = PrimaryColor.blackTransparent1()
        //
        //        lblHeading.text = "Major Medical Complaints"
        
        
        createRadioButton(fruitYes, title: "Yes")
        createRadioButton(fruitNo, title: "No")
        createRadioButton(dietDryYes, title: "Yes")
        createRadioButton(dietDryNo, title: "No")
        createRadioButton(vegDailyYes, title: "Yes")
        createRadioButton(vegDailyNo, title: "No")
        
        createRadioButton(eatDaily, title: "Daily")
        createRadioButton(eatWeekly, title: "Weekly")
        createRadioButton(eatMonth, title: "Once a month")
        createRadioButton(eatRear, title: "Very rare")
    }
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.keyboardType = .decimalPad
    }
    
    func configureLabel(_ label: UILabel, setString: String) {
        label.text = setString
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = CustomColor.darker_gray()
    }
    
    func configureButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textGrayColor(), for: UIControlState.normal)
    }
    
    func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textBlackColor(), for: UIControlState.normal)
        
        refreshView()
    }
    
    func refreshView() {
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
    
    private func createRadioButton(_ button: DLRadioButton, title: String) {
        button.indicatorColor = CustomColor.dark_gray()
        button.setTitle(title, for: .normal)
        button.setTitleColor(CustomColor.dark_gray(), for: .normal)
        button.isIconSquare = false
        button.isMultipleSelectionEnabled = false
    }
    
    @objc @IBAction private func radioFruitClick(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            eat_fruits_daily = radioButton.selected()!.titleLabel!.text!
        }
    }
    
    @objc @IBAction private func radioDietClick(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            diet_dry_fruits = radioButton.selected()!.titleLabel!.text!
        }
    }
    
    @objc @IBAction private func radioVegClick(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            eat_vegetables = radioButton.selected()!.titleLabel!.text!
        }
    }
    
    @objc @IBAction private func radioEatClick(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            eatout_preferences = radioButton.selected()!.titleLabel!.text!
        }
    }
}


