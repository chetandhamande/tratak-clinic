//
//  HerbsView.swift
//  Salk
//
//  Created by Chetan on 02/01/18.
//  Copyright © 2018 Muddy Roads Technology. All rights reserved.
//

import UIKit
import SnapKit

class HerbsView: UIView, UITableViewDelegate, UITableViewDataSource {

    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var backCardView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    
    @IBOutlet weak var currTableView: UITableView!
        
    let currTableCellIdentifier = "CurrentHistoryCell"
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "HerbsView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        currTableView.delegate = self
        currTableView.dataSource = self
        
        currTableView.snp.makeConstraints { (make: ConstraintMaker) in
            make.height.equalTo(200 * 8)
        }
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        headerView.backgroundColor = CustomColor.blackTransparent1()
    }
    
    func configureView(_ classRef: AnyObject) {
        self.classRefs = classRef
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var currCell: CurrentHistoryCell!  = currTableView.dequeueReusableCell(withIdentifier: currTableCellIdentifier) as? CurrentHistoryCell
        
        if currCell == nil {
            currTableView.register(UINib(nibName: currTableCellIdentifier, bundle: nil), forCellReuseIdentifier: currTableCellIdentifier)
            currCell = currTableView.dequeueReusableCell(withIdentifier: currTableCellIdentifier) as? CurrentHistoryCell
        }
        
        currCell.configureCell(classRefs)
        
        return currCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
