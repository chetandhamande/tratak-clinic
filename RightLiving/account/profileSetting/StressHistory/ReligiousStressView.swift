//
//  ReligiousStressView.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class ReligiousStressView: UIView {

    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var editRelDuration: MKTextField!
    @IBOutlet weak var editRelSource: MKTextField!
    @IBOutlet weak var editRelCause: MKTextField!
    @IBOutlet weak var editRelTrigger: MKTextField!
    @IBOutlet weak var editRelRate: MKTextField!
    
    @IBOutlet weak var btnRelMonth: UIButton!
    @IBOutlet weak var btnRelGrade: UIButton!
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        self.viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ReligiousStressView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        //        headerView.backgroundColor = PrimaryColor.blackTransparent1()
        //
        //        lblHeading.text = "Major Medical Complaints"
        
        configureTextField(editRelDuration, placeholder: "")
        configureTextField(editRelSource, placeholder: "")
        configureTextField(editRelCause, placeholder: "")
        configureTextField(editRelTrigger, placeholder: "")
        configureTextField(editRelRate, placeholder: "")
        
        configureButton(btnRelMonth, setString: "Month")
        configureButton(btnRelGrade, setString: "None")
    }
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
        
        if textField == editRelDuration  {
            textField.keyboardType = .decimalPad
        }
    }
    
    func configureLabel(_ label: UILabel, setString: String) {
        label.text = setString
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = CustomColor.darker_gray()
    }
    
    func configureButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textGrayColor(), for: UIControlState.normal)
    }
    
    func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textBlackColor(), for: UIControlState.normal)
        
        refreshView()
    }
    
    func refreshView() {
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
    
}


