//
//  MedicalHistoryVC.swift
//  Salk
//
//  Created by Chetan on 07/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class MedicalHistoryVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource  {

    @IBOutlet weak var mainScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var objMajHView: MajorMedicalCmptView!
    @IBOutlet weak var objMinHView: MinorMedicalCmptView!
    @IBOutlet weak var objMedRView: MedicalReportView!
    
    fileprivate var loadingObj = CustomLoading()
    fileprivate let arr = ["Day", "Week", "Month", "Year"]
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareNavigationItem()
        
        mainScrollView.backgroundColor = CustomColor.background()
        
        picker.isHidden = true
        picker.backgroundColor = CustomColor.grayLight()
        picker.showsSelectionIndicator = true
        self.picker.delegate = self
        self.picker.dataSource = self
        UIApplication.shared.keyWindow!.bringSubview(toFront: picker)
        
        init1()
        visiOprMajComp()
        
        //getting Latest record
        getRecordServer()
    }
    
    private func init1() {
        let tap = UITapGestureRecognizer(target: self, action:#selector(MedicalHistoryVC.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        objMajHView.btn1.addTarget(self, action: #selector(perMajBtn1), for: .touchUpInside)
        objMajHView.btn2.addTarget(self, action: #selector(perMajBtn2), for: .touchUpInside)
        objMajHView.btn3.addTarget(self, action: #selector(perMajBtn3), for: .touchUpInside)
        objMajHView.btn4.addTarget(self, action: #selector(perMajBtn4), for: .touchUpInside)
        
        objMinHView.btn1.addTarget(self, action: #selector(perMinBtn1), for: .touchUpInside)
        objMinHView.btn2.addTarget(self, action: #selector(perMinBtn2), for: .touchUpInside)
        objMinHView.btn3.addTarget(self, action: #selector(perMinBtn3), for: .touchUpInside)
        objMinHView.btn4.addTarget(self, action: #selector(perMinBtn4), for: .touchUpInside)
        
        refreshParameters()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviMedicalCond".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
        
        let submitButton = UIBarButtonItem(title: "SAVE", style: .bordered, target: self, action: #selector(performNextOpr))
        self.navigationItem.rightBarButtonItem = submitButton
    }
    
    func refreshParameters() {
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        let sql = "SELECT * FROM " + datasource.AssessmentList_tlb
            + " WHERE " + datasource.dbGroupType + " = '" + "assGrpMedicalHistory".localized + "' "
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("major_complaint1") {
                    objMajHView.txtCmpt1.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("major_since1") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objMajHView.txtDur1.text = duraArr[0]
                        objMajHView.configureSelectButton(objMajHView.btn1, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("major_complaint2") {
                    objMajHView.txtCmpt2.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("major_since2") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objMajHView.txtDur2.text = duraArr[0]
                        objMajHView.configureSelectButton(objMajHView.btn2, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("major_complaint3") {
                    objMajHView.txtCmpt3.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("major_since3") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objMajHView.txtDur3.text = duraArr[0]
                        objMajHView.configureSelectButton(objMajHView.btn3, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("major_complaint4") {
                    objMajHView.txtCmpt4.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("major_since4") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objMajHView.txtDur4.text = duraArr[0]
                        objMajHView.configureSelectButton(objMajHView.btn4, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("minor_complaint1") {
                    objMinHView.txtCmpt1.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("minor_since1") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objMinHView.txtDur1.text = duraArr[0]
                        objMajHView.configureSelectButton(objMinHView.btn1, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("minor_complaint2") {
                    objMinHView.txtCmpt2.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("minor_since2") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objMinHView.txtDur2.text = duraArr[0]
                        objMajHView.configureSelectButton(objMinHView.btn2, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("minor_complaint3") {
                    objMinHView.txtCmpt3.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("minor_since3") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objMinHView.txtDur3.text = duraArr[0]
                        objMajHView.configureSelectButton(objMinHView.btn3, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("minor_complaint4") {
                    objMinHView.txtCmpt4.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("minor_since4") {
                    guard let dura = cursor!.string(forColumnIndex: 1) else {
                        return
                    }
                    if !dura.trimmed.isEmpty {
                        let duraArr = dura.split(" ")
                        objMinHView.txtDur4.text = duraArr[0]
                        objMajHView.configureSelectButton(objMinHView.btn4, setString: duraArr[1])
                    }
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("past_illness") {
                    objMedRView.txtIllness.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("prev_hospitalization") {
                    objMedRView.txtPrev.text = cursor!.string(forColumnIndex: 1)
                } else if cursor!.string(forColumnIndex: 0).equalsIgnoreCase("surgery") {
                    objMedRView.txtSurgery.text = cursor!.string(forColumnIndex: 1)
                }
            }
            cursor?.close();
            datasource.closeDatabase();
            
//            if(!editMajorCmpt2.getText().toString().trim().isEmpty()){
//                linMajComp2.setVisibility(View.VISIBLE);
//            }
//
//            if(!editMajorCmpt3.getText().toString().trim().isEmpty()){
//                linMajComp3.setVisibility(View.VISIBLE);
//            }
//
//            if(!editMajorCmpt4.getText().toString().trim().isEmpty()){
//                linMajComp4.setVisibility(View.VISIBLE);
//            }
//
//            if(!editMinorCmpt2.getText().toString().trim().isEmpty()){
//                linMinComp2.setVisibility(View.VISIBLE);
//            }
//
//            if(!editMinorCmpt3.getText().toString().trim().isEmpty()){
//                linMinComp3.setVisibility(View.VISIBLE);
//            }
//
//            if(!editMinorCmpt4.getText().toString().trim().isEmpty()){
//                linMinComp4.setVisibility(View.VISIBLE);
//            }
        }
    }
    
    @objc func performNextOpr() {
        dismissKeyboard()
        
        let datasource = DBOperation()
        datasource.openDatabase(true)
        
        datasource.UpdateAssessmentItemTable(paramName: "major_complaint1", paramVal: objMajHView.txtCmpt1.text!, groupType: "assGrpMedicalHistory".localized)
        if !objMajHView.txtDur1.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "major_since1", paramVal: objMajHView.txtDur1.text! + " " + objMajHView.btn1.titleLabel!.text!, groupType: "assGrpMedicalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "major_complaint2", paramVal: objMajHView.txtCmpt2.text!, groupType: "assGrpMedicalHistory".localized)
        if !objMajHView.txtDur2.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "major_since2", paramVal: objMajHView.txtDur2.text! + " " + objMajHView.btn2.titleLabel!.text!, groupType: "assGrpMedicalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "major_complaint3", paramVal: objMajHView.txtCmpt3.text! , groupType: "assGrpMedicalHistory".localized)
        if !objMajHView.txtDur3.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "major_since3", paramVal: objMajHView.txtDur3.text! + " " + objMajHView.btn3.titleLabel!.text!, groupType: "assGrpMedicalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "major_complaint4", paramVal: objMajHView.txtCmpt4.text!, groupType: "assGrpMedicalHistory".localized)
        if !objMajHView.txtDur4.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "major_since4", paramVal: objMajHView.txtDur4.text! + " " + objMajHView.btn4.titleLabel!.text!, groupType: "assGrpMedicalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "minor_complaint1", paramVal: objMinHView.txtCmpt1.text!, groupType: "assGrpMedicalHistory".localized)
        if !objMinHView.txtDur1.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "minor_since1", paramVal: objMinHView.txtDur1.text! + " " + objMinHView.btn1.titleLabel!.text!, groupType: "assGrpMedicalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "minor_complaint2", paramVal: objMinHView.txtCmpt2.text!, groupType: "assGrpMedicalHistory".localized)
        if !objMinHView.txtDur2.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "minor_since2", paramVal: objMinHView.txtDur2.text! + " " + objMinHView.btn2.titleLabel!.text!, groupType: "assGrpMedicalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "minor_complaint3", paramVal: objMinHView.txtCmpt3.text!, groupType: "assGrpMedicalHistory".localized)
        if !objMinHView.txtDur3.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "minor_since3", paramVal: objMinHView.txtDur3.text! + " " + objMinHView.btn3.titleLabel!.text!, groupType: "assGrpMedicalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "minor_complaint4", paramVal: objMinHView.txtCmpt4.text!, groupType: "assGrpMedicalHistory".localized)
        if !objMinHView.txtDur4.text!.isEmpty {
            datasource.UpdateAssessmentItemTable(paramName: "minor_since4", paramVal: objMinHView.txtDur4.text! + " " + objMinHView.btn4.titleLabel!.text!, groupType: "assGrpMedicalHistory".localized)
        }
        
        datasource.UpdateAssessmentItemTable(paramName: "past_illness", paramVal: objMedRView.txtIllness.text!, groupType: "assGrpMedicalHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "prev_hospitalization", paramVal: objMedRView.txtPrev.text!, groupType: "assGrpMedicalHistory".localized)
        datasource.UpdateAssessmentItemTable(paramName: "surgery", paramVal: objMedRView.txtSurgery.text!, groupType: "assGrpMedicalHistory".localized)
        
        datasource.closeDatabase();
        
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            
            self.view.addSubview(loadingObj.loading())
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpMedicalHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }

    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            let appPref = MySharedPreferences()
            appPref.setUpdateReqType(text: "assGrpMedicalHistory".localized)
            UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodSyncProfile, classRef: self)
        }
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        SweetAlert().showAlert("Oops...", subTitle: appPref.getErrorMessage(), style: .warning)
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showAlert("success".localized, subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: PrimaryColor.colorPrimary()) { (isOkButton) -> Void in
            if isOkButton == true {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    //method for visibility of major complaint
    private func visiOprMajComp() {
//    final TextView btnAddMajComp = (TextView) findViewById(R.id.btnAddMajorComp);
//    final TextView btnRemoveMajComp = (TextView) findViewById(R.id.btnRemoveMajorComp);
//    btnAddMajComp.setOnClickListener(new View.OnClickListener() {
//    @Override
//    public void onClick(View v) {
//
//    if (linMajComp2.getVisibility() != View.VISIBLE) {
//    linMajComp2.setVisibility(View.VISIBLE);
//    btnRemoveMajComp.setVisibility(View.VISIBLE);
//    } else if (linMajComp2.getVisibility() == View.VISIBLE
//    && linMajComp3.getVisibility() != View.VISIBLE) {
//    linMajComp3.setVisibility(View.VISIBLE);
//    } else if (linMajComp2.getVisibility() == View.VISIBLE
//    && linMajComp3.getVisibility() == View.VISIBLE
//    && linMajComp4.getVisibility() != View.VISIBLE) {
//    linMajComp4.setVisibility(View.VISIBLE);
//    btnAddMajComp.setVisibility(View.GONE);
//    }
//
//    }
//    });
//
//
//    btnRemoveMajComp.setOnClickListener(new View.OnClickListener() {
//    @Override
//    public void onClick(View v) {
//    if (linMajComp2.getVisibility() == View.VISIBLE
//    && linMajComp3.getVisibility() == View.VISIBLE
//    && linMajComp4.getVisibility() == View.VISIBLE) {
//    linMajComp4.setVisibility(View.GONE);
//    btnAddMajComp.setVisibility(View.VISIBLE);
//    } else if (linMajComp2.getVisibility() == View.VISIBLE
//    && linMajComp3.getVisibility() == View.VISIBLE
//    && linMajComp4.getVisibility() != View.VISIBLE) {
//    linMajComp3.setVisibility(View.GONE);
//    } else if (linMajComp2.getVisibility() == View.VISIBLE
//    && linMajComp3.getVisibility() != View.VISIBLE
//    && linMajComp4.getVisibility() != View.VISIBLE) {
//    linMajComp2.setVisibility(View.GONE);
//    btnRemoveMajComp.setVisibility(View.GONE);
//
//    }
//    }
//    });
//
//    final TextView btnAddMinComp = (TextView) findViewById(R.id.btnAddMinorComp);
//    final TextView btnRemoveMinComp = (TextView) findViewById(R.id.btnRemoveMinorComp);
//    btnAddMinComp.setOnClickListener(new View.OnClickListener() {
//    @Override
//    public void onClick(View v) {
//    if (linMinComp2.getVisibility() != View.VISIBLE) {
//    linMinComp2.setVisibility(View.VISIBLE);
//    btnRemoveMinComp.setVisibility(View.VISIBLE);
//    } else if (linMinComp2.getVisibility() == View.VISIBLE
//    && linMinComp3.getVisibility() != View.VISIBLE) {
//    linMinComp3.setVisibility(View.VISIBLE);
//    } else if (linMinComp2.getVisibility() == View.VISIBLE
//    && linMinComp3.getVisibility() == View.VISIBLE
//    && linMinComp4.getVisibility() != View.VISIBLE) {
//    linMinComp4.setVisibility(View.VISIBLE);
//    btnAddMinComp.setVisibility(View.GONE);
//    }
//
//    }
//    });
//
//    btnRemoveMinComp.setOnClickListener(new View.OnClickListener() {
//    @Override
//    public void onClick(View v) {
//    if (linMinComp2.getVisibility() == View.VISIBLE
//    && linMinComp3.getVisibility() == View.VISIBLE
//    && linMinComp4.getVisibility() == View.VISIBLE) {
//    linMinComp4.setVisibility(View.GONE);
//    btnAddMinComp.setVisibility(View.VISIBLE);
//    } else if (linMinComp2.getVisibility() == View.VISIBLE
//    && linMinComp3.getVisibility() == View.VISIBLE
//    && linMinComp4.getVisibility() != View.VISIBLE) {
//    linMinComp3.setVisibility(View.GONE);
//    } else if (linMinComp2.getVisibility() == View.VISIBLE
//    && linMinComp3.getVisibility() != View.VISIBLE
//    && linMinComp4.getVisibility() != View.VISIBLE) {
//    linMinComp2.setVisibility(View.GONE);
//    btnRemoveMinComp.setVisibility(View.GONE);
//    }
//
//    }
//    });
    }
    
    @objc func perMajBtn1() {
        picker.isHidden = false
        picker.tag = 1
        picker.reloadAllComponents()
    }
    @objc func perMajBtn2() {
        picker.isHidden = false
        picker.tag = 2
        picker.reloadAllComponents()
    }
    @objc func perMajBtn3() {
        picker.isHidden = false
        picker.tag = 3
        picker.reloadAllComponents()
    }
    @objc func perMajBtn4() {
        picker.isHidden = false
        picker.tag = 4
        picker.reloadAllComponents()
    }
    
    @objc func perMinBtn1() {
        picker.isHidden = false
        picker.tag = 5
        picker.reloadAllComponents()
    }
    @objc func perMinBtn2() {
        picker.isHidden = false
        picker.tag = 6
        picker.reloadAllComponents()
    }
    @objc func perMinBtn3() {
        picker.isHidden = false
        picker.tag = 7
        picker.reloadAllComponents()
    }
    @objc func perMinBtn4() {
        picker.isHidden = false
        picker.tag = 8
        picker.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return arr.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            self.objMajHView.configureSelectButton(self.objMajHView.btn1, setString: arr[row])
        } else if pickerView.tag == 2 {
            self.objMajHView.configureSelectButton(self.objMajHView.btn2, setString: arr[row])
        } else if pickerView.tag == 3 {
            self.objMajHView.configureSelectButton(self.objMajHView.btn3, setString: arr[row])
        } else if pickerView.tag == 4 {
            self.objMajHView.configureSelectButton(self.objMajHView.btn4, setString: arr[row])
        } else if pickerView.tag == 5 {
            self.objMinHView.configureSelectButton(self.objMinHView.btn1, setString: arr[row])
        } else if pickerView.tag == 6 {
            self.objMinHView.configureSelectButton(self.objMinHView.btn2, setString: arr[row])
        } else if pickerView.tag == 7 {
            self.objMinHView.configureSelectButton(self.objMinHView.btn3, setString: arr[row])
        } else if pickerView.tag == 8 {
            self.objMinHView.configureSelectButton(self.objMinHView.btn4, setString: arr[row])
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        picker.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
