//
//  FamilyHistoryView.swift
//  Salk
//
//  Created by Chetan on 07/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class FamilyHistoryView: UIView {

    @IBOutlet weak var backCardView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    
    @IBOutlet weak var lblFather: UILabel!
    @IBOutlet weak var txtFather: MKTextField!
    
    @IBOutlet weak var lblMother: UILabel!
    @IBOutlet weak var txtMother: MKTextField!
    
    @IBOutlet weak var lblSibling: UILabel!
    @IBOutlet weak var txtSibling: MKTextField!
    
    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        self.viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "FamilyHistoryView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        headerView.backgroundColor = CustomColor.blackTransparent1()
        
        configureTextField(txtFather, placeholder: "Father Disease")
        configureTextField(txtMother, placeholder: "Mother Disease")
        configureTextField(txtSibling, placeholder: "Sibling Disease")
        
        configureLabel(lblFather, setString: "Father")
        configureLabel(lblMother, setString: "Mother")
        configureLabel(lblSibling, setString: "Sibling")
    }
    
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
    }
    
    func configureLabel(_ label: UILabel, setString: String) {
        label.text = setString
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = CustomColor.darker_gray()
    }
}


