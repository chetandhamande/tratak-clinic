//
//  BodyMeasurementView.swift
//  Salk
//
//  Created by Chetan on 07/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class BodyMeasurementView: UIView {

    @IBOutlet weak var backCardView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblHeading: UILabel!
    
    @IBOutlet weak var lblChest: UILabel!
    @IBOutlet weak var txtChest: MKTextField!
    @IBOutlet weak var btnChest: UIButton!
    @IBOutlet weak var imgChest: UIImageView!
    
    @IBOutlet weak var lblWaist: UILabel!
    @IBOutlet weak var txtWaist: MKTextField!
    @IBOutlet weak var btnWaist: UIButton!
    @IBOutlet weak var imgWaist: UIImageView!
    
    @IBOutlet weak var lblHip: UILabel!
    @IBOutlet weak var txtHip: MKTextField!
    @IBOutlet weak var btnHip: UIButton!
    @IBOutlet weak var imgHip: UIImageView!
    
    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        self.viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "BodyMeasurementView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        headerView.backgroundColor = CustomColor.blackTransparent1()
        
        configureTextField(txtChest, placeholder: "Chest size")
        configureTextField(txtWaist, placeholder: "Waist size")
        configureTextField(txtHip, placeholder: "Hip size")
        
        configureLabel(lblChest, setString: "Chest")
        configureLabel(lblWaist, setString: "Waist")
        configureLabel(lblHip, setString: "Hip")
        
        btnChest.setTitle("cm", for: .normal)
        btnWaist.setTitle("cm", for: .normal)
        btnHip.setTitle("cm", for: .normal)
        
        btnChest.setTitleColor(CustomColor.textBlackColor(), for: .normal)
        btnWaist.setTitleColor(CustomColor.textBlackColor(), for: .normal)
        btnHip.setTitleColor(CustomColor.textBlackColor(), for: .normal)
    
    }
    
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.keyboardType = .decimalPad
        if textField == txtHip {
            textField.returnKeyType = .done
        } else {
            textField.returnKeyType = .next
        }
    }
    
    func configureLabel(_ label: UILabel, setString: String) {
        label.text = setString
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = CustomColor.darker_gray()
    }
    
    func configureButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        //        label.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(CustomColor.textGrayColor(), for: .normal)
        refreshView()
    }
    
    func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        //        label.font = UIFont.systemFont(ofSize: 13)
        button.setTitleColor(CustomColor.textBlackColor(), for: .normal)
        refreshView()
    }
    
    func refreshView() {
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
}

