//
//  ProfileSettingHealthInfoVC.swift
//  Salk
//
//  Created by Chetan  on 16/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material

class ProfileSettingHealthInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var settingTableView: UITableView!

    let optionTableCellIdentifier = "OptionsTableViewCell"
    
    var settingArray : [String?] = []
    var settingImageArray : [String?] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        settingTableView.delegate = self
        settingTableView.dataSource = self
        
        self.prepareNavigationItem()
        
//        naviMenstrualHist,,);
        
        let appPrefs = MySharedPreferences()
        if(appPrefs.getDrRefCode().uppercased() == "WELFORTZ")
        {
            settingArray = ["naviPrimaryInfo".localized, "naviBasicInfo".localized ,
                            "naviFamilyHistory".localized, "naviMedicalCond".localized,
                            "naviPersonalHist".localized, "naviDailyLifestyle".localized]
            settingImageArray = ["primary_information.png", "basic_information",
                                 "family_history", "medical_history",
                                 "personal_history", "daily_lifestyle"]
        } else if(appPrefs.getDrRefCode().uppercased() == "ORJA")
        {
            settingArray = ["naviPrimaryInfo".localized, "naviBasicInfo".localized ,
                            "naviFamilyHistory".localized, "naviMedicalCond".localized,
                            "naviPersonalHist".localized, "naviStressHist".localized,
                            "naviYogaHist".localized, "naviCurrentHist".localized,
                            "naviReikiHist".localized,
                            "naviPersonalityHist".localized, "naviDailyLifestyle".localized]
            settingImageArray = ["primary_information.png", "basic_information",
                                 "family_history", "medical_history",
                                 "personal_history", "stress_history",
                                 "yoga_history", "current_history_",
                                 "reiki_history",
                                 "personal_history.png", "daily_lifestyle"]
        } else if(appPrefs.getDrRefCode().uppercased() == "TRATAK")
        {
            settingArray = ["naviPrimaryInfo".localized, "naviBasicInfo".localized ,
                            "naviFamilyHistory".localized, "naviMedicalCond".localized,
                            "naviPersonalHist".localized, "naviStressHist".localized,
                            "naviYogaHist".localized, "naviReikiHist".localized,
                            "naviDailyLifestyle".localized]
            settingImageArray = ["primary_information.png", "basic_information",
                                 "family_history", "medical_history",
                                 "personal_history", "stress_history",
                                 "yoga_history", "reiki_history",
                                 "daily_lifestyle"]
        } else if(appPrefs.getDrRefCode().uppercased() == "VITALK"
            || appPrefs.getDrRefCode().uppercased() == "YOGAKSEMA")
        {
            settingArray = ["naviPrimaryInfo".localized, "naviBasicInfo".localized ,
                            "naviFamilyHistory".localized, "naviMedicalCond".localized,
                            "naviPersonalHist".localized, "naviStressHist".localized,
                            "naviCurrentHist".localized,
                            "naviPersonalityHist".localized, "naviDailyLifestyle".localized]
            settingImageArray = ["primary_information.png", "basic_information",
                                 "family_history", "medical_history",
                                 "personal_history", "stress_history",
                                 "current_history_",
                                 "personal_history.png", "daily_lifestyle"]
        } else if(appPrefs.getDrRefCode().uppercased() == "NMLF"
            || appPrefs.getDrRefCode().uppercased() == "DAMS"
            || appPrefs.getDrRefCode().uppercased() == "VASUNDHARA"
            || appPrefs.getDrRefCode().uppercased() == "RLIVING"
            || appPrefs.getDrRefCode().uppercased() == "TNWC")
        {
            settingArray = ["naviPrimaryInfo".localized, "naviBasicInfo".localized ,
                            "naviFamilyHistory".localized,
                            "naviPersonalHist".localized, "naviDailyLifestyle".localized]
            settingImageArray = ["primary_information.png", "basic_information",
                                 "family_history",
                                 "personal_history", "daily_lifestyle"]
        } else {
            settingArray = ["naviPrimaryInfo".localized, "naviBasicInfo".localized ,
                            "naviFamilyHistory".localized, "naviMedicalCond".localized,
                            "naviPersonalHist".localized, "naviStressHist".localized,
                            "naviYogaHist".localized, "naviCurrentHist".localized,
                            "naviReikiHist".localized, "naviPainHist".localized,
                            "naviPersonalityHist".localized, "naviDailyLifestyle".localized]
            settingImageArray = ["primary_information.png", "basic_information",
                                 "family_history", "medical_history",
                                 "personal_history", "stress_history",
                                 "yoga_history", "current_history_",
                                 "reiki_history", "pain_history",
                                 "personal_history.png", "daily_lifestyle"]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Profile Setting"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var settingCell: OptionsTableViewCell! = settingTableView.dequeueReusableCell(withIdentifier: optionTableCellIdentifier) as? OptionsTableViewCell
        
        if settingCell == nil {
            settingTableView.register(UINib(nibName: optionTableCellIdentifier, bundle: nil), forCellReuseIdentifier: optionTableCellIdentifier)
            settingCell = tableView.dequeueReusableCell(withIdentifier: optionTableCellIdentifier) as? OptionsTableViewCell
        }
        
        settingCell.optionLabel.text = settingArray[indexPath.row]
        settingCell.optionImage.image = UIImage(named: settingImageArray[indexPath.row]!)
        
        return settingCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if settingArray[indexPath.row] == "naviPrimaryInfo".localized {
            let a = PrimaryInformationVC(nibName: "PrimaryInformationVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if settingArray[indexPath.row] == "naviBasicInfo".localized {
            let a = BasicInformationVC(nibName: "BasicInformationVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if settingArray[indexPath.row] == "naviFamilyHistory".localized {
            let a = FamilyHistoryVC(nibName: "FamilyHistoryVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if settingArray[indexPath.row] == "naviMedicalCond".localized {
            let a = MedicalHistoryVC(nibName: "MedicalHistoryVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if settingArray[indexPath.row] == "naviPersonalHist".localized {
            let a = PersonalHistoryVC(nibName: "PersonalHistoryVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if settingArray[indexPath.row] == "naviStressHist".localized {
            let a = StressHistoryVC(nibName: "StressHistoryVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if settingArray[indexPath.row] == "naviYogaHist".localized {
            let a = YogaHistoryVC(nibName: "YogaHistoryVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if settingArray[indexPath.row] == "naviReikiHist".localized {
            let a = ReikiHistoryVC(nibName: "ReikiHistoryVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if settingArray[indexPath.row] == "naviDailyLifestyle".localized {
            let a = DailyLifestyleVC(nibName: "DailyLifestyleVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if settingArray[indexPath.row] == "naviPainHist".localized {
            let a = PainHistoryVC(nibName: "PainHistoryVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if  settingArray[indexPath.row] == "naviCurrentHist".localized {
            let a = CurrentHistoryVC(nibName: "CurrentHistoryVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        } else if  settingArray[indexPath.row] == "naviPersonalityHist".localized {
            let a = PersonalityHistoryVC(nibName: "PersonalityHistoryVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
