
import UIKit

@IBDesignable class CustomTrackerFeaturesView: UIView {
    
    @IBOutlet weak var stack1: UIStackView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var prBar: UIProgressView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var lblMaxLimit: UILabel!
        
    var view: UIView!
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        SalkRippleEffect.SalkRippleEffect(mainView)
        SalkRippleEffect.SalkRippleEffect(btnAdd)
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomTrackerFeaturesView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }    
    
}
