//
//  FoodScheduleType.swift
//  Salk
//
//  Created by Chetan  on 01/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class FoodScheduleType {
    
    static func getScheduleDefaultImage(type: String) -> String {
        
        if(type.uppercased().contains("MORNING")) {
            return "morning_routine.png"
        } else if (type.uppercased() == "BREAKFAST") {
            return "breakfast.png"
        } else if (type.uppercased() == "LUNCH") {
            return "lunch.png"
        } else if (type.uppercased().contains("EVENING")) {
            return "evening_snacks.png"
        } else if (type.uppercased() == "DINNER") {
            return "dinner.png"
        } else if (type.uppercased() == "BEDTIME") {
            return "dinner.png"
        } else if (type.uppercased() == "WORKOUT") {
            return "workout.png"
        } else if (type.uppercased() == "PRE WORKOUT") {
            return "pre_workout.png"
        } else if (type.uppercased() == "POST WORKOUT") {
            return "post_workout.png"
        } else if (type.uppercased() == "NUTRACEUTICALS") {
            return "nutracuticals.png"
        } else if (type.equalsIgnoreCase("Herbs")) {
            return "nutracuticals.png"
        } else if (type.equalsIgnoreCase("Functional Food")) {
            return "nutracuticals.png"
        } else if (type.equalsIgnoreCase("Nutrition Suppliments")) {
            return "nutracuticals.png"
        } else if (type.equalsIgnoreCase("Allopathy Medicine")) {
            return "nutracuticals.png"
        } else if (type.uppercased() == "THERAPY") {
            return "therapy.png"
        } else {
            return "food_color.png"
        }
    }
    
}
