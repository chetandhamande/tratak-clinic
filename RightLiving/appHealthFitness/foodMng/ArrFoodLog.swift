//
//  ArrFoodLog.swift
//  Salk
//
//  Created by Chetan  on 11/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class ArrFoodLog {
    var recID = "", foodName = "", foodID = "", actCaloriesPerGram = "",
    actCaloriesTotal = "", quantity = "", serveIn = "", serveInID = "", containerIDStr = "", serverID = "", dateTime = ""
    var isChecked:Bool = false
}
