//
//  FitnessPanelTabLayoutVC.swift
//  Salk
//
//  Created by Chetan  on 13/06/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import MaterialControls
import Floaty

class FitnessPanelTabLayoutVC: UIViewController, MDTabBarViewControllerDelegate
{

    var floaty = Floaty()
    
    /// FabMenu component.
    //    fileprivate var fabMenu: Menu!
    /// Default spacing size
    fileprivate let spacing: CGFloat = 16
    
    /// Diameter for FabButtons.
    fileprivate let diameter: CGFloat = 56
    
    /// Height for FlatButtons.
    fileprivate let height: CGFloat = 36
    
    fileprivate var vcDiet = DietTabVC()
    fileprivate var vcDashboard = DashboardTabVC()
    
    fileprivate var tabBarViewController = MDTabBarViewController()
    
    fileprivate let mySharedObj = MySharedPreferences()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func initContent() {
        tabBarViewController = MDTabBarViewController(delegate: self)
        
        mySharedObj.setLastDashboardTypeOpen(text: "Dashboard")
        
        var names = [String]()
        names = ["Routine", "Dashboard"]
        
        tabBarViewController.setItems(names)
        tabBarViewController.tabBar.backgroundColor = Color.white
        tabBarViewController.tabBar.textColor = Color.black
        tabBarViewController.tabBar.indicatorColor = Color.black
        
        vcDiet = DietTabVC(nibName: "DietTabVC", bundle: nil)
        vcDashboard = DashboardTabVC(nibName: "DashboardTabVC", bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        self.initContent()
        
        self.addChildViewController(tabBarViewController)
        self.view.addSubview(tabBarViewController.view)
        tabBarViewController.didMove(toParentViewController: self)
        let controllerView : UIView = tabBarViewController.view
        
        let rootTopLayoutGuide = self.topLayoutGuide
        let rootBottomLayoutGuide = self.bottomLayoutGuide
        
        let viewsDictionary = ["rootTopLayoutGuide": rootTopLayoutGuide, "rootBottomLayoutGuide": rootBottomLayoutGuide, "controllerView": controllerView] as [String : Any]
        
        let verticalConstraints : [AnyObject] = NSLayoutConstraint.constraints(withVisualFormat: "V:[rootTopLayoutGuide][controllerView][rootBottomLayoutGuide]",                                                                                               options: NSLayoutFormatOptions(rawValue: 0),                                                                                               metrics: nil,                                                                                               views:viewsDictionary as [String : AnyObject])
        self.view.addConstraints(verticalConstraints as AnyObject as! [NSLayoutConstraint])
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[controllerView]|",                                                                                   options: NSLayoutFormatOptions(rawValue: 0),                                                                            metrics: nil, views: viewsDictionary as [String : AnyObject])
        self.view.addConstraints(horizontalConstraints)
        
        tabBarViewController.selectedIndex = 1
        
        self.prepareNavigationItem()
        
        let internet:Bool = CheckInternetConnection().isCheckInternetConnection()
        if(internet) {
            let appPref = MySharedPreferences()
            var time:Double = ConvertionClass().currentTime() - appPref.getLastLogDataSycnTime()
            time = (time / (60)) //in minute
            
            if(time > 1) {
                do {
                    try PendingLogSyncService().onHandleIntent()
                } catch _ {}
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(onReceive(_:)), name: NSNotification.Name(rawValue: "app_name".applocalized), object: nil)
        
        layoutFAB()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "app_name".applocalized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let timelineImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_timeline.png")!, scaledToSize: CGSize(width: 30, height: 30))
        let timelineButton = UIBarButtonItem(image: timelineImage, style: .plain, target: self, action: #selector(self.openTimeline))
        
        let diaryImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_today.png")!, scaledToSize: CGSize(width: 30, height: 30))
        let diaryButton = UIBarButtonItem(image: diaryImage, style: .plain, target: self, action: #selector(self.openDiary))
        
        navigationItem.rightBarButtonItems = [diaryButton]
        
        self.navigationController?.navigationBar.hideBottomHairline()
    }
    
    @objc fileprivate func openTimeline() {
        let a = TimelineTableVC(nibName: "TimelineTableVC", bundle: nil)
        self.navigationController?.pushViewController(a, animated: true)
    }
    
    @objc fileprivate func openDiary() {
        let a = MyDiaryPanelTabLayout(nibName: "MyDiaryPanelTabLayout", bundle: nil)
        self.navigationController?.pushViewController(a, animated: true)
    }
    
    func tabBarViewController(_ viewController: MDTabBarViewController, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
            return vcDiet
        } else {
            return vcDashboard
        }
    }
    
    func imageWithImage(_ image: UIImage!, scaledToSize: CGSize!) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(scaledToSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: scaledToSize.width, height: scaledToSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    //manage Floating buttong
    func layoutFAB() {
        let itemFood = FloatyItem()
        itemFood.hasShadow = false
        itemFood.buttonColor = UIColor.white
        itemFood.icon = UIImage().imageWithImage(UIImage(named: "food_color.png")!, scaledToSize: CGSize(width: 25, height: 25))
        itemFood.title = "Food"
        itemFood.handler = { item in
            let a = FoodLogActivityMngVC(nibName: "FoodLogActivityMngVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        }
        
        let itemWater = FloatyItem()
        itemWater.hasShadow = false
        itemWater.buttonColor = UIColor.white
        itemWater.icon = UIImage().imageWithImage(UIImage(named: "water_color.png")!, scaledToSize: CGSize(width: 20, height: 20))
        itemWater.title = "Water"
        itemWater.handler = { item in
            let a = WaterIntakeMngVC(nibName: "WaterIntakeMngVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        }
        
        let itemSleep = FloatyItem()
        itemSleep.hasShadow = false
        itemSleep.buttonColor = UIColor.white
        itemSleep.icon = UIImage().imageWithImage(UIImage(named: "sleep_color.png")!, scaledToSize: CGSize(width: 25, height: 25))
        itemSleep.title = "Sleep"
        itemSleep.handler = { item in
            let a = SleepMngVC(nibName: "SleepMngVC", bundle: nil)
            self.navigationController?.pushViewController(a, animated: true)
        }
        
        floaty.hasShadow = true
        floaty.buttonColor = PrimaryColor.colorAccent()
        floaty.addItem(item: itemSleep)
        floaty.addItem(item: itemWater)
        floaty.addItem(item: itemFood)
        floaty.sticky = true
        floaty.paddingX = 20
        floaty.paddingY = 70
        
        self.view.addSubview(floaty)
    }
    
    
    
    /// Handle orientation.
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Handle orientation change.
        //        fabMenu.origin = CGPoint(x: view.bounds.height - diameter - spacing + 64, y: view.bounds.width - diameter - spacing - 64)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Define the callback for what to do when data is received
    @objc func onReceive(_ notification: Notification) {
        guard let intent = notification.userInfo else {
            return
        }
        let resultCode = intent["resultCode"] as? Int
        let nType = intent["udpateType"] as? String
        //Print.printLog("Broadcaster : \(String(describing: nType))")
        
        do {
            if resultCode == 1 {
                if nType == "foodRef" {
                    try vcDashboard.loadFoodInformation()
                } else if nType == "DietRef" || nType == "ActivityRef" {
                    do {
                        try vcDiet.onLoad()
                    } catch _ {}
                } else if (nType == "dailyGoal") {
                    do {
                        try vcDiet.onLoad()
                    } catch _ {}
                    
                    //findNotificationCounter();
                    
                } else if (nType == "padometerUpdate") {
                    try vcDashboard.showFitnessBandData()
                    try vcDashboard.loadSleepInformation()
                    try vcDashboard.loadWaterInformation()
                    
                    let internet:Bool = CheckInternetConnection().isCheckInternetConnection()
                    if(internet) {
                        do {
                            try PendingLogSyncService().onHandleIntent()
                        } catch _ {}
                    }
                    
                } else if(nType == "sendPendingData") {
                    let internet:Bool = CheckInternetConnection().isCheckInternetConnection()
                    if(internet) {
                        do {
                            try PendingLogSyncService().onHandleIntent()
                        } catch _ {}
                    }
                } else if (nType == "waterRef") {
                    try vcDashboard.loadWaterInformation()
                } else if (nType == "sleepRef") {
                    try vcDashboard.loadSleepInformation()
                } else if (nType == "foodRef") {
                    try vcDashboard.loadFoodInformation()
                }
            }
        } catch {}
    }
    
}
