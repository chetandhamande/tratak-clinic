//
//  WaterIntakeMngVC.swift
//  Salk
//
//  Created by Chetan  on 09/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material

class WaterIntakeMngVC: UIViewController {

    @IBOutlet weak var btnGlassMinus: UIButton!
    @IBOutlet weak var btnGlassAdd: UIButton!
    @IBOutlet weak var btnBottleMinus: UIButton!
    @IBOutlet weak var btnBottleAdd: UIButton!
    @IBOutlet weak var btnLargeBottleMinus: UIButton!
    @IBOutlet weak var btnLargeBottleAdd: UIButton!
    
    @IBOutlet weak var lblWaterCount: UILabel!
    
    fileprivate var backDateButton: UIBarButtonItem? = nil, nextDateButton: UIBarButtonItem? = nil
    
    fileprivate var currentValue:Int = 0, endValue:Int = 10000
    fileprivate var currentDayDate = "0"
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationItem()
        layoutDesign()
        findDateNLoadRecord(isNextDay: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func layoutDesign() {
        
        btnGlassAdd.setImage(UIImage().imageWithImage(UIImage(named: "ic_add_circle_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30)), for: .normal)
        
        btnGlassMinus.setImage(UIImage().imageWithImage(UIImage(named: "ic_remove_circle_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30)), for: .normal)
        
        btnBottleAdd.setImage(UIImage().imageWithImage(UIImage(named: "ic_add_circle_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30)), for: .normal)
        
        btnBottleMinus.setImage(UIImage().imageWithImage(UIImage(named: "ic_remove_circle_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30)), for: .normal)
        
        btnLargeBottleAdd.setImage(UIImage().imageWithImage(UIImage(named: "ic_add_circle_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30)), for: .normal)
        
        btnLargeBottleMinus.setImage(UIImage().imageWithImage(UIImage(named: "ic_remove_circle_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30)), for: .normal)
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Today"
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let backDateImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_chevron_left_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        backDateButton = UIBarButtonItem(image: backDateImage, style: .plain, target: self, action: #selector(self.findBackDate))
        
        let nextDateImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_chevron_right_black_36dp.png")!, scaledToSize: CGSize(width: 30, height: 30))
        nextDateButton = UIBarButtonItem(image: nextDateImage, style: .plain, target: self, action: #selector(self.findNextDate))
        
        navigationItem.rightBarButtonItems = [nextDateButton!, backDateButton!]
    }
    
}

extension WaterIntakeMngVC {
    @objc fileprivate func findBackDate() {
        findDateNLoadRecord(isNextDay: false)
    }
    @objc fileprivate func findNextDate() {
        findDateNLoadRecord(isNextDay: true)
    }
    
    @IBAction func performAddRemoveWater(button: UIButton) {
        if button == btnGlassMinus {
            performModifyWater(isAdd: false, value: 250)
        } else if button == btnGlassAdd {
            performModifyWater(isAdd: true, value: 250)
        } else if button == btnBottleMinus {
            performModifyWater(isAdd: false, value: 500)
        } else if button == btnBottleAdd {
            performModifyWater(isAdd: true, value: 500)
        } else if button == btnLargeBottleMinus {
            performModifyWater(isAdd: false, value: 750)
        } else if button == btnLargeBottleAdd {
            performModifyWater(isAdd: true, value: 750)
        }
    }
    
    fileprivate func findDateNLoadRecord(isNextDay: Bool)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if(currentDayDate == "0") {
            currentDayDate = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter))"
        } else {
            
            let dateTimeStr = currentDayDate + " 12:00:10 AM"
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            let dateTimeLong = ConvertionClass().conDateToLong(dateFormatter.date(from: dateTimeStr)!)
            
            var value:Double = Double(dateTimeLong)
            if(isNextDay) {
                value += (60 * 60 * 24); // day
            } else {
                value -= (60 * 60 * 24); // day
            }
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            currentDayDate = "\(ConvertionClass().conLongToDate(value, dateFormat: dateFormatter))"
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let currLongTime = ConvertionClass().conDateToLong(dateFormatter.date(from: currentDayDate)!)
        
        dateFormatter.dateFormat = "dd MMMM"
        var selDate = "\(ConvertionClass().conLongToDate(currLongTime, dateFormat: dateFormatter))"
        
        let currDate = "\(ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: dateFormatter))"
        
        if(selDate == currDate) {
            selDate = "Today"
            
            nextDateButton?.isEnabled = false
        } else {
            nextDateButton?.isEnabled = true
        }
        navigationItem.titleLabel.text = selDate
        
        loadCurrentRecord();
    }
    
    fileprivate func loadCurrentRecord() {
        //load previous values
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        let fromTime = ConvertionClass().conDateToLong(dateFormatter.date(from: currentDayDate + " 12:00:01 am")!)
        let toTime = ConvertionClass().conDateToLong(dateFormatter.date(from: currentDayDate + " 11:59:59 pm")!)
    
        let datasource = DBOperation()
        datasource.openDatabase(false)
    
        let sql = "SELECT * FROM " + datasource.WaterTrackList_tlb + " WHERE CAST(" + datasource.dbDateTime + " as INTEGER) >= \(fromTime) AND CAST(" + datasource.dbDateTime + " as INTEGER) <= \(toTime)"
        
        let cursor = datasource.selectRecords(sql)
        if cursor != nil {
            while cursor!.next() {
                if Int(cursor!.int(forColumnIndex: 3)) == 0 {
                    currentValue += Int(cursor!.int(forColumnIndex: 0))
                } else {
                    currentValue -= Int(cursor!.int(forColumnIndex: 0))
                }
            }
            cursor!.close()
        }
        datasource.closeDatabase();
        
        setValues()
        
        if currentValue > 0 {
            lblWaterCount.text = "\(currentValue)"
        } else {
            lblWaterCount.text = "0"
        }
    }
    
    fileprivate func performModifyWater(isAdd:Bool, value:Int) {
    
        var isDeleteFlag = ""
        if(isAdd) {
            if(currentValue >= endValue || ((currentValue + value) >= endValue))
            {
                return
            }
            currentValue += value
            isDeleteFlag = "0";
        } else {
            if(currentValue <= 0 || ((currentValue - value) < 0)) {
                return
            }
            currentValue -= value
            isDeleteFlag = "1";
        }
    
        //apply into database
        let df = DateFormatter()
        df.dateFormat = "hh:mm:ss a"
        let currDate = ConvertionClass().conLongToDate(ConvertionClass().currentTime(), dateFormat: df)
        
        df.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        let currDateStr = "\(ConvertionClass().conDateToLong(df.date(from: "\(currentDayDate) \(currDate)")!))"
        
        let datasource = DBOperation()
        datasource.openDatabase(true)
        datasource.InsertWaterTrackList(currentValue: "\(value)", dateTime: currDateStr, isServerSync: "0", isDelete: isDeleteFlag)
        datasource.closeDatabase()
        
        
        setValues()
        
        // Fire the broadcast with intent packaged
        var dict = ["resultCode":1,
                    "udpateType":"sendPendingData"
            ] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
       
        dict = ["resultCode":1,
                    "udpateType":"waterRef"
            ] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "app_name".applocalized), object: self, userInfo: dict as? [AnyHashable: Any])
        
       
        lblWaterCount.text = "\(currentValue)"
    }
    
    fileprivate func setValues() {
        
        if(currentValue >= endValue) {
            btnGlassAdd.isEnabled = false
            btnBottleAdd.isEnabled = false
            btnLargeBottleAdd.isEnabled = false
        } else {
            btnGlassAdd.isEnabled = true
            btnBottleAdd.isEnabled = true
            btnLargeBottleAdd.isEnabled = true
        }
    
        if(currentValue <= 0) {
            btnBottleMinus.isEnabled = false
            btnGlassMinus.isEnabled = false
            btnLargeBottleMinus.isEnabled = false
        } else {
            btnBottleMinus.isEnabled = true
            btnGlassMinus.isEnabled = true
            btnLargeBottleMinus.isEnabled = true
        }
    }
    
    
}
