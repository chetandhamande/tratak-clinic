//
//  SalkCheckBox.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation
import BEMCheckBox

class SalkCheckBox: BEMCheckBox {
    func SalkCheckBox() {
        self.onAnimationType = BEMAnimationType.bounce
        self.offAnimationType = BEMAnimationType.bounce
        self.boxType = .square
        self.lineWidth = 2
        self.animationDuration = 0.5
        
        self.tintColor = CustomColor.grayLight()
        self.onTintColor = PrimaryColor.colorPrimary()
        self.onFillColor = PrimaryColor.colorPrimary()
        self.onCheckColor = UIColor.white
    }
}
