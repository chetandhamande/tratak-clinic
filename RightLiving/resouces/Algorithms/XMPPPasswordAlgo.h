//
//  XMPPPasswordAlgo.h
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface XMPPPasswordAlgo : NSObject

-(NSString *)getXMPPUserPassword :(NSString *)inputStr;

-(NSString*) sha1:(NSString*)input;

@end
