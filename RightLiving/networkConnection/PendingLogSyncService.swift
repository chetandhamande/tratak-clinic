//
//  PendingLogSyncService.swift
//  Salk
//
//  Created by Chetan  on 06/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class PendingLogSyncService {
    
    func onHandleIntent() throws {
        
        let datasource = DBOperation()
        datasource.openDatabase(false)
        
        var isNeedToSend:Bool = false
        var requestType = ""
        
        //food
        var sql = "SELECT count(*) FROM " + datasource.FoodRecordLogList_tlb
            + " WHERE " + datasource.dbIsServerSync + " = '0' "
        var cursor = datasource.selectRecords(sql)
        if cursor != nil {
            if cursor!.next() {
                if cursor!.int(forColumnIndex: 0) > 0 {
                    isNeedToSend = true
                    requestType = "food"
                }
            }
            cursor!.close()
        }
        
        if(!isNeedToSend) {
            //activity
            sql = "SELECT count(*) FROM " + datasource.SportActivityRecordList_tlb
                + " WHERE " + datasource.dbIsServerSync + " = '0' ";
            cursor = datasource.selectRecords(sql)
            if cursor != nil {
                if cursor!.next() {
                    if cursor!.int(forColumnIndex: 0) > 0 {
                        isNeedToSend = true
                        requestType = "activity"
                    }
                }
                cursor!.close()
            }
        }
        
        if(!isNeedToSend) {
            //Water
            sql = "SELECT count(*) FROM " + datasource.WaterTrackList_tlb
                + " WHERE " + datasource.dbIsServerSync + " = '0' ";
            cursor = datasource.selectRecords(sql)
            if cursor != nil {
                if cursor!.next() {
                    if cursor!.int(forColumnIndex: 0) > 0 {
                        isNeedToSend = true
                        requestType = "water"
                    }
                }
                cursor!.close()
            }
        }
        
        if(!isNeedToSend) {
            //Pedometer
            sql = "SELECT count(*) FROM " + datasource.PedometerRecordLogList_tlb
                + " WHERE " + datasource.dbIsServerSync + " = '0' ";
            cursor = datasource.selectRecords(sql)
            if cursor != nil {
                if cursor!.next() {
                    if cursor!.int(forColumnIndex: 0) > 0 {
                        isNeedToSend = true
                        requestType = "pedometer"
                    }
                }
                cursor!.close()
            }
        }
        
        if(!isNeedToSend) {
            //Sleep
            sql = "SELECT count(*) FROM " + datasource.SleepTrackList_tlb
                + " WHERE " + datasource.dbIsServerSync + " = '0' ";
            cursor = datasource.selectRecords(sql)
            if cursor != nil {
                if cursor!.next() {
                    if cursor!.int(forColumnIndex: 0) > 0 {
                        isNeedToSend = true
                        requestType = "sleep"
                    }
                }
                cursor!.close()
            }
        }
        
        if(!isNeedToSend) {
            //Routing Tracking
            sql = "SELECT count(*) FROM " + datasource.RoutingTrackList_tlb;
            cursor = datasource.selectRecords(sql)
            if cursor != nil {
                if cursor!.next() {
                    if cursor!.int(forColumnIndex: 0) > 0 {
                        isNeedToSend = true
                        requestType = "routingTrack"
                    }
                }
                cursor!.close()
            }
        }
        datasource.closeDatabase();
        
        if(!isNeedToSend) {
            //Expert chat message
            let appPref = MySharedPreferences()
            if(appPref.getIsPendingChatRec()) {
                isNeedToSend = true
                requestType = "expertChat"
            }
        }
        if(!isNeedToSend) {
            //Expert chat message status
            let appPref = MySharedPreferences()
            if(!appPref.getIsPendingDeleteChatRec().isEmpty
                || !appPref.getIsPendingDeliveredChatRec().isEmpty
                || !appPref.getIsPendingReadChatRec().isEmpty)
            {
                isNeedToSend = true
                requestType = "expertChatStatus"
            }
        }
        if(!isNeedToSend) {
            //Reminder Pending Data
            let appPref = MySharedPreferences()
            if(appPref.getIsPendingReminder()) {
                isNeedToSend = true
                requestType = "reminder"
            }
        }
        
        
        if(isNeedToSend) {
            let appPref = MySharedPreferences()
            appPref.setLastLogDataSycnType(text: requestType)
            
            if (requestType == "expertChat") {
                FitnessRequestManager().getRequest(Constant.ExpertChat.MethodSyncExpertChat, classRef: self)
            } else if (requestType == "expertChatStatus") {
                FitnessRequestManager().getRequest(Constant.ExpertChat.MethodSyncChatStatus, classRef: self)
            } else if (requestType == "routingTrack") {
                FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncRoutingTrack, classRef: self)
            } else if (requestType == "reminder") {
                FitnessRequestManager().getRequest(Constant.Fitness.MethodSendReminder, classRef: self)
            } else {
                FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncFitnessLog, classRef: self)
            }
        }
        
    }
    
}
