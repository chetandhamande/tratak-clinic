//
//  ArrTimeline.swift
//  Salk
//
//  Created by Chetan  on 09/08/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class ArrTimelineDay {
    
    var title = "", date = "", day = "", matchDate = ""
    var listLayout:Int = 1 //1 - Empty, 2 - Month, 3 - Day & Data
    
    var arrMainData = [ArrTimelineData]()
}
