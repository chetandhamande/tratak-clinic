//
//  MakeProperNumber.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation
import libPhoneNumber_iOS

class MakeProperNumber {
    
    static func getProperNumber(_ number: String, appPrefs: MySharedPreferences) -> String {
        var number = number
        number = number.replaceAll(" ", val2: "")
        number = number.replaceAll("(", val2: "")
        number = number.replaceAll(")", val2: "")
        number = number.replaceAll("-", val2: "")
        number = number.replaceAll("'", val2: "")
        number = number.trimmed
        
        var isCntCodeAvail = false
        if number.hasPrefix("+") {
            isCntCodeAvail = true;
        }
        
        number = number.removeSpecialCharsFromString(number)
        number = number.trimmed
        
        if number.characters.count < 4 {
            number = ""
        } else if isCntCodeAvail {
            number = number.hasPrefix("+") ? number : "+" + number
        } else {
            number = appPrefs.getMobileCountryCodeNumber() + number
        }
        
        return number
    }
    
    class func showPhoneNumber(_ number: String) -> String {
        var orignal = number
        
        do {
            let phoneUtil = NBPhoneNumberUtil()
            
            let phoneNumber: NBPhoneNumber = try phoneUtil.parse(number, defaultRegion: "IN")
            var nat: String = try phoneUtil.format(phoneNumber, numberFormat: .NATIONAL)
            
            if nat.hasPrefix("0") {
                nat = nat.substringFromIndex(1)
            }
            
            orignal = "+" + "\(phoneNumber.countryCode!)" + " " + nat
            
        } catch let error as NSError {
            Print.printLog(error.localizedDescription)
        }
        
        return orignal
    }

    static func getPhoneNumberCountry(_ number: String) -> String {
        var orignal = number
        
        let pnu = NBPhoneNumberUtil()
        do {
            let phoneNumber: NBPhoneNumber = try pnu.parse(number, defaultRegion: "IN")
            var nat: String = try pnu.format(phoneNumber, numberFormat: .NATIONAL)
            
            if nat.hasPrefix("0") {
                nat = nat.substringFromIndex(1)
            }
            
            orignal = "+" + "\(phoneNumber.countryCode!)"
        } catch let error as NSError {
            orignal = "+91"
            Print.printLog(error.localizedDescription)
        }
        
        return orignal
    }

    static func getPhoneNumberWithoutCountry(_ number: String) -> String {
        var orignal = number
        
        let pnu = NBPhoneNumberUtil()
        do {
            
            let pn: NBPhoneNumber = try pnu.parse(number, defaultRegion: "IN")
            var nat: String = try pnu.format(pn, numberFormat: .NATIONAL)
            
            if nat.hasPrefix("0") {
                nat = nat.substringFromIndex(1)
            }
            
            orignal = nat
        } catch let error as NSError {
            orignal = number
            Print.printLog(error.localizedDescription)
        }
        return orignal
    }
}
