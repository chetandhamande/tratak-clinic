//
//  DemoViewController.swift
//  Salk
//
//  Created by Chetan on 18/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
//import ScalePicker
import Async

class DemoViewController: UIViewController {
    
//    @IBOutlet weak var scaleHeight: ScalePicker!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        scale(scaleView: scaleHeight)
    }
    
//    func scale(scaleView: ScalePicker) {
//        scaleView.minValue = 80
//        scaleView.maxValue = 240
//        scaleView.numberOfTicksBetweenValues = 10
//        scaleView.spaceBetweenTicks = 2.0
//        scaleView.showTickLabels = true
//        //        scaleView.delegate = self
//        scaleView.snapEnabled = true
//        scaleView.bounces = true
//        scaleView.tickColor = UIColor.black
//        scaleView.centerArrowImage = UIImage(named: "ic_arrow_drop_down_black_24dp.png")
//        scaleView.gradientMaskEnabled = true
//        scaleView.blockedUI = true
//        scaleView.sidePadding = 10.0
//        scaleView.title = "Height"
//        scaleView.showCurrentValue = true
//        scaleView.trackProgress = true
//        scaleView.invertProgress = true
//        scaleView.progressColor = UIColor.green
//
//
//        //        scaleView.values = [32, 40, 50, 64, 80, 100, 125, 160, 200, 250, 320, 400, 500, 640, 800, 1000, 1250, 1600]
//
//        scaleView.valueFormatter = {(value: CGFloat) -> NSAttributedString in
//            let attrs = [NSForegroundColorAttributeName: UIColor.white,
//                         NSFontAttributeName: UIFont.systemFont(ofSize: 12.0)]
//
//            let text = "\(value)" + " auto"
//            let attrText = NSMutableAttributedString(string: text, attributes: attrs)
//
//            if let range = text.range(of: "auto") {
//                let rangeValue = text.nsRange(fromRange: range)
//
//                attrText.addAttribute(NSForegroundColorAttributeName, value:UIColor.orange, range:rangeValue)
//            }
//
//            return attrText
//        }
//
//        Async.main(after: 0.1, {_ in
//            scaleView.setInitialCurrentValue(value: 80)
//        })
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
