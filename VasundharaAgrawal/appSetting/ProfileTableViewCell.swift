//
//  ProfileTableViewCell.swift


import UIKit
import Kingfisher

@available(iOS 10.0, *)
@available(iOS 10.0, *)
class ProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var ProfileButton: UIButton!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var mobileNoLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!

    var mySharedObj = MySharedPreferences()
    
    var imageName : UIImage!
    var path : String!
    
    var classRefs: AnyObject! = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewConfiguration()
    }
    
    func getRefs(_ classRef: AnyObject) {
        self.classRefs = classRef
    }
    
    func viewConfiguration() {
        
        mobileNoLabel.text = mySharedObj.getMobile()
        nameLabel.text = mySharedObj.getFirstName() + " " + mySharedObj.getLastName();
      
        let appPref = MySharedPreferences()
        
        let URL = Foundation.URL(string: "\(SalkProjectConstant().ServerFitnessProfileImg)\( String(appPref.getUserID()).sha1() + Constant.FolderNames.imageExtensionjpg)")!
        KingfisherManager.shared.retrieveImage(with: URL, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
            if error == nil {
                self.ProfileButton.setImage(image, for: UIControlState())
            }
        })
        let image = loadImage(String(appPref.getMobile()).sha1())
        self.ProfileButton.setImage(image, for: UIControlState())
        backImageView.image = image
        self.backgroundBlurImage()
    }
    
    
    func backgroundBlurImage() {
        // show image
        backImageView.contentMode = UIViewContentMode.scaleToFill
        
        if #available(iOS 10.0, *) {
            let blur = UIBlurEffect(style: UIBlurEffectStyle.regular)
            let effectView = UIVisualEffectView(effect: blur)
            effectView.frame = backImageView.frame
            effectView.width = 1
            effectView.frame = backImageView.frame
            
            // add both effect views to the image viewP
            backImageView.addSubview(effectView)
        } else {
            // Fallback on earlier versions
        }
    }
    
    func loadImage(_ getImage: String) -> UIImage? {
        let mainPath: NSArray = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true) as NSArray
        
        path = (mainPath[0] as AnyObject).appendingPathComponent("\(Constant.FolderNames.mainFolderName)/\(Constant.FolderNames.folderUserProfile)/\(getImage)\(Constant.FolderNames.imageExtension)")
        
        let image = UIImage(contentsOfFile: path)
        return image
    }
    
    @IBAction func profileButtonClick(_ sender: UIButton) {
        (classRefs as! SettingTableView).moveToProfileViewController()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
