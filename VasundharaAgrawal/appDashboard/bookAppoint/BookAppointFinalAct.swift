//
//  BookAppointFinalAct.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController
import Async

class BookAppointFinalAct: UIViewController {

    private let loading = CustomLoading()
    
    var bundle: NSMutableDictionary! = nil
    
    private var sp_id: String = "", speciality = "", doc_id = "", name = "", date = "", time = ""
    , aptID = "", timeSlot = "";
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        sp_id = JsonParser.getJsonValueString(bundle, valueForKey: "sp_id")
        doc_id = JsonParser.getJsonValueString(bundle, valueForKey: "doc_id")
        name = JsonParser.getJsonValueString(bundle, valueForKey: "name")
        speciality = JsonParser.getJsonValueString(bundle, valueForKey: "speciality")
        aptID = JsonParser.getJsonValueString(bundle, valueForKey: "aptID")
        date = JsonParser.getJsonValueString(bundle, valueForKey: "date")
        time = JsonParser.getJsonValueString(bundle, valueForKey: "time")
        timeSlot = JsonParser.getJsonValueString(bundle, valueForKey: "timeSlot")
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Enter contact details".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    // create JSON values to send it to server
    func createJsonAppointSlot() throws -> Data {
        
        let appUserInfo = MySharedPreferences()
        var data = NSMutableDictionary()
        
        let df = DateFormatter()
        df.dateFormat = "dd MMM, yyyy"
        let dateLong = ConvertionClass().conDateToLong(df.date(from: date)!)
        df.dateFormat = "yyyy-MM-dd"
        
        let timeArr = time.split("-")
        
        data = ["osType":"iOS",
                "specialityID": sp_id,
                "doctorID": doc_id,
                "date": ConvertionClass().conLongToDate(dateLong, dateFormat: df),
                "fromTime": timeArr[0], //e.g. 8:30 AM
            "toTime": timeArr[1], //e.g. 8.45 AM
            //            "aptFor": txtAptFor, //e.g. First Consultation,Followup,None
            //            "reminderBy": txtRemindBy, //e.g. SMS,Email,Notification
            //            "details": editDetails,
            "aptType": "ONLINE", //e.g. ONLINE
            //            "name": editName,
            //            "email": editEmail,
            //            "mobileNo": editMobile,
        ]
        
        if !aptID.isEmpty {
            data.setValue(aptID, forKey: "id")
        }
        
        Print.printLog("Sending Json==\(data)")
        
        let json: Data = try! JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        return json
    }
    
    public func successResponse(respJson: Data) throws {
        loading.removeFromSuperview()

        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: respJson, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            if Int(JsonParser.getJsonValueString(dict, valueForKey: "code")) == 200 {
                
                if aptID.isEmpty {
                    mSuccDialogue("bookAppointSuccess".localized)
                } else {
                    mSuccDialogue("editAppointSuccess".localized)
                }
            }
        } catch _ { }
    }
    
    // Alert Box to Show Success Messages
    private func mSuccDialogue(_ message: String) {
        SweetAlert().showAlert("popupSuccess".localized, subTitle: message, style: .success, buttonTitle: "btnOk".localized) { click in
            self.navigationController?.popViewController(animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
