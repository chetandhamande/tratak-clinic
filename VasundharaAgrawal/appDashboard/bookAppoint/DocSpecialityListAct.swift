//
//  DocSpecialityListAct.swift
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import Material
import RAMAnimatedTabBarController

class DocSpecialityListAct: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet var tableView: UITableView!
    
//    var detailViewController: DetailViewController? = nil
    var arrSpecialities: Array<ArrSpeciality>! = nil
    var arrTempSpecialities: Array<ArrSpeciality>! = nil
    
    let searchController = UISearchController(searchResultsController: nil)
    
    let tableCellIdentifier = "CustomAdapterSpecialityList"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareNavigationItem()
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Candies"
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            // Fallback on earlier versions
        }
        definesPresentationContext = true
        
        // Setup the Scope Bar
        //    searchController.searchBar.scopeButtonTitles = ["All", "Chocolate", "Hard", "Other"]
        searchController.searchBar.delegate = self
        
        getRecordServer()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "Find & Book".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        navigationItem.backButton.tintColor = Color.black
    }
    
    private func getRecordServer() {
        // call for getting Register user
        let checkConn = CheckInternetConnection()
        if checkConn.isCheckInternetConnection() {
            DashboardRequestManager().getRequest(Constant.BookAppointment.methodLoadSpecialList, classRef: self)
        } else {
            SweetAlert().showAlert("Oops...", subTitle: "noInternet".localized, style: .warning)
        }
    }
    
    public func failGettingInfo() {
        let appPrefs = MySharedPreferences()
        SweetAlert().showAlert("popupAlert".localized, subTitle: appPrefs.getErrorMessage(), style: .warning)
    }
    
    public func successResponse(respJson: Data){
        arrSpecialities = [ArrSpeciality]()
        arrTempSpecialities = [ArrSpeciality]()
        
        let appPrefs = MySharedPreferences()
        let dict: NSMutableDictionary
        do {
            dict = try JSONSerialization.jsonObject(with: respJson, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
            
            if Int(JsonParser.getJsonValueString(dict, valueForKey: "code")) == 200 {
                let dataArray = JsonParser.getJsonValueMutableArray(dict, valueForKey: "data")
                
                for i in 0 ..< dataArray.count {
                    guard let subJson = dataArray.object(at: i) as? NSMutableDictionary else {
                        continue
                    }
                    
                    let temp: ArrSpeciality = ArrSpeciality()
                    
                    temp.id = JsonParser.getJsonValueString(subJson, valueForKey: "id")
                    temp.speciality = JsonParser.getJsonValueString(subJson, valueForKey: "speciality")
                    temp.description = JsonParser.getJsonValueString(subJson, valueForKey: "description")
                    temp.reg_date = JsonParser.getJsonValueString(subJson, valueForKey: "reg_date")
                    temp.reg_id = JsonParser.getJsonValueString(subJson, valueForKey: "reg_id")
                    temp.is_delete = JsonParser.getJsonValueString(subJson, valueForKey: "is_delete")
                    temp.status = JsonParser.getJsonValueString(subJson, valueForKey: "status")
                
                    arrTempSpecialities.append(temp)
                }
                filterContentForSearchText("")
            }
        } catch _ { }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return arrTempSpecialities.count
        }
        
        return arrSpecialities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var currCell: CustomAdapterSpecialityList!  = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? CustomAdapterSpecialityList
        
        if currCell == nil {
            tableView.register(UINib(nibName: tableCellIdentifier, bundle: nil), forCellReuseIdentifier: tableCellIdentifier)
            currCell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier) as? CustomAdapterSpecialityList
        }
        
        let candy: ArrSpeciality
        if isFiltering() {
            candy = arrTempSpecialities[indexPath.row]
        } else {
            candy = arrSpecialities[indexPath.row]
        }
        currCell.title.text = candy.id
    
        return currCell
    }
    
    // MARK: - Private instance methods
    
    func filterContentForSearchText(_ searchText: String, scope: String = "") {
        arrTempSpecialities = arrSpecialities.filter({( candy : ArrSpeciality) -> Bool in
            let doesCategoryMatch = (scope == "") || (candy.id == scope)
            
            if searchBarIsEmpty() {
                return doesCategoryMatch
            } else {
                return doesCategoryMatch && candy.id.lowercased().contains(searchText.lowercased())
            }
        })
        tableView.reloadData()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension DocSpecialityListAct: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}

extension DocSpecialityListAct: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        //    let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!, scope: "")
    }
}
