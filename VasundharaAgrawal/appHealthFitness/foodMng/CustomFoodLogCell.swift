//
//  CustomFoodLogCell.swift
//  Salk
//
//  Created by Chetan  on 11/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import SnapKit

class CustomFoodLogCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var backgroundGadgetView: UIView!
    @IBOutlet weak var backgroundCellView: UIView!
    
    @IBOutlet weak var imgSchedule: UIImageView!
    @IBOutlet weak var lblScheduleName: UILabel!
    @IBOutlet weak var lblTotalCal: UILabel!
    
    @IBOutlet weak var stackAddFood: UIStackView!
    
    @IBOutlet weak var foodCollectionView: UICollectionView!
    @IBOutlet weak var foodCollectionHeight: NSLayoutConstraint!
    
    var classRefs: AnyObject! = nil
    
    var arrFoodList = [ArrFoodLog]()
    var scheduleName = ""
    let CustomFoodItemCellIdentifier = "CustomFoodItemCell"
    var offscreenCells = Dictionary<String, UICollectionViewCell>()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundCellView.backgroundColor = CustomColor.grayBackground()
        
        foodCollectionView.delegate = self
        foodCollectionView.dataSource = self
        
        foodCollectionView.isHidden = true
        
        let nib = UINib(nibName: CustomFoodItemCellIdentifier, bundle: nil)
        foodCollectionView.register(nib, forCellWithReuseIdentifier: CustomFoodItemCellIdentifier)
    }
}

extension CustomFoodLogCell {
    
    func onBindViewHolder(_ featureList: [ArrFoodSchedule], position: Int, classRef: AnyObject)
    {
        classRefs = classRef
        
        let user: ArrFoodSchedule = featureList[position]
        
        self.lblScheduleName.text = user.scheduleName
        self.lblTotalCal.text = "\(user.totalCal)"
        
        self.imgSchedule.image = UIImage(named: user.imgResouceName)
        
        self.arrFoodList = user.arrFoodList
        self.scheduleName = user.scheduleName
        
        if arrFoodList.count == 0 {
            self.foodCollectionView.isHidden = true
        } else {
            self.foodCollectionView.isHidden = false
            self.foodCollectionView.snp.makeConstraints({ (make: ConstraintMaker) -> Void in
                let height = Int(foodCollectionHeight.constant) * arrFoodList.count
                make.height.equalTo(height)
            })
            // Make sure layout subviews
            self.setNeedsLayout()
            self.layoutIfNeeded()
            self.foodCollectionView.reloadData()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFoodList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CustomFoodItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomFoodItemCellIdentifier, for: indexPath) as! CustomFoodItemCell
        cell.onBindViewHolder(arrFoodList, position: indexPath.row, classRefs: classRefs, scheduleName: scheduleName)
        return cell
    }
}

