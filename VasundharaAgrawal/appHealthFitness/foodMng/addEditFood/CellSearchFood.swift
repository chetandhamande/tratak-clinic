//
//  CellSearchFood.swift
//  Salk
//
//  Created by Salk on 24/11/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CellSearchFood: UICollectionViewCell {

    @IBOutlet weak var lblFoodName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func getView(arrMainList: [ArrFoodLog], position: Int) {
        lblFoodName.text = arrMainList[position].foodName
    }

}
