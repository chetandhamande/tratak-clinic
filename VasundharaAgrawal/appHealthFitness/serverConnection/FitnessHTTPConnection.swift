
class FitnessHTTPConnection {
    
    var classRefs: AnyObject! = nil
    
    func sendServerRequest(_ url: String, mMethod: String, classRef: AnyObject!, dict: NSMutableDictionary?) {
        
        classRefs = classRef
        var json = Data()
        
        if dict != nil {
            json = try! JSONSerialization.data(withJSONObject: dict!, options: JSONSerialization.WritingOptions.prettyPrinted)
        }
        
        do {
            if (mMethod == Constant.Fitness.MethodDownloadFoodLog
                || mMethod == Constant.Fitness.MethodDownloadActivityLog
                || mMethod == Constant.Fitness.MethodDownloadFoodContainer
                || mMethod == Constant.Fitness.MethodScheduleList
                || mMethod == Constant.Fitness.MethodSyncTargetVal)
            {
                try json = FitnessJsonCreator().putJsonExpertUserSync() as Data
            } else if (mMethod == Constant.Fitness.MethodSyncFitnessLog) {
                let appPrefs = MySharedPreferences()
                if(appPrefs.getLastLogDataSycnType() == "food") {
                    try json = FitnessJsonCreator().putJsonFoodSyncLog() as Data
                } else if(appPrefs.getLastLogDataSycnType() == "activity") {
                    try json = FitnessJsonCreator().putJsonActivitySyncLog() as Data
                } else if(appPrefs.getLastLogDataSycnType() == "water") {
                    try json = FitnessJsonCreator().putJsonWaterSyncLog() as Data
                } else if(appPrefs.getLastLogDataSycnType() == "pedometer") {
                    try json = FitnessJsonCreator().putJsonPedometerSyncLog() as Data
                } else if(appPrefs.getLastLogDataSycnType() == "sleep") {
                    try json = FitnessJsonCreator().putJsonSleepSyncLog() as Data
                }
            } else if (mMethod == Constant.ExpertChat.MethodSyncExpertChat) {
                try json = FitnessJsonCreator().putJsonExpertChatSync() as Data
            } else if (mMethod == Constant.ExpertChat.MethodSyncExpertUser) {
                try json = FitnessJsonCreator().putJsonExpertUserSync() as Data
            } else if (mMethod == Constant.Fitness.MethodSyncRoutingTrack) {
                try json = FitnessJsonCreator().putJsonSyncRoutingTrack() as Data
            } else if (mMethod == Constant.Fitness.MethodSyncMyDiary
                || mMethod == Constant.Fitness.MethodSyncReminder)
            {
                try json = FitnessJsonCreator().putJsonSyncMyDiary() as Data
            } else if (mMethod == Constant.ExpertChat.MethodSyncChatMessage) {
                try json = FitnessJsonCreator().putJsonSyncChatMessage() as Data
            } else if (mMethod == Constant.ExpertChat.MethodSyncChatStatus) {
                try json = FitnessJsonCreator().putJsonSyncChatStatus() as Data
            } else if (mMethod == Constant.Fitness.MethodSyncTLSleep
                || mMethod == Constant.Fitness.MethodSyncTLWater
                || mMethod == Constant.Fitness.MethodSyncTLFood
                || mMethod == Constant.Fitness.MethodSyncTLPedometer)
            {
                try json = FitnessJsonCreator().putJsonSyncTimeLine() as Data
            } else if (mMethod == Constant.Fitness.MethodSendReminder) {
                try json = FitnessJsonCreator().putJsonSendReminder() as Data
            } else if (mMethod == Constant.Fitness.MethodSyncTimeline) {
                try json = FitnessJsonCreator().putJsonSyncTimeLineSingle()  as Data
            } else if (mMethod == Constant.Fitness.MethodSyncSummeryTimeline) {
                try json = FitnessJsonCreator().putJsonSyncSummeryTimeLineSingle()  as Data
            } else if (mMethod == Constant.DocumentWallet.methodDocumentWallet)
            {
                //try json = FitnessJsonCreator().putJsonSendDataVault()  as Data
            } else if (mMethod == Constant.DocumentWallet.methodSyncDocumentWallet) {
                //try json = FitnessJsonCreator().putJsonExpertUserSync()  as Data
            } else if (mMethod == Constant.DocumentWallet.methodDocumentWalletDelete) {
                //try json = FitnessJsonCreator().putJsonSendDataVaultDelete()  as Data
            } else if (mMethod == Constant.Fitness.MethodDeleteFoodRec) {
                try json = FitnessJsonCreator().putJsonDeleteFoodRec()  as Data
            }
        } catch {}
        
        
        //execute Http Connection
        let customObj = CustomNetworkConnection()
        customObj.sendGetRequestToRest(url, json: json, methodName: mMethod, classRef: self)
    }
    
    func getServerResponce(_ respCode: Int?, json: Data?, mMethod: String) {
        
        let appPrefs = MySharedPreferences()
        
        do {
            if respCode == 200 {
                let respJson = NSString(data: json!, encoding: String.Encoding.utf8.rawValue)!
                
                Print.printLog("Rec Code= \(String(describing: respCode)), Resp Data=\(respJson)")
                
                if (mMethod == Constant.Fitness.MethodSyncFitnessLog)
                {
                    if(appPrefs.getLastLogDataSycnType() == "food") {
                        try FitnessJsonCreator().storeJsonFoodSyncLog(json!)
                    } else if(appPrefs.getLastLogDataSycnType() == "activity") {
                        try FitnessJsonCreator().storeJsonActivitySyncLog(json!)
                    } else if(appPrefs.getLastLogDataSycnType() == "water") {
                        try FitnessJsonCreator().storeJsonWaterSyncLog(json!)
                    } else if(appPrefs.getLastLogDataSycnType() == "pedometer") {
                        try FitnessJsonCreator().storeJsonPedometerSyncLog(json!)
                    } else if(appPrefs.getLastLogDataSycnType() == "sleep") {
                        try FitnessJsonCreator().storeJsonSleepSyncLog(json!)
                    }
                    
                    if(appPrefs.getErrorCode() == 200) {
                        do {
                            try PendingLogSyncService().onHandleIntent()
                        } catch {}
                    }
                } else if (mMethod == Constant.ExpertChat.MethodSyncExpertChat)
                {
                    try FitnessJsonCreator().storeJsonExpertChatSync(json!)
                    
                    if(appPrefs.getErrorCode() == 200) {
                        do {
                            try PendingLogSyncService().onHandleIntent()
                        } catch {}
                    }
                } else if (mMethod == Constant.Fitness.MethodSendReminder)
                {
                    try FitnessJsonCreator().storeJsonSendReminder(json!)
                    
                    if(appPrefs.getErrorCode() == 200) {
                        do {
                            try PendingLogSyncService().onHandleIntent()
                        } catch {}
                    }
                } else if (mMethod == Constant.Fitness.MethodSyncRoutingTrack)
                {
                    try FitnessJsonCreator().storeJsonSyncRoutingTrack(json!)
                    if(appPrefs.getErrorCode() == 200) {
                        do {
                            try PendingLogSyncService().onHandleIntent()
                        } catch {}
                    }
                } else if (mMethod == Constant.Fitness.MethodDeleteFoodRec)
                {
                    try FitnessJsonCreator().storeJsonDefault(json!)
                    if(appPrefs.getErrorCode() == 200) {
                        do {
                            try (classRefs as! CustomFoodItemCell).performDeleteLocalDB(recID: appPrefs.getTempRecordID(), isLocal: false)
                        } catch {}
                    }
                } else if (mMethod == Constant.Fitness.MethodDownloadFoodLog) {
                    try FitnessJsonCreator().storeFoodDetailsLogJson(json!)
                    
                    if(classRefs.isKind(of: BuildSalkViewController.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            (classRefs as! BuildSalkViewController).refreshLoadNextData();
                        } else {
                            (classRefs as! BuildSalkViewController).failGettingInfo();
                        }
                    } else if(classRefs.isKind(of: SettingTableView.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            (classRefs as! SettingTableView).refreshFoodList()
                        } else {
                            (classRefs as! SettingTableView).stopProgessList()
                        }
                    }
                    //                else if(classRefs.isKind(of: FoodLogActivityMng)) {
                    //                    if (appPrefs.getErrorCode() == 200) {
                    //                        ((FoodLogActivityMng)classRefs).downloadContainerList();
                    //                    }
                    //                }
                } else if (mMethod == Constant.Fitness.MethodDownloadFoodContainer)
                {
                    try FitnessJsonCreator().storeFoodContainerLogJson(json!)
                    
                    if (classRefs.isKind(of: BuildSalkViewController.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            (classRefs as! BuildSalkViewController).refreshLoadNextData();
                        } else {
                            (classRefs as! BuildSalkViewController).failGettingInfo();
                        }
                    } else if(classRefs.isKind(of: SettingTableView.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            (classRefs as! SettingTableView).refreshFoodContinerList()
                        } else {
                            (classRefs as! SettingTableView).stopProgessList()
                        }
                    }
                } else if (mMethod == Constant.Fitness.MethodScheduleList) {
                    try FitnessJsonCreator().storeJsonScheduleList(json!)
                    
                    if (classRefs.isKind(of: BuildSalkViewController.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            (classRefs as! BuildSalkViewController).refreshLoadNextData();
                        } else {
                            (classRefs as! BuildSalkViewController).failGettingInfo();
                        }
                    }
                } else if (mMethod == Constant.Fitness.MethodDownloadActivityLog) {
                    try FitnessJsonCreator().storeActivityDetailsLogJson(json!)
                    
                    if(classRefs.isKind(of: BuildSalkViewController.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            (classRefs as! BuildSalkViewController).refreshLoadNextData();
                        } else {
                            (classRefs as! BuildSalkViewController).failGettingInfo();
                        }
                    } else if(classRefs.isKind(of: SettingTableView.self)) {
                        if (appPrefs.getErrorCode() == 200) {
                            (classRefs as! SettingTableView).refreshActivityListDone()
                        } else {
                            (classRefs as! SettingTableView).stopProgessList()
                        }
                    }
                } else if (mMethod == Constant.ExpertChat.MethodSyncExpertUser) {
                    try FitnessJsonCreator().storeJsonExpertUserSync(json!)
                    
                    if (appPrefs.getErrorCode() == 200) {
                        (classRefs as! MainExpertVC).refreshExpertList()
                    } else {
                        (classRefs as! MainExpertVC).stopProgessList()
                    }
                } else if (mMethod == Constant.Fitness.MethodSyncMyDiary) {
                    try FitnessJsonCreator().storeJsonSyncMyDiary(json!)
                    if (appPrefs.getErrorCode() == 200) {
                        if(appPrefs.getTempRecordID().equalsIgnoreCase("diet")) {
                            appPrefs.setTempRecordID(text: "fitness")
                            FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncMyDiary, classRef: classRefs)
                        } else if(appPrefs.getTempRecordID().equalsIgnoreCase("fitness")) {
                            appPrefs.setTempRecordID(text: "nutraceuticals")
                            FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncMyDiary, classRef: classRefs)
                        } else if(appPrefs.getTempRecordID().equalsIgnoreCase("nutraceuticals")) {
                            appPrefs.setTempRecordID(text: "therapy")
                            FitnessRequestManager().getRequest(Constant.Fitness.MethodSyncMyDiary, classRef: classRefs)
                        } else {
                            if classRefs.isKind(of: MyDiaryPanelTabLayout.self) {
                                (classRefs as! MyDiaryPanelTabLayout).refreshMyDiaryList()
                            }
                        }
                    }
                } else if (mMethod == Constant.ExpertChat.MethodSyncChatMessage) {
                    try FitnessJsonCreator().storeJsonSyncChatMessage(json!)
                    
                    if (appPrefs.getErrorCode() == 200) {
                        if (classRefs.isKind(of: ChatViewController.self)) {
                            (classRefs as! ChatViewController).refreshChatMessage()
                        }
                    }
                } else if (mMethod == Constant.Fitness.MethodSyncTLSleep
                    || mMethod == Constant.Fitness.MethodSyncTLWater
                    || mMethod == Constant.Fitness.MethodSyncTLFood
                    || mMethod == Constant.Fitness.MethodSyncTLPedometer)
                {
                    try FitnessJsonCreator().storeJsonSyncTimeLine(json!, method: mMethod)
                    
                    if(classRefs.isKind(of: SleepTimelineTVC.self)) {
                            (classRefs as! SleepTimelineTVC).refreshOnResponse()
                    } else if(classRefs.isKind(of: WaterTimelineTVC.self))
                    {
                        (classRefs as! WaterTimelineTVC).refreshOnResponse()
                    } else if(classRefs.isKind(of: StepsTimelineTVC.self)) {
                        (classRefs as! StepsTimelineTVC).refreshOnResponse()
                    } else if(classRefs.isKind(of: CaloriesTimelineTVC.self))
                    {
                        (classRefs as! CaloriesTimelineTVC).refreshOnResponse()
                    } else if(classRefs.isKind(of: DistanceTimelineTVC.self))
                    {
                        (classRefs as! DistanceTimelineTVC).refreshOnResponse()
                    } else if(classRefs.isKind(of: FoodTimelineTVC.self)) {
                        (classRefs as! FoodTimelineTVC).refreshOnResponse()
                    } else if(classRefs.isKind(of: BuildSalkViewController.self))
                    {
                        (classRefs as! BuildSalkViewController).refreshLoadNextData()
                    }
                } else if (mMethod == Constant.Fitness.MethodSyncReminder) {
                    try FitnessJsonCreator().storeJsonSyncReminder(json!)
                    
                    if (appPrefs.getErrorCode() == 200) {
                        if(classRefs.isKind(of: BuildSalkViewController.self)) {
                            (classRefs as! BuildSalkViewController).refreshLoadNextData()
                        }
                    }
                } else if (mMethod == Constant.Fitness.MethodSyncTimeline) {
                    try FitnessJsonCreator().storeJsonSyncTimeLine(json!)
                    
                    if (appPrefs.getErrorCode() == 200) {
                        //((FragmentScheduleList)classRefs).refreshList();
                    }
                } else if (mMethod == Constant.Fitness.MethodSyncSummeryTimeline)
                {
                    try FitnessJsonCreator().storeJsonSyncTimeLineSummery(json!)
                    
                    if (appPrefs.getErrorCode() == 200) {
                        (classRefs as! TimelineTableVC).refreshList()
                    } else {
                        (classRefs as! TimelineTableVC).stopProgessList()
                    }
                } else if (mMethod == Constant.DocumentWallet.methodDocumentWallet)
                {
                    //try FitnessJsonCreator().storeJsonDefault(!json)
                    
                    //                if(appPrefs.getErrorCode() == 200) {
                    //                    ((UploadDocuAct)classRefs).documentUploadSuccess(mJSON);
                    //                } else {
                    //                    ((UploadDocuAct)classRefs).documentUploadFail("");
                    //                }
                } else if (mMethod == Constant.DocumentWallet.methodSyncDocumentWallet)
                {
                    //try FitnessJsonCreator().storeJsonSendDataVault(json!)
                    
                   
                } else if (mMethod == Constant.DocumentWallet.methodDocumentWalletDelete)
                {
                    //try FitnessJsonCreator().storeJsonDefaultDelete(json!)
                    
                    //                ((DocumentPhotoDesign)classRefs).performDeleteOpr();
                } else if (mMethod == Constant.ExpertChat.MethodSyncChatStatus)
                {
                    try FitnessJsonCreator().storeJsonSyncChatStatus(json!)
                    
                } else if (mMethod == Constant.Fitness.MethodSyncTargetVal) {
                    try FitnessJsonCreator().storeJsonSyncTargetVal(json!)
                    
                    if (appPrefs.getErrorCode() == 200) {
                        if(classRefs.isKind(of: BuildSalkViewController.self)) {
                            (classRefs as! BuildSalkViewController).refreshLoadNextData()
                        }
                    }
                }
                
            } else {
                if (mMethod == Constant.Fitness.MethodDownloadFoodLog
                    || mMethod == Constant.Fitness.MethodSyncTargetVal)
                {
                    if(classRefs.isKind(of: BuildSalkViewController.self)) {
                        (classRefs as! BuildSalkViewController).failGettingInfo();
                    }
                } else if (mMethod == Constant.Fitness.MethodDownloadFoodContainer) {
                    if(classRefs.isKind(of: BuildSalkViewController.self)) {
                        (classRefs as! BuildSalkViewController).failGettingInfo();
                    }
                } else if (mMethod == Constant.Fitness.MethodDownloadActivityLog) {
                    if(classRefs.isKind(of: BuildSalkViewController.self)) {
                        (classRefs as! BuildSalkViewController).failGettingInfo();
                    }
                } else if (mMethod == Constant.ExpertChat.MethodSyncExpertUser) {
                    (classRefs as! MainExpertVC).stopProgessList()
                } else if (mMethod == Constant.Fitness.MethodSyncMyDiary && classRefs.isKind(of: MyDiaryPanelTabLayout.self)) {
                    (classRefs as! MyDiaryPanelTabLayout).failGettingInfo()
                }
                else if (mMethod == Constant.ExpertChat.MethodSyncChatMessage || mMethod == Constant.Fitness.MethodSyncTimeline)
                {
                    mErrDialogue(appPrefs.getErrorMessage())
                } else if (mMethod == Constant.Fitness.MethodSyncTLSleep
                    || mMethod == Constant.Fitness.MethodSyncTLWater
                    || mMethod == Constant.Fitness.MethodSyncTLFood
                    || mMethod == Constant.Fitness.MethodSyncTLPedometer)
                {
                    
                    if(classRefs.isKind(of: SleepTimelineTVC.self)) {
                        (classRefs as! SleepTimelineTVC).stopOnResponse()
                    } else if(classRefs.isKind(of: WaterTimelineTVC.self))
                    {
                        (classRefs as! WaterTimelineTVC).stopOnResponse()
                    } else if(classRefs.isKind(of: StepsTimelineTVC.self)) {
                        (classRefs as! StepsTimelineTVC).stopOnResponse()
                    } else if(classRefs.isKind(of: CaloriesTimelineTVC.self))
                    {
                        (classRefs as! CaloriesTimelineTVC).stopOnResponse()
                    } else if(classRefs.isKind(of: DistanceTimelineTVC.self))
                    {
                        (classRefs as! DistanceTimelineTVC).stopOnResponse()
                    } else if(classRefs.isKind(of: FoodTimelineTVC.self)) {
                        (classRefs as! FoodTimelineTVC).stopOnResponse()
                    } else if(classRefs.isKind(of: BuildSalkViewController.self))
                    {
                        (classRefs as! BuildSalkViewController).refreshLoadNextData();
                    }
                } else if (mMethod == Constant.Fitness.MethodSyncReminder) {
                    if(classRefs.isKind(of: BuildSalkViewController.self)) {
                        (classRefs as! BuildSalkViewController).refreshLoadNextData();
                    }
                } else if (mMethod == Constant.DocumentWallet.methodDocumentWallet
                    || mMethod == Constant.DocumentWallet.methodSyncDocumentWallet)
                {
                    mErrDialogue(appPrefs.getErrorMessage())
                } else if (mMethod == Constant.Fitness.MethodSyncSummeryTimeline)
                {
                    (classRefs as! TimelineTableVC).stopProgessList()
                }
            }
        } catch {}
    }
    
    // Alert Box to Show error Messages
    fileprivate func mErrDialogue(_ message: String) {
        SweetAlert().showAlert("fail".localized, subTitle: message, style: .error)
    }
}
