//
//  ArrActivity.swift
//  Salk
//
//  Created by Chetan  on 03/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import Foundation

class ArrActivity {
    var recID = "", recName = "", recSubName = "", recOriID = "", type = "", mainType = "", videoLink = "", days = "", logTime = ""
    var isChecked:Bool = false, isChoice:Bool = false, isSubmited:Bool = false, isSelOptVisible:Bool = true
}
