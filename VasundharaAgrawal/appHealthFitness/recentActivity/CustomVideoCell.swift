//
//  CustomVideoCell.swift
//  Salk
//
//  Created by Chetan  on 05/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomVideoCell: UITableViewCell {

    @IBOutlet weak var imgOption: UIImageView!
    @IBOutlet weak var imgPlay: UIButton!
    @IBOutlet weak var lblDetails: UILabel!
    
    var classRefs: AnyObject! = nil
    fileprivate var arrMainList = [ArrActivity]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        SalkRippleEffect.SalkRippleEffect(imgPlay)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func onBindViewHolder(_ featureList: [ArrActivity], position: Int, classRef: AnyObject)
    {
        classRefs = classRef
        arrMainList = featureList
        
        let user: ArrActivity = featureList[position]
        
        lblDetails.attributedText = user.recSubName.html2AttributedString
        
        imgOption.image = UIImage(named: "radio_on.png")
        
        if(user.videoLink.lengthOfBytes(using: String.Encoding.utf8) < 5) {
            imgPlay.isHidden = true
        } else {
            imgPlay.isHidden = false
        }
        
        imgPlay.tag = position
    }
    
    @IBAction func detailButtonClick(_ sender: UIButton) {
        let position = sender.tag
        
        (classRefs as! VideoPopupVC).playVideo(videoLink: arrMainList[position].videoLink, title: arrMainList[position].recSubName)
    }
    
}
