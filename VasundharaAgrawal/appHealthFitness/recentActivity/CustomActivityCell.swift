//
//  CustomActivityCell.swift
//  Salk
//
//  Created by Chetan  on 04/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit

class CustomActivityCell: UITableViewCell {

    @IBOutlet weak var imgOption: UIImageView!
    @IBOutlet weak var imgPlay: UIButton!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    
    var classRefs: AnyObject! = nil
    fileprivate var arrMainList = [ArrActivity]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        SalkRippleEffect.SalkRippleEffect(imgPlay)
    }
    
    func onBindViewHolder(_ featureList: [ArrActivity], position: Int, classRef: AnyObject)
    {
        classRefs = classRef
        arrMainList = featureList
        
        let user: ArrActivity = featureList[position]
        
        self.lblHeading.text = user.recName
        self.lblDetails.attributedText = user.recSubName.html2AttributedString
        lblDetails.sizeToFit()
        
        if(user.isSelOptVisible) {
            imgOption.isHidden = false
        } else {
            imgOption.isHidden = true
        }
        
        if(user.isChecked) {
            if(user.isChoice) {
                self.imgOption.image = UIImage(named: "radio_on.png")
            } else {
                self.imgOption.image = UIImage(named: "check_select.png")
            }
        } else {
            if(user.isChoice) {
                self.imgOption.image = UIImage(named: "radio_off.png")
            } else {
                self.imgOption.image = UIImage(named: "check_unselect.png")
            }
        }
        
        imgPlay.tag = position
        if(user.videoLink.lengthOfBytes(using: String.Encoding.utf8) < 5) {
            imgPlay.isHidden = true
        } else {
            imgPlay.isHidden = false
        }
    }
    
    @IBAction func detailButtonClick(_ sender: UIButton) {
        let position = sender.tag
        
        (classRefs as! ActivityDonePopupVC).moveToVideoController(arrMainList[position].recID, mainType: arrMainList[position].mainType)
    }    
}
