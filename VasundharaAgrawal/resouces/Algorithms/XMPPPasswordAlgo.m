//
//  XMPPPasswordAlgo.m
//  Salk
//
//  Created by Chetan  on 06/05/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

#import "XMPPPasswordAlgo.h"

@implementation XMPPPasswordAlgo
{
    int numRound;
}

-(NSString *)getXMPPUserPassword :(NSString *)inputStr
{
    numRound  = 3;
    NSString *outputStr=inputStr;
    
    @try {
        for(int i=1; i<=numRound; i++) {
            //call for make 60-40
            NSMutableArray *temp60_40 =[[NSMutableArray alloc]init];
            temp60_40=[self splitStr60_40:outputStr];
            
            if([self isEvenStr:[temp60_40 objectAtIndex:0]]){
                //mix 1st part
                NSString *temp = [temp60_40 objectAtIndex:0];
                [temp60_40 removeObjectAtIndex:0];
                [temp60_40 insertObject:[self getMixString:temp] atIndex:0];
            } else {
                //reverse 1st part
                NSString *temp = [temp60_40 objectAtIndex:0];
                [temp60_40 removeObjectAtIndex:0];
                [temp60_40 insertObject:[self getReverseString:temp] atIndex:0];
            }
            
            if([self isEvenStr:[temp60_40 objectAtIndex:1]]){
                //mix 1st part
                NSString *temp = [temp60_40 objectAtIndex:1];
                [temp60_40 removeObjectAtIndex:1];
                [temp60_40 insertObject:[self getMixString:temp] atIndex:1];
            }else{
                //reverse 1st part
                NSString *temp = [temp60_40 objectAtIndex:1];
                [temp60_40 removeObjectAtIndex:1];
                [temp60_40 insertObject:[self getReverseString:temp] atIndex:1];
            }
            
            NSString *tempOutput=@"";
            for(int j=0; j<temp60_40.count; j++) {
                NSString *partStr = [temp60_40 objectAtIndex:j];
                BOOL isEven = [self isEvenStr:partStr];
                
                NSMutableArray *temp50_50=[[NSMutableArray alloc]init];
                if(isEven) {
                    //make equal pars
                    temp50_50 = [self splitStr50_50:partStr];
                    
                    if([self isEvenStr:[temp50_50 objectAtIndex:0]]) {
                        //reverse 1st part
                        NSString *temp = [temp50_50 objectAtIndex:0];
                        [temp50_50 removeObjectAtIndex:0];
                        [temp50_50 insertObject:[self getMixString:temp] atIndex:0];
                    } else {
                        //reverse 1st part
                        NSString *temp = [temp50_50 objectAtIndex:0];
                        [temp50_50 removeObjectAtIndex:0];
                        [temp50_50 insertObject:[self getReverseString:temp] atIndex:0];
                    }
                } else {
                    temp50_50 = [self splitStr60_40:partStr];
                    
                    if([self isEvenStr:[temp50_50 objectAtIndex:1]]) {
                        //reverse 1st part
                        NSString *temp = [temp50_50 objectAtIndex:1];
                        [temp50_50 removeObjectAtIndex:1];
                        [temp50_50 insertObject:[self getMixString:temp] atIndex:1];
                    } else {
                        //reverse 1st part
                        NSString *temp = [temp50_50 objectAtIndex:1];
                        [temp50_50 removeObjectAtIndex:1];
                        [temp50_50 insertObject:[self getReverseString:temp] atIndex:1];
                    }
                }
                
                for(int k=(int)temp50_50.count-1; k >= 0; k--)
                {
                    tempOutput=[tempOutput stringByAppendingFormat:@"%@",[temp50_50 objectAtIndex:k]];
                }
            }
            
            outputStr=[self sha1:tempOutput];            
            
        }
    } @catch (NSException *exception) {
        outputStr = [self sha1:inputStr];
    }
    return outputStr;
}


//split string in 60-40
-(NSMutableArray *)splitStr60_40:(NSString *)str{
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    
    int totalLen = (int)[str length];
    @try {
        if(totalLen > 0) {
            //make 60 %
            int sixtyPer = (int)(totalLen * 0.68);
            NSString *strTemp = [str substringWithRange:NSMakeRange(0, sixtyPer)];
            [temp addObject:strTemp];
            
            //make 40 %
            strTemp=[str substringFromIndex:sixtyPer];
            [temp addObject:strTemp];
        }
    }
    @catch (NSException *exception) {}
    
    return temp;
}


//split string in 50-50
-(NSMutableArray *)splitStr50_50:(NSString *)str{
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    
    int totalLen = (int)[str length];
    @try {
        if(totalLen > 0) {
            //make 50 %
            int fiftyPer = (int)(totalLen * 0.5);
            NSString *strTemp = [str substringWithRange:NSMakeRange(0, fiftyPer)];
            [temp addObject:strTemp];
            
            //make 50 %
            strTemp = [str substringFromIndex:fiftyPer];
            [temp addObject:strTemp];
        }
    } @catch (NSException *exception) {}
				
    return temp;
}


//reverse string
-(NSString *)getReverseString:(NSString *)inputStr {
    NSString *output=@"";
    
    for(int i=(int)inputStr.length-1; i>=0; i--) {
        output=[output stringByAppendingFormat:@"%c",[inputStr characterAtIndex:i]];
    }
    
    return output;
}

//Mix string
-(NSString *)getMixString:(NSString *)inputStr {
    NSString *output=@"";
    
    for(int i=0; i<(int)inputStr.length; i++) {
        int index = i;
        if(i%2 == 0)
            index +=1;
        else
            index -=1;
        
        @try {
            output=[output stringByAppendingFormat:@"%c", [inputStr characterAtIndex:index]];
        } @catch (NSException *exception) {
            output = [output stringByAppendingFormat:@"%c",[inputStr characterAtIndex:i]];
        }
    }
    
    return output;
}

//function for even odd 
-(BOOL)isEvenStr:(NSString *)str {
    if((str.length)%2==0){
        return true;
    }
    else{
        return false;
    }
}

-(NSString*) sha1:(NSString*)input{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1 (data.bytes, (int)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

@end
