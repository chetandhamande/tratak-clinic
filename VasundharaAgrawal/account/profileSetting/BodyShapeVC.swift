//
//  BodyShapeVC.swift
//  Salk
//
//  Created by Chetan  on 16/07/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
import Material

class BodyShapeVC: UIViewController, GenderViewDelegate {

    @IBOutlet weak var txtHip: UITextField!
    @IBOutlet weak var txtChest: UITextField!
    @IBOutlet weak var txtWaist: UITextField!
    
    @IBOutlet weak var btnBodyShape: UIButton!
    
    fileprivate var loadingObj = CustomLoading()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.hidesBottomBarWhenPushed = true
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let aa = self.tabBarController as! RAMAnimatedTabBarController
        aa.animationTabBarHidden(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareNavigationItem()
        self.loadPreviousData()
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Prepares the navigationItem.
    fileprivate func prepareNavigationItem() {
        navigationItem.titleLabel.text = "naviBodyShape".localized
        navigationItem.titleLabel.textColor = Color.black
        navigationItem.titleLabel.textAlignment = .left
        
        let submitImage: UIImage = UIImage().imageWithImage(UIImage(named: "ic_agree_colored.png")!, scaledToSize: CGSize(width: 20, height: 20))
        let submitButton = UIBarButtonItem(image: submitImage, style: .plain, target: self, action: #selector(self.performSubmit))
        
        navigationItem.rightBarButtonItems = [submitButton]
    }
    
    @IBAction func changeWeightUnit(button: UIButton) {
        
        var arrBodyShape = [String] ()
        
        let appPref = MySharedPreferences()
        if(appPref.getGender().uppercased() == "FEMALE") {
            arrBodyShape = ["Slim", "Average", "Heavy", "Pretty heavy"]
        } else if(appPref.getGender().uppercased() == "MALE") {
            arrBodyShape = ["Slim", "Average", "Slightly Heavy", "Heavy", "Pretty heavy"]
        }
        
        let genderViewObj = GenderViewController(nibName: "GenderViewController", bundle: nil)
        genderViewObj.genderArray = arrBodyShape
        genderViewObj.headingStr = "Your Body Shape"
        genderViewObj.delegate = self
        
        self.showPopUpViewController(selfViewController: self, presentViewController: genderViewObj)
    }
    
    func didSelectGender(_ controller: GenderViewController, gender: String)
    {
        btnBodyShape.setTitle(gender, for: UIControlState())
    }
}

extension BodyShapeVC {
    
    fileprivate func loadPreviousData() {
        
        let appPrefs = MySharedPreferences()
        
        txtHip.text = appPrefs.getUserHip()
        txtWaist.text = appPrefs.getUserWaist()
        txtChest.text = appPrefs.getUserChest()
        
        btnBodyShape.setTitle(appPrefs.getBodyShape(), for: UIControlState.normal)
    }
    
    @objc fileprivate func performSubmit() {
        
        self.dismissKeyboard()
        
        let isInternet = CheckInternetConnection().isCheckInternetConnection()
        if isInternet {
            
            if(txtChest.text?.isEmpty)! {
                showSweetDialog(title: "Oops...", message: "Missing Chest Size")
            } else if(txtHip.text?.isEmpty)! {
                showSweetDialog(title: "Oops...", message: "Missing Hip Size")
            } else if(txtWaist.text?.isEmpty)! {
                showSweetDialog(title: "Oops...", message: "Missing Waist Size")
            } else {
                
                let appPrefs = MySharedPreferences()
                
                appPrefs.setUserHip(text: txtHip.text!)
                appPrefs.setUserWaist(text: txtWaist.text!)
                appPrefs.setUserChest(text: txtChest.text!)
                
                appPrefs.setBodyShape(text: (btnBodyShape.titleLabel?.text)!)
                
                self.view.addSubview(loadingObj.loading())
                
                UserRegistationRequestManager().getRequest(Constant.AccountMng.MethodUpdateProfile, classRef: self)
            }
        } else {
            showSweetDialog(title: "Oops...", message: "noInternet".localized)
        }
    }
    
    fileprivate func showSweetDialog(title: String, message: String) {
        SweetAlert().showOnlyAlert(title, subTitle: message, style: AlertStyle.warning, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
    
    func errorInConnection() {
        loadingObj.removeFromSuperview()
        
        let appPref = MySharedPreferences()
        showSweetDialog(title: "Oops...", message: appPref.getErrorMessage())
    }
    
    func success() {
        loadingObj.removeFromSuperview()
        
        SweetAlert().showOnlyAlert("Success", subTitle: "Your profile update successfully", style: AlertStyle.success, buttonTitle: "btnOK".localized, buttonColor: UIColor.colorFromRGB(0xDD6B55), action: nil)
    }
}
