//
//  PersonalHabbitsView.swift
//  Salk
//
//  Created by Chetan on 13/12/17.
//  Copyright © 2017 Muddy Roads Technology. All rights reserved.
//

import UIKit
import DLRadioButton

class PersonalHabbitsView: UIView {

    var view: UIView!
    
    var classRefs: AnyObject! = nil
    
    @IBOutlet weak var smokeYes: DLRadioButton!
    @IBOutlet weak var smokeNo: DLRadioButton!
    @IBOutlet weak var alcoholYes: DLRadioButton!
    @IBOutlet weak var alcoholNo: DLRadioButton!
    
    @IBOutlet weak var editHobbies: MKTextField!
    
    var smoking_since = "", alcohol = ""
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        viewConfiguration()
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PersonalHabbitsView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func viewConfiguration() {
        
        //        headerView.backgroundColor = PrimaryColor.blackTransparent1()
        //
        //        lblHeading.text = "Major Medical Complaints"
        
        configureTextField(editHobbies, placeholder: "Hobby")
        
        createRadioButton(smokeYes, title: "Yes")
        createRadioButton(smokeNo, title: "No")
        createRadioButton(alcoholYes, title: "Yes")
        createRadioButton(alcoholNo, title: "No")
    }
    
    func configureTextField(_ textField: MKTextField, placeholder: String) {
        
        textField.placeholder = placeholder
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.floatingPlaceholderEnabled = true
        textField.tintColor = PrimaryColor.colorPrimary()
        textField.rippleLocation = .right
        textField.cornerRadius = 0
        textField.bottomBorderEnabled = true
        textField.font = UIFont.systemFont(ofSize: 15)
    }
    
    func configureLabel(_ label: UILabel, setString: String) {
        label.text = setString
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = CustomColor.darker_gray()
    }
    
    func configureButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textGrayColor(), for: UIControlState.normal)
    }
    
    func configureSelectButton(_ button: UIButton, setString: String) {
        button.setTitle(setString, for: .normal)
        button.setTitleColor(CustomColor.textBlackColor(), for: UIControlState.normal)
        
        refreshView()
    }
    
    func refreshView() {
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
    
    private func createRadioButton(_ button: DLRadioButton, title: String) {
        button.indicatorColor = CustomColor.dark_gray()
        button.setTitle(title, for: .normal)
        button.setTitleColor(CustomColor.dark_gray(), for: .normal)
        button.isIconSquare = false
        button.isMultipleSelectionEnabled = false
    }
    
    @objc @IBAction private func radioSmokingClick(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            smoking_since = radioButton.selected()!.titleLabel!.text!
        }
    }
    
    @objc @IBAction private func radioAlcoholClick(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            alcohol = radioButton.selected()!.titleLabel!.text!
        }
    }
}


